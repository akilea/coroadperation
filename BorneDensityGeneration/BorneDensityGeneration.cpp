#include "PathCache.h"
#include "carte.h"
#include "SimulationSetting.h"
#include "ScenarioWriter.h"

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

using namespace std;

int main()
{
	//Compute cache for borne and other
	string fileNameCarte = "quebec-carte.txt";

	//string fileNameBorne = "bornes-osm.txt"; // petit
	//string fileNameBorne = "bornes-ce-L3.txt"; // moyen
	string fileNameBorne = "bornes-ce-L2L3.txt"; // grand

	string fileNameBorneBoost = fileNameBorne + "b";
	string fileNameCacheBoost = fileNameBorne + "c";
	int maxAddBorne = 0; //0 when not adding
	int maxChunk = 100; //Memory limitation... we must recreate the carte at regular intervals...

	//Initial carte loading
	Carte* carte = new Carte(fileNameCarte, fileNameBorne);

	///////////////////////////////////////////////
	//Boost the carte
	///////////////////////////////////////////////

	//Set max amount of borne
	float ret = INFINITY;
	size_t nbTerminal = carte->GetBorne().size();
	Point pMin(INFINITY, INFINITY);
	Point pMax(-INFINITY, -INFINITY);
	if (nbTerminal > 0)
	{

		for (auto var : carte->GetBorne())
		{
			pMin.moveToMin(var.point);
			pMax.moveToMax(var.point);
		}
	}

	string prefixNomBorne = "Artificielle_";
	for (int i = 0; i < maxAddBorne; ++i)
	{
		string nomBorne = prefixNomBorne + to_string(i);
		//Certaines bornes pourraient ne pas �tre atteignable malgr� tout mais ce sera suffisant.
		Point pt = ScenarioWriter::GenererPointAleatoireAtteignable(pMin, pMax, carte, SimulationSetting::D_ACCEPTABLE_MAX_DISTANCE);
		//Tir au hasard un noeud et sa position
		carte->ajouterBorne(nomBorne, pt);
	}

	ofstream outputFileBorne(fileNameBorneBoost);
	outputFileBorne << std::setprecision(8);
	for (auto b : carte->GetBorne())
	{
		outputFileBorne << b.nom << "   " << "L " << b.niveau << "   " << b.point << endl;
	}
	outputFileBorne.close();
	int finalSize = (int)carte->GetBorne().size();
	delete carte;

	///////////////////////////////////////////////
	//Compute cache by chunk
	///////////////////////////////////////////////
	ofstream outputFileCache(fileNameCacheBoost);
	outputFileBorne << std::setprecision(8);
	for (int i = 0; i < finalSize+1; i += maxChunk)
	{
		Carte* tmpCarte = new Carte(fileNameCarte, fileNameBorneBoost);
		tmpCarte->calculMatriceDistancesBornes(outputFileCache,i,i+maxChunk);
		delete tmpCarte;
	}
	outputFileCache.close();

	//cin.get();
	return 0;
}