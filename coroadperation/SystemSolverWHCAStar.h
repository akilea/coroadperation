#pragma once
#include "SystemSolverArranged.h"
#include <math.h>

class SystemSolverWHCAStar :
	public SystemSolverArranged
{
protected:

	virtual void AddVehicleInternal(Vehicle* newVehicle);

	//How much we solve forward
	float mfWindowWidth = 1000.0f;
	//How often we recompute
	float mfResolveStep = mfWindowWidth / 2.0f;

	float mfMinimumDepartureTime = INFINITY; //So we know when to start the computation
public:
	SystemSolverWHCAStar(IObjectiveFunction* inObjectiveFunction, IVehicleSolver* inVehicleSolver, IArrangement* inArrangment, ElectricCircuit* inElectricCircuit, double in_dComputationTimeCap, float infWindowWidth, float infResolveStep);
	virtual ~SystemSolverWHCAStar();

	virtual void SolveSystem();
};

