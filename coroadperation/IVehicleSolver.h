#pragma once

class Vehicle;
class VehicleSolution;
class IObjectiveFunction;
class ElectricCircuit;


class IVehicleSolver
{
protected:
	ElectricCircuit* mRefElectricCircuit = nullptr;

public:
	IVehicleSolver(IObjectiveFunction* inObjectiveFunction, ElectricCircuit * inRefElectricCircuit):mRefElectricCircuit(inRefElectricCircuit){}
	virtual ~IVehicleSolver(){}

	virtual void SolveFor(Vehicle* inVehicleToSolve, VehicleSolution* out_Solution) = 0;

	void ReplaceRefElectricCircuit(ElectricCircuit * inRefElectricCircuit) { mRefElectricCircuit = inRefElectricCircuit; }
};

