#include "HighLevelCarte.h"

#include "RequestCarte.h"
#include "Vehicle.h"
#include "DjistraTerminalSpace.h"
#include "Path.h"

#include "Action.h"
#include "ActionTerminal.h"
#include "ElectricTerminal.h"

#include <fstream>
#include <vector>

bool HighLevelCarte::tryAddVehiclePathInCache(const Point &in_ptStart, const Point &in_ptEnd, int in_mapIDStart, int in_mapIDEnd, int in_nIndexStart, int in_nIndexEnd, double in_dMaxAutonomie) const
{
	double dFlyDistance = in_ptStart.distance(in_ptEnd);
	bool wasAdded = false;
	if (in_dMaxAutonomie > dFlyDistance) //Pruning with flying distance
	{
		//Compute exact distance (LONG)
		//cout << "GO!!!!!!" << endl;
		double d = mRefCarte->calculerDistance(in_nIndexStart, in_nIndexEnd, in_dMaxAutonomie);
		//cout << "Computing distance between " << in_nIndexStart << " and " << in_nIndexEnd << " with aut. of " << in_dMaxAutonomie << " = " << d << endl;
		//cout << "Point=" << in_ptStart << " and " << in_ptEnd << endl;
		//cout << "MapID=" << in_mapIDStart << " and " << in_mapIDEnd << endl;
		//Add only if within reachable distance
		if (d < in_dMaxAutonomie)
		{
			//cout << "Accepted!" << endl;
			m_vecPathToClear.push_back(TrajectoryTuple(in_mapIDStart, in_mapIDEnd));
			//cout << "mCache.addPath " << in_mapIDStart << " " << in_mapIDEnd << endl;
			mCache.addPath(in_mapIDStart, in_mapIDEnd, d);
			wasAdded = true;
		}
	}
	return wasAdded;
}

void HighLevelCarte::clearAllVehiclePathInCache() const
{
	//Clean up indexes used in cache for vehicles
	for each (auto var in m_vecPathToClear)
	{
		mCache.removePath(var.nIndexDepart, var.nIndexArrive);
	}
	m_vecPathToClear.clear();
}

HighLevelCarte::HighLevelCarte(string fileCache, Carte* lowLevelCarte)
{
	loadCache(fileCache);
	mRefCarte = lowLevelCarte;
	onPostLoadCache();
}

HighLevelCarte::~HighLevelCarte()
{
}

float HighLevelCarte::ComputeTerminalDensity() const {
	set<int> setVehicle;
	setVehicle.insert(getStartVehicleID());
	setVehicle.insert(getEndVehicleID());
	return mCache.ComputeTerminalDensity(setVehicle);
}

void HighLevelCarte::loadCache(string fileCache)
{
	ifstream inputFile(fileCache);
	inputFile >> mCache;
	inputFile.close();
}

void HighLevelCarte::onPostLoadCache()
{
	//Find the max in the carte
	mCache.stripAtAndAbove(mRefCarte->GetBorne().size());
	m_idCacheVehicleStart = mCache.getHighestTerminalID() + 1;
	m_idCacheVehicleEnd = mCache.getHighestTerminalID() + 2;

	//cout << "m_idCacheVehicleStart " << m_idCacheVehicleStart << endl; //1000
	//cout << "m_idCacheVehicleEnd " << m_idCacheVehicleEnd << endl; //1001
}

double HighLevelCarte::calculerTrajet(Path& in_out_path, RequestCarte& inRequestCarte) const
{
	//Main function
	//1) Compute from start to accessible borne and put in cache
	//2) Compute from accessible borne to end and put int cache
	//3) Djistra (behavior will depend on RequestCarte options!)
	//4) Clean up the vehicle fake cache data
	//O(E log V)
	//Implementation de https://rosettacode.org/wiki/Dijkstra%27s_algorithm#C.2B.2B
	//Lien utile pour idees pour justification dans memoire

	//1) Compute start and end and insert in cache
	//Bien evaluer si c'est mieux d'avoir un vec de vec carre ou encore un vec irregulier
	//Penser a la modification pour les voitures.
	//Map: c'est constant pour modification voiture.
	//Vector carre: on doit parcourrir tout a chaque fois qu'on calcul
	//Vector irregulier: on doit trouver un index pour mettre depart et arrive pour chaque borne... ish!

	//double mRefCarte->calculerDistance(const Point& origine, const Point& destination, int maxdistance) const

	//Clean up the path cache from previous vehicle data
	clearAllVehiclePathInCache();
	bool bHasPartialSolution = in_out_path.HasPartialSolution();

	//Calculer distance entre le depart et les bornes accessibles
	//Calculer distance entre les bornes accessibles et l'arrivee
	//O(nbBornes)

	//Step 1) and 2)
	//cout << "Will enter cache start=" << in_out_path.mSequenceAction.empty() << endl;
	if (!bHasPartialSolution)
	{
		Point ptDepart = inRequestCarte.mRefVehicleRequested->GetPointDeparture();
		int indexDebutTrajet = mRefCarte->getIndexPlusPres(ptDepart);
		for (unsigned int i = 0; i < mRefCarte->GetBorne().size(); i++) {
			//Start to current borne
			//We need this one only if the vehicle just departed
			tryAddVehiclePathInCache(ptDepart, mRefCarte->GetBorne()[i].point, getStartVehicleID(), i, indexDebutTrajet, mRefCarte->GetBorne()[i].indexlieu, inRequestCarte.mRefVehicleRequested->GetAutonomie());
		}

		//Validate we have at least on entry for begin and end, otherwise we have a problem...
		if (!mCache.hasNeighbour(getStartVehicleID()))
		{
			cout << "No terminal links the START of this vehicle" << endl;
			cout << *inRequestCarte.mRefVehicleRequested << endl;
			cout << "asserting";
			assert(false);
		}
	}

	//Remplir la cache pour les bornes accessibles de la fin
	Point ptEnd = inRequestCarte.mRefVehicleRequested->GetPointArrival();
	int indexArrivee = mRefCarte->getIndexPlusPres(ptEnd);
	bool bEndAdded = false;
	for (unsigned int i = 0; i < mRefCarte->GetBorne().size(); i++) {
		//current borne to end
		bEndAdded |= tryAddVehiclePathInCache(mRefCarte->GetBorne()[i].point, ptEnd, i, getEndVehicleID(), mRefCarte->GetBorne()[i].indexlieu, indexArrivee, inRequestCarte.mRefVehicleRequested->GetAutonomie());
	}

	if (!bEndAdded)
	{
		cout << "No terminal links the END of this vehicle" << endl;
		cout << *inRequestCarte.mRefVehicleRequested << endl;
		cout << getEndVehicleID() << endl;
		cout << "Cache aspect" << endl;
		cout << mCache << endl;
		cout << "Cache aspect END" << endl;
		cout << "asserting";
		assert(false);
	}

	//Step 3)
	DjistraTerminalSpace djikstra;
	//When have a partial solution
	//Replace: getStartVehicleID() -> indexDepart
	//New: TimeStart -> Time end last action
	//New: Current path distribution
	int indexStartId = getStartVehicleID();
	float fStartTime = inRequestCarte.mRefVehicleRequested->GetDepartureTime();
	PathDistribution pathD;
	pathD.Set0();
	if (bHasPartialSolution)
	{
		Action* act = in_out_path.mSequenceAction.back();
		assert(act != nullptr);
		
		if (act == nullptr)
		{
			cout << "HighLevelCarte: Last action not existing" << endl;
			assert(false);
		}
		if (act != nullptr)
		{
			if (act->getType() == Action::eRoad)
			{
				cout << "HighLevelCarte: Last action is a road. Not yet supported (but should be...)" << endl;
				assert(false);
			}
			else
			{
				indexStartId = act->getTerminal()->GetIndex();
				fStartTime = act->getTimeRange().mEnd;
				in_out_path.recomputeProportion();
				pathD = in_out_path.mTotalPathDistribution;
			}

		}
	}
	djikstra.PreComputePaths(indexStartId, fStartTime,getEndVehicleID(),mCache, inRequestCarte,mRefCarte, pathD);
	//TODO: to compute only if needed
	djikstra.ActionSequenceTo(in_out_path,inRequestCarte,mRefCarte, fStartTime, pathD);
	in_out_path.mTotalPathDistribution = djikstra.DistributionTo();
	//Return distance
	return djikstra.TrueCostTo();
}

