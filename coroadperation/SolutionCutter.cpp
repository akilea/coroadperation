﻿#include "SolutionCutter.h"
#include "VehicleSolution.h"
#include "SystemSolution.h"
#include "Path.h"
#include "Action.h"
#include "ActionTerminal.h"
#include "ElectricTerminal.h"
#include "ElectricReservation.h"
#include "ElectricCircuit.h"
#include "Vehicle.h"
#include <assert.h>


void SolutionCutter::cutFromAction(VehicleSolution* solToCut, std::list<Action*>::iterator itToCut) const
{
	for (std::list<Action*>::iterator itDel = itToCut; itDel != solToCut->mPath->mSequenceAction.end(); ++itDel)
	{
		//Revert terminal actions
		if ((*itDel)->getType() == Action::eTerminal)
		{
			ActionTerminal* varTerminal = dynamic_cast<ActionTerminal*>(*itDel);
			bool success = varTerminal->getTerminal()->TryFree(varTerminal->getCommitedReservation());
			if (success)
			{
				//cout << "SolutionCutter::cutFromAction - Freeing reservation" << endl;
				//cout << "SolutionCutter::cutFromAction - ve name" << endl << solToCut->mRefVehicle->GetName() << endl;
				//cout << "SolutionCutter::cutFromAction - varTerminal" << endl << varTerminal->toString() << endl;
				//cout << "SolutionCutter::cutFromAction - varTerminal->getCommitedReservation()" << endl << varTerminal->getCommitedReservation() << endl;
				//cout << "SolutionCutter::cutFromAction - full ve solution Ptr" << endl << solToCut << endl;
				//cout << "SolutionCutter::cutFromAction - EC Ptr" << endl << varTerminal->GetElectricCircuit() << endl;
				//cout << "SolutionCutter::cutFromAction - EC" << endl << *varTerminal->GetElectricCircuit() << endl;
				//cout << "SolutionCutter::cutFromAction - full terminal" << endl << *varTerminal->getTerminal() << endl;
				//cout << "SolutionCutter::cutFromAction - full ve solution" << endl << *solToCut << endl;

				delete varTerminal->getCommitedReservation();
				varTerminal->setCommitedReservation(nullptr);
			}
			else
			{
				//cout << "SolutionCutter::cutFromAction - Failed to free terminal" << endl;
				//cout << "SolutionCutter::cutFromAction - ve name" << endl << solToCut->mRefVehicle->GetName() << endl;
				//cout << "SolutionCutter::cutFromAction - varTerminal" << endl << varTerminal->toString() << endl;
				//cout << "SolutionCutter::cutFromAction - varTerminal->getCommitedReservation()" << endl << varTerminal->getCommitedReservation() << endl;
				//cout << "SolutionCutter::cutFromAction - full ve solution Ptr" << endl << solToCut << endl;
				//cout << "SolutionCutter::cutFromAction - full ve solution" << endl << *solToCut << endl;
				//cout << "SolutionCutter::cutFromAction - EC Ptr" << endl << varTerminal->GetElectricCircuit() << endl;
				//cout << "SolutionCutter::cutFromAction - EC" << endl << *varTerminal->GetElectricCircuit() << endl;
				//cout << "SolutionCutter::cutFromAction - full terminal" << endl << *varTerminal->getTerminal() << endl;
			}

			assert(success);
		}
		delete* itDel;
	}
	solToCut->mPath->mSequenceAction.erase(itToCut, solToCut->mPath->mSequenceAction.end());

	//Special case: first action
	if (solToCut->mPath->mSequenceAction.size() == 1)
	{
		for (std::list<Action*>::iterator itDelComplete = solToCut->mPath->mSequenceAction.begin(); itDelComplete != solToCut->mPath->mSequenceAction.end(); ++itDelComplete)
		{
			delete* itDelComplete;
		}
		solToCut->mPath->mSequenceAction.clear();
	}
	//Update proportions
	solToCut->mPath->recomputeProportion();
}

bool SolutionCutter::cutFromPreviousCommitment(SystemSolution* solToCut, float fLimitTime) const
{
	bool wasChanged = false;
	for each (auto var in solToCut->mMapVehicleSolution)
	{
		VehicleSolution* newSolution = var.second;
		wasChanged |= cutFromPreviousCommitment(newSolution,fLimitTime);
	}
	if (wasChanged)
	{
		solToCut->RecomputeSystemDistribution();
	}
	return wasChanged;
}

bool SolutionCutter::cutRoadToTerminal(SystemSolution* solToCut, VehicleSolution* solVToCut, ElectricTerminal* terminalToCut) const
{

	auto previousCommitmentActionBegin = solVToCut->mPath->mSequenceAction.end();
	//Remove first ActionRoad we find that is in the time range
	for (auto it = solVToCut->mPath->mSequenceAction.begin(); it != solVToCut->mPath->mSequenceAction.end(); ++it)
	{
		//Keep in memory the last road action, this is the on to eliminate
		if ((*it)->getType() == Action::eRoad)
		{
			previousCommitmentActionBegin = it;
		}

		if ((*it)->getType() == Action::eTerminal)
		{
			ActionTerminal* actTer = dynamic_cast<ActionTerminal*>(*it);
			if (actTer->getTerminal()->GetIndex() == terminalToCut->GetIndex())
			{
				//Just break and last action is a road!
				break;
			}
		}
	}

	//TODO: collect collision and remove them



	bool wasChanged = previousCommitmentActionBegin != solVToCut->mPath->mSequenceAction.end();
	if (wasChanged)
	{
		cutFromAction(solVToCut, previousCommitmentActionBegin);
	}
	return wasChanged;
}

bool SolutionCutter::cutFromPreviousCommitment(VehicleSolution* solToCut, float fLimitTime) const
{
	std::list<Action*>::iterator previousCommitmentActionBegin = solToCut->mPath->mSequenceAction.end();
	//Remove first ActionRoad we find that is in the time range
	for (std::list<Action*>::iterator it = solToCut->mPath->mSequenceAction.begin(); it != solToCut->mPath->mSequenceAction.end(); ++it)
	{
		if ((*it)->getStartTime() > fLimitTime && (*it)->getType() == Action::eRoad)
		{
			previousCommitmentActionBegin = it;
			break;
		}
	}

	bool wasChanged = previousCommitmentActionBegin != solToCut->mPath->mSequenceAction.end();
	if (wasChanged)
	{
		cutFromAction(solToCut, previousCommitmentActionBegin);
	}
	return wasChanged;
}

bool SolutionCutter::cutRoadToTerminal(VehicleSolution* solToCut, ElectricTerminal* terminalToCut) const
{
	auto previousCommitmentActionBegin = solToCut->mPath->mSequenceAction.end();
	//Remove first ActionRoad we find that is in the time range
	for (auto it = solToCut->mPath->mSequenceAction.begin(); it != solToCut->mPath->mSequenceAction.end(); ++it)
	{
		//Keep in memory the last road action, this is the on to eliminate
		if ((*it)->getType() == Action::eRoad)
		{
			previousCommitmentActionBegin = it;
		}

		if ((*it)->getType() == Action::eTerminal)
		{
			ActionTerminal* actTer = dynamic_cast<ActionTerminal*>(*it);
			if (actTer->getTerminal()->GetIndex() == terminalToCut->GetIndex())
			{
				//Just break and last action is a road!
				break;
			}
		}
	}
	bool wasChanged = previousCommitmentActionBegin != solToCut->mPath->mSequenceAction.end();
	if (wasChanged)
	{
		cutFromAction(solToCut, previousCommitmentActionBegin);
	}
	return wasChanged;
}


//return true if any change was made
bool SolutionCutter::addRemainingReservation(VehicleSolution* solToCut) const
{
	for (auto it = solToCut->mPath->mSequenceAction.begin(); it != solToCut->mPath->mSequenceAction.end(); ++it)
	{
		//Verify reservation upon crossing a terminal action
		if ((*it)->getType() == Action::eTerminal)
		{
			ActionTerminal* actTer = (ActionTerminal*)(*it);
			assert(actTer->getTerminal() != nullptr);

			std::vector<ElectricReservation*> vOverlap;
			actTer->getTerminal()->ReservationOverlap(actTer->getTimeRange(), vOverlap);
			ElectricReservation* actReservation = actTer->getCommitedReservation();
			//The leader is the action, terminal will adjust according to action terminal info
			//if (solToCut->mRefVehicle->GetName().compare("Vehicle_7") == 0)
			//{
			//	cout << "SolutionCutter::addRemainingReservation - ve name=" << solToCut->mRefVehicle->GetName() << endl;
			//	cout << "SolutionCutter::addRemainingReservation - actTer" << actTer->toString() << endl;
			//	cout << "SolutionCutter::addRemainingReservation - actReservation == nullptr=" << (actReservation == nullptr) << endl;
			//	cout << "SolutionCutter::addRemainingReservation - vOverlap.size()=" << vOverlap.size() << endl;
			//}
			//No internal reservation in action yet
			if (actReservation == nullptr)
			{
				//const float fOverlapValueTolerance = 2.0f;
				//float fOverlapValue = 1.0f;
				//if (vOverlap.size() == 1)
				//{
				//	fOverlapValue = vOverlap[0]->GetTimeRange().
				//}

				//No overlap. Make reservation as usual!
				if (vOverlap.size() == 0)
				{
					//Create a new one then!
					actReservation = new ElectricReservation(actTer->getTimeRange(), solToCut->mRefVehicle);
					bool success = actTer->getTerminal()->TryReserveDeltaOne(actReservation);
					actTer->setCommitedReservation(actReservation);
					assert(success);
				}
				else if (vOverlap.size() >= 2) //Yikes... cry
				{
					cout << "SolutionCutter::updateReservation - overlap of 2 reservation. Should not happened" << endl;
					assert(false);
				}
				else //vOverlap.size() == 1 and it is already present. Could it be us?
				{
					//Does it belong to us?
					if (vOverlap[0]->m_pVehicle->GetName().compare(solToCut->mRefVehicle->GetName()) == 0)
					{
						//Yes! Remove this and add a new one
						actTer->getTerminal()->TryFree(vOverlap[0]);
						delete vOverlap[0];
						//Make a new one
						actReservation = new ElectricReservation(actTer->getTimeRange(), solToCut->mRefVehicle);
						bool success = actTer->getTerminal()->TryReserveDeltaOne(actReservation);
						actTer->setCommitedReservation(actReservation);
						assert(success);
					}
					else // :-( overlap not belonging to us. Should not happen
					{
						cout << "SolutionCutter::updateReservation - overlaping a single reservation that does not belong to us." << endl;
						assert(false);
					}
				}
			}
			else // internal reservation present
			{
				//Internal reservation present but not in terminal. Wierd.
				if (vOverlap.size() == 0)
				{
					//Use it then
					bool success = actTer->getTerminal()->TryReserveDeltaOne(actReservation);
					actTer->setCommitedReservation(actReservation);
					assert(success);
				}
				else if (vOverlap.size() >= 2)
				{
					cout << "SolutionCutter::updateReservation - overlap of 2 reservation and reservation is already commited inside action! Should not happened" << endl;
					assert(false);
				}
				else //vOverlap.size() == 1 should be the case
				{
					//Does it belong to us?
					if (vOverlap[0]->m_pVehicle->GetName().compare(solToCut->mRefVehicle->GetName()) == 0)
					{
						//yay!
					}
					else//Nay :-(
					{
						cout << "SolutionCutter::updateReservation - overlaping a single reservation that does not belong to us. We have also a reservation internaly. Dang!" << endl;
						assert(false);
					}
				}
			}
		}
	}
	return false;
}
