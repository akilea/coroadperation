#pragma once
#include "IAlgoRun.h"
class AlgoRunTestTerminal :
	public IAlgoRun
{
public:
	AlgoRunTestTerminal();
	virtual ~AlgoRunTestTerminal();

	virtual int Run(int argc, const char** argv);
};

