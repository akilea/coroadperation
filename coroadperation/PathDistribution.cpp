#include "PathDistribution.h"
#include <math.h>

PathDistribution::PathDistribution(const PathDistribution & obj)
{
	this->mChargeTime = obj.mChargeTime;
	this->mWaitTime = obj.mWaitTime;
	this->mRoadTime = obj.mRoadTime;
}

PathDistribution::PathDistribution(float wait, float charge, float road)
{
	Set(wait, charge, road);
}


PathDistribution::~PathDistribution()
{
}

void PathDistribution::Set(float wait, float charge, float road)
{
	mChargeTime = charge;
	mRoadTime = road;
	mWaitTime = wait;
}

void PathDistribution::Set0() { Set(0.0f, 0.0f, 0.0f); }

void PathDistribution::SetInf() { Set(INFINITY, INFINITY, INFINITY); }

float PathDistribution::Total() const
{
	return mChargeTime+mRoadTime+mWaitTime;
}

PathDistribution PathDistribution::normalize() const
{
	return *this/Total();
}

PathDistribution PathDistribution::operator+(const PathDistribution & rhs) const
{
	PathDistribution p;
	p.mRoadTime   = this->mRoadTime + rhs.mRoadTime;
	p.mChargeTime = this->mChargeTime + rhs.mChargeTime;
	p.mWaitTime   = this->mWaitTime + rhs.mWaitTime;
	return p;
}

PathDistribution PathDistribution::operator-(const PathDistribution& rhs) const
{
	PathDistribution p;
	p.mRoadTime = this->mRoadTime- rhs.mRoadTime;
	p.mChargeTime = this->mChargeTime - rhs.mChargeTime;
	p.mWaitTime = this->mWaitTime - rhs.mWaitTime;
	return p;
}

PathDistribution PathDistribution::operator*(const PathDistribution& rhs) const
{
	PathDistribution p;
	p.mRoadTime = this->mRoadTime * rhs.mRoadTime;
	p.mChargeTime = this->mChargeTime * rhs.mChargeTime;
	p.mWaitTime = this->mWaitTime * rhs.mWaitTime;
	return p;
}

PathDistribution PathDistribution::operator/(const float & rhs) const
{
	PathDistribution p = *this;
	p.mRoadTime   /= rhs;
	p.mWaitTime   /= rhs;
	p.mChargeTime /= rhs;
	return p;
}

float PathDistribution::operator/(const PathDistribution& rhs) const
{
	return Total()/rhs.Total();
}

PathDistribution& PathDistribution::operator=(const PathDistribution & rhs)
{
	this->mChargeTime = rhs.mChargeTime;
	this->mRoadTime = rhs.mRoadTime;
	this->mWaitTime = rhs.mWaitTime;
	return *this;
}

PathDistribution& PathDistribution::operator+=(const PathDistribution& rhs)
{
	this->mChargeTime += rhs.mChargeTime;
	this->mRoadTime += rhs.mRoadTime;
	this->mWaitTime += rhs.mWaitTime;
	return *this;
}

PathDistribution& PathDistribution::operator-=(const PathDistribution& rhs)
{
	this->mChargeTime -= rhs.mChargeTime;
	this->mRoadTime   -= rhs.mRoadTime;
	this->mWaitTime   -= rhs.mWaitTime;
	return *this;
}

bool PathDistribution::operator<(const PathDistribution & rhs)
{
	return this->Total() < rhs.Total();
}

ostream& operator<<(ostream& os, const PathDistribution& dt)
{
	os << "Road=" << dt.mRoadTime << endl;
	os << "Wait=" << dt.mWaitTime << endl;
	os << "Char=" << dt.mChargeTime << endl;
	os << "Tot =" << dt.Total() << endl;
	return os;
}
