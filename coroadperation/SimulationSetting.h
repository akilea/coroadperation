#pragma once

#include <vector>
#include <string>

class SimulationSetting {
public:
	static const double D_ACCEPTABLE_MAX_DISTANCE;
	static const double D_ACCEPTABLE_MIN_DISTANCE_SCENARIO;
	static const double D_ACCEPTABLE_MIN_DISTANCE;

	static const double D_AVERAGE_VEHICLE_SPEED;
	static const double D_AVERAGE_VEHICLE_CHARGE_RATE_M_PER_SECOND;

	static const double D_COMPUTATION_TIME_CAP;
	static const std::vector<float> VF_VEC_WHCA_WINDOWS;
	static const std::vector<std::string> VSTR_VEC_WHCA_WINDOWS_NAME;
	static const float F_WHCA_RECOMPUTE_PERCENTAGE;

	static const std::string STR_LAST_ACTION_NAME;
};