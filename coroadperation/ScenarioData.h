#pragma once

#include <vector>
#include <string>
#include <math.h>

class ElectricCircuit;
class Vehicle;
class SystemSolution;

class ScenarioData
{
private:
	bool mMustAssignPartialSolutionEachAdd = true;
	//Optionnal solution should match the vehicle list.
	SystemSolution* mObjOptionnalPartialSolution = nullptr;

public:
	ScenarioData();
	~ScenarioData();

	std::string toString();
	virtual std::string toStringInteractive();

	//TODO: EVALUATE  a piority queue? If necessary... does not look like it for now
	std::vector<Vehicle*> mVecVehicle;
	ElectricCircuit* mElectricCircuit = nullptr;

	void SetAndOwnPartialSolution(SystemSolution* partialSolution);
	bool HasPartialSolution() const {return mObjOptionnalPartialSolution!= nullptr;}
	SystemSolution* GetAndClearPartialSolution();

	inline void SetAssignPartialSolutionEachAdd(bool bMustActivate) { mMustAssignPartialSolutionEachAdd = bMustActivate; }
	inline bool GetAssignPartialSolutionEachAdd() const { return mMustAssignPartialSolutionEachAdd; }
};

