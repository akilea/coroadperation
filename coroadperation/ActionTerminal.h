#pragma once

#include "Action.h"
#include "StructureCarte.h"

class ElectricCircuit;
class ElectricTerminal;
class ElectricReservation;

class ActionTerminal :
	public Action
{

	ElectricReservation* mRefReservation = nullptr;
public:
	ActionTerminal(ElectricCircuit* const inRefCircuit,ElectricTerminal* inTerminal, ElectricReservation* inRefReservation = nullptr);
	virtual ~ActionTerminal();

	virtual TAction getType() const { return TAction::eTerminal; }
	ElectricReservation * const getCommitedReservation() const { return mRefReservation; }
	void setCommitedReservation(ElectricReservation* inRefReservation);


	virtual Action* clone() const;
};

