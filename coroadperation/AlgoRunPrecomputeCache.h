#pragma once
#include "IAlgoRun.h"
class AlgoRunPrecomputeCache :
	public IAlgoRun
{
public:
	AlgoRunPrecomputeCache();
	virtual ~AlgoRunPrecomputeCache();

	virtual int Run(int argc, const char** argv);
};

