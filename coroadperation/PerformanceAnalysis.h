#pragma once
#include <chrono>
#include <string>
#include <iostream>
#include "PathDistribution.h"
class IObjectiveFunction;

class PerformanceAnalysis
{
public:

private :
	struct Result {
		double dComputationTime = 0.0f;
		double dTimeCap = INFINITY;
		int nbPass = 0;
		PathDistribution mSystemPathDistribution;
		//Ajouter mesure de la memoire
		//Voir ici:
		//https://stackoverflow.com/questions/5120861/how-to-measure-memory-usage-from-inside-a-c-program

	};
	Result result;

	std::chrono::system_clock::time_point expStart;
public:
	static std::string toStringHeader(const string& strObjFx);
	
	void CopyResultFrom(const Result& otherResult);

	void StartExperiment();
	double RunningTime() const;
	void StopExperiment(const PathDistribution& in_pd);

	bool HasReachdedTimeCap() const;

	bool HasReachdedRunningTimeCap() const;

	//Pas certain encore c'est quoi xD
	void AddPassCount();
	std::string toString(int nbVehicle,float fObjCost) const; //Nb vehicle to average
	Result GetResult() const;
	PerformanceAnalysis(double in_dTimeCap = INFINITY);
	~PerformanceAnalysis();
};

