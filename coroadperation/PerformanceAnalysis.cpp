#include "PerformanceAnalysis.h"
#include <sstream>
#include "Scenario.h"
#include <iomanip>
#include "IObjectiveFunction.h"


void PerformanceAnalysis::StartExperiment()
{
	//cout << "Started chrono!" << endl;
	expStart = std::chrono::system_clock::now();
}

double PerformanceAnalysis::RunningTime() const
{
	std::chrono::system_clock::time_point endExp = std::chrono::system_clock::now();
	std::chrono::duration<double> diff = (endExp - expStart);
	//cout <<"diff.count()" <<diff.count() << endl;
	return diff.count();
}

void PerformanceAnalysis::StopExperiment(const PathDistribution& in_pd)
{
	//cout << "Ending chrono!" << endl;
	result.dComputationTime = RunningTime();
	result.mSystemPathDistribution = in_pd;
	if (result.nbPass == 0) {
		result.nbPass = 1;
	}
}

bool PerformanceAnalysis::HasReachdedTimeCap() const
{
	//cout << "Timing" << endl;
	//cout << GetResult().dTimeCap << endl;
	//cout << result.dComputationTime << endl;
	return result.dComputationTime>GetResult().dTimeCap;
}

bool PerformanceAnalysis::HasReachdedRunningTimeCap() const
{
	//cout << "Timing" << endl;
	//cout << GetResult().dTimeCap << endl;
	//cout << result.dComputationTime << endl;
	return RunningTime() > GetResult().dTimeCap;
}

void PerformanceAnalysis::AddPassCount()
{
	result.nbPass++;
}

std::string PerformanceAnalysis::toString(int nbVehicle, float fObjCost) const
{
	//Verifier les donnes affichees ici
	//Verifier precision
	std::ostringstream ss;
	float totalComputeTime = result.dComputationTime;
	int nNbPass = result.nbPass;
	float perPassComputeTime = totalComputeTime;
	perPassComputeTime /= nNbPass;
	PathDistribution perVehicleDistr   = result.mSystemPathDistribution/(float)nbVehicle;

	//For vehicle travelling
	ss << fixed << setprecision(0); //s precision
	ss << std::setw(8) << left << perVehicleDistr.Total() << std::setw(8) << left << perVehicleDistr.mRoadTime;
	ss << std::setw(7) << left << perVehicleDistr.mWaitTime << std::setw(9) << left << perVehicleDistr.mChargeTime;
	//For comp time
	ss << fixed << setprecision(3); //ms precision
	ss << std::setw(8) << left << totalComputeTime << std::setw(7) << left << nNbPass << std::setw(13) << left << perPassComputeTime << left << std::setw(9) << fObjCost << endl;
	
	
return ss.str();
}

std::string PerformanceAnalysis::toStringHeader(const string& strObjFx)
{
	std::ostringstream ss;
	ss << left << "**************Methode**************   *****Temps moy.trajet(s)*****   ***Temps calcul(s)****      ****Metrique****"<<endl;
	ss << left << "Nom    Combinaison Parametre          Total = Route + Att. + Charge   Total = Pass * Temps/p      "+strObjFx<<endl;
	return ss.str();
}

void PerformanceAnalysis::CopyResultFrom(const Result & otherResult)
{
	this->result = otherResult;
}

PerformanceAnalysis::Result PerformanceAnalysis::GetResult() const
{
	return result;
}

PerformanceAnalysis::PerformanceAnalysis(double in_dTimeCap)
{
	result.dTimeCap = in_dTimeCap;
}


PerformanceAnalysis::~PerformanceAnalysis()
{
}