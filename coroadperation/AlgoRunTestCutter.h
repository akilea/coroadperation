#pragma once
#include "AlgoRunCommon.h"
#include "Scenario.h"
#include <string>

using namespace std;

class AlgoRunTestCutter :
    public AlgoRunCommon
{
public:
	AlgoRunTestCutter() {}
	virtual ~AlgoRunTestCutter() {}


	virtual int Run(int argc, const char** argv);
	void TestSolverSingleVechicle(Scenario::SolverType type);
	void TestCutAndRebuild(float timeLimit, string title, bool cloneSolution, int indexVehicle, Scenario::SolverType type);

	void TestSolverDuplicateVechicle(Scenario::SolverType type);
	void TestDuplicateInsertion(float timeLimit, string title, bool cloneSolution, int indexVehicle, Scenario::SolverType type);

};

