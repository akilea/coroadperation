#include "CombinatorialExploration.h"

CombinatorialExploration::CombinatorialExploration(int mini, int maxi)
:mMaximum(maxi)
,mMinimum(mini)
{
}

bool CombinatorialExploration::Next()
{
	bool incremented = false;
	while(!incremented || !mVec.empty())
	{
		int& valBack = mVec.back();
		if (valBack >= mMaximum)
		{
			mVec.pop_back();
		}
		else
		{
			++valBack;
			incremented = true;
		}
	}

	return incremented;
}

int CombinatorialExploration::AddDepth()
{
	mVec.push_back(mMinimum);
	return mVec.size();
}
