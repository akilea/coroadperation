#pragma once
#include "SystemSolverArranged.h"
#include "SystemSolverImmediate.h"
#include "SystemSolver.h"
#include "ISystemSolver.h"
#include "ElectricCircuit.h"
#include "ScenarioData.h"
#include "SystemSolution.h"
#include "IVehicleSolver.h"
#include "PerformanceAnalysis.h"
#include "IObjectiveFunction.h"
#include "IArrangement.h"
#include <assert.h>
#include <iostream>
#include <set>

class AlternativeSearch;

class SystemSolverLRCAStar :
	public SystemSolverArranged
{
	private:
		ISystemSolver* mReferenceSystemSolver = nullptr;
		ISystemSolver* mWaitingSystemSolver = nullptr;
		const int nAllowedDepth = 3;
		int m_nCAInvertedCounter = nAllowedDepth;

	public:
		SystemSolverLRCAStar(IObjectiveFunction* inObjectiveFunction, IVehicleSolver* inVehicleSolver, ISystemSolver* inReferenceSystemSolver, ISystemSolver* inWaitingSystemSolver, IArrangement* inArrangment, ElectricCircuit* inElectricCircuit, double in_dComputationTimeCap);
		virtual ~SystemSolverLRCAStar();

		virtual void SolveSystem();

		//Return if any better solution was found
		SystemSolution* SolveCANoOwnership(AlternativeSearch& newSearch, std::set<Vehicle*>* vAdmissibleVehicle, ElectricCircuit*& out_bestElectricCircuit);
};

