#pragma once
#include "SystemSolverImmediate.h"
#include "SystemSolver.h"
#include "ISystemSolver.h"
#include "ElectricCircuit.h"
#include "ScenarioData.h"
#include "SystemSolution.h"
#include "IVehicleSolver.h"
#include "PerformanceAnalysis.h"
#include "IObjectiveFunction.h"
#include <assert.h>
#include <iostream>
#include <set>
#include <vector>

class AlternativeSearch;
class Vehicle;
class SystemSolverPCARL :
	public SystemSolverImmediate
{
protected:
	ISystemSolver* mReferenceSystemSolver;
	std::vector<Vehicle*> m_vecVehicleAccumulator;
	virtual void AddVehicleInternal(Vehicle* newVehicle);
	virtual void AddVehicleInternalCorrectBiggestCollisionFirst(Vehicle* newVehicle);
	virtual void AddVehicleInternalCorrectBiggestImprovment(Vehicle* newVehicle);

	//6 variations de la réparation locale
	bool m_bIsGreedy = false;
	bool m_bFromStartEverytime = false;
	bool m_bForceCooperationAsStartingSolution = false;

	bool IsWorthComputing(Vehicle* vehicule)const;
	Vehicle* FindVehiclePointer(string strName);

	void ForceSolutionAndCircuit(SystemSolution* newSolution, ElectricCircuit* newCircuit);

	private:
		//ISystemSolver* mReferenceSystemSolver = nullptr;
		//ISystemSolver* mWaitingSystemSolver = nullptr;
		//const int nAllowedDepth = 3;
		//int m_nCAInvertedCounter = nAllowedDepth;

		void SolveForOne(Vehicle* vehicule);

	public:
		SystemSolverPCARL(IObjectiveFunction* inObjectiveFunction, IVehicleSolver* inVehicleSolver, ElectricCircuit* inElectricCircuit, double in_dComputationTimeCap, ISystemSolver* inReferenceSystemSolver, bool in_bIsGreedy, bool isFromStartEverytime, bool isForceCooperationAsStartingSolution);
		virtual ~SystemSolverPCARL();
		void SetVariation(bool isGreedy,bool isFromStartEverytime,bool isForceCooperationAsStartingSolution);
		virtual void SolveSystem();

};