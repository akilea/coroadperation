#pragma once
#include "Arrangement.h"

class ArrangementNone :
	public Arrangement
{
protected:
	bool wasReturned = false;
public:
	ArrangementNone();
	virtual ~ArrangementNone();

	virtual void startSequence(std::vector<Vehicle*>* in_refVector);
	virtual std::vector<Vehicle*>* GetNextSequence();
};

