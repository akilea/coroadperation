#pragma once
#include "Scenario.h"

class ScenarioPermutation :
	public Scenario
{
protected:
	IVehicleSolver* mVehicleSolverPermutation;
	ISystemSolver* mSystemSolverPermutation;

public:

	ScenarioPermutation(IScenarioLoader* inScenarioLoader, Carte const & inRefCarte);
	virtual ~ScenarioPermutation();

	virtual string toString() const;

	virtual bool SolveNextVehicle();
};

