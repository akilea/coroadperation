#include "AlgoRunTestElectricCircuit.h"
#include "IScenarioLoader.h"
#include "ScenarioLoader.h"
#include "ElectricCircuit.h"
#include "ElectricReservation.h"
#include "ElectricTerminal.h"
#include "ScenarioData.h"
#include <vector> 

#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "carte.h"
#include "Path.h"



AlgoRunTestElectricCircuit::AlgoRunTestElectricCircuit()
{
}


AlgoRunTestElectricCircuit::~AlgoRunTestElectricCircuit()
{
}

int AlgoRunTestElectricCircuit::Run(int argc, const char ** argv)
{
	Carte carte;
	{
		ifstream iscarte(argv[1]);
		if (iscarte.fail()) {
			cerr << "Erreur d'ouverture du fichier carte: '" << argv[1] << "' !" << endl;
			return 2;
		}
		iscarte >> carte;
		cerr << "Carte charg�e!" << endl;
	}
	{
		ifstream isbornes(argv[2]);
		if (isbornes.fail()) {
			cerr << "Erreur d'ouverture du fichier bornes: '" << argv[1] << "' !" << endl;
			return 2;
		}
		while (isbornes) {
			string nom;
			char L;
			int niveau;
			Point point;
			isbornes >> nom;
			if (!isbornes || nom.empty()) break;
			isbornes >> L >> niveau >> point >> ws;
			assert(L == 'L');
			cerr << "."; cerr.flush();
			carte.ajouterBorne(nom, point, niveau);
		}
		cerr << "Bornes charg�es!" << endl;
	}

	IScenarioLoader* loader = new ScenarioLoader(argv[3], carte);
	ScenarioData data;
	loader->Load(&data);
	ElectricCircuit* circuit = new ElectricCircuit(carte);

	cout << "CIRCUIT AT START" << endl;
	cout << *circuit << endl;

	//Reservations
	ElectricTerminal* terminal = circuit->GetByIndexTerminal(0);

	TimeRange available = terminal->FindNextAvailableReservation(TimeRange(13, 63));

	ElectricReservation* r1 = new ElectricReservation(available, data.mVecVehicle[0]);
	cout << "Reservation is " << endl;
	cout << *r1<< endl;

	cout << "Inserting reservation..." << endl;
	terminal->TryReserve(r1);

	cout << "CIRCUIT IS" << endl;
	cout << *circuit << endl;

	TimeRange isSometimeFailing(14,64);
	TimeRange bestFailing = terminal->FindNextAvailableReservation(isSometimeFailing);

	ElectricReservation* r2 = new ElectricReservation(bestFailing, data.mVecVehicle[1]);
	cout << "Reservation is " << endl;
	cout << *r2 << endl;
	terminal->TryReserve(r2);

	cout << "CIRCUIT IS" << endl;
	cout << *circuit << endl;

	delete circuit;
	delete loader;

	cin.get();

	return 0;
}
