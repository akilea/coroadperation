#pragma once
#include "IAlgoRun.h"
class AlgoRunTestAVLTree :
	public IAlgoRun
{
public:
	AlgoRunTestAVLTree();
	virtual ~AlgoRunTestAVLTree();

	virtual int Run(int argc, const char** argv);
};

