#pragma once
#include <iostream>
#include "srcMap/StructureCarte.h"
#include "AVLTree.h"

class ElectricReservation;

using namespace std;

class ElectricReservationList;
typedef ElectricReservationList* ElectricReservationListPtr;

class ElectricReservation;

class ElectricReservationList
{
private:

	TimeUnit mTimeStamp = 0;
	//Best container for this case (remove, add, search)
	// See https://baptiste-wicht.com/posts/2012/12/cpp-benchmark-vector-list-deque.html
	CRP::set<ElectricReservation*> electricReservationList;

public:
	ElectricReservationList(TimeUnit in_TimeStamp);
	ElectricReservationList();
	~ElectricReservationList();

	bool IsEmpty();
	void Insert(ElectricReservation* er);
	void Erase(ElectricReservation* er);
	void TryErase(ElectricReservation* er);
	CRP::set<ElectricReservation*>::iterator Begin() { return electricReservationList.begin(); }
	CRP::set<ElectricReservation*>::iterator End() { return electricReservationList.end(); }

	void UnionOut(CRP::set<ElectricReservation*>& out_setToAdd);
	void UnionWith(CRP::set<ElectricReservation*>& in_setToUnion);
	void UnionWith(ElectricReservationList& in_listToUnion);
	TimeUnit GetTimeStamp() const { return mTimeStamp; }
	string ReservationToString() const;

	//Comparison operator so AVL tree works
	bool operator== (const ElectricReservationList &c2) const
	{
		return GetTimeStamp() == c2.GetTimeStamp();
	}

	bool operator!= (const ElectricReservationList &c2) const
	{
		return GetTimeStamp() != c2.GetTimeStamp();
	}

	bool operator> (const ElectricReservationList &c2) const
	{
		return GetTimeStamp() > c2.GetTimeStamp();
	}

	bool operator>= (const ElectricReservationList &c2) const
	{
		return GetTimeStamp() >= c2.GetTimeStamp();
	}

	bool operator< (const ElectricReservationList &c2) const
	{
		return GetTimeStamp() < c2.GetTimeStamp();
	}

	bool operator<= (const ElectricReservationList &c2) const
	{
		return GetTimeStamp() <= c2.GetTimeStamp();
	}

	friend ostream& operator<<(ostream& os, const ElectricReservationList& dt);
	friend ostream& operator<<(ostream& os, const ElectricReservationListPtr& dt);
};

ostream& operator<<(ostream& os, const ElectricReservationList& dt);
ostream& operator<<(ostream& os, const ElectricReservationListPtr& dt);

inline int ElectricReservationListPointerCompare(const  ElectricReservationListPtr& u, const ElectricReservationListPtr & v)
{
	if (*u < *v) return -1; else if (*v < *u) return 1; else return 0;
}