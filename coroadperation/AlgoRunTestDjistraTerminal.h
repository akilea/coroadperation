#pragma once
#include "IAlgoRun.h"

class DjistraTerminalSpace;
class PathCache;
class RequestCarte;

class AlgoRunTestDjistraTerminal :
	public IAlgoRun
{
public:
	AlgoRunTestDjistraTerminal();
	virtual ~AlgoRunTestDjistraTerminal();
	void TestMass(int src, DjistraTerminalSpace & dji, PathCache& cache, RequestCarte& rq);
	void DisplayPath(int src, int to, DjistraTerminalSpace & dji, RequestCarte& rq);
	void DisplayPathIfSolution(int src, int to, DjistraTerminalSpace & dji, RequestCarte& rq);

	virtual int Run(int argc, const char** argv);
};

