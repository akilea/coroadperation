#pragma once
#include "ElectricReservation.h"
#include "ElectricTerminal.h"
#include "Collision.h"

class VehicleSolution;
class ActionTerminal;
class ActionWait;

class CollisionWithReference : public Collision
{
public:
	CollisionWithReference(VehicleSolution* solW, VehicleSolution* solRefW, ActionWait* actW, VehicleSolution* solB, VehicleSolution* solRefB, ActionTerminal* actB);
	virtual ~CollisionWithReference() {}

	//Can generate usefull information regarding the collision
	VehicleSolution* GetWaiterReferenceSolution() const {return mWaiterReferenceSolution;}
	VehicleSolution* GetBlockerReferenceSolution() const {return mBlockerReferenceSolution;}
	bool GetBlockerCanMove() const { return bBlockerCanMove; }
	inline void SetBlockerCanMove(bool bCan) { bBlockerCanMove = bCan; }

private:
	VehicleSolution* mWaiterReferenceSolution = nullptr;
	VehicleSolution* mBlockerReferenceSolution = nullptr;
	bool bBlockerCanMove = true;

	friend ostream& operator<<(ostream& os, const CollisionWithReference& dt);

};

ostream& operator<<(ostream& os, const CollisionWithReference& dt);
bool operator<(const CollisionWithReference& lhs, const CollisionWithReference& rhs);
inline bool operator>(const CollisionWithReference& lhs, const CollisionWithReference& rhs) { return rhs < lhs; }
inline bool operator<=(const CollisionWithReference& lhs, const CollisionWithReference& rhs) { return !(lhs > rhs); }
inline bool operator>=(const CollisionWithReference& lhs, const CollisionWithReference& rhs) { return !(lhs < rhs); }

class CollisionCompare
{
public:
	bool operator() (CollisionWithReference* a, CollisionWithReference* b);
};