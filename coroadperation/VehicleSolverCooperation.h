#pragma once
#include "IVehicleSolver.h"
#include <math.h>

class IMapSolver;
class ElectricCircuit;
class IObjectiveFunction;

class VehicleSolverCooperation :
	public IVehicleSolver
{
private:

	IMapSolver const *const mRefCarte = nullptr;
	IObjectiveFunction *const mRefObjectiveFunction = nullptr;
	float m_fCollisionHorizon = INFINITY;

public:
	VehicleSolverCooperation(IMapSolver const *const inCarte, IObjectiveFunction* inObjectiveFunction, ElectricCircuit* inElectricCircuit, float in_CollisionHorizon = INFINITY);
	virtual ~VehicleSolverCooperation();

	void SetCollisionHorizon(float in_fCollisionHorizon) { m_fCollisionHorizon = in_fCollisionHorizon;}

	virtual void SolveFor(Vehicle* inVehicleToSolve, VehicleSolution* out_RefSolution);
};

