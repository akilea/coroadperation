#include "SystemSolver.h"

#include <algorithm>

#include "ScenarioData.h"
#include "SystemSolution.h"
#include "ElectricCircuit.h"
#include "IArrangement.h"

#include <assert.h>

SystemSolver::SystemSolver(IObjectiveFunction* inObjectiveFunction, IVehicleSolver* inVehicleSolver, IArrangement* inArrangement,ElectricCircuit* inElectricCircuit, double in_dComputationTimeCap)
:ISystemSolver(inObjectiveFunction,inVehicleSolver, inElectricCircuit, in_dComputationTimeCap),
mArrangement(inArrangement)
{
	ResetSolution();
}

SystemSolver::~SystemSolver()
{
	delete mSystemSolution;
}

void SystemSolver::ForceCurrentSystemSolution(SystemSolution* forcedSolution)
{
	if (mSystemSolution != nullptr)
	{
		delete mSystemSolution;
	}
	mSystemSolution = forcedSolution;
}

void SystemSolver::ResetSolution()
{
	if (mSystemSolution != nullptr)
	{
		delete mSystemSolution;
	}
	mSystemSolution = new SystemSolution();
}

void SystemSolver::AddVehicleInternal(Vehicle * newVehicle)
{
	if (!this->HasVehicle(newVehicle))
	{
		mSolverData->mVecVehicle.push_back(newVehicle);
	}
}

void SystemSolver::RemoveVehicleInternal(Vehicle * newVehicle)
{
	auto vIt = find(mSolverData->mVecVehicle.begin(), mSolverData->mVecVehicle.end(), newVehicle);
	if (vIt != mSolverData->mVecVehicle.end())
	{
		mSolverData->mVecVehicle.erase(vIt);
	}
}

bool SystemSolver::HasVehicle(Vehicle* newVehicle) const
{
	auto vIt = find(mSolverData->mVecVehicle.begin(), mSolverData->mVecVehicle.end(), newVehicle);
	return vIt != mSolverData->mVecVehicle.end();
}

size_t SystemSolver::GetVehicleCount() const
{
	return mSolverData->mVecVehicle.size();
}
