#include "SystemSolverImmediate.h"

#include "ScenarioData.h"
#include "SystemSolution.h"
#include "IVehicleSolver.h"
#include "IObjectiveFunction.h"
#include "VehicleSolution.h"
#include "Vehicle.h"
#include "ElectricCircuit.h"
#include "PerformanceAnalysis.h"
#include "Path.h"
#include <iostream>

SystemSolverImmediate::SystemSolverImmediate(IObjectiveFunction* inObjectiveFunction, IVehicleSolver* inVehicleSolver, ElectricCircuit* inElectricCircuit, double in_dComputationTimeCap)
:SystemSolver(inObjectiveFunction,inVehicleSolver,nullptr,inElectricCircuit, in_dComputationTimeCap)
{
	mSystemSolution->mDescription = "Generic system solver.";
}


SystemSolverImmediate::~SystemSolverImmediate()
{
}

void SystemSolverImmediate::SolveSystem()
{
	if (!mIsActivated)
	{
		return;
	}
}



void SystemSolverImmediate::AddVehicleInternal(Vehicle * newVehicle)
{
	if (HasReachdedRunningTimeCap())
	{
		return;
	}
	if (m_bMesurePerformance)
	{
		mPerformanceAnalysis->StartExperiment();
	}

	//Optionnal data handling
	if (mSolverData->GetAssignPartialSolutionEachAdd() && mSolverData->HasPartialSolution())
	{
		cout << "WARNING - Assing each time" << endl;
		ForceCurrentSystemSolution(mSolverData->GetAndClearPartialSolution());
	}

	//Init when we solve first vehicle
	if (mSolverData->mVecVehicle.size() == 0)
	{
		mSystemSolution->mSystemPathDistribution.Set0();
	}

	SystemSolver::AddVehicleInternal(newVehicle);
	bool hasSolution = mSystemSolution->HasVehiculeSolution(newVehicle);

	VehicleSolution* vehicleSolution = nullptr;
	bool hasReachEnd = false;
	if (hasSolution)
	{
		vehicleSolution = mSystemSolution->GetVehiculeSolution(newVehicle);
		hasReachEnd = vehicleSolution->HasReachedEnd();
	}
	else
	{
		vehicleSolution = new VehicleSolution(newVehicle);
		hasReachEnd = false;
	}

	if(!hasReachEnd)
	{
		mVehicleSolver->SolveFor(newVehicle, vehicleSolution);
		//vehicleSolution->displayTrajectoryDetail(cout) << endl;
		//vehicleSolution->HasReachedEnd();
		mSystemSolution->AddOrReplaceSolution(vehicleSolution);	
	}

	mSystemSolution->RecomputeVehicleAndSystemDistribution();

	if (m_bMesurePerformance)
	{
		mPerformanceAnalysis->StopExperiment(GetCurrentSystemSolution()->mSystemPathDistribution);
	}
}
