#pragma once

#include <vector>
#include <queue>
#include "CollisionWithReference.h"

class SystemSolution;
class Vehicle;
class CollisionWithReference;
class ElectricCircuit;
class Action;

using namespace std;


//Objects are already prepared
class AlternativeSearch
{
public:
	AlternativeSearch(SystemSolution* inSystemSolution, Vehicle* inVehicle, ElectricCircuit* inElectricCircuit)
		:mSystemSolution(inSystemSolution), mRefVehicle(inVehicle),mElectricCircuit(inElectricCircuit)
	{}

	SystemSolution* mSystemSolution; //Hard copy already prepared
	Vehicle* mRefVehicle; //Reference
	ElectricCircuit* mElectricCircuit; //Hard copy already prepared

	friend ostream& operator<<(ostream& os, const AlternativeSearch& dt);
};

ostream& operator<<(ostream& os, const AlternativeSearch& dt);

class CollisionAnalyser
{
protected:
	typedef std::priority_queue<CollisionWithReference*, std::vector<CollisionWithReference*>, CollisionCompare> PqCollision;
	PqCollision pqOCollision;
	SystemSolution* mSysSol;
	SystemSolution* mSysSolRef;
	ElectricCircuit const * const mOriginalElectricCircuit;


	static void PierceFromRecursive(std::set<Vehicle*>& out_Affected, SystemSolution* sysSol,Vehicle* curVehicle, ElectricTerminal* term, float fLimitTime);
public:
	CollisionAnalyser(SystemSolution* sysSol, SystemSolution* sysSolRef, ElectricCircuit const * const simulatedCircuit);
	~CollisionAnalyser();

	//Create a collision with appropriate info
	void FindAndSortWaiting(std::set<Vehicle*> &vVehicleAdmissible, float limitTime, Vehicle* newVehicleNoTimeConstraint);

	//Will generate alternative search scenarios
	void PopNextAlternative(std::vector<AlternativeSearch>& vAltSearch);
	void DestroyAlternativeContent(AlternativeSearch& refAltSearch);
	void PopNextCollision();
	void PopAllCollision();
	CollisionWithReference* PeakNextCollision() const;


	//utility
	//nullptr for ElectriTerminal will yield collision along whole path
	static void PierceFrom(std::set<Vehicle*>& out_Affected, SystemSolution* sysSol,Vehicle* curVehicle, ElectricTerminal* term, float fLimitTime);
	static void InvestigateWaiterAcrossSolution(VehicleSolution* vehicleSol, ActionTerminal* actionToTest,SystemSolution* sysSol, Collision* out_Collision);

};

