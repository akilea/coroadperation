#pragma once

#include <string>
#include <vector>
#include "IScenarioLoader.h"
#include "Model.h"

class Carte;
class ElectricCircuit;
class ScenarioData;
class Vehicle;

class ScenarioLoader : public IScenarioLoader
{
private :
	std::string mFileName;
	const Carte& mRefCarte;

	int mNamelessID = 1;

	std::vector<SModel> m_vecPossibleAutonomy;
	SModel PickCarModel() const;

public:
	static const std::string NO_SCENARIO;
	virtual void SetPossibleAutonomy(std::vector<SModel>& in_vecPossibleAutonomy);

	ScenarioLoader(std::string filename, const Carte & carte);
	virtual void Load(ScenarioData* outRefData);
	//Return new Vehicle loaded
	virtual void LoadNextVehicle(std::istream& inDataVehicle, ScenarioData* outRefData);
	virtual void LoadNextNamelessVehicle(std::istream& inDataVehicle, ScenarioData* outRefData);
	~ScenarioLoader();
};

