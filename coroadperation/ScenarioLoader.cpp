#include "ScenarioLoader.h"


#include <assert.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>

#include "Scenario.h"
#include "srcMap/StructureCarte.h"
#include "Vehicle.h"
#include "ElectricCircuit.h"
#include "ScenarioData.h"
#include "Vehicle.h"
#include <stdlib.h>

const string ScenarioLoader::NO_SCENARIO = "None!";

SModel ScenarioLoader::PickCarModel() const
{
	return m_vecPossibleAutonomy[rand() % m_vecPossibleAutonomy.size()];
}

void ScenarioLoader::SetPossibleAutonomy(std::vector<SModel>& in_vecPossibleAutonomy)
{
	m_vecPossibleAutonomy = in_vecPossibleAutonomy;
}

ScenarioLoader::ScenarioLoader(std::string filename, const Carte & carte)
	:mFileName(filename),
	mRefCarte(carte)
{
	srand(1234);
	//Have a default value
	//m_vecPossibleAutonomy.push_back(250);
}

void ScenarioLoader::Load(ScenarioData* outRefData)
{
	if (mFileName.compare(NO_SCENARIO) != 0)
	{
		std::ifstream file(mFileName);

		if (file.is_open())
		{
			while (file) {

				LoadNextVehicle(file, outRefData);
				if (!file) break;
			}
		}
		else
		{
			cout << "***** ERROR: could not open file " << mFileName << endl;
		}

		file.close();
		cerr << "Scenario charg�e!" << endl;
	}
	outRefData->mElectricCircuit = new ElectricCircuit(mRefCarte);
}

void ScenarioLoader::LoadNextVehicle(std::istream& inDataVehicle, ScenarioData* outRefData)
{
	string nom;
	Point a, b;
	float depart;
	float aut;
	float l1charge;
	inDataVehicle >> nom;
	if (nom.length() > 0)
	{
		inDataVehicle >> a >> b >> aut >> depart >> l1charge >> ws;
		
		//TODO: regler la conversion d'unite
		SModel model = PickCarModel();
		outRefData->mVecVehicle.push_back(new Vehicle(nom, a, b, model.AutonomyToMeterInWinter(), depart, l1charge));
	}
}

void ScenarioLoader::LoadNextNamelessVehicle(std::istream & inDataVehicle, ScenarioData* outRefData)
{
	string nom = "Generic_" + std::to_string(mNamelessID);
	++mNamelessID;
	Point a, b;
	float aut;
	float depart = 0.0f;
	float l1charge = 1800.0f;
	inDataVehicle >> a >> b >> aut >> l1charge >> ws;
	//TODO: regler la conversion d'unite
	SModel model = PickCarModel();
	outRefData->mVecVehicle.push_back(new Vehicle(nom, a, b, model.AutonomyToMeterInWinter(), depart, l1charge));
}

ScenarioLoader::~ScenarioLoader()
{
}
