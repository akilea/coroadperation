#include "Model.h"

SModel::SModel(int in_nBestAutonomyMile, int in_nPercentRecurrence)
{
	m_nBestAutonomyMile = in_nBestAutonomyMile;
	m_nPercentRecurrence = in_nPercentRecurrence;
}

int SModel::AutonomyToMeterInWinter() const
{
	const float miles_to_km = 1.60934f;
	const float perte_hivers = 0.7316f; //Source
	const float kilo = 1000.0f;
	return int(m_nBestAutonomyMile * miles_to_km * perte_hivers * kilo);
}

string SModel::toString() const
{
	std::ostringstream ss;
	float maxTimeSecond = AutonomyToMeterInWinter() / SimulationSetting::D_AVERAGE_VEHICLE_CHARGE_RATE_M_PER_SECOND;
	float maxTimeHour = maxTimeSecond / 3600.0f;
	ss << "AutonomyToMeterInWinter():" << AutonomyToMeterInWinter() << " Percent: " << m_nPercentRecurrence << " maxTimeSecond:" << maxTimeSecond << " maxTimeHour:" << maxTimeHour;
	return ss.str();
}
