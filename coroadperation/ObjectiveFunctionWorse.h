#pragma once
#include "IObjectiveFunction.h"
class ObjectiveFunctionWorse :
    public IObjectiveFunction
{
public:
	ObjectiveFunctionWorse();
	virtual ~ObjectiveFunctionWorse();
	virtual float Evaluate(SystemSolution const* const in_A);
	float EvaluateBest(SystemSolution const* const in_A);
	virtual std::string Description() const { return  "PireDuree"; }
};

