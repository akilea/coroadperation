#pragma once
#include "SystemSolver.h"
class SystemSolverArranged :
	public SystemSolver
{
protected:
	ISystemSolver* mBestSolver = nullptr;
	ElectricCircuit* mBestCircuit = nullptr;

	void ResetSolver();
	virtual void AddVehicleInternal(Vehicle* newVehicle);
public:
	SystemSolverArranged(IObjectiveFunction* inObjectiveFunction, IVehicleSolver* inVehicleSolver, IArrangement* inArrangment, ElectricCircuit* inElectricCircuit, double in_dComputationTimeCap);
	virtual ~SystemSolverArranged();

	virtual SystemSolution* GetCurrentSystemSolution() const;
};

