#pragma once


#include "IVehicleSolver.h"

class IMapSolver;
class ElectricCircuit;
class IObjectiveFunction;

class VehicleSolverWait :
	public IVehicleSolver
{
private:

	IMapSolver const *const mRefCarte = nullptr;
	IObjectiveFunction *const mRefObjectiveFunction = nullptr;


public:
	bool m_bForceCooperation = false;
	VehicleSolverWait(IMapSolver const *const inCarte, IObjectiveFunction* inObjectiveFunction,ElectricCircuit* inElectricCircuit);
	virtual ~VehicleSolverWait();

	virtual void SolveFor(Vehicle* inVehicleToSolve, VehicleSolution* out_RefSolution);

};


