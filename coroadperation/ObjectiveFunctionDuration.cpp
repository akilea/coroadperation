#include "ObjectiveFunctionDuration.h"

#include "VehicleSolution.h"
#include "SystemSolution.h"
#include "Path.h"


ObjectiveFunctionDuration::ObjectiveFunctionDuration()
{
}


ObjectiveFunctionDuration::~ObjectiveFunctionDuration()
{
}

float ObjectiveFunctionDuration::Evaluate(SystemSolution const* const in_A)
{
	return in_A->mSystemPathDistribution.Total()/ in_A->NbVehiculeSolution();
}
