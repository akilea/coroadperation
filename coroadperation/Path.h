#pragma once

#include <string>
#include <list>
#include <unordered_set>
#include "point.h"
#include "Action.h"
#include "PathDistribution.h"
class ElectricTerminal;
class ElectricCircuit;
using namespace std;

class Path
{
public:
	float mDistance = 0.0f; //Changer pour PathProportion
	//TODO: ajouter penalite ici

	std::list<Action*> mSequenceAction;
	PathDistribution mTotalPathDistribution;



	Path();
	Path(const Path &old_obj);
	~Path();
	Path& operator=(const Path& other);

	string toString();
	string toStringShortenRoad();
	string toStringName();
	string toStringInteractive();

	//Refresh proportion in accordance to seqeunce of action
	void recomputeProportion();
	float getDuration() const;

	void ReplaceElectricCircuit(ElectricCircuit* inEC);
	Action* FindActionAt(TimeRange target) const;
	int CountAction(Action::TAction actType) const;
	bool HasActionAtTerminal(Action::TAction actType,ElectricTerminal* chargingAt) const;
	bool HasPartialSolution() const;
};
