#pragma once
#include "SystemSolverArranged.h"
#include <math.h>

class IArrangement;
class ElectricCircuit;

class SystemSolverCAStar :
	public SystemSolverArranged
{
	public:
		SystemSolverCAStar(IObjectiveFunction* inObjectiveFunction, IVehicleSolver* inVehicleSolver, IArrangement* inArrangment, ElectricCircuit* inElectricCircuit, double in_dComputationTimeCap);
		virtual ~SystemSolverCAStar();

		virtual void SolveSystem();
};

