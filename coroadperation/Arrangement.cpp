#include "Arrangement.h"
#include "Vehicle.h"

Arrangement::Arrangement()
{
	m_currentArrangement = new std::vector<Vehicle*>();
}


Arrangement::~Arrangement()
{
	delete m_currentArrangement;
}

void Arrangement::startSequence(std::vector<Vehicle*>* in_refVector)
{
	m_refVector = in_refVector;
	m_currentArrangement->clear();
}
