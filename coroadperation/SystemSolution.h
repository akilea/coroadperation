#pragma once

#include <map>
#include <ostream>
#include <string>
#include<vector>
#include "PathDistribution.h"
class Vehicle;
class VehicleSolution;
class ElectricCircuit;

using namespace std;

class SystemSolution
{
	friend class SolutionCutter;
protected:
	std::map<Vehicle*, VehicleSolution*> mMapVehicleSolution;

public:
	PathDistribution mSystemPathDistribution;
	string mDescription = " No description given";

	float ComputePenalityFor(Vehicle* v,PathDistribution pd) const;

	SystemSolution();
	SystemSolution(const SystemSolution& old_obj);
	~SystemSolution();
	SystemSolution& operator=(const SystemSolution& other);
	SystemSolution* clone() const;
	void ReplaceElectricCircuit(ElectricCircuit* inEC);
	void AddOrReplaceSolution(VehicleSolution* newSolution);
	void RemoveSolution(VehicleSolution* newSolution);
	VehicleSolution* GetVehiculeSolution(Vehicle* veh) const;
	bool HasVehiculeSolution(Vehicle* veh) const;
	size_t NbVehiculeSolution() const;

	auto GetBeginIterator() const { return mMapVehicleSolution.begin(); }
	auto GetEndIterator() const { return mMapVehicleSolution.end(); }

	bool IsImpossible() const;
	bool HasReachedEnd() const;

	void RecomputeSystemDistribution();
	void RecomputeVehicleAndSystemDistribution();

	friend ostream& operator<<(ostream& os, const SystemSolution& dt);

	ostream& displayCompletionList(ostream& os) const;

};

ostream& operator<<(ostream& os, const SystemSolution& dt);

