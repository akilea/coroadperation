#pragma once

#include <vector>
class Vehicle;

class IArrangement
{
protected:

public:
	IArrangement();
	virtual ~IArrangement();

	virtual void startSequence(std::vector<Vehicle*>* in_refVector) = 0;
	//Returns a reference from the internal copy! No garantee it is the original structure
	//Returns an empty vector when done
	virtual std::vector<Vehicle*>* GetNextSequence() = 0;
};

