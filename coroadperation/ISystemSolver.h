#pragma once

class Vehicle;
class ElectricCircuit;
class SystemSolution;
class IObjectiveFunction;
class IVehicleSolver;
class ScenarioData;
class PerformanceAnalysis;
#include "TraceConvergenceExporter.h"
#include <string>


class ISystemSolver
{

public:
	struct Description
	{
		std::string sSolverType;
		std::string sParameter;
		std::string sPermutation;
		std::string sShort;
	};

protected:

	Description desc;

	IObjectiveFunction* mObjectiveFunction = nullptr;
	IVehicleSolver* mVehicleSolver = nullptr;

	//Solver has it's own data internally
	ScenarioData* mSolverData = nullptr;

	PerformanceAnalysis* mPerformanceAnalysis = nullptr;
	bool mIsActivated = true;

	TraceConvergenceExporter m_traceExporter;

protected:
	IObjectiveFunction* GetObjectiveFunction() const { return mObjectiveFunction; }
	//These will solve when adding and removing candidates
	virtual void AddVehicleInternal(Vehicle* newVehicle) = 0;
	virtual void RemoveVehicleInternal(Vehicle* newVehicle) = 0;

public:
	IVehicleSolver* GetVehicleSolver() const {return mVehicleSolver;}
	ISystemSolver(IObjectiveFunction* inObjectiveFunction, IVehicleSolver* inVehicleSolver, ElectricCircuit* inElectricCircuit, double in_dComputationTimeCap);
	virtual ~ISystemSolver();

	void SetTraceExporterName(string traceName);

	//These will solve when adding and removing candidates
	virtual void AddVehicle(Vehicle* newVehicle);
	virtual void RemoveVehicle(Vehicle* newVehicle);

	virtual void SolveSystem() = 0;

	void SetActivation(bool bActivation);
	bool HasActivation() const { return mIsActivated; }

	virtual SystemSolution* GetCurrentSystemSolution() const = 0;
	virtual void ForceCurrentSystemSolution(SystemSolution* forcedSolution) = 0; //takes ownership

	virtual Description& GetDescription() {
		return desc;
	}

	//One should only modify optionnal parameters when getting ScenarioData
	//TODO: have a proxy?
	ScenarioData* ModifyOptionnalScenarioData() const { return mSolverData; }
	PerformanceAnalysis * const GetPerformanceAnalysis() const { return mPerformanceAnalysis; }
	bool HasReachdedRunningTimeCap() const;
	bool CheckTimeCapAndDisable();
};
