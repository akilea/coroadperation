#pragma once

#include <list>

class SystemSolution;
class VehicleSolution;
class ElectricTerminal;
class Action;

class SolutionCutter
{
private:

	//Unsafe to use alone
	void cutFromAction(VehicleSolution* solToCut, std::list<Action*>::iterator itToCut) const;
	//Unsafe to use alone. It will not remove the waiting from other vehicle in system solution
	bool cutFromPreviousCommitment(VehicleSolution* solToCut,float fLimitTime) const;
public:
	//Cut all vehicle solution
	bool cutFromPreviousCommitment(SystemSolution* solToCut,float fLimitTime) const;

	//Vehicle
	bool cutRoadToTerminal(SystemSolution* solToCut, VehicleSolution* solVToCut,ElectricTerminal* terminalToCut) const;

	//Commitment
	bool addRemainingReservation(VehicleSolution* solToCut)const;

	//CAREFULL
	//DEPRECATED
	//Unsafe to use alone. It will not remove the waiting from other vehicle in system solution
	bool cutRoadToTerminal(VehicleSolution* solToCut,ElectricTerminal* terminalToCut) const;
};

