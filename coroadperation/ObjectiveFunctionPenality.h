#pragma once
#include "IObjectiveFunction.h"
class SystemSolution;

class ObjectiveFunctionPenality :
	public IObjectiveFunction
{

private :
	SystemSolution* mRefReferenceSystemSolution = nullptr;

public:
	ObjectiveFunctionPenality(SystemSolution* refReferenceSystemSolution);
	virtual ~ObjectiveFunctionPenality();
	virtual std::string Description() const { return  "Penalite"; }
	virtual float Evaluate(SystemSolution const* const in_A);
};

