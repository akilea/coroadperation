#pragma once

#include <string>
#include "PathDistribution.h"

class Action;
class Path;
class Vehicle;
class ElectricCircuit;
class ElectricTerminal;
using namespace std;

class VehicleSolution
{
public:

	Vehicle * mRefVehicle = nullptr;
	Path* mPath = nullptr;

	const string cst_no_path = "Impossible!";

	VehicleSolution(Vehicle* inRefVehicle);
	VehicleSolution(const VehicleSolution &old_obj);
	~VehicleSolution();
	VehicleSolution& operator=(const VehicleSolution& other);
	VehicleSolution * clone() const;

	string ComputeTimeRoundedSecondInteractive() const;

	void ReplaceElectricCircuit(ElectricCircuit* inEC);
	string toString() const;
	string toStringInteractive() const;
	ostream& displayTrajectoryDetail(ostream& os) const;
	bool HasReachedEnd() const;
	bool IsImpossible() const;
	size_t actionCount() const;

	friend ostream& operator<<(ostream& os, const VehicleSolution& dt);

};

ostream& operator<<(ostream& os, const VehicleSolution& dt);

