#include "ActionRoad.h"



ActionRoad::ActionRoad(ElectricCircuit* refElectricCircuit, ElectricTerminal* refTerminal)
	:Action(refElectricCircuit, refTerminal)
{
}


ActionRoad::~ActionRoad()
{
}

inline Action * ActionRoad::clone() const {
	ActionRoad* newRoad = new ActionRoad(this->GetElectricCircuit(), this->getTerminal());
	newRoad->mDistanceFromLast = this->mDistanceFromLast;
	return CopyActionDataTo(newRoad);
}
