#pragma once

class  Path; //Bad design... carefull
class RequestCarte; //Also bad... double carefull

class IMapSolver
{
public:
	IMapSolver() {}
	virtual ~IMapSolver() {}

	virtual double calculerTrajet(Path& in_out_path, RequestCarte & inRequestCarte) const = 0;
};