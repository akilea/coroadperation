#include "CollisionWithReference.h"

#include "ActionTerminal.h"
#include "ActionWait.h"
#include "VehicleSolution.h"
#include "Vehicle.h"
#include "Path.h"

CollisionWithReference::CollisionWithReference(VehicleSolution* solW, VehicleSolution* solRefW, ActionWait* actW, VehicleSolution* solB, VehicleSolution* solRefB, ActionTerminal* actB)
: Collision(solW, actW, solB, actB)
, mWaiterReferenceSolution(solRefW)
, mBlockerReferenceSolution(solRefB)
{

}

ostream& operator<<(ostream& os, const CollisionWithReference& dt)
{
	//Same as Collision for now
	os << dt;
	return os;
}

bool operator<(const CollisionWithReference& a, const CollisionWithReference& b)
{
	//Compute waiting for A
	int nTerminalCountA = a.GetWaiterSolution()->mPath->CountAction(Action::eTerminal);
	float fDetourA = 0.f;
	if(nTerminalCountA > 0)
	{
		fDetourA = (a.GetWaiterSolution()->mPath->mTotalPathDistribution.mRoadTime - a.GetWaiterReferenceSolution()->mPath->mTotalPathDistribution.mRoadTime) / (float)nTerminalCountA;
	}
	float waitA = a.GetWaiterActionWait()->getDuration() + fDetourA;

	//Same for B
	int nTerminalCountB = b.GetWaiterSolution()->mPath->CountAction(Action::eTerminal);
	float fDetourB = 0.f;
	if (nTerminalCountB > 0)
	{
		fDetourB = (b.GetWaiterSolution()->mPath->mTotalPathDistribution.mRoadTime - b.GetWaiterReferenceSolution()->mPath->mTotalPathDistribution.mRoadTime) / (float)nTerminalCountB;
	}
	float waitB = b.GetWaiterActionWait()->getDuration() + fDetourB;

	return waitA < waitB;
}

bool CollisionCompare::operator()(CollisionWithReference* a, CollisionWithReference* b)
{
	return *a < * b;
}
