#pragma once
#include "ElectricReservation.h"
#include "ElectricTerminal.h"

class VehicleSolution;
class ActionTerminal;
class ActionWait;

class Collision
{
protected :
	VehicleSolution* mWaiterSolution = nullptr;
	ActionWait* mWaiterWaitAction = nullptr;

	VehicleSolution* mBlockerSolution = nullptr;
	ActionTerminal* mBlockerTerminalAction = nullptr;

public:

	Collision(VehicleSolution* solW, ActionWait* actW, VehicleSolution* solB, ActionTerminal* actB);
	Collision();
	virtual ~Collision() {}

	VehicleSolution* GetWaiterSolution() const { return mWaiterSolution; }
	VehicleSolution* GetBlockerSolution() const { return mBlockerSolution; }
	ActionTerminal* GetBlockerActionTerminal() const { return mBlockerTerminalAction; }
	ActionWait* GetWaiterActionWait() const { return mWaiterWaitAction; }

	bool HasCollision() const;

	void Set(VehicleSolution* solW, ActionWait* actW, VehicleSolution* solB, ActionTerminal* actB);

	friend ostream& operator<<(ostream& os, const Collision& dt);

};

ostream& operator<<(ostream& os, const Collision& dt);

