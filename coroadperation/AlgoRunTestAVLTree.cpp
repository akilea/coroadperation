#include "AlgoRunTestAVLTree.h"

#include "AVLTree.h"
#include "ElectricReservationList.h"


AlgoRunTestAVLTree::AlgoRunTestAVLTree()
{
}


AlgoRunTestAVLTree::~AlgoRunTestAVLTree()
{
}

int AlgoRunTestAVLTree::Run(int argc, const char** argv)
{
	CRP::set<int> t;

	for (int i = 0; i < 10; ++i)
	{
		t.insert(i);
	}
	t.insert(-5);
	t.insert(-10);
	//t.insert(5); //Exception!

	cout << t << endl;

	CRP::set<ElectricReservationList> er;

	for (int i = 0; i < 10; ++i)
	{
		ElectricReservationList elA(i);
		er.insert(elA);
	}
	cout << er << endl;

	cin.get();

	return 0;
}
