#pragma once
#include "SystemSolver.h"
class SystemSolverImmediate :
	public SystemSolver
{
protected:
	virtual void AddVehicleInternal(Vehicle* newVehicle);
	bool m_bMesurePerformance = true;

public:
	SystemSolverImmediate(IObjectiveFunction* inObjectiveFunction, IVehicleSolver* inVehicleSolver, ElectricCircuit* inElectricCircuit, double in_dComputationTimeCap);
	virtual ~SystemSolverImmediate();

	virtual void SolveSystem();

};

