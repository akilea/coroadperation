#pragma once

#include <string>
#include <vector>
#include <ostream>

class IScenarioLoader;
class ScenarioData;
class IObjectiveFunction;
class IVehicleSolver;
class ISystemSolver;
class ElectricCircuit;
class IArrangement;
class Vehicle;

class IScenario
{
protected:
	std::vector<IVehicleSolver*> mVehicleSolver;
	std::vector<ISystemSolver*> mSystemSolver;
	std::vector<ISystemSolver*> mSystemSolverAssist; //Sub solvers used by members of mSystemSolver
	std::vector<IArrangement*> mArrangement;
	std::vector<ElectricCircuit*> mSolverCircuit;
	IScenarioLoader* mRefScenarioLoader;

	unsigned int mDataIndexSimulator = 0;
	std::string m_strAutonomy;

public:
	//TODO: have a getter for objective function
	std::vector<IObjectiveFunction*> mObjectiveFunction;
	IScenario(IScenarioLoader* inScenarioLoader):mRefScenarioLoader(inScenarioLoader){}
	virtual ~IScenario();

	virtual ScenarioData* GetRefData() const = 0;

	//Return if a vehicle was solved
	virtual bool SolveNextVehicle();
	virtual void AfterSolveNextVehicleCallback(ISystemSolver* lastSolverUsed,Vehicle* lastVehicleAdded) {}
	virtual void SolveAllVehicle();
	virtual void SolveAllSystem();
	virtual void SolveNextBatch(int max);
	void SetAutonomy(std::string autonomyName);
	

	virtual ISystemSolver* const GetSystemSolver(int i) const {
		return mSystemSolver[i];
	}

	unsigned int NbSystemSolver() const { return mSystemSolver.size(); }

	virtual void SetActivation(bool toActivate[]);
	virtual void SetActivation(int indexSolver,bool toActivate);
	virtual void SetActivationAll(bool active);
	virtual void SetActivationIfTimeLeft();

	virtual void LoadNamelessVehicle(std::istream& inDataVehicle);

	virtual std::string toString() const;
	std::string toStringCost() const;
	virtual std::string toStringPerformance(IObjectiveFunction* ptrObjFx) const;
	virtual std::string toStringInteractive() const ;

	void ExportHeader(std::ostream& st) const ;
	void Export(std::ostream& st) const ;
};