#include "ObjectiveFunctionSquaredDifferencePenality.h"
#include "SystemSolution.h"
#include "VehicleSolution.h"
#include "Path.h"
#include "ObjectiveFunctionPenality.h"
#include <assert.h>

ObjectiveFunctionSquaredDifferencePenality::ObjectiveFunctionSquaredDifferencePenality(SystemSolution* refReferenceSystemSolution) 
:mRefReferenceSystemSolution(refReferenceSystemSolution)
{
	assert(refReferenceSystemSolution != nullptr);
	m_ptrPenality = new ObjectiveFunctionPenality(refReferenceSystemSolution);
}

ObjectiveFunctionSquaredDifferencePenality::~ObjectiveFunctionSquaredDifferencePenality() 
{
	delete m_ptrPenality;
}

float ObjectiveFunctionSquaredDifferencePenality::Evaluate(SystemSolution const* const in_A)
{

	float fDiffPenalite = INFINITY;
	float fNbSolution = 1.0f;
	if (in_A != nullptr)
	{
		float fAvgPenality = m_ptrPenality->Evaluate(in_A);
		fDiffPenalite = 0.0f;
		fNbSolution = 0.0f;
		for (auto it = in_A->GetBeginIterator(); it != in_A->GetEndIterator(); ++it)
		{
			auto refVehicleSolution = mRefReferenceSystemSolution->GetVehiculeSolution(it->first);
			if (refVehicleSolution != nullptr)
			{
				float fCurrent = it->second->mPath->getDuration() / refVehicleSolution->mPath->getDuration();
				float diff = fCurrent - fAvgPenality;
				fDiffPenalite += diff* diff;
				fNbSolution += 1.0f;
			}
			else
			{
				cout << "ObjectiveFunctionSquaredDifferencePenality::Evaluate - CANNOT FIND SOLUTION - CRITICAL ERROR" << endl;
			}
		}
	}
	return sqrt(fDiffPenalite) / fNbSolution;
}
