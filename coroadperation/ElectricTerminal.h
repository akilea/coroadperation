#pragma once
//#include "AVLTree.h"
#include "srcMap/StructureCarte.h"
#include "ElectricReservation.h"
#include <string>
#include <vector>
#include <set>
#include "srcMap/point.h"

using namespace std;

template< class T> class vector;

typedef set<ElectricReservationPtr, ElectricResservationCompare> setER;
typedef setER::iterator(setER::* SearchMethodReservation)(const ElectricReservationPtr& Element) const;
#define SET_OPERATOR_RESERVATION(curOperator) &setER::##curOperator


class ElectricTerminal
{
private:
	setER mOrderedSetReservation;
	Borne mRefBorne;
	int mIndexTerminal = -1;

	setER::iterator Find(TimeUnit timeStamp, setER& in_SearchIn, SearchMethodReservation in_SearchMethod);

public:
	ElectricTerminal(Borne & in_RefBorne, int nIndexTerminal);
	ElectricTerminal(const ElectricTerminal & otherTerminal);
	~ElectricTerminal();

	bool TryReserve(ElectricReservation* er);
	bool TryReserveDeltaOne(ElectricReservation* er); //To accomodate floating point error... super hack
	bool TryFree(ElectricReservation* er);

	void FreeAll();

	string GetName() const { return mRefBorne.nom; }
	inline int GetIndex() const {
		return mIndexTerminal;
	}
	Point GetPosition() const { return mRefBorne.point; }
	unsigned int size() const { return mOrderedSetReservation.size(); }
	int GetIndexLieu() const { return mRefBorne.indexlieu; }
	void ReservationOverlap(ElectricReservation* er, std::vector<ElectricReservation*>& out_VecOverlapingReservation);
	void ReservationOverlap(TimeRange tr, std::vector<ElectricReservation*>& out_VecOverlapingReservation);

	//Return the best range for accomoding the ideal range.
	//At best returns in_RefIdealRange
	//Otherwise, it will return a TimeRange for the same duration but that will start after
	TimeRange FindNextAvailableReservation(TimeRange in_IdealRange);

	//Information string
	string toString() const;
	friend ostream& operator<<(ostream& os, const ElectricTerminal& dt);
};

ostream& operator<<(ostream& os, const ElectricTerminal& dt);


