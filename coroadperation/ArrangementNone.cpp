#include "ArrangementNone.h"



ArrangementNone::ArrangementNone()
{
}


ArrangementNone::~ArrangementNone()
{
}

void ArrangementNone::startSequence(std::vector<Vehicle*>* in_refVector)
{
	Arrangement::startSequence(in_refVector);
	wasReturned = false;
}

std::vector<Vehicle*>* ArrangementNone::GetNextSequence()
{
	if (wasReturned)
	{
		return nullptr;
	}
	else
	{
		wasReturned = true;
		return m_refVector;
	}
}
