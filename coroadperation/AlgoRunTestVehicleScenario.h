#pragma once
#include "IAlgoRun.h"
#include <string>
class IScenario;
class AlgoRunTestVehicleScenario :
	public IAlgoRun
{
public:
	AlgoRunTestVehicleScenario();
	virtual ~AlgoRunTestVehicleScenario();
	void ExportData(IScenario* scenario, std::string strOption[]);
	virtual int Run(int argc, const char** argv);
};

