#include "Scenario.h"

#include <sstream>
#include <iomanip>

#include "srcMap/point.h"
#include "IMapSolver.h"
#include "Vehicle.h"
#include "ElectricCircuit.h"
#include "ScenarioLoader.h"
#include "ScenarioData.h"
#include "VehicleSolverAbsoluteOptimal.h"
#include "VehicleSolverCooperation.h"
#include "VehicleSolverWait.h"
#include "ObjectiveFunctionDuration.h"
#include "ObjectiveFunctionPenality.h"
#include "ObjectiveFunctionWorse.h"
#include "ObjectiveFunctionSquaredDifference.h"
#include "ObjectiveFunctionWorstPenality.h"
#include "ObjectiveFunctionSquaredDifferencePenality.h"
#include "SystemSolverImmediate.h"
#include "SystemSolution.h"
#include "ISystemSolver.h"
#include "PerformanceAnalysis.h"
#include "SystemSolverCAStar.h"
#include "SystemSolverWHCAStar.h"
#include "SystemSolverPCARL.h"
#include "ArrangementNone.h"
#include "ArrangementCascade.h"
#include "ArrangementPermutation.h"

const string Scenario::sArrangementAssociation[Scenario::eMAX_ARRANGEMENT] =
{"Premier"
,"Cascade"
,"Permutation"
};


Scenario::Scenario(IScenarioLoader* inScenarioLoader, IMapSolver const & inRefCarte,int in_nMetrique)
	:IScenario(inScenarioLoader)
{
	//Data
	mLoaderData = new ScenarioData();
	mRefScenarioLoader->Load(mLoaderData);

	//Objective function
	IObjectiveFunction* ptrObjectiveFx = nullptr;
	mObjectiveFunction.push_back(new ObjectiveFunctionDuration());
		
	//Arrangements possible
	mArrangement.push_back(new ArrangementNone()); //eNONE
	mArrangement.push_back(new ArrangementCascade()); //eCASCADE
	mArrangement.push_back(new ArrangementPermutation()); //ePERMUTATION
	//TODO: add new method

	//************************************
	//******SCENARIO CREATION*******
	//************************************
	//******NON CACHED SOLVERS******
	//BASIC SOLVERS
	////We use the first solver (that is fast) to initialise
	mSolverCircuit.push_back(this->GetRefData()->mElectricCircuit->clone());
	mVehicleSolver.push_back(new VehicleSolverAbsoluteOptimal(&inRefCarte, mSolverCircuit.back(), mObjectiveFunction.back()));
	mSystemSolver.push_back(new SystemSolverImmediate(mObjectiveFunction.back(), mVehicleSolver.back(), mSolverCircuit.back(), INFINITY));
	mSystemSolver.back()->GetCurrentSystemSolution()->mDescription = "Init cache (Précalcul la carte des bornes augmentées)";
	mSystemSolver.back()->GetDescription().sSolverType = "Cache";
	mSystemSolver.back()->GetDescription().sShort = "Cache";

	//Then adding objectives
	switch (in_nMetrique)
	{
	case TObjective::eAVERAGEDURATION:
		break;
	case TObjective::eWORSTDURATION:
		mObjectiveFunction.push_back(new ObjectiveFunctionWorse());
		break;
	case TObjective::eSQUAREDDIFFERENCEDURATION:
		mObjectiveFunction.push_back(new ObjectiveFunctionSquaredDifference(mSystemSolver.back()->GetCurrentSystemSolution()));
		break;
	case TObjective::eAVERAGEPENALITY:
		mObjectiveFunction.push_back(new ObjectiveFunctionPenality(mSystemSolver.back()->GetCurrentSystemSolution()));
		break;
	case TObjective::eWORSTPENALITY:
		mObjectiveFunction.push_back(new ObjectiveFunctionWorstPenality(mSystemSolver.back()->GetCurrentSystemSolution()));
		break;
	case TObjective::eSQUAREDDIFFERENCEPENALITY:
		mObjectiveFunction.push_back(new ObjectiveFunctionSquaredDifferencePenality(mSystemSolver.back()->GetCurrentSystemSolution()));
		break;
	}

	//What people think they will get
	mSolverCircuit.push_back(this->GetRefData()->mElectricCircuit->clone());
	mVehicleSolver.push_back(new VehicleSolverAbsoluteOptimal(&inRefCarte, mSolverCircuit.back(), mObjectiveFunction.back()));
	mSystemSolver.push_back(new SystemSolverImmediate(mObjectiveFunction.back(), mVehicleSolver.back(), mSolverCircuit.back(), INFINITY));
	mSystemSolver.back()->GetCurrentSystemSolution()->mDescription = "Planification non coopérative sans réservation";
	mSystemSolver.back()->GetDescription().sSolverType = "PNCSR";
	mSystemSolver.back()->GetDescription().sShort = "PNCSR";

	//What people will experience
	mSolverCircuit.push_back(this->GetRefData()->mElectricCircuit->clone());
	mVehicleSolver.push_back(new VehicleSolverWait(&inRefCarte, mObjectiveFunction.back(),mSolverCircuit.back()));
	mSystemSolver.push_back(new SystemSolverImmediate(mObjectiveFunction.back(), mVehicleSolver.back(), mSolverCircuit.back(), INFINITY));
	mSystemSolver.back()->GetCurrentSystemSolution()->mDescription = "Planification non coopérative avec réservation.";
	mSystemSolver.back()->GetDescription().sSolverType = "PNCAR";
	mSystemSolver.back()->GetDescription().sShort = "PNCAR";

	//Cooperation but immediate
	mSolverCircuit.push_back(this->GetRefData()->mElectricCircuit->clone());
	mVehicleSolver.push_back(new VehicleSolverCooperation(&inRefCarte, mObjectiveFunction.back(), mSolverCircuit.back()));
	mSystemSolver.push_back(new SystemSolverImmediate(mObjectiveFunction.back(), mVehicleSolver.back(), mSolverCircuit.back(), INFINITY));
	mSystemSolver.back()->GetCurrentSystemSolution()->mDescription = "Planficiation coopérative avec réservation retardable.";
	mSystemSolver.back()->GetDescription().sSolverType = "PNCARR";
	mSystemSolver.back()->GetDescription().sShort = "PNCARR";

	//All first in orders
	for (size_t i = 0; i < size_t(eMAX_ARRANGEMENT); i++)
	{
		mSolverCircuit.push_back(this->GetRefData()->mElectricCircuit->clone());
		mVehicleSolver.push_back(new VehicleSolverCooperation(&inRefCarte, mObjectiveFunction.back(), mSolverCircuit.back()));
		mSystemSolver.push_back(new SystemSolverCAStar(mObjectiveFunction.back(), mVehicleSolver.back(), mArrangement[i], mSolverCircuit.back(), SimulationSetting::D_COMPUTATION_TIME_CAP));
		mSystemSolver.back()->GetCurrentSystemSolution()->mDescription = "PCAPDP " + sArrangementAssociation[i];
		mSystemSolver.back()->GetDescription().sSolverType = "PCAPDP";
		mSystemSolver.back()->GetDescription().sPermutation = sArrangementAssociation[i];
		mSystemSolver.back()->GetDescription().sShort = "PCAPDP" + sArrangementAssociation[i];
		for (size_t j = 0; j < SimulationSetting::VF_VEC_WHCA_WINDOWS.size(); j++)
		{
			//Sweep window (small)
			mSolverCircuit.push_back(this->GetRefData()->mElectricCircuit->clone());
			mVehicleSolver.push_back(new VehicleSolverCooperation(&inRefCarte, mObjectiveFunction.back(), mSolverCircuit.back()));
			mSystemSolver.push_back(new SystemSolverWHCAStar(mObjectiveFunction.back(), mVehicleSolver.back(), mArrangement[i], mSolverCircuit.back(), SimulationSetting::D_COMPUTATION_TIME_CAP, SimulationSetting::VF_VEC_WHCA_WINDOWS[j], SimulationSetting::VF_VEC_WHCA_WINDOWS[j] * SimulationSetting::F_WHCA_RECOMPUTE_PERCENTAGE));
			mSystemSolver.back()->GetCurrentSystemSolution()->mDescription = "PCAPF " + SimulationSetting::VSTR_VEC_WHCA_WINDOWS_NAME[j] + " " + sArrangementAssociation[i];
			mSystemSolver.back()->GetDescription().sSolverType = "PCAPF";
			mSystemSolver.back()->GetDescription().sPermutation = sArrangementAssociation[i];
			mSystemSolver.back()->GetDescription().sShort = "PCAPF" + sArrangementAssociation[i] + "_" + SimulationSetting::VSTR_VEC_WHCA_WINDOWS_NAME[j];
			std::ostringstream ss;
			ss << fixed << setprecision(0);
			ss << "W=" << (SimulationSetting::VF_VEC_WHCA_WINDOWS[j]) << " R=" << (SimulationSetting::VF_VEC_WHCA_WINDOWS[j] / SimulationSetting::F_WHCA_RECOMPUTE_PERCENTAGE);
			string param = ss.str();
			mSystemSolver.back()->GetDescription().sParameter = param;
		}
	}
	//LRCA*

	//Sub solver: best optimal
	//mSolverCircuit.push_back(this->GetRefData()->mElectricCircuit->clone());
	//mVehicleSolver.push_back(new VehicleSolverAbsoluteOptimal(&inRefCarte, mSolverCircuit.back(), mObjectiveFunction.back()));
	//mSystemSolverAssist.push_back(new SystemSolverImmediate(mObjectiveFunction.back(), mVehicleSolver.back(), mSolverCircuit.back(), INFINITY));
	//ISystemSolver* refSolver = mSystemSolverAssist.back();
	//mSystemSolverAssist.back()->GetCurrentSystemSolution()->mDescription = "A* for LRCA*";
	//mSystemSolverAssist.back()->GetDescription().sSolverType = "A*->SubSolverLRCA*";
	//mSystemSolverAssist.back()->GetDescription().sShort = "A*->SubSolverLRCA*";

	//PCARLG
	//Electric circuit will not get cleaned for that one

	CreateLocalSolver(inRefCarte, true,true  ,true);
	CreateLocalSolver(inRefCarte, false,true ,true);
	CreateLocalSolver(inRefCarte, true,false ,true);
	CreateLocalSolver(inRefCarte, false,false,true);
	CreateLocalSolver(inRefCarte,  true, true,  false);
	CreateLocalSolver(inRefCarte,  false, true, false);
	CreateLocalSolver(inRefCarte,  true, false, false);
	CreateLocalSolver(inRefCarte,  false, false,false);

	//TODO create PCC
	mSolverCircuit.push_back(this->GetRefData()->mElectricCircuit->clone());
	mVehicleSolver.push_back(new VehicleSolverWait(&inRefCarte, mObjectiveFunction.back(), mSolverCircuit.back()));
	mSystemSolver.push_back(new SystemSolverImmediate(mObjectiveFunction.back(), mVehicleSolver.back(), mSolverCircuit.back(), INFINITY));
	mSystemSolver.back()->GetCurrentSystemSolution()->mDescription = "Planification coopérative complète.";
	mSystemSolver.back()->GetDescription().sSolverType = "PCC";
	mSystemSolver.back()->GetDescription().sShort = "PCC";

	//for (size_t i = 0; i < size_t(eMAX_ARRANGEMENT); i++)
	//{
	//	//Sub solver: optimal with waiting
	//	mSolverCircuit.push_back(this->GetRefData()->mElectricCircuit->clone());
	//	mVehicleSolver.push_back(new VehicleSolverWait(&inRefCarte, mObjectiveFunction.back(), mSolverCircuit.back()));
	//	//BUG: delete is buggy since solver is referenced somewhere else in the LRCA*
	//	//mSystemSolverAssist.push_back(new SystemSolverImmediate(mObjectiveFunction.back(), mVehicleSolver.back(), mSolverCircuit.back(), INFINITY));
	//	ISystemSolver* waitSolver = new SystemSolverImmediate(mObjectiveFunction.back(), mVehicleSolver.back(), mSolverCircuit.back(), INFINITY);//mSystemSolverAssist.back();
	//	waitSolver->GetCurrentSystemSolution()->mDescription = "A*NC for LRCA*";
	//	waitSolver->GetDescription().sSolverType = "A*NC->SubSolverLRCA";
	//	waitSolver->GetDescription().sShort = "A*NC->SubSolverLRCA";
	//
	//	//Final solver
	//	//Uses the same electric circuit as the optimal with waiting. This is normal
	//	mSolverCircuit.push_back(mSolverCircuit.back()); //<---- Here is normal
	//	mVehicleSolver.push_back(new VehicleSolverCooperation(&inRefCarte, mObjectiveFunction.back(), mSolverCircuit.back()));
	//	mSystemSolver.push_back(new SystemSolverLRCAStar(mObjectiveFunction.back(), mVehicleSolver.back(), mSystemSolver[Scenario::eNO_COOP], waitSolver, mArrangement[i], mSolverCircuit.back(), SimulationSetting::D_COMPUTATION_TIME_CAP));
	//	mSystemSolver.back()->GetCurrentSystemSolution()->mDescription = "Local repair cooperation: LRCA*." + sArrangementAssociation[i];
	//	mSystemSolver.back()->GetDescription().sSolverType = "LRCA*";
	//	mSystemSolver.back()->GetDescription().sShort = "LRCA*" + sArrangementAssociation[i];
	//	mSystemSolver.back()->GetDescription().sPermutation = sArrangementAssociation[i];
	//
	//}
}

Scenario::~Scenario()
{
	delete mLoaderData;
}

inline std::string Scenario::toStringPerformance(IObjectiveFunction* ptrObjFx) const
{
	std::ostringstream ss;
	int nbVec = (int)mLoaderData->mVecVehicle.size();
	ss << "Vehicle count: " << to_string(nbVec) << endl;
	ss  << PerformanceAnalysis::toStringHeader(ptrObjFx->Description()) << endl;

	for (size_t i = 0; i < mSystemSolver.size(); i++)
	{
		//Display solution details
		//Not super clean I must admit
		ISystemSolver::Description desc = mSystemSolver[i]->GetDescription();
		ss << std::setw(6) << left << desc.sSolverType << std::setw(12) << left << desc.sPermutation << std::setw(20) << left << desc.sParameter;
		//Display costs
		float fCost = ptrObjFx->Evaluate(mSystemSolver[i]->GetCurrentSystemSolution());
		ss << left << mSystemSolver[i]->GetPerformanceAnalysis()->toString(nbVec, fCost) << endl;
	}
	return ss.str();
}

void Scenario::AfterSolveNextVehicleCallback(ISystemSolver* lastSolverUsed, Vehicle* lastVehicleAdded)
{
	if (!lastSolverUsed->HasActivation())
	{
		return;
	}
	bool canContinue = !lastSolverUsed->GetPerformanceAnalysis()->HasReachdedTimeCap();
	if (!canContinue)
	{
		lastSolverUsed->SetActivation(false);
	}
	string solverType = lastSolverUsed->GetDescription().sSolverType;
	//if (!lastSolverUsed->HasActivation() && (solverType == "PCAPDP" || solverType == "PCAPF"))
	//{
	//	if (lastSolverUsed->GetDescription().sPermutation == "Cascade")
	//	{
	//		cout << "MASS DEACTIVATE CASCADE" << endl;
	//		mSystemSolver[eHCA_CASCADE]->SetActivation(false);
	//		mSystemSolver[eWHCA_SMALL_CASCADE]->SetActivation(false);
	//		mSystemSolver[eWHCA_MEDIUM_CASCADE]->SetActivation(false);
	//		mSystemSolver[eWHCA_LARGE_CASCADE]->SetActivation(false);
	//	}
	//	else if (lastSolverUsed->GetDescription().sPermutation == "Permutation")
	//	{
	//		cout << "MASS DEACTIVATE PERMUTATION" << endl;
	//		mSystemSolver[eHCA_PERMUTATION]->SetActivation(false);
	//		mSystemSolver[eWHCA_SMALL_PERMUTATION]->SetActivation(false);
	//		mSystemSolver[eWHCA_MEDIUM_PERMUTATION]->SetActivation(false);
	//		mSystemSolver[eWHCA_LARGE_PERMUTATION]->SetActivation(false);
	//	}
	//}

	const int nPermutationMaxCarIndex = 8;
	if (lastSolverUsed->HasActivation() && mDataIndexSimulator >= nPermutationMaxCarIndex && (solverType == "PCAPDP" || solverType == "PCAPF"))
	{
		//cout << "PERMUTATION DEACTIVATE" << endl;
		if (mSystemSolver[eWHCA_SMALL_PERMUTATION]->HasActivation())
		{
			mSystemSolver[eWHCA_SMALL_PERMUTATION]->SetActivation(false);
		}
		if (mSystemSolver[eWHCA_MEDIUM_PERMUTATION]->HasActivation())
		{
			mSystemSolver[eWHCA_MEDIUM_PERMUTATION]->SetActivation(false);
		}
		if (mSystemSolver[eWHCA_LARGE_PERMUTATION]->HasActivation())
		{
			mSystemSolver[eWHCA_LARGE_PERMUTATION]->SetActivation(false);
		}
		if (mSystemSolver[eHCA_PERMUTATION]->HasActivation())
		{
			mSystemSolver[eHCA_PERMUTATION]->SetActivation(false);
		}
	}
}

void Scenario::CreateLocalSolver(IMapSolver const& inRefCarte, bool isGreedy, bool isStartingFresh, bool isBaseCooperative)
{
	string desc = "PCARL";
	if (isGreedy)
	{
		desc += "G";
	}

	if (isBaseCooperative)
	{
		desc += "_PNCARR";
	}
	else
	{
		desc += "_PNCAR";
	}

	if (isStartingFresh)
	{
	}
	else
	{
		desc += "_C";
	}

	ElectricCircuit* tmpCircuit = this->GetRefData()->mElectricCircuit->clone();
	mVehicleSolver.push_back(new VehicleSolverWait(&inRefCarte, mObjectiveFunction.back(), tmpCircuit));
	mSystemSolver.push_back(new SystemSolverPCARL(mObjectiveFunction.back(), mVehicleSolver.back(), tmpCircuit, SimulationSetting::D_COMPUTATION_TIME_CAP, mSystemSolver[SolverType::eINIT], isGreedy, isStartingFresh, isBaseCooperative));
	mSystemSolver.back()->GetCurrentSystemSolution()->mDescription = desc;
	mSystemSolver.back()->GetDescription().sSolverType = desc;
	mSystemSolver.back()->GetDescription().sShort = desc;
}
