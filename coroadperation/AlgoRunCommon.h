#pragma once
#include "IAlgoRun.h"

class Carte;
class IMapSolver;
class IScenarioLoader;
class IScenario;

class AlgoRunCommon :
	public IAlgoRun
{
public:
	AlgoRunCommon();
	virtual ~AlgoRunCommon();

	virtual int Run(int argc, const char** argv);
	void endRun();

protected:
	Carte* carte = nullptr;
	IMapSolver* highLevelCarte = nullptr;
	IScenarioLoader* scenarioLoader = nullptr;
	IScenario* scenario = nullptr;
};

