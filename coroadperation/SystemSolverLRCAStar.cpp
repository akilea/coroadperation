#include "SystemSolverLRCAStar.h"

#include "VehicleSolverAbsoluteOptimal.h"
#include "VehicleSolverWait.h"
#include "VehicleSolution.h"
#include "Path.h"
#include "CollisionWithReference.h"
#include "CollisionAnalyser.h"
#include "Vehicle.h"
#include "ActionTerminal.h"
#include <algorithm>

SystemSolverLRCAStar::SystemSolverLRCAStar(IObjectiveFunction* inObjectiveFunction, IVehicleSolver* inVehicleSolver,ISystemSolver* inReferenceSystemSolver, ISystemSolver* inWaitingSystemSolver, IArrangement* inArrangment, ElectricCircuit* inElectricCircuit, double in_dComputationTimeCap)
:SystemSolverArranged(inObjectiveFunction, inVehicleSolver, inArrangment, inElectricCircuit, in_dComputationTimeCap)
,mReferenceSystemSolver(inReferenceSystemSolver)
,mWaitingSystemSolver(inWaitingSystemSolver)
{

}

SystemSolverLRCAStar::~SystemSolverLRCAStar()
{
	ResetSolver();
}

void SystemSolverLRCAStar::SolveSystem()
{
	if (!mIsActivated)
	{
		return;
	}
	mPerformanceAnalysis->StartExperiment();
	ResetSolver();
	//Reset global solution
	mSystemSolution->mSystemPathDistribution.Set0();

	ElectricCircuit* bestElectricCircuit = mSolverData->mElectricCircuit;

	//For each vehicle...
	for (auto itVehicle : mSolverData->mVecVehicle)
	{
		mPerformanceAnalysis->AddPassCount();
		/////////////////////////////////////////////////////////
		//Wait solver needs an updated solution from updated solution
		/////////////////////////////////////////////////////////
		//Waiting solver needs up to date solution and circuit
		mWaitingSystemSolver->ForceCurrentSystemSolution(mSystemSolution->clone()); //give a copy of initial solution
		ElectricCircuit* waitSolverElectricCircuit = bestElectricCircuit->clone(); // give a copy of initial elec circuit
		mWaitingSystemSolver->GetVehicleSolver()->ReplaceRefElectricCircuit(waitSolverElectricCircuit);

		cout << "**************************************************************************" << endl;
		cout << "SystemSolverLRCAStar::SolveSystem - when adding car " << itVehicle->GetName() << endl;
		cout << "**************************************************************************" << endl;
		cout << "SystemSolverLRCAStar::SolveSystem - BEFORE, electric circuit is " << endl << *waitSolverElectricCircuit << endl;
		mWaitingSystemSolver->AddVehicle(itVehicle);
		cout << "SystemSolverLRCAStar::SolveSystem - AFTER, electric circuit is " << endl << *waitSolverElectricCircuit << endl;

		//Give back the solution and electrical circuit
		delete mSystemSolution;
		mSystemSolution = mWaitingSystemSolver->GetCurrentSystemSolution()->clone();
		mSystemSolution->RecomputeVehicleAndSystemDistribution();
		delete bestElectricCircuit;
		bestElectricCircuit = waitSolverElectricCircuit->clone(); // give a copy of initial elec circuit
		mSystemSolution->ReplaceElectricCircuit(bestElectricCircuit);

		/////////////////////////////////////////////////////////
		//Gather solution and cost from waiting and reference solutions
		/////////////////////////////////////////////////////////
		//Reset CA inverted counter
		VehicleSolution* refVehiculeSolution  = mReferenceSystemSolver->GetCurrentSystemSolution()->GetVehiculeSolution(itVehicle);
		VehicleSolution* waitVehiculeSolution = mWaitingSystemSolver->GetCurrentSystemSolution()->GetVehiculeSolution(itVehicle);

		if (!waitVehiculeSolution->HasReachedEnd())
		{
			cout << "SystemSolverLRCAStar::SolveSystem - waitVehiculeSolution is incomplete before strippping it!" << endl;
			assert(false);
		}
		float fCostRef = refVehiculeSolution->mPath->mTotalPathDistribution.Total();
		float fCostWait = waitVehiculeSolution->mPath->mTotalPathDistribution.Total();

		//cout << "SystemSolverLRCAStar::SolveSystem - mSolverData->mElectricCircuit " << endl << *mSolverData->mElectricCircuit << endl;
		//cout << "SystemSolverLRCAStar::SolveSystem - fCostRef=" << fCostRef << endl;
		//cout << "SystemSolverLRCAStar::SolveSystem - fCostWait=" << fCostWait << endl;
		/////////////////////////////////////////////////////////
		//Init and do we need to solve something?
		/////////////////////////////////////////////////////////
		std::vector<AlternativeSearch> vAltSearch;
		std::set<Vehicle*> setAdmissible;
		setAdmissible.insert(itVehicle);
		float fEpsilon = 1.0f;
		//Worth to solve?
		if ((fCostWait - fCostRef) > fEpsilon)
		{
			//cout << "Worth the recomputation" << endl;
			m_nCAInvertedCounter = nAllowedDepth;
			/////////////////////////////////////////////////////////
			//Main solver
			//Solve biggest collision
			//When solve, add admissible vehicle, compute collisions and so on, untill limit is reached
			/////////////////////////////////////////////////////////
			bool bFoundBetterSolution = true;
			while (m_nCAInvertedCounter >= 0 && bFoundBetterSolution)
			{
				bFoundBetterSolution = false;
				cout << "SystemSolverLRCAStar::SolveSystem - m_nCAInvertedCounter" << m_nCAInvertedCounter << endl;
				//Gather collisions with admissible vehicles
				CollisionAnalyser* colAnalyser = new CollisionAnalyser(mSystemSolution, mReferenceSystemSolver->GetCurrentSystemSolution(), bestElectricCircuit);
				colAnalyser->FindAndSortWaiting(setAdmissible, itVehicle->GetDepartureTime(), itVehicle);
				if (colAnalyser->PeakNextCollision() == nullptr)//no collision, next!
				{
					//cout << "SystemSolverLRCAStar::SolveSystem - No collision, next car!" << endl;
					break;
				}

				//For all collision involved (always inside the counter limit)
				AlternativeSearch* curBestSearch = nullptr;
				while (colAnalyser->PeakNextCollision() != nullptr && colAnalyser->PeakNextCollision()->HasCollision() && m_nCAInvertedCounter >= 0)
				{
					SystemSolution* sysClone = mSystemSolution->clone();
					ElectricCircuit* ecClone = bestElectricCircuit->clone();
					sysClone->ReplaceElectricCircuit(ecClone);
					std::set<Vehicle*> setNewSearchAdmissible;
					setNewSearchAdmissible.insert(setAdmissible.begin(), setAdmissible.end());

					Collision* curCol = colAnalyser->PeakNextCollision();

					cout << "SystemSolverLRCAStar::SolveSystem - Nb reservation before Piercing" << ecClone->computeTotalReservation() << endl;
					//CollisionAnalyser::PierceFrom(setNewSearchAdmissible, sysClone, curCol->GetBlockerSolution()->mRefVehicle, curCol->GetBlockerActionTerminal()->getTerminal(), itVehicle->GetDepartureTime());
					int maxCounter = 0;
					for (auto itVe : mSolverData->mVecVehicle)
					{
						cout << "SystemSolverLRCAStar::SolveSystem - Start time - " << itVehicle->GetDepartureTime() << endl;
						CollisionAnalyser::PierceFrom(setNewSearchAdmissible, sysClone, itVe, nullptr, itVehicle->GetDepartureTime());
						if (itVe->GetName().compare(itVehicle->GetName()) == 0 || maxCounter >= 5)
						{
							break;
						}
						maxCounter++;
					}
					cout << "SystemSolverLRCAStar::SolveSystem - Nb reservation after Piercing" << ecClone->computeTotalReservation() << endl;
					cout << "SystemSolverLRCAStar::SolveSystem - setNewSearchAdmissible.size" << setNewSearchAdmissible.size() << endl;

					ElectricCircuit* out_associatedEC = nullptr;
					//Test to see if we have CA*
					//ecClone->clearAllReservation();
					//AlternativeSearch search(new SystemSolution(), nullptr, ecClone);
					//std::set<Vehicle*> fakeAdmissible;
					//for (auto itVe : mSolverData->mVecVehicle)
					//{
					//	fakeAdmissible.insert(itVe);
					//	if (itVe->GetName().compare(itVehicle->GetName()) == 0)
					//	{
					//		break;
					//	}
					//}
					//SystemSolution* betterSolution = SolveCANoOwnership(search, &fakeAdmissible, out_associatedEC);
					//Original below (2 lines)
					AlternativeSearch search(sysClone, nullptr, ecClone);
					SystemSolution* betterSolution = SolveCANoOwnership(search, &setNewSearchAdmissible, out_associatedEC);
					--m_nCAInvertedCounter;

					//Important to delete the collision before it becomes invalid
					//cout << "colAnalyser.PopNextCollision()" << endl;
					colAnalyser->PopNextCollision();
					//cout << "colAnalyser.PopNextCollision() after" << endl;

					if (betterSolution != nullptr && out_associatedEC != nullptr)
					{
						colAnalyser->PopAllCollision();

						//cout << "SystemSolverLRCAStar::SolveSystem - better solution found distribution" << endl;
						//cout << betterSolution->mSystemPathDistribution << endl;
						//cout << "SystemSolverLRCAStar::SolveSystem - old solution distribution" << endl;
						//cout << mSystemSolution->mSystemPathDistribution << endl;

						delete mSystemSolution;
						mSystemSolution = betterSolution;
						delete bestElectricCircuit;
						bestElectricCircuit = out_associatedEC;

						setAdmissible.insert(setNewSearchAdmissible.begin(), setNewSearchAdmissible.end());
						break;

						bFoundBetterSolution = true;
					}
				}

				delete colAnalyser;
				colAnalyser = nullptr;
			}
		}
	}

	//cout << "Final solution:" << endl;
	//cout << *mSystemSolution << endl;
	//cout << "Pointer=" << mSystemSolution << "=?=" << mSystemSolution << endl;

	mPerformanceAnalysis->StopExperiment(mSystemSolution->mSystemPathDistribution);
}

SystemSolution* SystemSolverLRCAStar::SolveCANoOwnership(AlternativeSearch& newSearch, std::set<Vehicle*>* vAdmissibleVehicle, ElectricCircuit*& out_bestElectricCircuit)
{
	//Counter safety
	if (m_nCAInvertedCounter <= 0 || vAdmissibleVehicle->size() ==0)
	{
		return nullptr;
	}


	/////////////////////////////////////////////////////////
	//Init
	/////////////////////////////////////////////////////////
	SystemSolution* bestSolution = nullptr;
	out_bestElectricCircuit = nullptr;
	std::vector<Vehicle*> vAdmissibleVec(vAdmissibleVehicle->begin(), vAdmissibleVehicle->end());

	//cout << "Admissible " << endl;
	//for (auto it : *vAdmissibleVehicle)
	//{
	//	cout << it->GetName() << endl;
	//}

	newSearch.mSystemSolution->RecomputeVehicleAndSystemDistribution();
	//cout << "Working with this solution " << endl;
	//cout << *newSearch.mSystemSolution << endl;


	mArrangement->startSequence(&vAdmissibleVec);
	std::vector<Vehicle*>* curArrangement = mArrangement->GetNextSequence();

	/////////////////////////////////////////////////////////
	//Try arrangment sequences
	/////////////////////////////////////////////////////////
	while (curArrangement != nullptr)
	{
		//Create a new electric circuit from current state
		ElectricCircuit* simulatedCircuit = newSearch.mElectricCircuit->clone();
		mVehicleSolver->ReplaceRefElectricCircuit(simulatedCircuit);
		SystemSolverImmediate* solverImmediate = new SystemSolverImmediate(mObjectiveFunction, mVehicleSolver, simulatedCircuit, INFINITY);
		ScenarioData* dataSolver = solverImmediate->ModifyOptionnalScenarioData();
		dataSolver->SetAssignPartialSolutionEachAdd(false);
		solverImmediate->ForceCurrentSystemSolution(newSearch.mSystemSolution->clone());

		//cout << "SystemSolverLRCAStar::SolveSystem - solverImmediate->GetAssignPartialSolutionEachAdd()=" << solverImmediate->GetAssignPartialSolutionEachAdd() << endl;

		//cout << "Before (partial)" << endl;
		//cout << *dataSolver->mObjOptionnalPartialSolution << endl;
		//cout << "HasReachedEnd" <<dataSolver->mObjOptionnalPartialSolution->HasReachedEnd() << endl;
		
		//Add vehicle sequence order
		//cout << "Sequence size " << vAdmissibleVec.size() << endl;
		for (unsigned int j = 0; j < curArrangement->size(); ++j)
		{
			//cout << "Adding vehicle " << endl;
			//cout << "j=" << j << endl;
			//cout << "Name=" << (*curArrangement)[j]->GetName() << endl;
			solverImmediate->AddVehicle((*curArrangement)[j]);
		}
		solverImmediate->GetCurrentSystemSolution()->RecomputeVehicleAndSystemDistribution();
		//cout << solverImmediate->GetCurrentSystemSolution() << endl;

		//Should not happen
		if (!solverImmediate->GetCurrentSystemSolution()->HasReachedEnd())
		{
			solverImmediate->GetCurrentSystemSolution()->displayCompletionList(cout);
			assert(false);
		}

		//cout << "SystemSolverLRCAStar::SolveCANoOwnership - solverImmediate->GetCurrentSystemSolution() before comparison" << solverImmediate->GetCurrentSystemSolution()->mSystemPathDistribution <<endl;
		//If we have a better solution keep the solver and order!
		if (mObjectiveFunction->IsBetterThan(solverImmediate->GetCurrentSystemSolution(), mSystemSolution))
		{
			if (bestSolution != nullptr)
			{
				delete bestSolution;
				bestSolution = nullptr;
			}
			if (out_bestElectricCircuit != nullptr)
			{
				delete out_bestElectricCircuit;
				out_bestElectricCircuit = nullptr;
			}

			bestSolution = solverImmediate->GetCurrentSystemSolution()->clone();
			out_bestElectricCircuit = solverImmediate->ModifyOptionnalScenarioData()->mElectricCircuit->clone();

			//cout << "SystemSolverLRCAStar::SolveCANoOwnership - found better solution!" << endl;
			//cout << "SystemSolverLRCAStar::SolveCANoOwnership - bestSolution" << endl;
			//cout << *bestSolution << endl;
			//cout << "SystemSolverLRCAStar::SolveCANoOwnership - out_bestElectricCircuit" << endl;
			//cout << *out_bestElectricCircuit << endl;
		}
		else
		{
			//cout << "SystemSolverLRCAStar::SolveCANoOwnership - not a better solution" << endl;
			//Otherwise, delete them
			//This will invalidate AltSearch as well
			delete solverImmediate;
			delete simulatedCircuit;
		}

		if (HasReachdedRunningTimeCap())
		{
			cout << "Early break!" << endl;
			break;
		}
		//Prepare next sequence
		//cout << "SystemSolverLRCAStar::SolveCANoOwnership - Next sequence up!" << endl;
		curArrangement = mArrangement->GetNextSequence();
	};

	//cout << "SystemSolverLRCAStar::SolveCANoOwnership - EXIT" << endl;
	return bestSolution;
}
