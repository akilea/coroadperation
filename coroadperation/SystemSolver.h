#pragma once
#include "ISystemSolver.h"

class ScenarioData;
class IObjectiveFunction;
class IVehicleSolver;
class IArrangement;

class SystemSolver :
	public ISystemSolver
{
protected:
	SystemSolution* mSystemSolution = nullptr;
	IArrangement* mArrangement = nullptr;

	void ResetSolution();

	//These will solve when adding and removing candidates
	virtual void AddVehicleInternal(Vehicle* newVehicle);
	virtual void RemoveVehicleInternal(Vehicle* newVehicle);
	bool HasVehicle(Vehicle* newVehicle) const;

public:
	SystemSolver(IObjectiveFunction* inObjectiveFunction,IVehicleSolver* inVehicleSolver, IArrangement* inArrangement, ElectricCircuit* inElectricCircuit, double in_dComputationTimeCap);
	virtual ~SystemSolver();

	virtual SystemSolution* GetCurrentSystemSolution() const { return mSystemSolution; }
	virtual void ForceCurrentSystemSolution(SystemSolution* forcedSolution);
	size_t GetVehicleCount() const;

};

