#include "IObjectiveFunction.h"

#include "SystemSolution.h"
#include <iostream>

bool IObjectiveFunction::IsBetterThan(SystemSolution const* const in_A, SystemSolution const* const in_B)
{
    float fA = this->Evaluate(in_A);
    float fB = this->Evaluate(in_B);
    if (fA < 0.01f)
    {
        fA = INFINITY;
    }
    if (fB < 0.01f)
    {
        fB = INFINITY;
    }
    bool bIsA = fA < fB;
    //cout << "=======Evaluation=======" << endl;
    //cout << "fA" << fA <<endl;
    //cout << "fB" << fB <<endl;
    //cout << "bIsA" << bIsA <<endl;
    //cout << "=======End evaluation======" << endl;
    return bIsA;
}
