#include "ScenarioData.h"

#include "ElectricCircuit.h"
#include "Vehicle.h"
#include <sstream>


ScenarioData::ScenarioData()
{
}


ScenarioData::~ScenarioData() {
	if (mElectricCircuit != nullptr)
	{
		delete mElectricCircuit;
	}

	for (int i = mVecVehicle.size() - 1; i >= 0; i--)
	{
		delete mVecVehicle[i];
	}
	mVecVehicle.clear();
}

std::string ScenarioData::toString()
{
	std::ostringstream ss;
	for (unsigned int i = 0; i < mVecVehicle.size(); ++i)
	{
		ss << mVecVehicle[i]->toString() << std::endl;
	}
	return ss.str();
}

std::string ScenarioData::toStringInteractive()
{
	std::ostringstream ss;
	for (unsigned int i = 0; i < mVecVehicle.size(); ++i)
	{
		ss << mVecVehicle[i]->toString() << std::endl << std::endl;
	}
	return ss.str();
}

void ScenarioData::SetAndOwnPartialSolution(SystemSolution* partialSolution)
{
	if (partialSolution != nullptr)
	{
		mObjOptionnalPartialSolution = partialSolution;
	}
}

SystemSolution* ScenarioData::GetAndClearPartialSolution() {
	SystemSolution* ret = mObjOptionnalPartialSolution;
	mObjOptionnalPartialSolution = nullptr;
	return ret;
}
