#pragma once
#include "IArrangement.h"

#include <vector>

class Arrangement :
	public IArrangement
{
protected:
	std::vector<Vehicle*>* m_refVector = nullptr;
	std::vector<Vehicle*>* m_currentArrangement = nullptr;

public:
	Arrangement();
	virtual ~Arrangement();

	//Original is untouched
	virtual void startSequence(std::vector<Vehicle*>* in_refVector);
};

