#include "AVLTree.h"

using namespace CRP;

void CRP::SwapNodeReference(Node *& first, Node *& second)
{
	Node* temporary = first; first = second; second = temporary;
}

void CRP::SwapNodes(Node * A, Node * B)
{
	if (B == A->Left)
	{
		if (B->Left) B->Left->Parent = A;
		if (B->Right) B->Right->Parent = A;

		if (A->Right) A->Right->Parent = B;

		if (!A->Parent->IsHeader())
		{
			if (A->Parent->Left == A)
				A->Parent->Left = B;
			else
				A->Parent->Right = B;
		}
		else A->Parent->Parent = B;

		B->Parent = A->Parent;
		A->Parent = B;

		A->Left = B->Left;
		B->Left = A;

		SwapNodeReference(A->Right, B->Right);
	}
	else if (B == A->Right)
	{
		if (B->Right) B->Right->Parent = A;
		if (B->Left) B->Left->Parent = A;

		if (A->Left) A->Left->Parent = B;

		if (!A->Parent->IsHeader())
		{
			if (A->Parent->Left == A)
				A->Parent->Left = B;
			else
				A->Parent->Right = B;
		}
		else A->Parent->Parent = B;

		B->Parent = A->Parent;
		A->Parent = B;

		A->Right = B->Right;
		B->Right = A;

		SwapNodeReference(A->Left, B->Left);
	}
	else if (A == B->Left)
	{
		if (A->Left) A->Left->Parent = B;
		if (A->Right) A->Right->Parent = B;

		if (B->Right) B->Right->Parent = A;

		if (!B->Parent->IsHeader())
		{
			if (B->Parent->Left == B)
				B->Parent->Left = A;
			else
				B->Parent->Right = A;
		}
		else B->Parent->Parent = A;

		A->Parent = B->Parent;
		B->Parent = A;

		B->Left = A->Left;
		A->Left = B;

		SwapNodeReference(A->Right, B->Right);
	}
	else if (A == B->Right)
	{
		if (A->Right) A->Right->Parent = B;
		if (A->Left) A->Left->Parent = B;

		if (B->Left) B->Left->Parent = A;

		if (!B->Parent->IsHeader())
		{
			if (B->Parent->Left == B)
				B->Parent->Left = A;
			else
				B->Parent->Right = A;
		}
		else B->Parent->Parent = A;

		A->Parent = B->Parent;
		B->Parent = A;

		B->Right = A->Right;
		A->Right = B;

		SwapNodeReference(A->Left, B->Left);
	}
	else
	{
		if (A->Parent == B->Parent)
			SwapNodeReference(A->Parent->Left, A->Parent->Right);
		else
		{
			if (!A->Parent->IsHeader())
			{
				if (A->Parent->Left == A)
					A->Parent->Left = B;
				else
					A->Parent->Right = B;
			}
			else A->Parent->Parent = B;

			if (!B->Parent->IsHeader())
			{
				if (B->Parent->Left == B)
					B->Parent->Left = A;
				else
					B->Parent->Right = A;
			}
			else B->Parent->Parent = A;
		}

		if (B->Left)  B->Left->Parent = A;
		if (B->Right) B->Right->Parent = A;

		if (A->Left)  A->Left->Parent = B;
		if (A->Right) A->Right->Parent = B;

		SwapNodeReference(A->Left, B->Left);
		SwapNodeReference(A->Right, B->Right);
		SwapNodeReference(A->Parent, B->Parent);
	}

	unsigned long Balance = A->Balance;
	A->Balance = B->Balance;
	B->Balance = (char)Balance;
}

void CRP::RotateLeft(Node *& Root)
{
	Node* Parent = Root->Parent;
	Node* x = Root->Right;

	Root->Parent = x;
	x->Parent = Parent;
	if (x->Left) x->Left->Parent = Root;

	Root->Right = x->Left;
	x->Left = Root;
	Root = x;
}

void CRP::RotateRight(Node *& Root)
{
	Node* Parent = Root->Parent;
	Node* x = Root->Left;

	Root->Parent = x;
	x->Parent = Parent;
	if (x->Right) x->Right->Parent = Root;

	Root->Left = x->Right;
	x->Right = Root;
	Root = x;
}

void CRP::BalanceLeft(Node *& Root)
{
	Node* Left = Root->Left; // Left Subtree of Root Node

	switch (Left->Balance)
	{
	case State::LeftHigh:
		Root->Balance = State::Balanced;
		Left->Balance = State::Balanced;
		RotateRight(Root);
		break;

	case State::RightHigh:
	{
		Node* subRight = Left->Right;  // Right subtree of Left
		switch (subRight->Balance)
		{
		case State::Balanced:
			Root->Balance = State::Balanced;
			Left->Balance = State::Balanced;
			break;

		case State::RightHigh:
			Root->Balance = State::Balanced;
			Left->Balance = State::LeftHigh;
			break;

		case State::LeftHigh:
			Root->Balance = State::RightHigh;
			Left->Balance = State::Balanced;
			break;
		}
		subRight->Balance = State::Balanced;
		RotateLeft(Left);
		Root->Left = Left;
		RotateRight(Root);
	}
	break;

	case State::Balanced:
		Root->Balance = State::LeftHigh;
		Left->Balance = State::RightHigh;
		RotateRight(Root);
		break;
	}
}

void CRP::BalanceRight(Node *& Root)
{
	Node* Right = Root->Right; // Right Subtree of Root Node

	switch (Right->Balance)
	{
	case State::RightHigh:
		Root->Balance = State::Balanced;
		Right->Balance = State::Balanced;
		RotateLeft(Root);
		break;

	case State::LeftHigh:
	{
		Node* subLeft = Right->Left; // Left Subtree of Right
		switch (subLeft->Balance)
		{
		case State::Balanced:
			Root->Balance = State::Balanced;
			Right->Balance = State::Balanced;
			break;

		case State::LeftHigh:
			Root->Balance = State::Balanced;
			Right->Balance = State::RightHigh;
			break;

		case State::RightHigh:
			Root->Balance = State::LeftHigh;
			Right->Balance = State::Balanced;
			break;
		}
		subLeft->Balance = State::Balanced;
		RotateRight(Right);
		Root->Right = Right;
		RotateLeft(Root);
	}
	break;

	case State::Balanced:
		Root->Balance = State::RightHigh;
		Right->Balance = State::LeftHigh;
		RotateLeft(Root);
		break;
	}
}

void CRP::BalanceTree(Node * Root, unsigned long From)
{
	bool Taller = true;

	while (Taller)
	{
		Node* Parent = Root->Parent;
		unsigned long NextFrom = (Parent->Left == Root) ? Direction::FromLeft : Direction::FromRight;

		if (From == Direction::FromLeft)
		{
			switch (Root->Balance)
			{
			case State::LeftHigh:
				if (Parent->IsHeader())
					BalanceLeft(Parent->Parent);
				else if (Parent->Left == Root)
					BalanceLeft(Parent->Left);
				else
					BalanceLeft(Parent->Right);
				Taller = false;
				break;

			case State::Balanced:
				Root->Balance = State::LeftHigh;
				Taller = true;
				break;

			case State::RightHigh:
				Root->Balance = State::Balanced;
				Taller = false;
				break;
			}
		}
		else
		{
			switch (Root->Balance)
			{
			case State::LeftHigh:
				Root->Balance = State::Balanced;
				Taller = false;
				break;

			case State::Balanced:
				Root->Balance = State::RightHigh;
				Taller = true;
				break;

			case State::RightHigh:
				if (Parent->IsHeader())
					BalanceRight(Parent->Parent);
				else if (Parent->Left == Root)
					BalanceRight(Parent->Left);
				else
					BalanceRight(Parent->Right);
				Taller = false;
				break;
			}
		}

		if (Taller) // skip up a level
		{
			if (Parent->IsHeader())
				Taller = false;
			else
			{
				Root = Parent;
				From = NextFrom;
			}
		}
	}
}

void CRP::BalanceTreeRemove(Node * Root, unsigned long From)
{
	if (Root->IsHeader()) return;
	bool Shorter = true;

	while (Shorter)
	{
		Node* Parent = Root->Parent;
		unsigned long NextFrom = (Parent->Left == Root) ? Direction::FromLeft : Direction::FromRight;

		if (From == Direction::FromLeft)
		{
			switch (Root->Balance)
			{
			case State::LeftHigh:
				Root->Balance = State::Balanced;
				Shorter = true;
				break;

			case State::Balanced:
				Root->Balance = State::RightHigh;
				Shorter = false;
				break;

			case State::RightHigh:
				if (Root->Right->Balance == State::Balanced)
					Shorter = false;
				else
					Shorter = true;
				if (Parent->IsHeader())
					BalanceRight(Parent->Parent);
				else if (Parent->Left == Root)
					BalanceRight(Parent->Left);
				else
					BalanceRight(Parent->Right);
				break;
			}
		}
		else
		{
			switch (Root->Balance)
			{
			case State::RightHigh:
				Root->Balance = State::Balanced;
				Shorter = true;
				break;

			case State::Balanced:
				Root->Balance = State::LeftHigh;
				Shorter = false;
				break;

			case State::LeftHigh:
				if (Root->Left->Balance == State::Balanced)
					Shorter = false;
				else
					Shorter = true;
				if (Parent->IsHeader())
					BalanceLeft(Parent->Parent);
				else if (Parent->Left == Root)
					BalanceLeft(Parent->Left);
				else
					BalanceLeft(Parent->Right);
				break;
			}
		}

		if (Shorter)
		{
			if (Parent->IsHeader())
				Shorter = false;
			else
			{
				From = NextFrom;
				Root = Parent;
			}
		}
	}
}

Node * CRP::PreviousItem(Node * node)
{
	if (node->IsHeader()) { return node->Right; }

	else if (node->Left != 0)
	{
		Node* y = node->Left;
		while (y->Right != 0) y = y->Right;
		node = y;
	}
	else
	{
		Node* y = node->Parent;
		if (y->IsHeader()) return y;
		while (node == y->Left) { node = y; y = y->Parent; }
		node = y;
	}
	return node;
}

Node * CRP::NextItem(Node * node)
{
	if (node->IsHeader()) return node->Left;

	if (node->Right != 0)
	{
		node = node->Right;
		while (node->Left != 0) node = node->Left;
	}
	else
	{
		Node* y = node->Parent;
		if (y->IsHeader()) return y;
		while (node == y->Right) { node = y; y = y->Parent; }
		node = y;
	}
	return node;
}

Node * CRP::Minimum(Node * node)
{
	while (node->Left) node = node->Left;
	return node;
}

Node * CRP::Maximum(Node * node)
{
	while (node->Right) node = node->Right;
	return node;
}

void CRP::AdjustAdd(Node * Root)
{
	Node* Header = Root->Parent;
	while (!Header->IsHeader()) Header = Header->Parent;

	if (Root->Parent->Left == Root)
	{
		BalanceTree(Root->Parent, Direction::FromLeft);
		if (Header->Left == Root->Parent) Header->Left = Root;
	}
	else
	{
		BalanceTree(Root->Parent, Direction::FromRight);
		if (Header->Right == Root->Parent) Header->Right = Root;
	}
}

void CRP::AdjustRemove(Node * Parent, unsigned long Direction)
{
	BalanceTreeRemove(Parent, Direction);

	Node* Header = Parent;
	while (!Header->IsHeader()) Header = Header->Parent;

	if (Header->Parent == 0)
	{
		Header->Left = Header;
		Header->Right = Header;
	}
	else
	{
		Header->Left = Minimum(Header->Parent);
		Header->Right = Maximum(Header->Parent);
	}
}

unsigned long CRP::Depth(const Node * root)
{
	if (root)
	{
		unsigned long left = root->Left ? Depth(root->Left) : 0;
		unsigned long right = root->Right ? Depth(root->Right) : 0;
		return left < right ? right + 1 : left + 1;
	}
	else
		return 0;
}

unsigned long CRP::Count(const Node * root)
{
	if (root)
	{
		unsigned long left = root->Left ? Count(root->Left) : 0;
		unsigned long right = root->Right ? Count(root->Right) : 0;
		return left + right + 1;
	}
	else
		return 0;
}
