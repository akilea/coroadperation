#pragma once
#include "IAlgoRun.h"

class AlgoRunTestHighLevelCarte :
	public IAlgoRun
{
public:
	AlgoRunTestHighLevelCarte();
	virtual ~AlgoRunTestHighLevelCarte();

	virtual int Run(int argc, const char** argv);
};

