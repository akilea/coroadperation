#include "SystemSolution.h"

#include "VehicleSolution.h"
#include "Vehicle.h"
#include "Path.h"
#include <algorithm>
#include <assert.h>


void SystemSolution::AddOrReplaceSolution(VehicleSolution * newSolution)
{
	auto itSolutionVehicle = mMapVehicleSolution.find(newSolution->mRefVehicle);
	//Existing solution: replace
	if (itSolutionVehicle != mMapVehicleSolution.end())
	{
		//Modify only if different pointer
		if (itSolutionVehicle->second != newSolution)
		{
			mSystemPathDistribution -= itSolutionVehicle->second->mPath->mTotalPathDistribution;
			cout << "SystemSolution::AddOrReplaceSolution - Replacing" << endl;
			cout << "SystemSolution::AddOrReplaceSolution - OldPtr" << itSolutionVehicle->second << endl;
			cout << "SystemSolution::AddOrReplaceSolution - NewPtr" << newSolution << endl;
			delete itSolutionVehicle->second;
			//Update map
			itSolutionVehicle->second = newSolution;
			mSystemPathDistribution += newSolution->mPath->mTotalPathDistribution;
		}
		else
		{
			//Simply update the system solution
			RecomputeSystemDistribution();
		}	
	}
	else //new solution
	{
		//Create a new entry
		mMapVehicleSolution.insert({ newSolution->mRefVehicle,newSolution });
		mSystemPathDistribution += newSolution->mPath->mTotalPathDistribution;
	}
	RecomputeSystemDistribution();
}

void SystemSolution::RemoveSolution(VehicleSolution* newSolution)
{
	auto itSolutionVehicle = mMapVehicleSolution.find(newSolution->mRefVehicle);
	//Replace the solution
	if (itSolutionVehicle != mMapVehicleSolution.end())
	{
		mMapVehicleSolution.erase(itSolutionVehicle);
	}
	RecomputeSystemDistribution();
}

VehicleSolution* SystemSolution::GetVehiculeSolution(Vehicle* veh) const
{
	//cout << veh << endl;
	//cout << *veh << endl;
	//cout << *mMapVehicleSolution.begin()->first << endl;
	//cout << *mMapVehicleSolution.begin()->second << endl;
	return mMapVehicleSolution.at(veh);
}

bool SystemSolution::HasVehiculeSolution(Vehicle* veh) const
{
	return mMapVehicleSolution.count(veh) != 0;
}

size_t SystemSolution::NbVehiculeSolution()const {
	
	return mMapVehicleSolution.size();
}

bool SystemSolution::IsImpossible() const
{
	bool impossible = mSystemPathDistribution.Total() > 1e100;
	if (!impossible)
	{
		for each(auto var in mMapVehicleSolution)
		{
			if (var.second->IsImpossible())
			{
				impossible = true;
				break;
			}
		}
	}
	return impossible;
}

bool SystemSolution::HasReachedEnd() const
{
	bool isComplete = true;
	for (auto it : mMapVehicleSolution)
	{
		isComplete = isComplete && it.second->HasReachedEnd();
	}
	return isComplete;
}

void SystemSolution::RecomputeSystemDistribution()
{
	mSystemPathDistribution.Set0();
	for (auto it : mMapVehicleSolution)
	{
		mSystemPathDistribution += it.second->mPath->mTotalPathDistribution;
	}
}

void SystemSolution::RecomputeVehicleAndSystemDistribution()
{
	mSystemPathDistribution.Set0();
	for (auto it : mMapVehicleSolution)
	{
		//cout << "SystemSolution::RecomputeVehicleAndSystemDistribution - it.first->GetName()" << it.first->GetName()<< endl;
		it.second->mPath->recomputeProportion();
		mSystemPathDistribution += it.second->mPath->mTotalPathDistribution;
	}
}

ostream& SystemSolution::displayCompletionList(ostream& os) const
{
	os << "Completion status for each vehicle solution" << endl;
	for (auto it : mMapVehicleSolution)
	{
		os << it.first->GetName() << "=" << it.second->HasReachedEnd() << endl;
	}
	return os;
}

float SystemSolution::ComputePenalityFor(Vehicle* v, PathDistribution pd) const
{
	float penality = 0.f;

	if (v != nullptr)
	{
		penality = pd/mMapVehicleSolution.at(v)->mPath->mTotalPathDistribution;
	}
	else
	{
		assert(false);
	}

	return penality;
}

SystemSolution::SystemSolution()
	:mSystemPathDistribution(INFINITY,INFINITY,INFINITY)
{
}

SystemSolution::SystemSolution(const SystemSolution& old_obj)
{
	*this = old_obj;
}


SystemSolution::~SystemSolution()
{
	for each (auto var in mMapVehicleSolution)
	{
		if (var.second != nullptr)
		{
			delete var.second;
			var.second = nullptr;
		}
	}

	mMapVehicleSolution.clear();
}

SystemSolution& SystemSolution::operator=(const SystemSolution & other)
{
	//Delete
	for each (auto var in mMapVehicleSolution)
	{
		delete var.second;
	}

	mMapVehicleSolution.clear();

	//Copy everything else
	this->mDescription = other.mDescription;
	this->mSystemPathDistribution = other.mSystemPathDistribution;
	for each(auto var in other.mMapVehicleSolution)
	{
		this->AddOrReplaceSolution(var.second->clone());
	}
	return *this;
}

SystemSolution * SystemSolution::clone() const
{
	return new SystemSolution(*this);
}

void SystemSolution::ReplaceElectricCircuit(ElectricCircuit* inEC)
{
	for (auto it = mMapVehicleSolution.begin(); it != mMapVehicleSolution.end(); ++it)
	{
		it->second->ReplaceElectricCircuit(inEC);
	}
}

ostream & operator<<(ostream & os, const SystemSolution & dt)
{
	os << "----------------------------------------------------" << endl;
	os << "-------------------System solution------------------" << endl;
	os << "----------------------------------------------------" << endl;
	os << dt.mDescription << endl;
	os << "Total objective function: " << dt.mSystemPathDistribution.Total() << endl;
	
	//Traverse in reverse order

	for (auto it = dt.mMapVehicleSolution.begin(); it != dt.mMapVehicleSolution.end(); ++it)
	{
		os << *it->first << *it->second << endl;
		os << "******" << endl << endl;
	}
	return os;
}
