#include "AlgoRunCommon.h"

#include "carte.h"
#include "HighLevelCarte.h"
#include "VehicleSolution.h"
#include "SystemSolution.h"
#include "ScenarioLoader.h"
#include "Scenario.h"

#include <iostream>
#include <fstream>

AlgoRunCommon::AlgoRunCommon()
{
}

AlgoRunCommon::~AlgoRunCommon()
{
}

int AlgoRunCommon::Run(int argc, const char** argv)
{
	bool bActivationState[Scenario::eMAX_SOLVERTYPE];// = { true,true,true,true,true,true,true };
	bActivationState[Scenario::eINIT]						= true;
	bActivationState[Scenario::eNO_COOP]					= true;
	bActivationState[Scenario::eNO_COOP_WAIT]				= false;
	bActivationState[Scenario::eCOOP_NO_OPTIMIZATION]		= true;
	bActivationState[Scenario::eHCA_FI]						= false;
	bActivationState[Scenario::eHCA_CASCADE]				= false;
	bActivationState[Scenario::eHCA_PERMUTATION]			= false;
	bActivationState[Scenario::eWHCA_SMALL_FI]				= false;
	bActivationState[Scenario::eWHCA_MEDIUM_FI]				= false;
	bActivationState[Scenario::eWHCA_LARGE_FI]				= false;
	bActivationState[Scenario::eWHCA_SMALL_CASCADE]			= false;
	bActivationState[Scenario::eWHCA_MEDIUM_CASCADE]		= false;
	bActivationState[Scenario::eWHCA_LARGE_CASCADE]			= false;
	bActivationState[Scenario::eWHCA_SMALL_PERMUTATION]		= false;
	bActivationState[Scenario::eWHCA_MEDIUM_PERMUTATION]	= false;
	bActivationState[Scenario::eWHCA_LARGE_PERMUTATION]		= false;

	enum TOption
	{
		eCARTE = 0,
		eBORNE,
		eBORNECACHE,
		eSCENARIO,
		eRESULT,
		eDETAIL,
		eMODE,
		eNBBORNEMAX,

		eMAX
	};

	//Default args
	string strOption[] = {
		"quebec-carte.txt"
		,"quebec-bornes.txt"
		,"distanceBorne.cache"
		,"Scenario.txt"
		,"result.csv"
		,"details.log"
		,"interactive"
		,"37"
	};

	//Chargement des arguments
	for (int i = 1; i < argc; ++i)
	{
		strOption[i - 1] = argv[i];
	}

	carte = new Carte(strOption[eCARTE], strOption[eBORNE], stoi(strOption[eNBBORNEMAX]));
	highLevelCarte = new HighLevelCarte(strOption[eBORNECACHE], carte);
	scenarioLoader = new ScenarioLoader(strOption[eSCENARIO], *carte);
	scenario = new Scenario(scenarioLoader, *highLevelCarte);
	scenario->SetActivationAll(false);

	return 0;
}

void AlgoRunCommon::endRun()
{
	delete carte;
	delete highLevelCarte;
	delete scenarioLoader;
	delete scenario;
}
