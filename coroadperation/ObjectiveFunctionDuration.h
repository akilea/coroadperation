#pragma once
#include "IObjectiveFunction.h"
class ObjectiveFunctionDuration :
	public IObjectiveFunction
{
public:
	ObjectiveFunctionDuration();
	virtual ~ObjectiveFunctionDuration();
	virtual float Evaluate(SystemSolution const * const in_A);
	virtual std::string Description() const { return  "DureeMoyenne"; }
};

