#include "VehicleSolution.h"

#include <string>
#include <sstream>
#include <iomanip>
#include <assert.h>
#include "Vehicle.h"
#include "Path.h"
#include "ElectricReservation.h"
#include "ActionTerminal.h"
#include "ElectricTerminal.h"

using namespace std;

VehicleSolution::VehicleSolution(Vehicle* inRefVehicle)
	:mRefVehicle(inRefVehicle)
{
	mPath = new Path();
	mPath->mTotalPathDistribution.Set(INFINITY, INFINITY, INFINITY);
}

VehicleSolution::VehicleSolution(const VehicleSolution & old_obj)
:mRefVehicle(old_obj.mRefVehicle)
, cst_no_path(old_obj.cst_no_path)
{
	this->mPath = new Path(*old_obj.mPath);
}


VehicleSolution::~VehicleSolution()
{
	if (mPath != nullptr)
	{
		delete mPath;
	}
}

VehicleSolution & VehicleSolution::operator=(const VehicleSolution & other)
{
	if (mPath != nullptr)
	{
		delete mPath;
	}

	//TODO delete stuff so we avoid leaks

	this->mPath = new Path(*other.mPath);
	//Exception when copying another solution
	this->mRefVehicle = other.mRefVehicle;
	return *this;
}

VehicleSolution * VehicleSolution::clone() const
{
	return new VehicleSolution(*this);
}


string VehicleSolution::ComputeTimeRoundedSecondInteractive() const
{
	//TODO: not clean, should change visionneuse
	std::ostringstream ss;
	if (!IsImpossible())
		ss << (TimeUnit)round(mPath->getDuration()) << " s";
	else
		ss << cst_no_path << endl;

	return ss.str();
}

string VehicleSolution::toString() const
{
	std::ostringstream ss;
	ss << setw(20) << left << "Cost:" << mPath->mTotalPathDistribution.Total() << endl;
	ss << setw(20) << left << "Wait:" << mPath->mTotalPathDistribution.mWaitTime << endl;
	ss << setw(20) << left << "Charge:" << mPath->mTotalPathDistribution.mChargeTime << endl;
	ss << setw(20) << left << "Road:" << mPath->mTotalPathDistribution.mRoadTime << endl;
	ss << setw(20) << left << "Distance:" << mPath->mDistance << endl;
	ss << setw(20) << left << "Duration: " << ComputeTimeRoundedSecondInteractive() << endl <<"---PATH---"<< endl << mPath->toString();
	return ss.str();
}

string VehicleSolution::toStringInteractive() const
{
	std::ostringstream ss;
	string duration = ComputeTimeRoundedSecondInteractive();
	ss << duration << endl;
	if (duration.compare(cst_no_path) != 0)
	{
		//Did we have to wait at any station? If so, compute if an alternate path is advantageous by ignoring the station we needed to wait
		for each (auto var in mPath->mSequenceAction)
		{
			if (var->getType() == Action::eRoad)
			{
				ss << var->getPosition() << "   ";
			}
		}
		ss << endl;
		for each (auto var in mPath->mSequenceAction)
		{
			if (var->getType() == Action::eTerminal)
			{
				ActionTerminal* acTerminal = dynamic_cast<ActionTerminal*>(var);
				ss << acTerminal->getTerminal()->GetName() << "   ";
			}
		}
		ss << endl;
	}
	else
	{
		ss << endl << endl;
	}

	return ss.str();
}

ostream& VehicleSolution::displayTrajectoryDetail(ostream& os) const
{
	os << "===Trajectory detail===" << endl;
	os << "car name=" << mRefVehicle->GetName() << endl;
	for each (auto var in mPath->mSequenceAction)
	{
		if (var->getType() == Action::eRoad)
		{
			os << "Road      ";
		}
		else if (var->getType() == Action::eTerminal)
		{
			os << "Terminal  ";
		}
		else
		{
			os << "Wait      ";
		}
		os << var->getPosition() << "   " << var->getName() << endl;;
	}
	os << endl;
	return os;
}


void VehicleSolution::ReplaceElectricCircuit(ElectricCircuit* inEC)
{
	mPath->ReplaceElectricCircuit(inEC);
}

bool VehicleSolution::HasReachedEnd() const
{
	bool isEnd = false;
	//cout << "VehicleSolution::HasReachedEnd - checking car" << mRefVehicle->GetName() << endl;
	//cout << "VehicleSolution::HasReachedEnd - !mPath->mSequenceAction.empty()" << !mPath->mSequenceAction.empty() << endl;
	if (!mPath->mSequenceAction.empty())
	{
		Action* lastAct = mPath->mSequenceAction.back();
		//cout << "lastAct" << lastAct << endl;
		//cout << "VehicleSolution::HasReachedEnd" << endl;
		//cout << "Action  name="<<lastAct->getName() << endl;
		//cout << "Setting name="<< SimulationSetting::STR_LAST_ACTION_NAME << endl << endl;
		isEnd = lastAct->getName().compare(SimulationSetting::STR_LAST_ACTION_NAME) == 0;
	}
	return isEnd;
}

bool VehicleSolution::IsImpossible() const { return mPath->mDistance > 1e100 || mPath->mTotalPathDistribution.Total() > 1e100; }

size_t VehicleSolution::actionCount() const
{
	return mPath->mSequenceAction.size();
}

ostream & operator<<(ostream & os, const VehicleSolution & dt)
{
	os << dt.toString();
	return os;
}
