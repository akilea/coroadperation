#pragma once
#include <string>
#include "srcMap/point.h"
#include "srcMap/StructureCarte.h"

using namespace std;
class ElectricCircuit;
class ElectricTerminal;

class Action
{

protected:
	ElectricCircuit* mRefCircuit = nullptr;
	int mIndexTerminal = -1;


	Action* CopyActionDataTo(Action* inClonedAction) const;
public:

	enum TAction {
		eUndefined = -1,
		eRoad = 0,
		eTerminal = 1,
		eWait = 2,

		eMax
	};

	static const string enumAssociation[eMax];

	Point mPosition;
	string mName = "No name";
	int mDuration = 0;
	int mStartTime = 0;

	Action(ElectricCircuit* refElectricCircuit, ElectricTerminal* refTerminal);
	virtual ~Action();

	virtual Action* clone() const = 0;
	virtual string toString() const;

	inline virtual string getName() const { return mName; }
	virtual TAction getType() const = 0;
	TimeRange getTimeRange() const { return TimeRange(TimeUnit(mStartTime),TimeUnit(mStartTime+mDuration)); }
	virtual Point getPosition() const { return mPosition; }
	virtual float getDuration() const { return mDuration; }
	virtual float getStartTime() const { return mStartTime; }
	virtual ElectricTerminal* const getTerminal() const;

	void ReplaceElectricCircuit(ElectricCircuit* inEC);
	ElectricCircuit* GetElectricCircuit() const;

};

