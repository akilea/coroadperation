#pragma once
#include "Arrangement.h"
class ArrangementPermutation :
	public Arrangement
{
protected:
	bool mIsFirstSequence = false;
public:
	ArrangementPermutation();
	virtual ~ArrangementPermutation();

	virtual void startSequence(std::vector<Vehicle*>* in_refVector);
	virtual std::vector<Vehicle*>* GetNextSequence();
};

