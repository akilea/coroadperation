#include "ObjectiveFunctionSquaredDifference.h"

#include "Vehicle.h"
#include "VehicleSolution.h"
#include "SystemSolution.h"
#include "Path.h"
#include <assert.h>
#include <cmath>


ObjectiveFunctionSquaredDifference::ObjectiveFunctionSquaredDifference(SystemSolution const* const optimalSolution)
:mOptimalSolution(optimalSolution)
{

}


ObjectiveFunctionSquaredDifference::~ObjectiveFunctionSquaredDifference()
{
}

float ObjectiveFunctionSquaredDifference::Evaluate(SystemSolution const* const in_A)
{
	float fSquaredDifferenceTotal = 0.0f;
	float fAverage = in_A->mSystemPathDistribution.Total()/in_A->NbVehiculeSolution();
	float fNbSolution = 0.0f;
	for (auto it = in_A->GetBeginIterator(); it != in_A->GetEndIterator(); ++it)
	{
		float fCurrent = it->second->mPath->getDuration() - fAverage;
		fSquaredDifferenceTotal += (fCurrent * fCurrent);
		fNbSolution += 1.0f;
	}
	return sqrt(fSquaredDifferenceTotal)/fNbSolution;
}