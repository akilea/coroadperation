#pragma once
#include "IObjectiveFunction.h"

class SystemSolution;
class VehicleSolution;


class ObjectiveFunctionSquaredDifference :
	public IObjectiveFunction
{
private:
	SystemSolution const* const mOptimalSolution;
public:
	ObjectiveFunctionSquaredDifference(SystemSolution const* const optimalSolution);
	virtual ~ObjectiveFunctionSquaredDifference();
	virtual std::string Description() const { return  "RacDiffCarDuree"; }
	virtual float Evaluate(SystemSolution const* const in_A);
};

