#include "AlgoRunTestCutter.h"

#include "Scenario.h"
#include "ISystemSolver.h"
#include "SystemSolution.h"
#include "VehicleSolution.h"
#include "Path.h"
#include "PathDistribution.h"
#include "CollisionWithReference.h"
#include "CollisionAnalyser.h"
#include "ScenarioData.h"
#include "SolutionCutter.h"


#include <iostream>
#include <iomanip>

using namespace std;

int AlgoRunTestCutter::Run(int argc, const char** argv)
{

	AlgoRunCommon::Run(argc, argv);

	scenario->SetActivation(Scenario::eINIT, true);
	scenario->SetActivation(Scenario::eNO_COOP, true);
	scenario->SetActivation(Scenario::eNO_COOP_WAIT, true);
	scenario->SetActivation(Scenario::eCOOP_NO_OPTIMIZATION, true);
	scenario->SetActivation(Scenario::eHCA_FI, true);

	scenario->SolveAllVehicle();
	scenario->SolveAllSystem();


	TestSolverSingleVechicle(Scenario::eCOOP_NO_OPTIMIZATION);
	TestSolverSingleVechicle(Scenario::eCOOP_NO_OPTIMIZATION);
	//TestSolverDuplicateVechicle(Scenario::eCOOP_NO_OPTIMIZATION);


	cout << "ENDED WITHOUT CRASH" << endl;

	AlgoRunCommon::endRun();
	return 0;
}

void AlgoRunTestCutter::TestSolverSingleVechicle(Scenario::SolverType type)
{
	//*************Cut and rebuild solution**********
	cout << "Full solution before cutting" << endl;
	cout << *scenario->GetSystemSolver(type)->GetCurrentSystemSolution();

	//OK
	//TestCutAndRebuild(-1.0f, "eNO_COOP_WAIT from start",false,0);

	//BUG: last charging and waiting action duplicate
	//FEATURE: Needed to clean the rebuild sequence. Also, all action have terminal
	//OK
	//TestCutAndRebuild(1000.f, "eNO_COOP_WAIT from 1000",false,0);

	//BUG: Distribution pas a jour
	//BUG: ou est le wait? (gros hasard car ce vehicle n'a pas de wait)
	//OK
	//TestCutAndRebuild(2500.f, "eNO_COOP_WAIT from 2500",false,0);

	//Le vehicle 3 a du waiting
	//OK
	TestCutAndRebuild(2500.f, "from 2500",false,8,type);

	//Clonage de solution du vehicle. Est-ce que la solution systeme est ok?
	//OK
	//TestCutAndRebuild(2500.f, "eNO_COOP_WAIT from 2500",true,2);



	cout << "Full solution after rebuild" << endl;
	cout << *scenario->GetSystemSolver(type)->GetCurrentSystemSolution();
}

void AlgoRunTestCutter::TestCutAndRebuild(float timeLimit, string title, bool cloneSolution, int indexVehicle, Scenario::SolverType type)
{
	//Before and afeter a cut
	SystemSolution* sysSolNCW = scenario->GetSystemSolver(type)->GetCurrentSystemSolution();
	//Working with first vehicle
	Vehicle* vec = scenario->GetRefData()->mVecVehicle[indexVehicle];
	VehicleSolution* solOriginal = cloneSolution? sysSolNCW->GetVehiculeSolution(vec)->clone(): sysSolNCW->GetVehiculeSolution(vec);
	cout << "****************** "<< title <<" ******************" << endl;
	solOriginal->displayTrajectoryDetail(cout) << endl;
	SolutionCutter solCutter;
	//solCutter.cutFromPreviousCommitment(solOriginal, timeLimit);
	cout << "****Cutted solution****" << endl;
	solOriginal->displayTrajectoryDetail(cout) << endl;
	cout << "****Rebuilt solution****" << endl;
	//We add existing vehicle here
	scenario->GetSystemSolver(type)->AddVehicle(vec);
	VehicleSolution* solRebuit = sysSolNCW->GetVehiculeSolution(vec);
	cout << "****Rebuilt solution full timing****" << endl;
	solRebuit->displayTrajectoryDetail(cout) << endl;
	cout << solRebuit << endl;
	cout << "****Pointers: original vs rebuilt****" << endl;
	cout << "Orig=   " << solOriginal << endl;
	cout << "Rebuilt=" << solRebuit << endl;
	cout << "Original still valid?" << *solOriginal << endl;
	cout << "******************END "<< title <<" ******************" << endl << endl << endl;
}

void AlgoRunTestCutter::TestSolverDuplicateVechicle(Scenario::SolverType type)
{
	//*************Cut and rebuild solution**********
	cout << "Full solution before duplicate" << endl;
	cout << *scenario->GetSystemSolver(type)->GetCurrentSystemSolution();


	//TEST
	TestDuplicateInsertion(2500.f, "from 2500", false, 8, type);

	cout << "Full solution after duplicate" << endl;
	cout << *scenario->GetSystemSolver(type)->GetCurrentSystemSolution();
}

void AlgoRunTestCutter::TestDuplicateInsertion(float timeLimit, string title, bool cloneSolution, int indexVehicle, Scenario::SolverType type)
{
	//Before and afeter a cut
	SystemSolution* sysSolNCW = scenario->GetSystemSolver(type)->GetCurrentSystemSolution();
	//Working with first vehicle
	Vehicle* vec = scenario->GetRefData()->mVecVehicle[indexVehicle];
	VehicleSolution* solOriginal = cloneSolution ? sysSolNCW->GetVehiculeSolution(vec)->clone() : sysSolNCW->GetVehiculeSolution(vec);
	cout << "****************** " << title << " ******************" << endl;
	solOriginal->displayTrajectoryDetail(cout) << endl;
	//We add existing vehicle here
	scenario->GetSystemSolver(type)->AddVehicle(vec);
	VehicleSolution* solRebuit = sysSolNCW->GetVehiculeSolution(vec);
	cout << "****Duplicated solution****" << endl;
	solRebuit->displayTrajectoryDetail(cout) << endl;
	cout << solRebuit << endl;
	cout << "****Pointers: original vs rebuilt****" << endl;
	cout << "Orig     =" << solOriginal << endl;
	cout << "Duplicate=" << solRebuit << endl;
	cout << "Original still valid?" << *solOriginal << endl;
	cout << "******************END " << title << " ******************" << endl << endl << endl;
}
