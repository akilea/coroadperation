#include "ActionWait.h"
#include "ElectricTerminal.h"
#include "ElectricCircuit.h"

ActionWait::ActionWait(ElectricCircuit* const inRefCircuit, ElectricTerminal* const inTerminal)
:Action(inRefCircuit, inTerminal)
{
}

ActionWait::~ActionWait()
{
}

Action * ActionWait::clone() const
{
	return CopyActionDataTo(new ActionWait(mRefCircuit,this->getTerminal()));
}
