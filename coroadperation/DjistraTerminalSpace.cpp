#include "DjistraTerminalSpace.h"

#include <map>
#include <algorithm>
#include <vector>
#include "Path.h"
#include "srcMap/carte.h"
#include "RequestCarte.h"
#include "ElectricCircuit.h"
#include "ElectricTerminal.h"
#include "Vehicle.h"
#include "Action.h"
#include "ActionWait.h"
#include "ActionRoad.h"
#include "ActionTerminal.h"



DjistraTerminalSpace::DjistraTerminalSpace()
{
}


DjistraTerminalSpace::~DjistraTerminalSpace()
{
}

void DjistraTerminalSpace::PreComputePaths(int source, float fstartTime,int destination,const PathCache & in_refPathCache, RequestCarte & inRequestCarte, const Carte* in_Carte, PathDistribution pathD)
{
	DijkstraComputePaths(source, fstartTime,destination,in_refPathCache,inRequestCarte, in_Carte,pathD);
}

double DjistraTerminalSpace::BeleivedCostTo()
{
	double c = INFINITY;
	if (m_VertexDestination != -1)
	{
		//c = mTrueGScore[m_VertexDestination];
		c = mGScore[m_VertexDestination];
	}

	return c;
}

double DjistraTerminalSpace::TrueCostTo()
{
	return DistributionTo().Total();
}

PathDistribution DjistraTerminalSpace::DistributionTo()
{
	return total_path_distribution_to_destination;
}

bool DjistraTerminalSpace::HasPath(int to)
{
	return mCameFrom[to] != -1;
}

void DjistraTerminalSpace::ActionSequenceTo(Path& out_Path, const RequestCarte & inRequestCarte, const Carte* in_Carte,float fDepartureTime, PathDistribution pathD)
{
	int startVertex = m_VertexSource;
	int endVertex = m_VertexDestination;
	int vertex_to = endVertex;
	bool bHasPartialSolution = out_Path.HasPartialSolution();
	std::list<Action*> listBuildingSequence;

	float epsilon = 0.1f;
	const float departureTime = fDepartureTime;
	if (vertex_to != -1 && mCameFrom[vertex_to] != -1)
	{
		total_path_distribution_to_destination = pathD;
		float currentTime = departureTime + mGScore[vertex_to];
		for (; vertex_to != -1; vertex_to = mCameFrom[vertex_to])
		{
			//This time is the end of the next action
			bool _debugVerbose = false;
			ElectricTerminal* ptrTerminal = inRequestCarte.IndexTerminalToTerminal(vertex_to);
			//Have a charge time not already in solution
			if (path_distribution[vertex_to].mChargeTime > epsilon && ptrTerminal  != nullptr &&!out_Path.HasActionAtTerminal(Action::TAction::eTerminal,ptrTerminal))
			{
				//Create charge time
				ActionTerminal* actionTerminal = new ActionTerminal(inRequestCarte.mRefElectricCircuit, ptrTerminal);
				string no = to_string(vertex_to);
				actionTerminal->mName = "from charger " + no;
				actionTerminal->mDuration = path_distribution[vertex_to].mChargeTime;
				total_path_distribution_to_destination.mChargeTime += actionTerminal->mDuration;
				actionTerminal->mStartTime = currentTime - actionTerminal->mDuration;
				actionTerminal->mPosition = inRequestCarte.IndexTerminalToTerminal(vertex_to)->GetPosition();
				listBuildingSequence.push_front(actionTerminal);
				//Update time stamp
				currentTime = actionTerminal->mStartTime;
				//_debugVerbose = inRequestCarte.IndexTerminalToTerminal(vertex_to)->GetName().compare("Artificielle_62") == 0;
				//_debugVerbose &= inRequestCarte.mRefVehicleRequested->GetName().compare("Vehicle_9") == 0;
				if (_debugVerbose)
				{
					cout << "****START Action sequence*****" << endl;
					cout << actionTerminal->toString() << endl;
				}
			}

			//No road action associated
			if (!out_Path.HasActionAtTerminal(Action::TAction::eRoad, ptrTerminal))
			{
				ActionRoad* actionRoad = new ActionRoad(inRequestCarte.mRefElectricCircuit, ptrTerminal);
				if (vertex_to == startVertex && !bHasPartialSolution) //only when no partial solution
				{
					actionRoad->mName = "Start";
					actionRoad->mPosition = inRequestCarte.mRefVehicleRequested->GetPointDeparture();
				}
				else if (vertex_to == endVertex)
				{
					actionRoad->mName = SimulationSetting::STR_LAST_ACTION_NAME;
					actionRoad->mPosition = inRequestCarte.mRefVehicleRequested->GetPointArrival();
				}
				else {
					string no = to_string(vertex_to);
					actionRoad->mName = "to charger " + no;
					actionRoad->mPosition = inRequestCarte.IndexTerminalToTerminal(vertex_to)->GetPosition();
				}
				actionRoad->mDuration = path_distribution[vertex_to].mRoadTime;
				total_path_distribution_to_destination.mRoadTime += actionRoad->mDuration;
				actionRoad->mDistanceFromLast = path_distribution[vertex_to].mRoadTime / inRequestCarte.mRefVehicleRequested->GetAverageSpeedMPerS();
				actionRoad->mStartTime = currentTime - actionRoad->mDuration;
				listBuildingSequence.push_front(actionRoad);
				currentTime = actionRoad->mStartTime;
				if (_debugVerbose)
				{
					cout << actionRoad->toString() << endl;
					cout << "currentTime: " << currentTime << endl;
					cout << "mCameFrom[vertex_to]: " << mCameFrom[vertex_to] << endl;
					cout << "departureTime + costStructure[vertex_to] (current time should be the same): " << departureTime + mGScore[mCameFrom[vertex_to]] << endl;
					cout << "****END Action sequence*****" << endl;
				}

			}
			
		}
	}

	//Add waiting action
	if (inRequestCarte.bCreateActionWaiting)
	{
		std::map<ActionTerminal* ,ActionWait*> mapActionTerminal;
		//For all terminal, check for charging collision
		float currentClock = 0.f;
		float totalDuration = 0.f;
		for (auto it = listBuildingSequence.begin(); it != listBuildingSequence.end(); ++it)
		{
			currentClock = departureTime + totalDuration;
			//Adjust action start time for each since waiting creates an offset
			ActionTerminal* actionTerminal = dynamic_cast<ActionTerminal*>(*it);
			if (actionTerminal != nullptr)
			{
				//Check if any time collision occurs at terminal
				ElectricTerminal* const refTerminal = actionTerminal->getTerminal();
				if (refTerminal != nullptr)
				{
					TimeUnit startTime = TimeUnit(currentClock);
					TimeUnit endTime = startTime + TimeUnit(actionTerminal->getDuration());
					TimeRange idealTimeRange(startTime, endTime);
					TimeRange possibleTimeRange = refTerminal->FindNextAvailableReservation(idealTimeRange);
					if (possibleTimeRange != idealTimeRange) //We need to wait!
					{
						//Create wait action
						ActionWait* actionWait = new ActionWait(inRequestCarte.mRefElectricCircuit, refTerminal);//ElectricTerminal
						//string no = to_string(vertex_to);
						actionWait->mName = "for charger"; //TODO insert charger no
						actionWait->mDuration = float(possibleTimeRange.mStart - idealTimeRange.mStart);
						actionWait->mStartTime = currentClock;
						actionWait->mPosition = refTerminal->GetPosition();
						mapActionTerminal[actionTerminal] = actionWait;
						totalDuration += actionWait->mDuration;

						total_path_distribution_to_destination.mWaitTime += actionWait->mDuration;
					}
				}
			}
			//If there was a duration update, start time of current action needs to be updated
			(*it)->mStartTime = departureTime + totalDuration;
			//Total duration is updated
			totalDuration += (*it)->getDuration();
		}

		//Add the waiting actions
		for(auto itTerminalWait : mapActionTerminal)
		{
			auto result = find(listBuildingSequence.begin(), listBuildingSequence.end(), itTerminalWait.first);
			if (result != listBuildingSequence.end())
			{
				listBuildingSequence.insert(result, itTerminalWait.second);
			}
			else
			{
				assert(false);
			}
		}
	}

	if (!listBuildingSequence.empty())
	{
		//Adding result at the end of out_path
		out_Path.mSequenceAction.splice(out_Path.mSequenceAction.end(), listBuildingSequence);
		out_Path.recomputeProportion();

		//cout << "DjistraTerminalSpace::ActionSequenceTo - path reconstructed" << endl;
		//cout << "DjistraTerminalSpace::ActionSequenceTo " << out_Path.toString() << endl;;
	}

}

float DjistraTerminalSpace::h(int source, int dest, RequestCarte & inRequestCarte, const Carte * in_Carte)
{
	Point a = findPoint(source, inRequestCarte, in_Carte);
	Point b = findPoint(dest, inRequestCarte, in_Carte);
	float f = a.distance(b) / inRequestCarte.mRefVehicleRequested->GetAverageSpeedMPerS();
	return f;
}

Point DjistraTerminalSpace::findPoint(int vertex, RequestCarte & inRequestCarte, const Carte * in_Carte)
{
	Point a;
	if (vertex == m_VertexSource)
	{
		a = inRequestCarte.mRefVehicleRequested->GetPointDeparture();
	}else if (vertex == m_VertexDestination)
	{
		a = inRequestCarte.mRefVehicleRequested->GetPointArrival();
	}
	else
	{
		a = inRequestCarte.IndexTerminalToTerminal(vertex)->GetPosition();
	}
	return a;
}

void DjistraTerminalSpace::DijkstraComputePaths(int source, float fstartTime, int destination, const PathCache& in_refPathCache, RequestCarte & inRequestCarte, const Carte* in_Carte, PathDistribution pathD)
{
	//Source needs to exists, otherwise, no solution is possible
	if (!in_refPathCache.hasNeighbour(source))
	{
		return;
	}

	//Init
	m_VertexSource = source;
	m_VertexDestination = destination;
	float fDepartureTime = fstartTime;

	//TODO: on compte les bornes mais on devrait pt prendre le id borne max
	//On devra pt changer nos ID pour les voitures par contre...
	size_t n = in_refPathCache.getHighestTerminalID() + 1;

	path_distribution.clear();
	path_distribution.resize(n, PathDistribution(0.f, 0.f, 0.f));
	path_distribution[source] = pathD;

	mGScore.clear();
	mGScore.resize(n,INFINITY);
	mGScore[source] = pathD.Total();

	//Necessaire??
	//mTrueGScore.clear();
	//mTrueGScore.resize(n, INFINITY);
	//mTrueGScore[source] = 0.0f;

	mFScore.clear();
	mFScore.resize(n, INFINITY);
	mFScore[source] = h(m_VertexSource,m_VertexDestination,inRequestCarte,in_Carte);

	mCameFrom.clear();
	mCameFrom.resize(n, -1);

	mSetOpen.clear();
	mSetOpen.insert(m_VertexSource);

	mSetClosed.clear();

	while (!mSetOpen.empty())
	{
		//Find the lowest f score vertex in open set
		int u = -1;
		float lowestFScore = INFINITY;
		set<int>::const_iterator lowestIt;
		for (auto it = mSetOpen.begin(); it != mSetOpen.end(); ++it)
		{
			if (mFScore[*it] < lowestFScore)
			{
				lowestIt = it;
				lowestFScore = mFScore[*it];
				u = *it;
			}
		}
		//Remove vertex from open set
		mSetOpen.erase(lowestIt);

		//Clock is absolute time
		//Time for departure is GScore
		float currentClock = fDepartureTime + mGScore[u];
		float currentTimeFromDeparture = mGScore[u];

		// Visit each edge exiting u
		if (in_refPathCache.hasNeighbour(u))
		{
			PathCacheNeighbourMap  neighborBegin = in_refPathCache.getPathNeighbourMap(u);
			for (auto neighbor_iter = neighborBegin.cbegin(); neighbor_iter != neighborBegin.cend(); neighbor_iter++)
			{
				bool _debugVerbose = false;

				int v = neighbor_iter->first;
				double driveDistance = neighbor_iter->second;
				//If too far to drive to, skip this neighbor
				if (driveDistance > inRequestCarte.mRefVehicleRequested->GetAutonomie())
				{
					continue;
				}
				//We have a distance but we need the time
				PathDistribution curDistribution(0.f,0.f, driveDistance / inRequestCarte.mRefVehicleRequested->GetAverageSpeedMPerS());
				//Calcul de l'attente (s'il y en a)
				//Compute the wait according to current battery level
				//Only when not the destination
				ElectricTerminal * const refTerminal = inRequestCarte.mRefElectricCircuit->GetByIndexTerminal(v);
				if (refTerminal != nullptr && inRequestCarte.mOptionnal_StationToIgnore.find(refTerminal) != inRequestCarte.mOptionnal_StationToIgnore.end())
				{
					//Skip this terminal
					continue;
				}
				if(v != m_VertexDestination)
				{
					switch (inRequestCarte.mChargingBehavior)
					{
					case RequestCarte::eMINIMUM:
						curDistribution.mChargeTime = driveDistance/ SimulationSetting::D_AVERAGE_VEHICLE_CHARGE_RATE_M_PER_SECOND;
						assert(false);
						break;
					case RequestCarte::ePREFER_CHARGING_TO_WAITING:
						assert(false);
						break;
					case RequestCarte::eMAXIMUM:
					default:
						curDistribution.mChargeTime = driveDistance / SimulationSetting::D_AVERAGE_VEHICLE_CHARGE_RATE_M_PER_SECOND;
						//cout << "Temps de chargement calcul� (h): " << curDistribution.mChargeTime/3600.0f << endl;
						//cout << "Autonomie (m): " << inRequestCarte.mRefVehicleRequested->GetAutonomie() << endl;
						//cout << "Max charge time: " << inRequestCarte.mRefVehicleRequested->GetAutonomie() / SimulationSetting::D_AVERAGE_VEHICLE_CHARGE_RATE_M_PER_SECOND << endl;
						assert(curDistribution.mChargeTime < 7200.0f);
						break;
					}
				}
				//Si on inclu attente de chargement
				float fArrivingTime = currentClock + curDistribution.mRoadTime;
				if (fArrivingTime < inRequestCarte.fMinimizeWaitingHorizon) //Take into account waiting
				{
					if (refTerminal != nullptr)
					{
						TimeUnit startTime = TimeUnit(currentClock + curDistribution.mRoadTime);
						TimeUnit endTime = TimeUnit(startTime + curDistribution.mChargeTime);
						//cout << *refTerminal << endl;
						TimeRange idealTimeRange(startTime,endTime);
						TimeRange possibleTimeRange = refTerminal->FindNextAvailableReservation(idealTimeRange);
						if (possibleTimeRange != idealTimeRange)
						{
							curDistribution.mWaitTime = float(possibleTimeRange.mStart - idealTimeRange.mStart);
							//cout << "Adding wait time of:" << curDistribution.mWaitTime << endl;
						}
						//_debugVerbose = refTerminal->GetName().compare("Artificielle_62") == 0;
						//_debugVerbose &= inRequestCarte.mRefVehicleRequested->GetName().compare("Vehicle_9") == 0;
						if(_debugVerbose)
						{
							//Verbose
							cout << "****START Collision computation*****" << endl;
							cout << "curDistribution" << endl;
							cout << curDistribution << endl;

							cout << "idealTimeRange: ";
							cout << idealTimeRange << endl;

							cout << "possibleTimeRange: ";
							cout << possibleTimeRange << endl;

							cout << "currentClock: ";
							cout << currentClock << endl;

							cout << "currentTimeFromDeparture: ";
							cout << currentTimeFromDeparture << endl;
							cout << "****END Collision computation*****" << endl;
						}
					}
				} // end OtherVehiclesPresence

				float gScoreTentative_v  = (float)currentTimeFromDeparture + curDistribution.Total();
				//When we have a better score
				if (gScoreTentative_v < mGScore[v])
				{
					float fScore = gScoreTentative_v + h(v, destination, inRequestCarte, in_Carte);
					//If there is a better path in open set, skip to next
					if (mSetOpen.count(v) == 1 && mFScore[v] < fScore)
					{
						continue;
					} else if (mSetClosed.count(v) == 1 && mFScore[v] < fScore)//If there is a better path in the closed list, skip to next
					{
						continue;
					}else
					{
						//Update g score, true score and f score
						mGScore[v] = gScoreTentative_v;
						//mTrueGScore[v] = mTrueGScore[u] + curDistribution.Total();
						mFScore[v] = fScore;
						//Update the path distribution
						path_distribution[v] = curDistribution;
						//Update the came from
						mCameFrom[v] = u;
						if (_debugVerbose)
						{
							cout << "****START Updating values*****" << endl;
							cout << "mGScore[v]"			 << endl;
							cout << mGScore[v]			 << endl;
							//cout << "mTrueGScore[v]"		 << endl;
							//cout << mTrueGScore[v]		 << endl;
							cout << "mFScore[v]"			 << endl;
							cout << mFScore[v]			 << endl;
							cout << "path_distribution[v]" << endl;
							cout << path_distribution[v] << endl;
							cout << "mCameFrom[v]"		 << endl;
							cout << mCameFrom[v]		 << endl;
							cout << "****END Updating values*****" << endl;
						}
						//We reached the destination! Exit early
						if (u == m_VertexDestination)
						{
							//Not ideal...
							return;
						}
						//Add to open list if not there already
						if (mSetOpen.count(v) == 0)
						{
							mSetOpen.insert(v);
						}

						//Remove from closed list if there already
						if (mSetClosed.count(v) == 1)
						{
							mSetClosed.erase(v);
						}
					}
				}
			}
		}
		if (mSetClosed.count(u) == 0)
		{
			mSetClosed.insert(u);
		}
	}
}

