#pragma once

#include <iostream>
#include <string>
#include <vector>

class CSVExporter
{
private:
	std::vector<std::vector<std::string>> m_vvData;
	void writeNextRow(std::ostream& str, const std::vector<std::string> & in_Data);

public:
	CSVExporter();
	void AddData(const std::vector<std::string> & in_vData);
	void Export(std::ostream& str);
	virtual ~CSVExporter();
};

