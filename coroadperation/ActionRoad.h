#pragma once
#include "Action.h"
class ActionRoad :
	public Action
{
public:
	ActionRoad(ElectricCircuit* refElectricCircuit, ElectricTerminal* refTerminal);
	~ActionRoad();

	virtual TAction getType() const { return eRoad; }
	float SpeedLimit() { return 10.f; }
	virtual Action* clone() const;

	float mDistanceFromLast = 0.0f;
};

