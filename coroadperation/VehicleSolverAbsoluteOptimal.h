#pragma once
#include "IVehicleSolver.h"

class IMapSolver;
class ElectricCircuit;
class IObjectiveFunction;

class VehicleSolverAbsoluteOptimal :
	public IVehicleSolver
{

protected:
	IMapSolver const *const mRefCarte = nullptr;
	IObjectiveFunction *const mRefObjectiveFunction = nullptr;

public:
	VehicleSolverAbsoluteOptimal(IMapSolver const *const inCarte, ElectricCircuit * inCircuit, IObjectiveFunction* inObjectiveFunction);
	virtual ~VehicleSolverAbsoluteOptimal();

	virtual void SolveFor(Vehicle* inVehicleToSolve, VehicleSolution* out_Solution);
};

