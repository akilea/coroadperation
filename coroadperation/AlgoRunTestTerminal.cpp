#include "AlgoRunTestTerminal.h"

#include "ElectricTerminal.h"
#include "ElectricReservation.h"

AlgoRunTestTerminal::AlgoRunTestTerminal()
{
}


AlgoRunTestTerminal::~AlgoRunTestTerminal()
{
}

int AlgoRunTestTerminal::Run(int argc, const char ** argv)
{

	Borne testBorne;
	testBorne.nom = "Test terminal";
	TimeUnit range(4);
	ElectricTerminal et(testBorne,-1);
	//ElectricReservationPtr erA = new ElectricReservation(TimeRange(0, 0+range), nullptr);
	//ElectricReservationPtr erB = new ElectricReservation(TimeRange(8, 8+range), nullptr);
	//ElectricReservationPtr erC = new ElectricReservation(TimeRange(3, 3+range), nullptr);
	//ElectricReservationPtr erD = new ElectricReservation(TimeRange(2, 2+range), nullptr);
	//ElectricReservationPtr erE = new ElectricReservation(TimeRange(1, 1+range), nullptr);
	//ElectricReservationPtr erF = new ElectricReservation(erA->GetTimeRange(), nullptr);
	//ElectricReservationPtr erG = new ElectricReservation(erD->GetTimeRange(), nullptr);

	ElectricReservationPtr erA = new ElectricReservation(TimeRange(42718, 44496), nullptr);
	ElectricReservationPtr erB = new ElectricReservation(TimeRange(37663, 39441), nullptr);
	ElectricReservationPtr erC = new ElectricReservation(TimeRange(34247, 36025), nullptr);
	ElectricReservationPtr erD = new ElectricReservation(TimeRange(13815, 14900), nullptr);
	ElectricReservationPtr erE = new ElectricReservation(TimeRange(41772, 43551), nullptr);

	et.TryReserve(erA);
	et.TryReserve(erB);
	et.TryReserve(erC); 
	et.TryReserve(erD);
	TimeRange newEr = et.FindNextAvailableReservation(TimeRange(41772, 43551));
	et.TryReserve(erE);
	cout << "***** TESTING RESERVATION****" << endl;
	cout << et.toString() << endl;

	cout << "***** TESTING OVERLAPS****" << endl;
	ElectricReservationPtr overlap = new ElectricReservation(TimeRange(4,9), nullptr);
	std::vector<ElectricReservationPtr> testOverlap;
	et.ReservationOverlap(overlap, testOverlap);
	for each (auto var in testOverlap)
	{
		cout << var << endl;
	}

	cout << "***** TESTING FINDING NEXT RESERVATION****" << endl;
	cout << et.FindNextAvailableReservation(TimeRange(2, 3)) << endl;
	cout << et.FindNextAvailableReservation(TimeRange(0, 0)) << endl;
	cout << et.FindNextAvailableReservation(TimeRange(0, 50)) << endl;
	cout << et.FindNextAvailableReservation(TimeRange(2, 7)) << endl;

	////TO BE COMPLETED
	et.TryFree(erB);
	et.TryFree(erC);
	et.TryFree(erA);
	et.TryFree(erD);
	et.TryFree(erE);
	//et.TryFree(erG);
	//et.TryFree(erF);

	//cout << "***** TESTING FREEING****" << endl;
	cout << et << endl;

	cin.get();

	return 0;
}
