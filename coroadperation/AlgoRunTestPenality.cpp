#include "AlgoRunTestPenality.h"

#include "Scenario.h"
#include "ISystemSolver.h"
#include "SystemSolution.h"
#include "VehicleSolution.h"
#include "Path.h"
#include "PathDistribution.h"
#include "CollisionWithReference.h"
#include "CollisionAnalyser.h"
#include "ScenarioData.h"

#include <iostream>
#include <iomanip>

using namespace std;

void DisplayPenality(SystemSolution* sysSol, SystemSolution* sysSolRef,string name)
{
	cout << "Name=" << name << endl;
	cout << "Total=" << sysSol->mSystemPathDistribution.Total() << endl;
	cout << "System penality=" << sysSol->mSystemPathDistribution / sysSolRef->mSystemPathDistribution << endl;
	cout << "Each v..." << endl;
	int i = 0;
	//TODO: insert into SystemSolution directly
	//for (auto it : sysSol->mMapVehicleSolution)
	//{
	//	cout << "i=" << i << " p=" << sysSolRef->ComputePenalityFor(it.first, it.second->mPath->mTotalPathDistribution) << endl;
	//	++i;
	//}

}


int AlgoRunTestPenality::Run(int argc, const char** argv)
{
	AlgoRunCommon::Run(argc, argv);

	scenario->SetActivation(Scenario::eINIT,true);
	scenario->SetActivation(Scenario::eNO_COOP,true);
	scenario->SetActivation(Scenario::eNO_COOP_WAIT,true);
	scenario->SetActivation(Scenario::eCOOP_NO_OPTIMIZATION,true);
	scenario->SolveAllVehicle();
	scenario->SolveAllSystem();

	SystemSolution* sysSolA = scenario->GetSystemSolver(Scenario::eNO_COOP)->GetCurrentSystemSolution();
	SystemSolution* sysSolB = scenario->GetSystemSolver(Scenario::eNO_COOP_WAIT)->GetCurrentSystemSolution();
	SystemSolution* sysSolC = scenario->GetSystemSolver(Scenario::eCOOP_NO_OPTIMIZATION)->GetCurrentSystemSolution();

	//Test validity of penalities
	cout << "Car amount " << sysSolA->NbVehiculeSolution() << endl;
	cout << setprecision(10);
	DisplayPenality(sysSolA, sysSolA, "eNO_COOP");
	DisplayPenality(sysSolB, sysSolA, "eNO_COOP_WAIT");
	DisplayPenality(sysSolC, sysSolA, "eNO_COOP_NO_OPTIMIZATION");

	//Test for having penality as Objective function
	//Directly done in scenario.. seems to work!

	//Test validity of collisions
	std::set<Vehicle*> vAdmissible;
	std::vector<AlternativeSearch> vAltSearch;
	for (int i = 0; i < scenario->GetRefData()->mVecVehicle.size(); ++i)
	{
		cout << "Testing for i=" << i << endl;
		vAdmissible.insert(scenario->GetRefData()->mVecVehicle[i]);
		CollisionAnalyser colAnalyser(sysSolB, sysSolA, scenario->GetRefData()->mElectricCircuit);
		colAnalyser.FindAndSortWaiting(vAdmissible,346.0f,0);
		if(colAnalyser.PeakNextCollision() != nullptr)
		{
			cout << *colAnalyser.PeakNextCollision();
		}
		cout << "End test" << endl << endl;
	}

	AlgoRunCommon::endRun();
	return 0;
}


