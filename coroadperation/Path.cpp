#include "Path.h"
#include <sstream>
#include <iomanip>
#include <vector>
#include <iomanip>

#include "ElectricTerminal.h"
//TEMP to remove
#include "Action.h"
#include "ActionRoad.h"
#include "ActionTerminal.h"
#include "ActionWait.h"

void Path::recomputeProportion()
{
	mDistance = 0.0f;
	mTotalPathDistribution.Set0();
	for (auto it = mSequenceAction.begin(); it != mSequenceAction.end(); ++it)
	{
		//if ((*it) == nullptr)
		//{
		//	cout << "recomputeProportion - nullptr here" << endl;
		//}
		//cout << "recomputeProportion - it=" << (*it)->toString() << endl;
		if ((*it)->getType() == Action::eRoad)
		{
			ActionRoad* ar = dynamic_cast<ActionRoad*>(*it);
			mDistance += ar->mDistanceFromLast;
			mTotalPathDistribution.mRoadTime += ar->getDuration();
		}
		else if ((*it)->getType() == Action::eTerminal)
		{
			mTotalPathDistribution.mChargeTime += (*it)->getDuration();
		}
		else
		{
			mTotalPathDistribution.mWaitTime += (*it)->getDuration();
		}
	}
}

int Path::CountAction(Action::TAction actType) const
{
	int c = 0;
	for (auto it : mSequenceAction)
	{
		if (it->getType() == actType)
		{
			++c;
		}
	}
	return c;
}

bool Path::HasActionAtTerminal(Action::TAction actType,ElectricTerminal* targetTerminal) const
{
	bool hasAction = false;
	if (targetTerminal != nullptr)
	{
		int index = targetTerminal->GetIndex();
		for (auto it : mSequenceAction)
		{
			if (it->getType() == Action::eTerminal)
			{
				ActionTerminal* actTerm = dynamic_cast<ActionTerminal*>(it);
				if (actTerm->getTerminal()->GetIndex() == index)
				{
					hasAction = true;
					break;
				}
			}
		}
	}

	return hasAction;
}

bool Path::HasPartialSolution() const
{
	return mSequenceAction.size() >2; //first 2 actions are for reaching the fist terminal
}

Path::Path()
	:mTotalPathDistribution(INFINITY, INFINITY, INFINITY)
{
}

Path::Path(const Path & old_obj)
: mDistance(old_obj.mDistance)
 ,mTotalPathDistribution(old_obj.mTotalPathDistribution)
{
	for each (auto var in old_obj.mSequenceAction)
	{
		mSequenceAction.push_back(var->clone());
	}
}

Action* Path::FindActionAt(TimeRange target) const
{
	Action* ret = nullptr;
	for (auto it : mSequenceAction)
	{
		//cout << "Path::FindActionAt - target" << target << endl;
		//cout << "Path::FindActionAt - it->getTimeRange()" << it->getTimeRange() << endl;
		if (it->getTimeRange() == target)
		{
			ret = it;
			break;
		}
	}

	return ret;
}

void Path::ReplaceElectricCircuit(ElectricCircuit* inEC)
{
	for each (auto var in mSequenceAction)
	{
		var->ReplaceElectricCircuit(inEC);
	}
}

Path::~Path()
{

	for each (auto var in mSequenceAction)
	{
		delete &(*var);
	}
}

Path & Path::operator=(const Path & old_obj)
{
	mTotalPathDistribution = old_obj.mTotalPathDistribution;
	mDistance = old_obj.mDistance;
	for each (auto var in mSequenceAction)
	{
		delete &(*var);
	}
	mSequenceAction.clear();
	for each (auto var in old_obj.mSequenceAction)
	{
		mSequenceAction.push_back(var->clone());
	}
	return *this;
}

string Path::toStringShortenRoad()
{
	std::ostringstream ss;
	int roadSegmentCounter = 0;
	//ss << setw(20) << left << "Start point:" << mStart << endl;
	//ss << setw(20) << left << "End point:" << mEnd << endl;
	for each (auto var in mSequenceAction)
	{
		//Ignore road as it is too cumbersome
 		if (var->getType() == Action::eRoad)
		{
			roadSegmentCounter++;
		}
		else
		{
			if (roadSegmentCounter > 0)
			{
				ss << setw(12) << left << Action::enumAssociation[Action::eRoad] << roadSegmentCounter << " segments" << endl;
				//ss << "..." << endl;
				roadSegmentCounter = 0;
			}
			ss << var->toString();
		}
	}

	if (roadSegmentCounter > 0)
	{
		ss << setw(12) << left << Action::enumAssociation[Action::eRoad] << roadSegmentCounter << " segments" << endl;
		roadSegmentCounter = 0;
	}
	return ss.str();
}

string Path::toString()
{
	std::ostringstream ss;
	for each (auto var in mSequenceAction)
	{
		ss << var->toString();
	}
	ss << "Distribution=" << endl << mTotalPathDistribution << endl;
	return ss.str();
}

string Path::toStringName()
{
	std::ostringstream ss;
	for each (auto var in mSequenceAction)
	{
		ss << var->getName() << endl;
	}
	return ss.str();
}

string Path::toStringInteractive()
{
	std::ostringstream ss;
	std::ostringstream st;
	ss << std::setprecision(8);
	for (std::list<Action*>::iterator iter = mSequenceAction.begin(); iter != mSequenceAction.end(); ++iter)
	{
		if ((*iter)->getType() == Action::eRoad)
		{
			ss << (*iter)->mPosition << '\t';
		}
		else
		{
			st << (*iter)->getName() << '\t';
		}

	}

	ss << endl;
	ss << st.str();

	return ss.str();
}

float Path::getDuration() const
{
	float retVal = mTotalPathDistribution.Total();
	return retVal;
}
