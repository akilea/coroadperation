#pragma once

#include <string>

class SystemSolution;
class VehicleSolution;

class IObjectiveFunction
{
public:
	IObjectiveFunction(){}
	~IObjectiveFunction(){}

	virtual bool IsBetterThan(SystemSolution const * const in_A, SystemSolution const * const in_B);
	virtual float Evaluate(SystemSolution const* const in_A) = 0;

	virtual std::string Description() const { return  "None given"; }
};