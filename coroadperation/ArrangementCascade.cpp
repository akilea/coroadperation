#include "ArrangementCascade.h"
#include <vector>
#include "Vehicle.h"



ArrangementCascade::ArrangementCascade()
{
}


ArrangementCascade::~ArrangementCascade()
{
}

void ArrangementCascade::startSequence(std::vector<Vehicle*>* in_refVector)
{
	Arrangement::startSequence(in_refVector);
	mIndexToMove = 0;
	mQuantityToMove = 0;
}

std::vector<Vehicle*>* ArrangementCascade::GetNextSequence()
{

	//cout << "mIndexToMove" << mIndexToMove << endl;
	//cout << "mQuantityToMove" << mQuantityToMove << endl;
	m_currentArrangement->clear();
	if (mIndexToMove >= m_refVector->size())
	{
		return nullptr;
	}
	else
	{
		//Construct the order
		//Push back the mIndexToMove for mQuantityToMove position
		for (unsigned int j = 0; j < m_refVector->size(); ++j)
		{
			if (j != mIndexToMove)
			{
				m_currentArrangement->push_back((*m_refVector)[j]);
			}
		}
		//Add back the uninserted element
		size_t destinationPosition = mQuantityToMove + mIndexToMove;
		m_currentArrangement->insert(m_currentArrangement->begin()+ destinationPosition, (*m_refVector)[mIndexToMove]);
	}
	++mQuantityToMove;
	if (mQuantityToMove + mIndexToMove >= m_refVector->size())
	{
		mQuantityToMove = 0;
		++mIndexToMove;
	}
	return m_currentArrangement;
}
