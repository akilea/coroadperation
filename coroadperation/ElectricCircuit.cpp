#include "ElectricCircuit.h"

#include "srcMap/carte.h"
#include "ElectricTerminal.h"

ElectricCircuit::ElectricCircuit(const Carte& inCarte)
{
	int i = 0;
	for (auto var : inCarte.GetBorne())
	{
		mMapStation.insert({ i, new ElectricTerminal(var,i) });
		++i;
	}
}

ElectricCircuit::ElectricCircuit(const ElectricCircuit & otherEc)
{
	for (auto var : mMapStation)
	{
		delete var.second;
	}

	this->mMapStation.clear();

	for (auto var : otherEc.mMapStation)
	{
		mMapStation.insert({ var.first,new ElectricTerminal(*var.second) });
	}
	//cout << "Copy constructor after: otherEc.mMapStation.size()=" << otherEc.mMapStation.size() << endl;
	//cout << "Copy constructor after: mMapStation.size()=" << mMapStation.size() << endl;
}

ElectricCircuit * ElectricCircuit::clone() const
{
	return new ElectricCircuit(*this);
}

ElectricCircuit::~ElectricCircuit()
{
	for (auto var : mMapStation)
	{
		delete var.second;
	}
	mMapStation.clear();
}

ElectricTerminal * ElectricCircuit::GetByIndexTerminal(int inIDTerminal) const
{
	auto it = mMapStation.find(inIDTerminal);
	ElectricTerminal* ret = nullptr;
	if (it != mMapStation.end())
	{
		ret = it->second;
	}

	return ret;
}

float ElectricCircuit::ComputeTerminalPer100km2() const
{
	//Find the extrema
	float ret = INFINITY;
	size_t nbTerminal = mMapStation.size();
	if (nbTerminal > 0)
	{
		Point pMin(INFINITY, INFINITY);
		Point pMax(-INFINITY, -INFINITY);

		for (auto var : mMapStation)
		{
			pMin.moveToMin(var.second->GetPosition());
			pMax.moveToMax(var.second->GetPosition());
		}
		Rectangle area(pMin, pMax);
		ret = nbTerminal / area.surface_m2();
		ret *= (1000 * 1000); //Per km^2
		ret *= (100 * 100); //Per 100 km^2
	}
	return ret;
}

unsigned int ElectricCircuit::computeTotalReservation() const
{
	unsigned int total = 0;
	for (auto var : mMapStation)
	{
		total += var.second->size();
	}
	return total;
}

void ElectricCircuit::clearAllReservation()
{
	for (auto var : mMapStation)
	{
		var.second->FreeAll();
	}
}

ostream & operator<<(ostream & os, const ElectricCircuit & dt)
{
	bool hasOne = false;
	for (auto var : dt.mMapStation)
	{
		if (var.second->size() > 0)
		{
			hasOne = true;
			os << *(var.second) << endl;
		}
	}
	if (!hasOne)
	{
		os << "No reservation across electric circuit!" << endl;
	}
	return os;
}
