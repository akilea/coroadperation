#include "SystemSolverArranged.h"
#include "ElectricCircuit.h"

SystemSolverArranged::SystemSolverArranged(IObjectiveFunction* inObjectiveFunction, IVehicleSolver* inVehicleSolver, IArrangement* inArrangment, ElectricCircuit* inElectricCircuit, double in_dComputationTimeCap)
	:SystemSolver(inObjectiveFunction, inVehicleSolver, inArrangment, inElectricCircuit, in_dComputationTimeCap)
{
}

SystemSolverArranged::~SystemSolverArranged()
{
}

void SystemSolverArranged::AddVehicleInternal(Vehicle * newVehicle)
{
	SystemSolver::AddVehicleInternal(newVehicle);
}

void SystemSolverArranged::ResetSolver()
{
	if (mBestSolver != nullptr)
	{
		delete mBestSolver;
		mBestSolver = nullptr;
	}

	if (mBestCircuit != nullptr)
	{
		delete mBestCircuit;
		mBestCircuit = nullptr;
	}
}

SystemSolution * SystemSolverArranged::GetCurrentSystemSolution() const
{
	if (mBestSolver != nullptr)
	{
		return mBestSolver->GetCurrentSystemSolution();
	}
	else
	{
		return mSystemSolution;
	}
}
