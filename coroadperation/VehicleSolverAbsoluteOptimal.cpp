#include "VehicleSolverAbsoluteOptimal.h"

#include "srcMap/carte.h"
#include "VehicleSolution.h"
#include "RequestCarte.h"
#include "IObjectiveFunction.h"


VehicleSolverAbsoluteOptimal::VehicleSolverAbsoluteOptimal(IMapSolver const *const inCarte, ElectricCircuit * inCircuit, IObjectiveFunction* inObjectiveFunction)
	:IVehicleSolver(inObjectiveFunction,inCircuit)
	, mRefCarte(inCarte)
	, mRefObjectiveFunction(inObjectiveFunction)
{
}


VehicleSolverAbsoluteOptimal::~VehicleSolverAbsoluteOptimal()
{

}

void VehicleSolverAbsoluteOptimal::SolveFor(Vehicle * inVehicleToSolve, VehicleSolution * out_Solution)
{
	RequestCarte request;
	request.mRefElectricCircuit = mRefElectricCircuit;
	request.mRefVehicleRequested = inVehicleToSolve;
	request.bCreateActionWaiting = false;
	request.NeverMinimizeWaitingHorizon();
	mRefCarte->calculerTrajet((*out_Solution->mPath), request);
}
