#include "CollisionAnalyser.h"

#include <assert.h>

#include "SystemSolution.h"
#include "VehicleSolution.h"
#include "ActionWait.h"
#include "ActionTerminal.h"
#include "Action.h"
#include "Path.h"
#include "ElectricTerminal.h"
#include "ElectricCircuit.h"
#include "Vehicle.h"
#include "CollisionWithReference.h"
#include "Collision.h"
#include "SolutionCutter.h"
#include <list>

using namespace std;

void CollisionAnalyser::FindAndSortWaiting(std::set<Vehicle*>& vVehicleAdmissible,float limitTime, Vehicle* newVehicleNoTimeConstraint)
{
	//cout << "CollisionAnalyser::FindAndSortWaiting - START" << endl;
	//cout << "CollisionAnalyser::FindAndSortWaiting - vVehicleAdmissible.size()="  << vVehicleAdmissible.size() << endl;
	VehicleSolution*			vCulpritSol		= nullptr;
	VehicleSolution*			vCulpritRefSol	= nullptr;
	ActionTerminal*				actTerminal		= nullptr;

	for (auto itvehAdmissible : vVehicleAdmissible)
	{
		//cout << "CollisionAnalyser::FindAndSortWaiting - itvehAdmissible="  << itvehAdmissible->GetName() << endl;

		VehicleSolution* vSol = mSysSol->GetVehiculeSolution(itvehAdmissible);
		VehicleSolution* vSolRef = mSysSolRef->GetVehiculeSolution(itvehAdmissible);

		//We find the biggest wait in the list
		//But we eliminate candidates where the vehicle would need to remove it's current commitment on the next terminal
		bool canCommit = false;
		bool isFirstAction = true;
		bool hasNoTimeConstraint = newVehicleNoTimeConstraint == itvehAdmissible;

		//cout << "CollisionAnalyser::FindAndSortWaiting - Before tests" << endl;
		//For the solution to test...
		for (auto itAct : vSol->mPath->mSequenceAction)
		{
			//Special case for first action since it is a road
			if (isFirstAction)
			{
				canCommit = itAct->mStartTime > limitTime;
				//cout << "Start:canCommit=" << canCommit << endl;
				isFirstAction = false;
				continue;
			}

			//Is the action prevents a commitment for the next reachable terminal?
			//When we reach the next terminal action that ends after the time limit
			if(!canCommit && itAct->getType() == Action::eTerminal && limitTime < itAct->getTimeRange().mEnd )
			{
				canCommit = true;
				//cout << "Middle:canCommit=" << canCommit << endl;
			}
			//When we have the biggest wait possible...
			if ((canCommit || hasNoTimeConstraint) && itAct->getType() == Action::eWait /*&& itAct->getDuration() > actBiggestWait->getDuration() + fDetour*/)
			{
				ActionWait* actBiggestWait = (ActionWait*)itAct;

				//Then return other vehicle associated
				std::vector<ElectricReservationPtr> vReservation;
				actBiggestWait->getTerminal()->ReservationOverlap(actBiggestWait->getTimeRange(), vReservation);

				if (vReservation.empty())
				{
					cout << "Problem with FindAndSortWaiting: no reservation found even if it should be the biggest." << endl;
					//Skip
					continue;
					//assert(false);
				}

				//From the reservation, get the solution from the culprit
				vCulpritSol = mSysSol->GetVehiculeSolution(vReservation[0]->m_pVehicle);
				vCulpritRefSol = mSysSolRef->GetVehiculeSolution(vReservation[0]->m_pVehicle);
				//Find the action associated
				Action* overlapAction = vCulpritSol->mPath->FindActionAt(vReservation[0]->GetTimeRange());
				if (overlapAction == nullptr)
				{
					cout << "Problem with FindAndSortWaiting: action returned by culprit is null!" << endl;
					cout << "Culprit solution ptr=" << vCulpritSol << endl;
					vCulpritSol->displayTrajectoryDetail(cout) << endl;
					cout << "SolutionHasReachedEnd=" << vCulpritSol->HasReachedEnd() << endl;
					assert(false);
				}

				actTerminal = (ActionTerminal*)overlapAction;
				if (actTerminal == nullptr)
				{
					cout << "Problem with FindAndSortWaiting: action returned by culprit is not an ActionTerminal!" << endl;
					cout << "Action received:" << endl;
					cout << vCulpritSol->mPath->FindActionAt(vReservation[0]->GetTimeRange()) << endl;
					assert(false); //Se rend parfois ici car on a des solutions incompletes
				}
				//Finally!
				CollisionWithReference* newCol = new CollisionWithReference(vSol, vSolRef, actBiggestWait, vCulpritSol, vCulpritRefSol, actTerminal);
				newCol->SetBlockerCanMove(canCommit);
				pqOCollision.push(newCol);
			}
		}
		//cout << "CollisionAnalyser::FindAndSortWaiting - After tests" << endl;
	}
}

CollisionWithReference* CollisionAnalyser::PeakNextCollision() const
{
	return pqOCollision.empty() ? nullptr : pqOCollision.top();
}

void CollisionAnalyser::PopNextCollision()
{
	if (!pqOCollision.empty())
	{
		CollisionWithReference* tmp = pqOCollision.top();
		pqOCollision.pop();
		delete tmp;
	}
}

void CollisionAnalyser::PopAllCollision()
{
	//delete collisions
	while (!pqOCollision.empty())
	{
		PopNextCollision();
	}
}

//This is a recursive method. 
void CollisionAnalyser::PierceFromRecursive(std::set<Vehicle*>& out_Affected, SystemSolution* sysSol, Vehicle* curVehicle, ElectricTerminal* terminalFrom, float fLimitTime)
{
	cout << "PierceFrom - curVehicle=" << curVehicle->GetName() << endl;
	cout << "PierceFrom - terminalFrom=" << terminalFrom << endl;
	if (terminalFrom != nullptr)
	{
		cout << "PierceFrom - terminalFrom=" << *terminalFrom << endl;
	}
	VehicleSolution* vSol = sysSol->GetVehiculeSolution(curVehicle);
	assert(vSol != nullptr);
	bool bStartedPierce = false;
	auto itErase = vSol->mPath->mSequenceAction.end();
	int iCounter = -1;
	for (auto it  = vSol->mPath->mSequenceAction.begin(); it != vSol->mPath->mSequenceAction.end();++it)
	{
		++iCounter;
		cout << "PierceFrom - it=" << (*it)->toString() << endl;
		cout << "PierceFrom - before test bStartedPierce=" << bStartedPierce << endl;
		//Skip...
		if ((*it) == nullptr)
		{
			cout << "PierceFrom - skipping because action is null" << endl;
			continue;
		}
		if ((*it)->getTerminal() == nullptr)
		{
			cout << "PierceFrom - skipping because first action" << endl;
			continue;
		}
		if (!bStartedPierce)
		{
			bool bIsAtTerminal = terminalFrom == nullptr || terminalFrom->GetIndex() == (*it)->getTerminal()->GetIndex();
			if (terminalFrom != nullptr)
			{
				cout << "PierceFrom - terminalFrom->GetIndex()=" << terminalFrom->GetIndex() << endl;
				cout << "PierceFrom - terminalFrom->GetIndex()=" << terminalFrom->GetIndex() << endl;
			}
			bool bIsAtTime = (*it)->getStartTime() > fLimitTime && (*it)->getType() == Action::eRoad;
			bStartedPierce = bIsAtTerminal && bIsAtTime;
			if (bStartedPierce)
			{
				cout << "CollisionAnalyser::PierceFromRecursive - iCounter" << iCounter<<endl;
			}
		}
		cout << "PierceFrom - after test bStartedPierce=" << bStartedPierce << endl;

		//When terminal is not specified, take all event stops
		if (bStartedPierce)
		{
			if (itErase == vSol->mPath->mSequenceAction.end())
			{
				itErase = it;
				out_Affected.insert(curVehicle);
			}
			Collision colWaiterInvestigation;

			std::vector<Vehicle*> vecColBlockerInvestigation;
			ElectricTerminal* colBlockerTerminalInvestigation = nullptr;
			//Remove reservation when appropriate
			if ((*it)->getType() == Action::eTerminal)
			{
				cout << "PierceFrom - is a terminal..." << endl;
				CollisionAnalyser::InvestigateWaiterAcrossSolution(vSol,(ActionTerminal*)*it, sysSol,&colWaiterInvestigation);
				cout << "PierceFrom - InvestigateWaiterAcrossSolution COMPLETED" << endl;
				ActionTerminal* actTerminal = dynamic_cast<ActionTerminal*>(*it);
				assert(actTerminal != nullptr);
				if (actTerminal != nullptr)
				{
					assert(actTerminal->getCommitedReservation() != nullptr);
					//Should have a reservation
					if (!actTerminal->getTerminal()->TryFree(actTerminal->getCommitedReservation()))
					{
						assert(false);
					}
					//Delete action and reservation
					delete actTerminal->getCommitedReservation();
					actTerminal->setCommitedReservation(nullptr);
				}

			}

			//Undo the knot when we wait as well
			if ((*it)->getType() == Action::eWait)
			{
				cout << "PierceFrom - is a waiter..." << endl;
				ActionWait* actWait = dynamic_cast<ActionWait*>(*it);
				assert(actWait != nullptr);
				if (actWait != nullptr)
				{
					//Find the other vehicle associated with overlap
					std::vector<ElectricReservation*> vecOverlapReservation;
					actWait->getTerminal()->ReservationOverlap(actWait->getTimeRange(), vecOverlapReservation);
					if (vecOverlapReservation.size() > 0)
					{
						for (auto it : vecOverlapReservation)
						{
							vecColBlockerInvestigation.push_back(it->m_pVehicle);
						}
						colBlockerTerminalInvestigation = actWait->getTerminal();
					}
				}

			}

			delete *it;
			//Recursion can iterate over it... needs to be done!
			*it = nullptr;

			//State of electrical circuit should be clean
			//If current vehicle block someone else
			//cout << "PierceFrom - col.HasCollision()" << col.HasCollision() << endl;
			if (colWaiterInvestigation.HasCollision())
			{
				cout << "PierceFrom - has waiter collision..." << endl;
				//Need to propagate the change when we have a collision
				//Pierce the waiter
				PierceFromRecursive(out_Affected, sysSol, colWaiterInvestigation.GetWaiterSolution()->mRefVehicle, /*colWaiterInvestigation.GetWaiterActionWait()->getTerminal()*/ nullptr , fLimitTime);
			}

			if (!vecColBlockerInvestigation.empty() && colBlockerTerminalInvestigation != nullptr)
			{
				cout << "PierceFrom - has blocker collision..." << endl;
				for (auto it : vecColBlockerInvestigation)
				{
					PierceFromRecursive(out_Affected, sysSol, it, /*colBlockerTerminalInvestigation*/nullptr, fLimitTime);
				}
			}
		}
	}

	//Clear sequence
	cout << "PierceFrom - erasing..." << endl;
	vSol->mPath->mSequenceAction.erase(itErase, vSol->mPath->mSequenceAction.end());
	if (vSol->mPath->mSequenceAction.size() == 1)
	{
		//delete start action
		delete *vSol->mPath->mSequenceAction.begin();
		vSol->mPath->mSequenceAction.clear();
	}
	//Update distribution
	//sysSol->RecomputeVehicleAndSystemDistribution();
	//cout << "PierceFrom - COMPLETED" << endl;
}

void CollisionAnalyser::PierceFrom(std::set<Vehicle*>& out_Affected, SystemSolution* sysSol, Vehicle* curVehicle, ElectricTerminal* term, float fLimitTime)
{
	cout << "CollisionAnalyser::PierceFrom - START" << endl;
	PierceFromRecursive(out_Affected, sysSol, curVehicle, term, fLimitTime);
	sysSol->RecomputeVehicleAndSystemDistribution();
	cout << "CollisionAnalyser::PierceFrom - END" << endl;
}

void CollisionAnalyser::InvestigateWaiterAcrossSolution(VehicleSolution* blockerSol,ActionTerminal* blockerACtion, SystemSolution* sysSol, Collision* out_Collision)
{
	//cout << "InvestigateWaiterAcrossSolution - START" << endl;
	//cout << "InvestigateWaiterAcrossSolution - blockerACtion=" << blockerACtion->toString() << endl;
	if (out_Collision != nullptr)
	{
		for (auto itVehicle = sysSol->GetBeginIterator(); itVehicle != sysSol->GetEndIterator(); ++itVehicle)
		{
			if (blockerSol->mRefVehicle->GetName().compare(itVehicle->first->GetName()) == 0)
			{
				//cout << "InvestigateWaiterAcrossSolution - skipping because same vehicle" << endl;
				continue;
			}

			//cout << "InvestigateWaiterAcrossSolution - itVehicle" << itVehicle->first->GetName()<<endl;
			for (auto itAction : itVehicle->second->mPath->mSequenceAction)
			{
				//We found an incomplete solution that is currently being erased
				if (itAction == nullptr)
				{
					break;
				}
				//cout << "InvestigateWaiterAcrossSolution - itAction" << itAction<<endl;
				//cout << "InvestigateWaiterAcrossSolution - itAction" << (itAction->toString())<<endl;
				//Is there a wait at the said terminal?
				if (itAction->getType() == Action::eWait && itAction->getTerminal() != nullptr && itAction->getTerminal()->GetIndex() == blockerACtion->getTerminal()->GetIndex())
				{
					//cout << "InvestigateWaiterAcrossSolution - is wait action at terminal" << endl;
					//Any chance there is an overlap with the reservation?
					if ((blockerACtion->getTimeRange() && itAction->getTimeRange()) > 0)
					{
						//cout << "InvestigateWaiterAcrossSolution - is within time range" << endl;
						//Yes!
						assert(out_Collision != nullptr);
						//cout << "InvestigateWaiterAcrossSolution - found collision!" << endl;
						out_Collision->Set(itVehicle->second, (ActionWait*)itAction, blockerSol, blockerACtion);
						break;
					}
				}
			}
			if (out_Collision->HasCollision())
			{
				break;
			}
		}
	}

	//cout << "InvestigateWaiterAcrossSolution - END" << endl;
}

void CollisionAnalyser::PopNextAlternative(std::vector<AlternativeSearch>& vAltSearch)
{
	//Pop next collision
	if (!pqOCollision.empty())
	{
		SolutionCutter solCutter;
		CollisionWithReference* col = pqOCollision.top();
		pqOCollision.pop();
		//Alternative 1: waiter changes course
		ElectricCircuit* elecCircuitA = mOriginalElectricCircuit->clone();
		SystemSolution* solWaiterChange = mSysSol->clone();
		solWaiterChange->ReplaceElectricCircuit(elecCircuitA);

		//Rend la solution partielle du waiter
		VehicleSolution* solWaiterAlt = solWaiterChange->GetVehiculeSolution(col->GetWaiterSolution()->mRefVehicle);
		//Remove before the wait time of the action
		if (solCutter.cutRoadToTerminal(solWaiterAlt, col->GetWaiterActionWait()->getTerminal()))
		{
			//cout << "CollisionAnalyser::PopNextAlternative - Solution just cut" << endl;
			//cout << "CollisionAnalyser::PopNextAlternative - solWaiterAlt" << *solWaiterAlt << endl;
			//cout << "CollisionAnalyser::PopNextAlternative - elecCircuitA" << *elecCircuitA << endl;

			solWaiterChange->RecomputeSystemDistribution();
		}
		else
		{
			//Should not happens
			cout << "CollisionAnalyser::PopNextAlternative - No change for waiter! Should not happend" << endl;
			assert(false);
		}
		//if (solWaiterAlt->mRefVehicle->GetName().compare("Vehicle_7") == 0)
		//{
		//	cout << "CollisionAnalyser::PopNextAlternative - pushing alt waiter" << endl;
		//	cout << "CollisionAnalyser::PopNextAlternative - ve name " << solWaiterAlt->mRefVehicle->GetName() << endl;
		//	cout << "CollisionAnalyser::PopNextAlternative - elecCircuitA " << *elecCircuitA << endl;
		//	cout << "CollisionAnalyser::PopNextAlternative - solWaiterChange " << *solWaiterChange << endl;
		//}
		vAltSearch.push_back(AlternativeSearch(solWaiterChange, solWaiterAlt->mRefVehicle, elecCircuitA));

		//Alternative 2: blocker change course
		//cout << "col->GetBlockerCanMove()=" << col->GetBlockerCanMove() << endl;
		if (col->GetBlockerCanMove())
		{
			ElectricCircuit* elecCircuitB = mOriginalElectricCircuit->clone();
			SystemSolution* solBlockerChange = mSysSol->clone();
			solBlockerChange->ReplaceElectricCircuit(elecCircuitB);
			VehicleSolution* solBlockerAlt = solBlockerChange->GetVehiculeSolution(col->GetBlockerSolution()->mRefVehicle);
			//Do we have a wait action before?
			if (solCutter.cutRoadToTerminal(solBlockerAlt, col->GetWaiterActionWait()->getTerminal()))
			{
				solBlockerChange->RecomputeSystemDistribution();
			}
			else
			{
				//Should not happens
				cout << "CollisionAnalyser::PopNextAlternative - No change for blocker! Should not happend" << endl;
				assert(false);
			}
			//if (solBlockerAlt->mRefVehicle->GetName().compare("Vehicle_7") == 0)
			//{
			//	cout << "CollisionAnalyser::PopNextAlternative - pushing alt blocker" << endl;
			//	cout << "CollisionAnalyser::PopNextAlternative - ve name " << solBlockerAlt->mRefVehicle->GetName() << endl;
			//	cout << "CollisionAnalyser::PopNextAlternative - elecCircuitB " << *elecCircuitB << endl;
			//	cout << "CollisionAnalyser::PopNextAlternative - solBlockerChange " << *solBlockerChange << endl;
			//}
			vAltSearch.push_back(AlternativeSearch(solBlockerChange, solBlockerAlt->mRefVehicle, elecCircuitB));
		}

		//Clean up the collision as it is not needed anymore
		delete col;
	}
}

void CollisionAnalyser::DestroyAlternativeContent(AlternativeSearch& refAltSearch)
{
	if (refAltSearch.mSystemSolution != NULL)
	{
		delete refAltSearch.mSystemSolution;
		refAltSearch.mSystemSolution = NULL;
		//cout << "mSystemSolution Deleted!" << endl;
	}
	if (refAltSearch.mElectricCircuit != NULL)
	{
		delete refAltSearch.mElectricCircuit;
		refAltSearch.mElectricCircuit = NULL;
		//cout << "mElectricCircuit Deleted!" << endl;
	}
}

CollisionAnalyser::CollisionAnalyser(SystemSolution* sysSol, SystemSolution* sysSolRef, ElectricCircuit const* const  simulatedCircuit)
:mSysSol(sysSol)
,mSysSolRef(sysSolRef)
,mOriginalElectricCircuit(simulatedCircuit)
{

}

CollisionAnalyser::~CollisionAnalyser()
{
	PopAllCollision();
}

ostream& operator<<(ostream& os, const AlternativeSearch& dt)
{
	os << "Alternative search" << endl;
	os << "mElectricCircuit->size()" << dt.mElectricCircuit->size() << endl;
	os << "mRefVehicle->GetName()" << dt.mRefVehicle->GetName() << endl;
	os << "*mSystemSolution" << *dt.mSystemSolution << endl << endl;
	os << "mSystemSolution->HasReachedEnd()" << dt.mSystemSolution->HasReachedEnd() << endl << endl;
	return os;
}
