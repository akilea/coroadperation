#include "ObjectiveFunctionPenality.h"
#include "SystemSolution.h"
#include "VehicleSolution.h"
#include "Path.h"
#include <assert.h>

ObjectiveFunctionPenality::ObjectiveFunctionPenality(SystemSolution* refReferenceSystemSolution)
	:mRefReferenceSystemSolution(refReferenceSystemSolution)
{
	assert(refReferenceSystemSolution != nullptr);
}

ObjectiveFunctionPenality::~ObjectiveFunctionPenality()
{
}

float ObjectiveFunctionPenality::Evaluate(SystemSolution const* const in_A)
{
	float fPenalite = 1.0f;
	float fNbSolution = 1.0f;
	if (in_A != nullptr)
	{
		fPenalite = 0.0f;
		fNbSolution = 0.0f;
		for (auto it = in_A->GetBeginIterator(); it != in_A->GetEndIterator(); ++it)
		{
			//Find reference vehicle solution
			
			auto refVehicleSolution = mRefReferenceSystemSolution->GetVehiculeSolution(it->first);
			if (refVehicleSolution != nullptr)
			{
				float fCurrent = it->second->mPath->getDuration()/refVehicleSolution->mPath->getDuration();
				fPenalite += fCurrent;
				fNbSolution += 1.0f;
			}
			else
			{
				cout << "ObjectiveFunctionPenality::Evaluate - CANNOT FIND SOLUTION - CRITICAL ERROR" << endl;
			}
		}
	}
	return fPenalite/fNbSolution;
}