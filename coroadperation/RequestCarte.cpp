#include "RequestCarte.h"
#include "ElectricCircuit.h"
#include "ElectricTerminal.h"
#include "SimulationSetting.h"
#include "Vehicle.h"

RequestCarte::RequestCarte()
{
}


RequestCarte::~RequestCarte()
{
}

void RequestCarte::AlwaysMinimizeWaitingHorizon()
{
	fMinimizeWaitingHorizon = INFINITY;
}

void RequestCarte::NeverMinimizeWaitingHorizon()
{
	fMinimizeWaitingHorizon = -1.f;
}

const bool RequestCarte::IsAcceptableDistance(double dDistance)
{
	return dDistance < SimulationSetting::D_ACCEPTABLE_MAX_DISTANCE && dDistance > SimulationSetting::D_ACCEPTABLE_MIN_DISTANCE;
}

void RequestCarte::UtilConstructIndexLieuFromStationToIgnore(std::unordered_set<int>& out_Result) const
{
	for (auto var : this->mOptionnal_StationToIgnore)
	{
		out_Result.insert(var->GetIndexLieu());
	}
}

ElectricTerminal * RequestCarte::IndexTerminalToTerminal(int inIdIndexTerminal) const
{
	return mRefElectricCircuit->GetByIndexTerminal(inIdIndexTerminal);
}

ostream& operator<<(ostream& os, const RequestCarte& dt)
{
	os << "bCreateActionWaiting=" << dt.bCreateActionWaiting << endl;
	os << "fMinimizeWaitingHorizon=" << dt.fMinimizeWaitingHorizon << endl;
	os << "mRefElectricCircuit=" << dt.mRefElectricCircuit << endl;
	os << "mRefVehicleRequested=" << *(dt.mRefVehicleRequested )<< endl;
	//os << "mOptionnal_StationToIgnore=" << dt.mOptionnal_StationToIgnore << endl;
	return os;
}
