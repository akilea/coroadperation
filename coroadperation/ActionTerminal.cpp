#include "ActionTerminal.h"
#include "ElectricCircuit.h"
#include "ElectricTerminal.h"

ActionTerminal::ActionTerminal(ElectricCircuit* const inRefCircuit, ElectricTerminal* inTerminal, ElectricReservation* inRefReservation)
:mRefReservation(inRefReservation)
,Action(inRefCircuit, inTerminal)
{

}

ActionTerminal::~ActionTerminal()
{
}


void ActionTerminal::setCommitedReservation(ElectricReservation* inRefReservation) 
{
	mRefReservation = inRefReservation;
}



Action * ActionTerminal::clone() const
{
	return CopyActionDataTo(new ActionTerminal(this->mRefCircuit,this->getTerminal(),this->getCommitedReservation()->clone()));
}
