#include "ObjectiveFunctionWorse.h"
#include "VehicleSolution.h"
#include "SystemSolution.h"
#include "Path.h"

ObjectiveFunctionWorse::ObjectiveFunctionWorse()
{
}

ObjectiveFunctionWorse::~ObjectiveFunctionWorse()
{
}

float ObjectiveFunctionWorse::Evaluate(SystemSolution const* const in_A)
{
	float fWorst = 0.0f;
	for (auto it = in_A->GetBeginIterator(); it != in_A->GetEndIterator();++it)
	{
		float fCurrent = it->second->mPath->getDuration();
		if (fCurrent > fWorst)
		{
			fWorst = fCurrent;
		}
	}
	return fWorst;
}

float ObjectiveFunctionWorse::EvaluateBest(SystemSolution const* const in_A)
{
	float fBest = 999999999999.9f;
	for (auto it = in_A->GetBeginIterator(); it != in_A->GetEndIterator(); ++it)
	{
		float fCurrent = it->second->mPath->getDuration();
		if (fCurrent < fBest)
		{
			fBest = fCurrent;
		}
	}
	return fBest;
}
