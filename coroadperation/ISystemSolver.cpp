#include "ISystemSolver.h"
#include "ScenarioData.h"
#include "PerformanceAnalysis.h"
#include "SystemSolution.h"
#include "Vehicle.h"
#include <iostream>

ISystemSolver::ISystemSolver(IObjectiveFunction * inObjectiveFunction, IVehicleSolver * inVehicleSolver, ElectricCircuit* inElectricCircuit, double in_dComputationTimeCap)
:mObjectiveFunction(inObjectiveFunction), mVehicleSolver(inVehicleSolver)
{
	mSolverData = new ScenarioData();
	mSolverData->mElectricCircuit = inElectricCircuit;

	mPerformanceAnalysis = new PerformanceAnalysis(in_dComputationTimeCap);
}

ISystemSolver::~ISystemSolver()
{
	//TODO:have a different object that shallow cop the data
	//delete mSolverData;
	delete mPerformanceAnalysis;
}

void ISystemSolver::SetTraceExporterName(string traceName)
{
	m_traceExporter.SetExportName(traceName);
}

void ISystemSolver::AddVehicle(Vehicle * newVehicle)
{
	if (mIsActivated)
	{
		//cout << this->desc.sShort << " adding " << newVehicle->GetName() << endl;
		AddVehicleInternal(newVehicle);
	}
}

void ISystemSolver::RemoveVehicle(Vehicle * newVehicle)
{
	if (mIsActivated)
	{
		RemoveVehicleInternal(newVehicle);
	}
}

void ISystemSolver::SetActivation(bool bActivation) {
	string strActivation = bActivation ? "Activating " :"Deactivating ";
	cout << strActivation << desc.sShort << endl;
	mIsActivated = bActivation;
}

bool ISystemSolver::HasReachdedRunningTimeCap() const
{
	return GetPerformanceAnalysis()->HasReachdedRunningTimeCap();
}

bool ISystemSolver::CheckTimeCapAndDisable()
{
	SetActivation(HasReachdedRunningTimeCap());
	return HasActivation();
}
