#include "ElectricReservation.h"



ElectricReservation::ElectricReservation(TimeRange in_TimeRange, Vehicle * const pVehicle)
:mTimeRange(in_TimeRange)
,m_pVehicle(pVehicle)
{
}

ElectricReservation::ElectricReservation(TimeUnit in_start, TimeUnit in_end, Vehicle * const pVehicle)
	: mTimeRange(in_start,in_end)
	, m_pVehicle(pVehicle) {
}

ElectricReservation::ElectricReservation(const ElectricReservation & otherReservation)
: mTimeRange(otherReservation.GetTimeRange())
, m_pVehicle(otherReservation.m_pVehicle)
{

}


ElectricReservation::~ElectricReservation()
{
}

ElectricReservation* ElectricReservation::clone() const
{
	return new ElectricReservation(this->GetTimeRange(), this->m_pVehicle);
}

ostream & operator<<(ostream & os, const ElectricReservation & dt)
{
	os << dt.mTimeRange;
	return os;
}

ostream & operator<<(ostream & os, const ElectricReservationPtr & dt)
{
	return os << *dt;
}
