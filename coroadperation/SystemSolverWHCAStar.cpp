#include "SystemSolverWHCAStar.h"

#include "SystemSolverCAStar.h"
#include "PerformanceAnalysis.h"
#include "SystemSolver.h"
#include "ISystemSolver.h"
#include "ElectricCircuit.h"
#include "ScenarioData.h"
#include "SystemSolution.h"
#include "IVehicleSolver.h"
#include "PerformanceAnalysis.h"
#include "IObjectiveFunction.h"
#include "IArrangement.h"
#include "Vehicle.h"
#include "VehicleSolution.h"
#include "VehicleSolverCooperation.h"
#include "SolutionCutter.h"
#include "TraceConvergenceExporter.h"
#include <assert.h>
#include <iostream>
#include <algorithm>

SystemSolverWHCAStar::SystemSolverWHCAStar(IObjectiveFunction* inObjectiveFunction, IVehicleSolver* inVehicleSolver, IArrangement* inArrangment, ElectricCircuit* inElectricCircuit, double in_dComputationTimeCap, float infWindowWidth, float infResolveStep)
	:SystemSolverArranged(inObjectiveFunction, inVehicleSolver, inArrangment, inElectricCircuit, in_dComputationTimeCap)
	, mfWindowWidth(infWindowWidth)
	, mfResolveStep(infResolveStep)
{

}


SystemSolverWHCAStar::~SystemSolverWHCAStar()
{
}

void SystemSolverWHCAStar::SolveSystem()
{
	if (!mIsActivated)
	{
		return;
	}

	VehicleSolverCooperation* pVSCoop = dynamic_cast<VehicleSolverCooperation*>(mVehicleSolver);
	if (pVSCoop == NULL)
	{
		cerr << "SystemSolverWHCAStar needs a VehicleSolverCooperation" << endl;
		assert(false);
	}

	//Init
	mPerformanceAnalysis->StartExperiment();
	m_traceExporter.SetExportName(this->GetDescription().sShort);
	//ResetSolver();
	SolutionCutter solCutter;
	float fCurrentWindowStart = mfMinimumDepartureTime;
	float fCurrentWindowEnd = fCurrentWindowStart + mfWindowWidth;
	bool needToSolve = true;
	//Main loop. We iterate over the time windows
	while (needToSolve)
	{
		//Will yield true for first iteration
		//False for future iterations
		needToSolve = mSystemSolution->NbVehiculeSolution() == 0;

		//Strip the current solution from actions that occur after the start window
		bool wasChanged = solCutter.cutFromPreviousCommitment(mSystemSolution, fCurrentWindowStart);
		//bool wasChanged = mSystemSolution->keepPartialSolution(fCurrentWindowStart);
		if (needToSolve || wasChanged || !mSystemSolution->HasReachedEnd())
		{
			needToSolve = true;
		}

		//Exit before computing if needed
		if(!needToSolve)
		{
			break;
		}

		//Invalid previous solution
		GetCurrentSystemSolution()->mSystemPathDistribution.SetInf();
		
		//Clone electric circuit from current state
		ElectricCircuit* simulatedCircuit = mSolverData->mElectricCircuit->clone();
		mVehicleSolver->ReplaceRefElectricCircuit(simulatedCircuit);

		//Update vehicle solver horizon collision
		pVSCoop->SetCollisionHorizon(fCurrentWindowEnd);

		//Create new CA* solver with arrangment
		double dTimeLeft = GetPerformanceAnalysis()->GetResult().dTimeCap - GetPerformanceAnalysis()->RunningTime();
		SystemSolverCAStar* solverCAStar = new SystemSolverCAStar(mObjectiveFunction, mVehicleSolver, mArrangement, simulatedCircuit, dTimeLeft);
		//solverCAStar->GetPerformanceAnalysis()->SetActivation(false);

		//Clone the partial solution and pass it to the CAStar
		ScenarioData* dataCAStar = solverCAStar->ModifyOptionnalScenarioData();
		dataCAStar->SetAndOwnPartialSolution(mSystemSolution->clone());
		//Add all vehicles, solver CA will take car of ordering
		for (unsigned int j = 0; j < mSolverData->mVecVehicle.size(); ++j)
		{
			solverCAStar->AddVehicle(mSolverData->mVecVehicle[j]);
		}
		//Solve system
		solverCAStar->SolveSystem();

		//We reset the solver but keep the previous solution
		ResetSolver();
		mBestSolver = solverCAStar;
		mBestCircuit = simulatedCircuit;

		//Prepare next time
		fCurrentWindowStart += mfResolveStep;
		fCurrentWindowEnd    = fCurrentWindowStart + mfWindowWidth;
		mSystemSolution	     = GetCurrentSystemSolution();

		//cout << "WHCA* start window: " << fCurrentWindowStart << endl;
		//cout << "WHCA* end window: " << fCurrentWindowEnd << endl;
		//cout << "Total: " << mSystemSolution->mSystemPathDistribution.Total() << endl;
		//cout << "WHCA* current time: " << mPerformanceAnalysis->RunningTime() << endl << endl;

		mPerformanceAnalysis->AddPassCount();

		//Stop if you hit a wall
		needToSolve &= !mSystemSolution->IsImpossible();
		if (mSystemSolution->IsImpossible())
		{
			cout << "WHCA* impossible!        " << endl;
			assert(false);
			return;
		}

		//Stop when hitting time cap
		needToSolve &= !HasReachdedRunningTimeCap();
		if(HasReachdedRunningTimeCap())
		{
			cout << "Early break!" << endl;
			break;
		}
		STrace tr;
		tr.valueA = mBestSolver->GetCurrentSystemSolution()->mSystemPathDistribution.Total();
		tr.valueB = fCurrentWindowEnd;
		tr.sizeA = mBestSolver->GetCurrentSystemSolution()->NbVehiculeSolution();
		m_traceExporter.Trace(tr);
	}

	m_traceExporter.ExportFlatWindowed();

	//Add a desription when we have a solution
	if (mBestSolver != nullptr)
	{
		mBestSolver->GetCurrentSystemSolution()->mDescription = mSystemSolution->mDescription;
		if (mBestSolver->GetCurrentSystemSolution()->IsImpossible())
		{
			mSystemSolution->mSystemPathDistribution.SetInf();
		}
	}

	mPerformanceAnalysis->StopExperiment(GetCurrentSystemSolution()->mSystemPathDistribution);
	if (mBestSolver != nullptr)
	{
		mBestSolver->GetPerformanceAnalysis()->CopyResultFrom(mPerformanceAnalysis->GetResult());
		mBestSolver->GetCurrentSystemSolution()->mDescription = mSystemSolution->mDescription;
	}
}

void SystemSolverWHCAStar::AddVehicleInternal(Vehicle * newVehicle)
{
	mfMinimumDepartureTime = min(mfMinimumDepartureTime, newVehicle->GetDepartureTime());
	SystemSolverArranged::AddVehicleInternal(newVehicle);
}
