	#include "VehicleSolverWait.h"

#include "IMapSolver.h"
#include "VehicleSolution.h"
#include "ElectricCircuit.h"
#include "RequestCarte.h"
#include "Path.h"
#include "IObjectiveFunction.h"
#include "SolutionCutter.h"

VehicleSolverWait::VehicleSolverWait(IMapSolver const *const inCarte, IObjectiveFunction* inObjectiveFunction, ElectricCircuit* inElectricCircuit)
	:IVehicleSolver(inObjectiveFunction, inElectricCircuit)
	, mRefCarte(inCarte)
	, mRefObjectiveFunction(inObjectiveFunction)
{
}


VehicleSolverWait::~VehicleSolverWait()
{
}

void VehicleSolverWait::SolveFor(Vehicle * inVehicleToSolve, VehicleSolution * out_RefSolution)
{
	RequestCarte request;
	request.mRefElectricCircuit = mRefElectricCircuit;
	request.mRefVehicleRequested = inVehicleToSolve;
	if (m_bForceCooperation)
	{
		request.fMinimizeWaitingHorizon = INFINITY;
	}
	else
	{
		request.NeverMinimizeWaitingHorizon();
	}
	request.bCreateActionWaiting = true;
	mRefCarte->calculerTrajet((*out_RefSolution->mPath), request);
	//cout << "VehicleSolverWait::SolveFor::SolveFor -Solution after (detail)" << endl;
	//out_RefSolution->displayTrajectoryDetail(cout) << endl;
	//cout << *out_RefSolution << endl << endl;
	//cout << "VehicleSolverWait::SolveFor::SolveFor -Solution after (detail end)" << endl;

	SolutionCutter solCutter;
	solCutter.addRemainingReservation(out_RefSolution);
}
