#pragma once
class IAlgoRun
{
public:
	IAlgoRun();
	virtual ~IAlgoRun();

	virtual int Run(int argc, const char** argv) = 0;
};

