#include "SystemSolverPCARL.h"

#include "VehicleSolverAbsoluteOptimal.h"
#include "VehicleSolverWait.h"
#include "VehicleSolution.h"
#include "Path.h"
#include "CollisionWithReference.h"
#include "CollisionAnalyser.h"
#include "Vehicle.h"
#include "ActionTerminal.h"
#include <algorithm>

SystemSolverPCARL::SystemSolverPCARL(IObjectiveFunction* inObjectiveFunction, IVehicleSolver* inVehicleSolver, ElectricCircuit* inElectricCircuit, double in_dComputationTimeCap, ISystemSolver* inReferenceSystemSolver, bool in_bIsGreedy, bool isFromStartEverytime, bool isForceCooperationAsStartingSolution)
:SystemSolverImmediate(inObjectiveFunction, inVehicleSolver, inElectricCircuit, in_dComputationTimeCap)
, mReferenceSystemSolver(inReferenceSystemSolver)
,m_bIsGreedy(in_bIsGreedy) //G vs pas de G
,m_bFromStartEverytime(isFromStartEverytime) //PCAR vs PNCARR
,m_bForceCooperationAsStartingSolution(isForceCooperationAsStartingSolution) // C vs pas de C
{
}

SystemSolverPCARL::~SystemSolverPCARL()
{

}

void SystemSolverPCARL::SetVariation(bool isGreedy, bool isFromStartEverytime, bool isForceCooperationAsStartingSolution)
{
    m_bIsGreedy = isGreedy;
    m_bFromStartEverytime = isFromStartEverytime;
    m_bForceCooperationAsStartingSolution = isForceCooperationAsStartingSolution;
}

void SystemSolverPCARL::SolveSystem()
{
    if (!mIsActivated)
    {
        return;
    }
    VehicleSolverWait* ws = dynamic_cast<VehicleSolverWait*>(this->GetVehicleSolver());
    if (ws == nullptr)
    {
        cout << "SystemSolverPCARL::SolveSystem - ws is null, wtf!!" << endl;
        assert(false);
    }
    else
    {
        mPerformanceAnalysis->StartExperiment();
        if (m_bFromStartEverytime)
        {
            //Reset solution, cheaply solve all, then local repair for the last vehicle
            ResetSolution();
            mSolverData->mVecVehicle.clear();
            mSolverData->mElectricCircuit->clearAllReservation();
            //cout << "Ajout des v�hicules dans Immediate en recommen�ant" << endl;
            for each (auto vec in m_vecVehicleAccumulator)
            {
                if (!HasReachdedRunningTimeCap())
                {
                    ws->m_bForceCooperation = m_bForceCooperationAsStartingSolution;
                    m_bMesurePerformance = false;
                    SystemSolverImmediate::AddVehicleInternal(vec);
                    m_bMesurePerformance = true;
                    ws->m_bForceCooperation = true;
                }
            }
            //cout << "FIN - Ajout des v�hicules dans Immediate" << endl;
            if (!HasReachdedRunningTimeCap())
            {
                SolveForOne(m_vecVehicleAccumulator.back()); //Local repair on current
            }
        }
        else
        {
            //Stack solutions on top
            //cout << "Ajout des v�hicules dans Immediate en empilant" << endl;
            for each (auto vec in m_vecVehicleAccumulator)
            {
                if (!HasReachdedRunningTimeCap())
                {
                    ws->m_bForceCooperation = m_bForceCooperationAsStartingSolution;
                    m_bMesurePerformance = false;
                    SystemSolverImmediate::AddVehicleInternal(vec);
                    m_bMesurePerformance = true;
                    ws->m_bForceCooperation = true;
                    SolveForOne(vec);
                }
            }
            //cout << "FIN - Ajout des v�hicules dans Immediate" << endl;
        }
        if (HasReachdedRunningTimeCap())
        {
            ResetSolution();
        }
        mPerformanceAnalysis->StopExperiment(GetCurrentSystemSolution()->mSystemPathDistribution);
    }
}


void SystemSolverPCARL::AddVehicleInternal(Vehicle* newVehicle)
{
    m_vecVehicleAccumulator.push_back(newVehicle);
}

void SystemSolverPCARL::SolveForOne(Vehicle* newVehicle)
{
if (m_bIsGreedy)
    {
        this->AddVehicleInternalCorrectBiggestCollisionFirst(newVehicle);
    }
    else
    {
        this->AddVehicleInternalCorrectBiggestImprovment(newVehicle);
    }
}

void SystemSolverPCARL::AddVehicleInternalCorrectBiggestCollisionFirst(Vehicle* newVehicle)
{
    if (HasReachdedRunningTimeCap())
    {
        return;
    }
    
    m_traceExporter.SetExportName(this->GetDescription().sSolverType);
    if(this->mSolverData->mVecVehicle.size() >= 2 && IsWorthComputing(newVehicle))
    {
        bool isImproving = true;
        while (isImproving)
        {
            isImproving = false;
            std::set<Vehicle*> setAdmissible;
            setAdmissible.insert(newVehicle);
            CollisionAnalyser* colAnalyser = new CollisionAnalyser(mSystemSolution, this->GetCurrentSystemSolution(), this->mSolverData->mElectricCircuit);
            colAnalyser->FindAndSortWaiting(setAdmissible, newVehicle->GetDepartureTime(), newVehicle);
            while (colAnalyser->PeakNextCollision() != nullptr && colAnalyser->PeakNextCollision()->HasCollision())
            {
                //cout << "SystemSolverPCARL::SolveSystem - Collision baby!" << endl;
                //cout << mSolverData->mVecVehicle.size() << std::endl;
                //cout << "SystemSolverLRCAStar::SolveSystem - Nb reservation before Piercing" << mSolverData->mElectricCircuit->computeTotalReservation() << endl;
                std::vector<AlternativeSearch> vecAlternativeScenario;
                colAnalyser->PopNextAlternative(vecAlternativeScenario); //Modified coies of solution and elec inside Alternative
                //Run alt search
                //cout << "Alternative amount:" << vecAlternativeScenario.size() << endl;
                for (int i = 0; i < vecAlternativeScenario.size(); ++i)
                {
                    //cout << "Alternative system solution" << endl;
                    //cout << vecAlternativeScenario[i].mRefVehicle << endl;
                    //cout << *vecAlternativeScenario[i].mSystemSolution << endl;
                    //cout << *vecAlternativeScenario[i].mElectricCircuit << endl;
                    //cout << "END alternative system solution" << endl;
                    SystemSolution* ptrOrignalSolution = mSystemSolution;
                    ElectricCircuit* ptrOrignalCircuit = mSolverData->mElectricCircuit;
                    this->ForceSolutionAndCircuit(vecAlternativeScenario[i].mSystemSolution, vecAlternativeScenario[i].mElectricCircuit);
                    Vehicle* vehicleToReAdd = this->FindVehiclePointer(vecAlternativeScenario[i].mRefVehicle->GetName());
                    m_bMesurePerformance = false;
                    SystemSolverImmediate::AddVehicleInternal(vehicleToReAdd); //Re-add the vehicle removed
                    m_bMesurePerformance = true;
                    STrace tr;
                    tr.valueA = ptrOrignalSolution->mSystemPathDistribution.Total();
                    tr.valueB = this->GetCurrentSystemSolution()->mSystemPathDistribution.Total();
                    tr.sizeA = ptrOrignalSolution->NbVehiculeSolution();
                    tr.sizeB = this->GetCurrentSystemSolution()->NbVehiculeSolution();
                    m_traceExporter.Trace(tr);

                    if (mObjectiveFunction->IsBetterThan(this->GetCurrentSystemSolution(), ptrOrignalSolution))
                    {
                        //Swap with so old scenario and circuit will get deleted.
                        vecAlternativeScenario[i].mSystemSolution = ptrOrignalSolution;
                        vecAlternativeScenario[i].mElectricCircuit = ptrOrignalCircuit;
                        isImproving = true;
                        //cout << "Improved!" << endl;
                        break;
                    }
                    else
                    {
                        //Set back original solution and circuit.
                        this->ForceSolutionAndCircuit(ptrOrignalSolution, ptrOrignalCircuit);
                    }
                    colAnalyser->DestroyAlternativeContent(vecAlternativeScenario[i]);
                    //Insert best solution. Either this repairs the solution OR it takes the best one.
                    //cout << mBestSolution << endl;
                    //cout << *mBestSolution << endl;
                    //cout << mBestCircuit << endl;
                    //cout << mBestCircuit->size() << endl;
                    //cout << *mBestCircuit << endl;
                }
                //Stops as soon as we improved
                if (isImproving)
                {
                    isImproving = false;
                    break;
                }
            }
        }
        m_traceExporter.ExportFlat();
    }
}

void SystemSolverPCARL::AddVehicleInternalCorrectBiggestImprovment(Vehicle* newVehicle)
{
    m_traceExporter.SetExportName(this->GetDescription().sSolverType);

    if (this->mSolverData->mVecVehicle.size() >= 2 && IsWorthComputing(newVehicle))
    {
        bool isImproving = true;
        while (isImproving)
        {
            //cout << "Starting improved..." << endl;
            isImproving = false;
            std::set<Vehicle*> setAdmissible;
            setAdmissible.insert(newVehicle);
            //cout << "Avant collision" << endl;
            CollisionAnalyser* colAnalyser = new CollisionAnalyser(mSystemSolution, this->GetCurrentSystemSolution(), this->mSolverData->mElectricCircuit);
            colAnalyser->FindAndSortWaiting(setAdmissible, newVehicle->GetDepartureTime(), newVehicle);
            //cout << "Apr�s collision" << endl;

            //Best solution init
            SystemSolution*  ptrBestSolution = mSystemSolution->clone();
            ElectricCircuit* ptrBestCircuit = mSolverData->mElectricCircuit->clone();
            ptrBestSolution->ReplaceElectricCircuit(ptrBestCircuit);

            SystemSolution* ptrOrignalSolution = mSystemSolution;
            ElectricCircuit* ptrOrignalCircuit = mSolverData->mElectricCircuit;

            while (colAnalyser->PeakNextCollision() != nullptr && colAnalyser->PeakNextCollision()->HasCollision())
            {
                //cout << "SystemSolverPCARL::SolveSystem - Collision baby!" << endl;
                //cout << mSolverData->mVecVehicle.size() << std::endl;
                //cout << "SystemSolverLRCAStar::SolveSystem - Nb reservation before Piercing" << mSolverData->mElectricCircuit->computeTotalReservation() << endl;
                std::vector<AlternativeSearch> vecAlternativeScenario;
                colAnalyser->PopNextAlternative(vecAlternativeScenario); //Modified coies of solution and elec inside Alternative
                //Run alt search
                //cout << "Alternative amount:" << vecAlternativeScenario.size() << endl;
                for (int i = 0; i < vecAlternativeScenario.size(); ++i)
                {
                    this->ForceSolutionAndCircuit(vecAlternativeScenario[i].mSystemSolution, vecAlternativeScenario[i].mElectricCircuit);
                    Vehicle* vehicleToReAdd = this->FindVehiclePointer(vecAlternativeScenario[i].mRefVehicle->GetName());
                    m_bMesurePerformance = false;
                    SystemSolverImmediate::AddVehicleInternal(vehicleToReAdd);
                    m_bMesurePerformance = true;
                    STrace tr;
                    tr.valueA = ptrOrignalSolution->mSystemPathDistribution.Total();
                    tr.valueB = this->GetCurrentSystemSolution()->mSystemPathDistribution.Total();
                    tr.sizeA = ptrOrignalSolution->NbVehiculeSolution();
                    tr.sizeB = this->GetCurrentSystemSolution()->NbVehiculeSolution();
                    m_traceExporter.Trace(tr);
                    if (mObjectiveFunction->IsBetterThan(this->GetCurrentSystemSolution(), ptrBestSolution))
                    {
                        //Delete old best
                        delete ptrBestSolution;
                        delete ptrBestCircuit;

                        //Replace by new best
                        ptrBestSolution = mSystemSolution;
                        ptrBestCircuit = mSolverData->mElectricCircuit;
                        ptrBestSolution->ReplaceElectricCircuit(ptrBestCircuit);
                        isImproving = true;
                        //cout << "Improved!" << endl;
                        break;
                    }
                    else
                    {
                        //Destroy new avenue
                        colAnalyser->DestroyAlternativeContent(vecAlternativeScenario[i]);
                    }
                    //Put back original solution
                    this->ForceSolutionAndCircuit(ptrOrignalSolution, ptrOrignalCircuit);
                    //Insert best solution. Either this repairs the solution OR it takes the best one.
                    //cout << mBestSolution << endl;
                    //cout << *mBestSolution << endl;
                    //cout << mBestCircuit << endl;
                    //cout << mBestCircuit->size() << endl;
                    //cout << *mBestCircuit << endl;
                }
                //if (HasReachdedRunningTimeCap())
                //{
                //    cout << "Early break!" << endl;
                //    isImproving = false;
                //    break;
                //}
            }
            if (isImproving)
            {
                this->ForceSolutionAndCircuit(ptrBestSolution, ptrBestCircuit);
            }
            else
            {
                delete ptrBestSolution;
                delete ptrBestCircuit;
            }
            //if (HasReachdedRunningTimeCap())
            //{
            //    cout << "Early break!" << endl;
            //    isImproving = false;
            //    break;
            //}
        }
        m_traceExporter.ExportFlat();
    }
}

bool SystemSolverPCARL::IsWorthComputing(Vehicle* vehicule) const
{
    VehicleSolution* refVehiculeSolution = mReferenceSystemSolver->GetCurrentSystemSolution()->GetVehiculeSolution(vehicule);
    float inReferenceCost = refVehiculeSolution->mPath->mTotalPathDistribution.Total();
    VehicleSolution* waitVehiculeSolution = GetCurrentSystemSolution()->GetVehiculeSolution(vehicule);
    if (!waitVehiculeSolution->HasReachedEnd())
    {
        cout << "SystemSolverLRCAStar::IsWorthComputing - waitVehiculeSolution is incomplete before strippping it!" << endl;
        assert(false);
    }

    float inWaitingCost = waitVehiculeSolution->mPath->mTotalPathDistribution.Total();
    float fEpsilon = 1.0f;
    //cout << inReferenceCost << endl;
    //cout << inWaitingCost << endl;
    return (inWaitingCost - inReferenceCost) > fEpsilon;
}

Vehicle* SystemSolverPCARL::FindVehiclePointer(string strName)
{
    for (int i = 0; i < mSolverData->mVecVehicle.size(); ++i)
    {
        if (mSolverData->mVecVehicle[i]->GetName() == strName)
        {
            return mSolverData->mVecVehicle[i];
        }
    }
    return nullptr;
}

void SystemSolverPCARL::ForceSolutionAndCircuit(SystemSolution* newSolution, ElectricCircuit* newCircuit)
{
    this->mSystemSolution = newSolution;
    this->mSolverData->mElectricCircuit = newCircuit;
    mSystemSolution->ReplaceElectricCircuit(this->mSolverData->mElectricCircuit);
    GetVehicleSolver()->ReplaceRefElectricCircuit(newCircuit);
}
