#pragma once
#include <string>
#include <vector>
using namespace std;

struct STrace
{
	float valueA;
	float valueB;
	int sizeA; 
	int sizeB;
};
class TraceConvergenceExporter
{
	public:
		static bool m_isEnabled;
	private:
		vector<STrace> m_vecTrace;
		float m_fAsIsOrder = 0.0f; //Like PNCARR
		string m_strAlgoName = "";
		string m_strExportName = "Trace.csv";


		bool FileExists()const;
	public:
		TraceConvergenceExporter();
		void SetExportName(string name);
		void Trace(STrace in_trace);

		//void Export(string fileName) const;
		void ExportFlat() const;
		void ExportFlatWindowed() const;

};

