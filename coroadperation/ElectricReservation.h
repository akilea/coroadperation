#pragma once
#include "srcMap/StructureCarte.h"

class Vehicle;

class ElectricReservation;
typedef ElectricReservation* ElectricReservationPtr;

class ElectricReservation
{
public:
	TimeRange mTimeRange;
	Vehicle * const m_pVehicle = nullptr;

public:
	ElectricReservation(TimeRange in_TimeRange, Vehicle  * const pVehicle);
	ElectricReservation(TimeUnit in_start, TimeUnit in_end, Vehicle  * const pVehicle);
	ElectricReservation(const ElectricReservation& otherReservation);
	~ElectricReservation();
	ElectricReservation* clone() const;


	TimeRange GetTimeRange() const { return mTimeRange; }


	//Comparison operator so AVL tree works
	bool operator== (const ElectricReservation &c2) const
	{
		return GetTimeRange().mStart == c2.GetTimeRange().mStart;
	}

	bool operator!= (const ElectricReservation &c2) const
	{
		return GetTimeRange().mStart != c2.GetTimeRange().mStart;
	}

	bool operator> (const ElectricReservation &c2) const
	{
		return GetTimeRange().mStart > c2.GetTimeRange().mStart;
	}

	bool operator>= (const ElectricReservation &c2) const
	{
		return GetTimeRange().mStart >= c2.GetTimeRange().mStart;
	}

	bool operator< (const ElectricReservation &c2) const
	{
		return GetTimeRange().mStart < c2.GetTimeRange().mStart;
	}

	bool operator<= (const ElectricReservation &c2) const
	{
		return GetTimeRange().mStart <= c2.GetTimeRange().mStart;
	}

	friend ostream& operator<<(ostream& os, const ElectricReservation& dt);
	friend ostream& operator<<(ostream& os, const ElectricReservationPtr& dt);
};

inline int ElectricReservationPointerCompare(const  ElectricReservationPtr& u, const ElectricReservationPtr & v)
{
	if (u->GetTimeRange().mStart < v->GetTimeRange().mStart) return -1; else if (v->GetTimeRange().mStart < u->GetTimeRange().mStart) return 1; else return 0;
}


struct ElectricResservationCompare
{
	bool operator()(const ElectricReservationPtr& lhs, const ElectricReservationPtr& rhs) const
	{
		return *lhs > *rhs;
	}
};

ostream& operator<<(ostream& os, const ElectricReservation& dt);
ostream& operator<<(ostream& os, const ElectricReservationPtr& dt);

