#pragma once
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

//Partially inspired from
//https://stackoverflow.com/questions/1120140/how-can-i-read-and-parse-csv-files-in-c

class CSVRow
{
public:
	inline std::string const& operator[](std::size_t index) const
	{
		return m_data[index];
	}

	inline std::size_t size() const
	{
		return m_data.size();
	}
	void readNextRow(std::istream& str);
private:
	std::vector<std::string> m_data;
};

std::istream& operator>>(std::istream& str, CSVRow& data);

