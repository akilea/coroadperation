#include "SystemSolverCAStar.h"

#include "SystemSolverImmediate.h"
#include "SystemSolver.h"
#include "ISystemSolver.h"
#include "ElectricCircuit.h"
#include "ScenarioData.h"
#include "SystemSolution.h"
#include "IVehicleSolver.h"
#include "PerformanceAnalysis.h"
#include "IObjectiveFunction.h"
#include "IArrangement.h"
#include <assert.h>
#include <iostream>
#include "TraceConvergenceExporter.h"

SystemSolverCAStar::SystemSolverCAStar(IObjectiveFunction* inObjectiveFunction, IVehicleSolver* inVehicleSolver, IArrangement* inArrangment, ElectricCircuit* inElectricCircuit, double in_dComputationTimeCap)
:SystemSolverArranged(inObjectiveFunction, inVehicleSolver, inArrangment, inElectricCircuit, in_dComputationTimeCap)
{
}


SystemSolverCAStar::~SystemSolverCAStar()
{
	ResetSolver();
}

void SystemSolverCAStar::SolveSystem()
{
	if (!mIsActivated)
	{
		return;
	}
	mPerformanceAnalysis->StartExperiment();
	m_traceExporter.SetExportName(this->GetDescription().sShort);

	ResetSolver();

	//std::cout << "Before " << mSolverData->mElectricCircuit->computeTotalReservation() << endl;

	//Find the best priority by getting the sequences
	mArrangement->startSequence(&mSolverData->mVecVehicle);
	std::vector<Vehicle*>* curArrangement = mArrangement->GetNextSequence();
	int i = 0;
	while(curArrangement != nullptr)
	{
		//cout << "Testing  arrangement" << i << endl;
		//++i;
		//Create a new electric circuit from current state
		ElectricCircuit* simulatedCircuit = mSolverData->mElectricCircuit->clone();
		mVehicleSolver->ReplaceRefElectricCircuit(simulatedCircuit);
		SystemSolverImmediate* solverImmediate = new SystemSolverImmediate(mObjectiveFunction, mVehicleSolver, simulatedCircuit, INFINITY);
		ScenarioData* dataSolver = solverImmediate->ModifyOptionnalScenarioData();
		if (dataSolver->HasPartialSolution())
		{ 
			dataSolver->SetAndOwnPartialSolution(mSolverData->GetAndClearPartialSolution());
		}
		//Add the vehicle according to current order previously generated
		for (unsigned int j = 0; j < curArrangement->size(); ++j)
		{
			solverImmediate->AddVehicle((*curArrangement)[j]);
		}
		//cout << "SIZE ARRANGMENT" << curArrangement->size() << endl;
		//cout << "SIZE VEHICLE" << solverImmediate->GetVehicleCount() << endl;
		//cout << "SIZE SOLUTION" << solverImmediate->GetCurrentSystemSolution()->NbVehiculeSolution() << endl;
		assert(curArrangement->size() == solverImmediate->GetVehicleCount());
		assert(curArrangement->size() == solverImmediate->GetCurrentSystemSolution()->NbVehiculeSolution());

		bool isBetterThan = mObjectiveFunction->IsBetterThan(solverImmediate->GetCurrentSystemSolution(), GetCurrentSystemSolution());
		STrace tr;
		tr.valueA = solverImmediate->GetCurrentSystemSolution()->mSystemPathDistribution.Total();
		tr.valueB = GetCurrentSystemSolution()->mSystemPathDistribution.Total();
		tr.sizeA = solverImmediate->GetCurrentSystemSolution()->NbVehiculeSolution();
		tr.sizeB = GetCurrentSystemSolution()->NbVehiculeSolution();
		m_traceExporter.Trace(tr);
		//If we have a better solution keep the solver and order!
		if (isBetterThan || mBestSolver == nullptr)
		{
			ResetSolver();
			//mSystemSolution = solverImmediate->GetCurrentSystemSolution();
			mBestSolver = solverImmediate;
			mBestCircuit = simulatedCircuit;
			//cout << "Keeping solution!" << endl;
		}
		else
		{
			//Otherwise, delete them
			//cout << "Delete solution..." << endl;
			delete solverImmediate;
			delete simulatedCircuit;
		}

		//cout << "Running time: " << GetPerformanceAnalysis()->RunningTime()<< endl;
		//cout << "Time cap: " << GetPerformanceAnalysis()->GetResult().dTimeCap<< endl;
		//cout << "HasReachdedTimeCap(): " << HasReachdedTimeCap() << endl;
		if (HasReachdedRunningTimeCap())
		{
			cout << "Early break!" << endl;
			break;
		}
		//Prepare next sequence
		curArrangement = mArrangement->GetNextSequence();
	};

	m_traceExporter.ExportFlat();

	//assert(false);
	if (mBestSolver != nullptr)
	{
		mBestSolver->GetCurrentSystemSolution()->mDescription = mSystemSolution->mDescription;
		if (mBestSolver->GetCurrentSystemSolution()->IsImpossible())
		{
			mSystemSolution->mSystemPathDistribution.SetInf();
		}
	}
	else
	{
		GetCurrentSystemSolution()->mSystemPathDistribution.SetInf();
	}

	mPerformanceAnalysis->StopExperiment(GetCurrentSystemSolution()->mSystemPathDistribution);
}