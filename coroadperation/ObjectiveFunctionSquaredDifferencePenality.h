#pragma once
#include "IObjectiveFunction.h"
class ObjectiveFunctionSquaredDifferencePenality :
    public IObjectiveFunction
{
private:
	SystemSolution* mRefReferenceSystemSolution = nullptr;
	IObjectiveFunction* m_ptrPenality = nullptr;

public:
	ObjectiveFunctionSquaredDifferencePenality(SystemSolution* refReferenceSystemSolution);
	virtual ~ObjectiveFunctionSquaredDifferencePenality();
	virtual std::string Description() const { return  "RacDiffCarPenalite"; }
	virtual float Evaluate(SystemSolution const* const in_A);
};

