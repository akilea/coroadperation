
#include "IScenario.h"
#include "ISystemSolver.h"
#include "IVehicleSolver.h"
#include "IObjectiveFunction.h"
#include "SystemSolution.h"
#include "ScenarioData.h"
#include "ElectricCircuit.h"
#include "IScenarioLoader.h"
#include "PerformanceAnalysis.h"
#include "IArrangement.h"
#include "CSVExporter.h"
#include "Vehicle.h"
#include "Path.h"
#include "VehicleSolution.h"

#include <sstream>
#include <istream>
#include <vector>

#include "ObjectiveFunctionWorse.h"

IScenario::~IScenario()
{
	for each (auto var in mSystemSolver)
	{
		delete var;
	}
	mSystemSolver.clear();

	for each (auto var in mSystemSolverAssist)
	{
		delete var;
	}
	mSystemSolverAssist.clear();

	

	for each (auto var in mVehicleSolver)
	{
		delete var;
	}
	mVehicleSolver.clear();

	for each (auto var in mObjectiveFunction)
	{
		delete var;
	}
	mObjectiveFunction.clear();

	for each (auto var in mSolverCircuit)
	{
		delete var;
	}
	mSolverCircuit.clear();

	for each(auto var in mArrangement)
	{
		delete var;
	}
	mArrangement.clear();

	//delete mRefScenarioLoader;
}

bool IScenario::SolveNextVehicle()
{
	bool bCanSolve = mDataIndexSimulator < GetRefData()->mVecVehicle.size();
	cout << "=====CAR INDEX===== " << mDataIndexSimulator  << endl;
	if (bCanSolve)
	{
		for each (auto var in mSystemSolver)
		{
			if (var->HasActivation())
			{
				cout << "Adding vehicle for " << var->GetDescription().sShort << endl;
				var->AddVehicle(GetRefData()->mVecVehicle[mDataIndexSimulator]);
				this->AfterSolveNextVehicleCallback(var, GetRefData()->mVecVehicle[mDataIndexSimulator]);
			}
			//else
			//{
			//	cout << "Skipping " << var->GetDescription().sShort << endl;
			//}
		}
		mDataIndexSimulator++;
	}
	return bCanSolve;
}

void IScenario::SolveAllVehicle()
{
	while (SolveNextVehicle());
	cout << "Solving all vehicle done!" << endl;
}

void IScenario::SolveAllSystem()
{
	cout << "---- SOLVING SYSTEM-----" << endl;
	for each (auto var in mSystemSolver)
	{
		if (var->HasActivation())
		{
			cout << var->GetCurrentSystemSolution()->mDescription << " - STARTING!" << endl;
			var->SolveSystem();
			cout << var->GetCurrentSystemSolution()->mDescription << " - COMPLETED! - Execution time:" << var->GetPerformanceAnalysis()->GetResult().dComputationTime<< endl;
		}
	}
}

void IScenario::SolveNextBatch(int max)
{
	for (int i = 0; i < max; ++i)
	{
		if (!SolveNextVehicle())
		{
			break;
		}
	}
	SolveAllSystem();
}

void IScenario::SetAutonomy(string autonomyName)
{
	m_strAutonomy = autonomyName;
}

void IScenario::SetActivation(bool toActivate[])
{
	for (unsigned int i = 0 ; i < mSystemSolver.size(); ++i)
	{
		mSystemSolver[i]->SetActivation(toActivate[i]);
	}
}

void IScenario::SetActivation(int indexSolver, bool toActivate)
{
	mSystemSolver[indexSolver]->SetActivation(toActivate);
}

void IScenario::SetActivationAll(bool active)
{
	for (unsigned int i = 0; i < mSystemSolver.size(); ++i)
	{
		mSystemSolver[i]->SetActivation(active);
	}
}

void IScenario::SetActivationIfTimeLeft()
{
	for (unsigned int i = 0; i < mSystemSolver.size(); ++i)
	{
		mSystemSolver[i]->SetActivation(!mSystemSolver[i]->GetPerformanceAnalysis()->HasReachdedTimeCap());
	}
}

void IScenario::LoadNamelessVehicle(std::istream & inDataVehicle)
{
	mRefScenarioLoader->LoadNextNamelessVehicle(inDataVehicle, GetRefData());
}

std::string IScenario::toString() const
{
	std::ostringstream ss;
	ss << GetRefData()->toString();
	for each (auto var in mSystemSolver)
	{
		 ss << (*(var->GetCurrentSystemSolution()));
	}
	return ss.str();
}

std::string IScenario::toStringCost() const
{
	std::ostringstream ss;
	ss << "Cost for each" << endl;
	for each (auto var in mSystemSolver)
	{
		ss << var->GetCurrentSystemSolution()->mSystemPathDistribution.Total() << endl;
	}
	return ss.str();
}

std::string IScenario::toStringPerformance(IObjectiveFunction * ptrObjFx) const
{
	std::ostringstream ss;
	ss << "Performance for each" << endl;
	for each (auto var in mSystemSolver)
	{
		float fCurrentValue = ptrObjFx->Evaluate(var->GetCurrentSystemSolution());
		ss << var->GetPerformanceAnalysis()->toString(GetRefData()->mVecVehicle.size(), fCurrentValue) << endl;
	}
	return ss.str();
}

		//TODO have the amount of vehicle
		//TODO: adjust time computation per vehicle instead of total
std::string IScenario::toStringInteractive() const
{
	return GetRefData()->toStringInteractive();
}

void IScenario::ExportHeader(std::ostream& st) const
{
	CSVExporter exporter;
	vector<string> partialHeader({"NbBorne","NbVoiture","Algo","NomMetrique","Metrique","TotalMinimum","TotalMaximum","TotalMoyen","RouteMoyenne","AttenteMoyenne","ChargeMoyenne","TotalEcartType","RouteEcartType","AttenteEcartType","ChargeEcartType","TempsCalcul","NombrePasse","Autonomie"});
	exporter.AddData(partialHeader);
	exporter.Export(st);
}

void IScenario::Export(std::ostream& str) const
{
	CSVExporter exporter;
	//Then add the solution data
	for (auto var : mSystemSolver)
	{
		if (var->HasActivation() && !var->GetCurrentSystemSolution()->IsImpossible())
		{
			std::vector<string> vData;
			vData.push_back(to_string(GetRefData()->mElectricCircuit->size())); //NbBorne
			unsigned int nbCar = var->GetCurrentSystemSolution()->NbVehiculeSolution();
			vData.push_back(to_string(nbCar)); //NbVoiture
			vData.push_back(var->GetDescription().sShort); //Algo
			vData.push_back(mObjectiveFunction.back()->Description()); //Nom metrique

			PathDistribution avgDistribution = var->GetCurrentSystemSolution()->mSystemPathDistribution / float(nbCar);
			PathDistribution stdDevDistribution;
			stdDevDistribution.Set0();
			float stdDevTotal = 0.f;
			for (auto it = var->GetCurrentSystemSolution()->GetBeginIterator(); it != var->GetCurrentSystemSolution()->GetEndIterator();++it)
			{
				PathDistribution delta = avgDistribution-it->second->mPath->mTotalPathDistribution;
				stdDevDistribution += delta * delta;
				stdDevTotal = delta.Total()* delta.Total();
			}
			stdDevDistribution = stdDevDistribution/float(nbCar);
			stdDevTotal = stdDevTotal/float(nbCar);

			ObjectiveFunctionWorse objDuration;
			vData.push_back(to_string(mObjectiveFunction.back()->Evaluate(var->GetCurrentSystemSolution()))); //Metrique
			vData.push_back(to_string(objDuration.EvaluateBest(var->GetCurrentSystemSolution()))); //TotalMinimum
			vData.push_back(to_string(objDuration.Evaluate(var->GetCurrentSystemSolution()))); //TotalMaximum
			vData.push_back(to_string(avgDistribution.Total())); //TotalMoyen
			vData.push_back(to_string(avgDistribution.mRoadTime));//RouteMoyenne
			vData.push_back(to_string(avgDistribution.mWaitTime));//AttenteMoyenne
			vData.push_back(to_string(avgDistribution.mChargeTime));//ChargeMoyenne
			vData.push_back(to_string(sqrt(stdDevTotal)));//TotalEcartType
			vData.push_back(to_string(sqrt(stdDevDistribution.mRoadTime)));//RouteEcartType
			vData.push_back(to_string(sqrt(stdDevDistribution.mWaitTime)));//AttenteEcartType
			vData.push_back(to_string(sqrt(stdDevDistribution.mChargeTime)));//ChargeEcartType
			const PerformanceAnalysis* perf = var->GetPerformanceAnalysis();
			//vData.push_back(to_string(perf->GetResult().mSystemPathDistribution.Total()/bestTotal - 1.f));
			//vData.push_back(to_string(perf->GetResult().mSystemPathDistribution.normalize().mRoadTime));
			//vData.push_back(to_string(perf->GetResult().mSystemPathDistribution.normalize().mChargeTime));
			//vData.push_back(to_string(perf->GetResult().mSystemPathDistribution.normalize().mWaitTime));
			vData.push_back(to_string(perf->GetResult().dComputationTime)); //TempsCalcul
			vData.push_back(to_string(perf->GetResult().nbPass)); //NombrePasse
			vData.push_back(m_strAutonomy); //Nom du profil d'autonomie
			//vData.push_back(to_string(perf->GetResult().dTimeCap));
			exporter.AddData(vData);
		}
	}
	exporter.Export(str);
}
