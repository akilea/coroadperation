#pragma once

#include <unordered_map>
#include <string>

class ElectricTerminal;
class Carte;

using namespace std;

class ElectricCircuit
{
private:
	unordered_map<int,ElectricTerminal*> mMapStation;//should be deprecated

public:
	ElectricCircuit(const Carte& inCarte);
	ElectricCircuit(const ElectricCircuit &otherEc);
	ElectricCircuit* clone() const;
	~ElectricCircuit();
	ElectricTerminal* GetByIndexTerminal(int inIDTerminal) const;
	float ComputeTerminalPer100km2() const;

	unsigned int size() const { return mMapStation.size(); }
	unsigned int computeTotalReservation() const;

	void clearAllReservation();

	friend ostream& operator<<(ostream& os, const ElectricCircuit& dt);
	
};


ostream& operator<<(ostream& os, const ElectricCircuit& dt);


