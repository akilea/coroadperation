#pragma once
#include "IAlgoRun.h"
class AlgoRunTestElectricCircuit :
	public IAlgoRun
{
public:
	AlgoRunTestElectricCircuit();
	virtual ~AlgoRunTestElectricCircuit();

	virtual int Run(int argc, const char** argv);
};

