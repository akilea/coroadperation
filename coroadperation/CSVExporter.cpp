#include "CSVExporter.h"



CSVExporter::CSVExporter()
{
}

void CSVExporter::AddData(const std::vector<std::string>& in_vData)
{
	m_vvData.push_back(in_vData);
}

void CSVExporter::Export(std::ostream & str)
{
	for (auto vecData : m_vvData)
	{
		writeNextRow(str, vecData);
	}
}

CSVExporter::~CSVExporter()
{
}

void CSVExporter::writeNextRow(std::ostream & str, const std::vector<std::string> & in_Data)
{
	//str << std::endl;
	bool isFirst = true;
	for (auto it : in_Data)
	{
		if (!isFirst)
		{
			str << ",";
		}
		isFirst = false;
		str << it;
	}
	str << std::endl;
}
