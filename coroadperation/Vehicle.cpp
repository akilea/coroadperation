#include <sstream>
#include "Vehicle.h"
#include "Path.h"
#include "srcMap/carte.h"
#include <iomanip>


Vehicle::Vehicle()
{

}

Vehicle::Vehicle(string name, Point a, Point b, double autonomie, float departureTime, float L1FullChargingTime)
{
	mName = name;
	mPointDeparture = a;
	mPointArrival = b;
	mAutonomie = autonomie;
	mDepartureTime = departureTime;
	mL1MaxChargingTime = L1FullChargingTime;
}


Vehicle::~Vehicle()
{
}


string Vehicle::toString() const
{
	std::ostringstream ss;
	ss << setw(20) << left << "Name: " << mName << endl;
	ss << setw(20) << left << "Departure time: " << mDepartureTime << endl;

	return ss.str();
}

string Vehicle::toStringScenario() const
{
	std::ostringstream ss;
	ss << std::setprecision(20);
	ss << mName << " " << mPointDeparture << " " << mPointArrival << " " << int(mAutonomie) << " " << TimeUnit(mDepartureTime) << " " << TimeUnit(mL1MaxChargingTime) ;
	return ss.str();
}

ostream & operator<<(ostream & os, const Vehicle & dt)
{
	os << dt.toString();
	return os;
}

bool VehicleAscendingSort(Vehicle * i, Vehicle * j) {
	//cout << "Yeah boy!" << endl;
	return (i->GetDepartureTime() < j->GetDepartureTime());
}
