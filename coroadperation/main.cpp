
#include "IAlgoRun.h"
#include "AlgoRunTestVehicleScenario.h"
#include "AlgoRunTestAVLTree.h"
#include "AlgoRunTestTerminal.h"
#include "AlgoRunTestElectricCircuit.h"
#include "AlgoRunPrecomputeCache.h"
#include "AlgoRunTestDjistraTerminal.h"
#include "AlgoRunTestHighLevelCarte.h"
#include "AlgoRunTestPenality.h"
#include "AlgoRunTestCutter.h"
#include "AlgoRunTestPiercing.h"
#include "windows.h"
#include "psapi.h"
#include <iostream>

int main(int argc, const char** argv) {

	IAlgoRun* algoRun = new AlgoRunTestVehicleScenario();
	//IAlgoRun* algoRun = new AlgoRunTestPenality();
	//IAlgoRun* algoRun = new AlgoRunTestCutter();
	//IAlgoRun* algoRun = new AlgoRunTestPiercing();
	//IAlgoRun* algoRun = new AlgoRunTestDjistraTerminal();
	//IAlgoRun* algoRun = new AlgoRunTestHighLevelCarte();
	//IAlgoRun* algoRun = new AlgoRunTestAVLTree();
	//IAlgoRun* algoRun = new AlgoRunTestElectricCircuit();
	//IAlgoRun* algoRun = new AlgoRunTestTerminal();
	//IAlgoRun* algoRun = new AlgoRunPrecomputeCache();
	algoRun->Run(argc, argv);
	delete algoRun;

	return 0;
}

