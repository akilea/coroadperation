#include "AlgoRunTestVehicleScenario.h"
#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sys/stat.h>
#include "carte.h"
#include "Path.h"
#include "ScenarioPermutation.h"
#include "ScenarioLoader.h"
#include "ScenarioData.h"
#include "ISystemSolver.h"
#include "SystemSolution.h"
#include "VehicleSolution.h"
#include "HighLevelCarte.h"
#include "SimulationSetting.h"
#include "Model.h"

enum TOption
{
	eCARTE = 0,
	eBORNE,
	eBORNECACHE,
	eSCENARIO,
	eRESULT,
	eDETAIL,
	eMODE,
	eNBBORNEMAX,
	eMETRIQUE,
	eAUTONOMY,

	eMAX
};


AlgoRunTestVehicleScenario::AlgoRunTestVehicleScenario()
{
}


AlgoRunTestVehicleScenario::~AlgoRunTestVehicleScenario()
{
}


void AlgoRunTestVehicleScenario::ExportData(IScenario* scenario, std::string strOption[])
{
	//Export csv results
	//Export detail log
	//Does file already exists?
	struct stat fileInfo;
	bool fileExist = stat(strOption[eRESULT].c_str(), &fileInfo) == 0;

	ofstream osExportCSV(strOption[eRESULT], std::fstream::app);
	if (osExportCSV.fail()) {
		cerr << "Erreur d'ouverture du fichier csv: '" << strOption[eRESULT] << "' !" << endl;
	}
	else
	{
		if (!fileExist)
		{
			scenario->ExportHeader(osExportCSV);
			cerr << "Header CSV export�s!" << endl;
		}
		scenario->Export(osExportCSV);
		cerr << "CSV export�s!" << endl;
	}

	//Export detail log
	bool isExportDetails = false;
	if (isExportDetails)
	{
		ofstream osExportDetail(strOption[eDETAIL], std::fstream::app);
		if (osExportDetail.fail()) {
			cerr << "Erreur d'ouverture du fichier detail: '" << strOption[eDETAIL] << "' !" << endl;
		}
		else
		{
			osExportDetail << scenario->toString() << endl;
			osExportDetail << scenario->toStringPerformance(scenario->mObjectiveFunction.back()) << endl;
			cerr << "D�tails export�s!" << endl;
		}
	}
}

void ajoutProbabiliteVE(SModel model, vector<SModel>& models)
{
	for (int i = 0; i < model.m_nPercentRecurrence; ++i)
	{
		models.push_back(model);
	}
}

int AlgoRunTestVehicleScenario::Run(int argc, const char ** argv)
{
	//cin.get();
	//MAIN IN
	//quebec-carte.txt quebec-bornes.txt distanceBorne.cache quebec-scenario.txt result.csv details.log scenario_viz
	
	string strOption[] = {
		"quebec-carte.txt"
		,"quebec-bornes.txt"
		,"distanceBorne.cache"
		,"Scenario.txt"
		,"result.csv"
		,"details.log"
		,"interactive"
		,"36"
		,"0"
		,"200"
	};

	//Chargement des arguments
	for (int i = 1; i < argc; ++i)
	{
		strOption[i-1] = argv[i];
	}

	TraceConvergenceExporter::m_isEnabled = false;

	bool bActivationState[Scenario::eMAX_SOLVERTYPE];
	bActivationState[Scenario::eINIT]						= true;
	bActivationState[Scenario::eNO_COOP]					= true; //PNCSR
	bActivationState[Scenario::eNO_COOP_WAIT]				= true; //PNCAR
	bActivationState[Scenario::eCOOP_NO_OPTIMIZATION]		= true; //PNCARR
	bActivationState[Scenario::eHCA_FI]						= true; //-----
	bActivationState[Scenario::eHCA_CASCADE]				= true; //PCACDP
	bActivationState[Scenario::eHCA_PERMUTATION]			= true; //PCAPDP
	bActivationState[Scenario::eWHCA_SMALL_FI]				= true; //PCAPF
	bActivationState[Scenario::eWHCA_MEDIUM_FI]				= true; //PCAPF
	bActivationState[Scenario::eWHCA_LARGE_FI]				= true; //PCAPF
	bActivationState[Scenario::eWHCA_SMALL_CASCADE]			= true;//PCAPF
	bActivationState[Scenario::eWHCA_MEDIUM_CASCADE]		= true;//PCAPF
	bActivationState[Scenario::eWHCA_LARGE_CASCADE]			= true;//PCAPF
	bActivationState[Scenario::eWHCA_SMALL_PERMUTATION]		= true; //PCAPF
	bActivationState[Scenario::eWHCA_MEDIUM_PERMUTATION]	= true; //PCAPF
	bActivationState[Scenario::eWHCA_LARGE_PERMUTATION]		= true; //PCAPF
	bActivationState[Scenario::PCARLG_PNCARR	]           = true;
	bActivationState[Scenario::PCARL_PNCARR		]           = true;
	bActivationState[Scenario::PCARLG_PNCARR_C	]           = true;
	bActivationState[Scenario::PCARL_PNCARR_C	]           = true;
	bActivationState[Scenario::PCARLG_PNCAR		]           = true;
	bActivationState[Scenario::PCARL_PNCAR		]           = true;
	bActivationState[Scenario::PCARLG_PNCAR_C	]           = true;
	bActivationState[Scenario::PCARL_PNCAR_C	]           = true;
	bActivationState[Scenario::ePCC]                        = false;
	//PCC
	//PNCAE
	//PCARL

	//if (argc < 4) {
	//	cout << "./coroadperation carte.txt bornes.txt scenario.txt" << endl;
	//	return 1;
	//}
	Carte carte(strOption[eCARTE], strOption[eBORNE], stoi(strOption[eNBBORNEMAX]));

	//Load high level map solver
	HighLevelCarte highLevelCarte(strOption[eBORNECACHE], &carte);
	
	//int interactiveMode = 1;
	//if (strOption[eMODE].compare("interactive") == 0)
	//{
	//	interactiveMode = 1;
	//}
	//else if (strOption[eMODE].compare("scenario_viz") == 0)
	//{
	//	interactiveMode = 2;
	//}

	IScenarioLoader* scenarioLoader = new ScenarioLoader(strOption[eSCENARIO], carte);
	vector<SModel> autonomy;
	//TODO:Schematic from roulez electrique
	//https://www.aveq.ca/actualiteacutes/statistiques-saaq-aveq-sur-lelectromobilite-au-quebec-en-date-du-30-juin-2021-infographie
	//En date du 30 juin 2021
	//Les distances par mod�le
	//https://en.wikipedia.org/wiki/Chevrolet_Bolt#/media/File:BEV_EPA_range_comparison_2016-2017_MY_priced_under_50K_US.png
	//En miles
	//Aucune version longue distance, seulement standard
	//Distance hivers = 73.16%
	// Taux de chargement 3km/minute � 50kwh
	SModel model3 = SModel(220, 12);
	SModel bolt = SModel(238,   10);
	SModel leaf = SModel(107,   9);
	SModel kona = SModel(189,   6);
	SModel ioniq = SModel(124,  3);
	SModel golfe = SModel(125,  3);
	SModel soulev = SModel(93,  2);
	SModel modely = SModel(240, 2);

	string auton = strOption[eAUTONOMY];

	if (auton == "SoulEV") //Test pour le plus cours
	{
		autonomy.push_back(soulev);
	}
	else if(auton == "Ioniq")
	{
		autonomy.push_back(ioniq);
		cout << "Ioniq chosen" << endl;
	}
	else{
		auton = "Distribution";
		//total:47
		ajoutProbabiliteVE( model3, autonomy);
		ajoutProbabiliteVE( bolt, autonomy);
		ajoutProbabiliteVE(leaf, autonomy);
		ajoutProbabiliteVE(kona, autonomy);
		ajoutProbabiliteVE(ioniq, autonomy);
		ajoutProbabiliteVE(golfe, autonomy);
		ajoutProbabiliteVE(soulev, autonomy);
		ajoutProbabiliteVE(modely, autonomy);
		cout << "Distribution chosen" << endl;
	}

	cout << "Model profile:" << auton << endl;
	for (int i = 0; i < autonomy.size(); ++i)
	{
		cout << autonomy[i].toString() << endl;
	}

	scenarioLoader->SetPossibleAutonomy(autonomy);
	IMapSolver* mapSolver = &highLevelCarte;
	IScenario* scenario = new Scenario(scenarioLoader, *mapSolver,stoi(strOption[eMETRIQUE]));
	scenario->SetAutonomy(auton);

	scenario->SetActivation(bActivationState);
	std::vector<int> batchAmount = { 0,1, 2, 5, 10, 20, 30, 40, 50, 75, 100, 150, 200, 300, 400, 500, 600, 700, 800,900,1000,1100,1200,1300,1400,1500};
	//std::vector<int> batchAmount = { 0,150, 200, 300, 400, 500, 600, 700, 800,900,1000,1100,1200,1300,1400,1500};
	//std::vector<int> batchAmount = { 0,50};
	//std::vector<int> batchAmount = { 0,1500};
	for(size_t i = 1; i < batchAmount.size();++i)
	{
		scenario->SolveNextBatch(batchAmount[i]-batchAmount[i-1]);
		ExportData(scenario, strOption);
	}
	//cout << scenario->toString() << endl;
	//cout << scenario->toStringPerformance(scenario->mObjectiveFunction.back()) << endl;
	//cout << endl << scenario->toStringInteractive();
	//cout << endl << scenario->toString();

	//Clean all
	delete scenario;
	delete scenarioLoader;
	return 0;
}
