#include "AlgoRunTestHighLevelCarte.h"

#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "carte.h"
#include "Path.h"
#include "Vehicle.h"
#include "HighLevelCarte.h"
#include "RequestCarte.h"
#include "ElectricCircuit.h"
#include "VehicleSolution.h"
#include "ElectricTerminal.h"
#include "ElectricReservation.h"


AlgoRunTestHighLevelCarte::AlgoRunTestHighLevelCarte()
{
}


AlgoRunTestHighLevelCarte::~AlgoRunTestHighLevelCarte()
{
}


void printStart(string start)
{
	static int i = 0;
	i++;
	cout << endl;
	cout << "Test " << i << ":" << start << endl;
}

void printEnd()
{
	cout << endl;
	cout << "END " << endl;
}



//Should use A* espere
void TestNoWaitOptimizationNoWaitAction(Carte& c, HighLevelCarte& highLevelCarte)
{
	printStart("No wait optimization no action");

	ElectricCircuit* elec = new ElectricCircuit(c);

	Vehicle* v = new Vehicle("Test", Point(46.839159026696364663, -71.244796855111786726), Point(45.481367303278823044, -73.590706178001298099), 200000, 1000.f, 1800.f);
	VehicleSolution sol(v);
	//Request base
	RequestCarte request;
	request.mRefElectricCircuit = elec; // for now
	//Artifically reserve the terminal 21 for a lot of time...
	request.mRefVehicleRequested = v;
	request.NeverMinimizeWaitingHorizon();
	request.bCreateActionWaiting = false;
	cout << highLevelCarte.calculerTrajet(*sol.mPath, request) << endl;
	//cout << *elec;
	for each (auto it in sol.mPath->mSequenceAction)
	{
		cout << it->toString() << endl;
	}

	delete v;
	delete elec;

	printEnd();
}

//Should use A* connaissance autres
void TestWaitOptimizationNoWaitAction(Carte& c, HighLevelCarte& highLevelCarte)
{
	printStart("Wait optimization alone no action");

	ElectricCircuit* elec = new ElectricCircuit(c);

	Vehicle* v = new Vehicle("Test", Point(46.839159026696364663, -71.244796855111786726), Point(45.481367303278823044, -73.590706178001298099), 200000, 1000.f, 1800.f);
	VehicleSolution sol(v);
	//Request base
	RequestCarte request;
	request.mRefElectricCircuit = elec; // for now
	request.mRefVehicleRequested = v;
	request.AlwaysMinimizeWaitingHorizon();
	request.bCreateActionWaiting = false;
	cout << highLevelCarte.calculerTrajet(*sol.mPath, request) << endl;
	//cout << *elec;
	for each (auto it in sol.mPath->mSequenceAction)
	{
		cout << it->toString() << endl;
	}

	delete v;
	delete elec;

	printEnd();
}

//Should pass through borne 21 but should be delayed
void TestWaitOptimizationLittleReservationNoWaitAction(Carte& c, HighLevelCarte& highLevelCarte)
{
	printStart("Wait optimization with little delay no action");

	ElectricCircuit* elec = new ElectricCircuit(c);

	Vehicle* v = new Vehicle("Test", Point(46.839159026696364663, -71.244796855111786726), Point(45.481367303278823044, -73.590706178001298099), 200000, 1000.f, 1800.f);
	Vehicle* v2 = new Vehicle("Test", Point(46.839159026696364663, -71.244796855111786726), Point(45.481367303278823044, -73.590706178001298099), 200000, 1000.f, 1800.f);
	VehicleSolution sol(v);
	//Request base
	RequestCarte request;
	request.mRefElectricCircuit = elec;
	ElectricReservation* res = new ElectricReservation(6450, 6550, v2);
	elec->GetByIndexTerminal(21)->TryReserveDeltaOne(res);
	request.mRefVehicleRequested = v;
	request.AlwaysMinimizeWaitingHorizon();
	request.bCreateActionWaiting = false;
	cout << highLevelCarte.calculerTrajet(*sol.mPath, request) << endl;
	//cout << *elec;
	for each (auto it in sol.mPath->mSequenceAction)
	{
		cout << it->toString() << endl;
	}

	delete v;
	delete elec;

	printEnd();
}

//Should avoid borne 21 to avoid waiting too long
//Pass through borne 24
void TestWaitOptimizationBigReservationNoWaitAction(Carte& c, HighLevelCarte& highLevelCarte)
{
	printStart("Wait optimization with detour no action");

	ElectricCircuit* elec = new ElectricCircuit(c);

	Vehicle* v = new Vehicle("Test", Point(46.839159026696364663, -71.244796855111786726), Point(45.481367303278823044, -73.590706178001298099), 200000, 1000.f, 1800.f);
	Vehicle* v2 = new Vehicle("Test", Point(46.839159026696364663, -71.244796855111786726), Point(45.481367303278823044, -73.590706178001298099), 200000, 1000.f, 1800.f);
	VehicleSolution sol(v);
	//Request base
	RequestCarte request;
	request.mRefElectricCircuit = elec;
	ElectricReservation* res = new ElectricReservation(6450, 8000, v2);
	elec->GetByIndexTerminal(21)->TryReserveDeltaOne(res);
	request.mRefVehicleRequested = v;
	request.AlwaysMinimizeWaitingHorizon();
	request.bCreateActionWaiting = false;
	cout << highLevelCarte.calculerTrajet(*sol.mPath, request) << endl;
	//cout << *elec;
	for each (auto it in sol.mPath->mSequenceAction)
	{
		cout << it->toString() << endl;
	}

	delete v;
	delete elec;

	printEnd();
}


void TestNoWaitOptimizationWithWaitAction(Carte& c, HighLevelCarte& highLevelCarte)
{
	printStart("Not wait optimization with long wait action");

	ElectricCircuit* elec = new ElectricCircuit(c);

	Vehicle* v = new Vehicle("Test", Point(46.839159026696364663, -71.244796855111786726), Point(45.481367303278823044, -73.590706178001298099), 200000, 1000.f, 1800.f);
	Vehicle* v2 = new Vehicle("Test", Point(46.839159026696364663, -71.244796855111786726), Point(45.481367303278823044, -73.590706178001298099), 200000, 1000.f, 1800.f);
	VehicleSolution sol(v);
	//Request base
	RequestCarte request;
	request.mRefElectricCircuit = elec;
	ElectricReservation* res = new ElectricReservation(6450, 8000, v2);
	elec->GetByIndexTerminal(21)->TryReserveDeltaOne(res);
	request.mRefVehicleRequested = v;
	request.NeverMinimizeWaitingHorizon();
	request.bCreateActionWaiting = true;
	cout << highLevelCarte.calculerTrajet(*sol.mPath, request) << endl;
	//cout << *elec;
	for each (auto it in sol.mPath->mSequenceAction)
	{
		cout << it->toString() << endl;
	}

	delete v;
	delete elec;

	printEnd();
}


void TestWaitOptimizationWithWaitAction(Carte& c, HighLevelCarte& highLevelCarte)
{
	printStart("Wait optimization with long wait action");

	ElectricCircuit* elec = new ElectricCircuit(c);

	Vehicle* v = new Vehicle("Test", Point(46.839159026696364663, -71.244796855111786726), Point(45.481367303278823044, -73.590706178001298099), 200000, 1000.f, 1800.f);
	Vehicle* v2 = new Vehicle("Test", Point(46.839159026696364663, -71.244796855111786726), Point(45.481367303278823044, -73.590706178001298099), 200000, 1000.f, 1800.f);
	VehicleSolution sol(v);
	//Request base
	RequestCarte request;
	request.mRefElectricCircuit = elec;
	ElectricReservation* res = new ElectricReservation(6450, 8000, v2);
	elec->GetByIndexTerminal(21)->TryReserveDeltaOne(res);
	request.mRefVehicleRequested = v;
	request.AlwaysMinimizeWaitingHorizon();
	request.bCreateActionWaiting = true;
	cout << highLevelCarte.calculerTrajet(*sol.mPath, request) << endl;
	//cout << *elec;
	for each (auto it in sol.mPath->mSequenceAction)
	{
		cout << it->toString() << endl;
	}

	delete v;
	delete elec;

	printEnd();
}


int AlgoRunTestHighLevelCarte::Run(int argc, const char ** argv)
{

	//cin.get();
	string fileNameCarte = "quebec-carte.txt";
	string fileNameBorne = "quebec-bornes.txt";
	string fileNameCache = "distanceBorne.cache";

	//if (argc < 4) {
	//	cout << "./coroadperation carte.txt bornes.txt scenario.txt" << endl;
	//	return 1;
	//}
	Carte carte;
	{
		ifstream iscarte(fileNameCarte);
		if (iscarte.fail()) {
			cerr << "Erreur d'ouverture du fichier carte: '" << fileNameCarte << "' !" << endl;
			return 2;
		}
		iscarte >> carte;
		cerr << "Carte charg�e!" << endl;
	}
	{
		ifstream isbornes(fileNameBorne);
		if (isbornes.fail()) {
			cerr << "Erreur d'ouverture du fichier bornes: '" << fileNameBorne << "' !" << endl;
			return 2;
		}
		while (isbornes) {
			string nom;
			char L;
			int niveau;
			Point point;
			isbornes >> nom;
			if (!isbornes || nom.empty()) break;
			isbornes >> L >> niveau >> point >> ws;
			assert(L == 'L');
			cerr << "."; cerr.flush();
			carte.ajouterBorne(nom, point, niveau);
		}
		cerr << "Bornes charg�es!" << endl;
	}
	//Load high level map solver
	HighLevelCarte highLevelCarte(fileNameCache, &carte);

	TestNoWaitOptimizationNoWaitAction(carte,highLevelCarte);
	TestWaitOptimizationNoWaitAction(carte,highLevelCarte);
	TestWaitOptimizationLittleReservationNoWaitAction(carte,highLevelCarte);
	TestWaitOptimizationBigReservationNoWaitAction(carte,highLevelCarte);
	TestNoWaitOptimizationWithWaitAction(carte,highLevelCarte);
	TestWaitOptimizationWithWaitAction(carte,highLevelCarte);
	TestWaitOptimizationWithWaitAction(carte,highLevelCarte);

	cin.get();

	return 0;
}
