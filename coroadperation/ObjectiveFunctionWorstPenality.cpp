#include "ObjectiveFunctionWorstPenality.h"
#include "SystemSolution.h"
#include "VehicleSolution.h"
#include "Path.h"
#include <assert.h>

ObjectiveFunctionWorstPenality::ObjectiveFunctionWorstPenality(SystemSolution* refReferenceSystemSolution)
	:mRefReferenceSystemSolution(refReferenceSystemSolution)
{
	assert(refReferenceSystemSolution != nullptr);
}

float ObjectiveFunctionWorstPenality::Evaluate(SystemSolution const* const in_A)
{
	float fWorstPenalite = -INFINITY;
	if (in_A != nullptr)
	{
		for (auto it = in_A->GetBeginIterator(); it != in_A->GetEndIterator(); ++it)
		{
			//Find reference vehicle solution

			auto refVehicleSolution = mRefReferenceSystemSolution->GetVehiculeSolution(it->first);
			if (refVehicleSolution != nullptr)
			{
				float fCurrent = it->second->mPath->getDuration()/refVehicleSolution->mPath->getDuration();
				if (fWorstPenalite < fCurrent)
				{
					fWorstPenalite = fCurrent;
				}
			}
			else
			{
				cout << "ObjectiveFunctionWorstPenality::Evaluate - CANNOT FIND SOLUTION - CRITICAL ERROR" << endl;
			}
		}
	}
	return fWorstPenalite;
}
