#pragma once

#include <istream>
#include <vector>

class ScenarioData;
class Vehicle;
class SModel;

class IScenarioLoader
{
public:
	IScenarioLoader(){}
	virtual void Load(ScenarioData* outRefData) = 0;
	virtual void LoadNextVehicle(std::istream& inDataVehicle, ScenarioData* outRefData) = 0;
	virtual void LoadNextNamelessVehicle(std::istream& inDataVehicle, ScenarioData* outRefData) = 0;
	virtual void SetPossibleAutonomy(std::vector<SModel>& in_vecPossibleAutonomy) = 0;
	virtual ~IScenarioLoader(){}
};