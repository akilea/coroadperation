#pragma once
#include <unordered_set>
#include <iostream>

using namespace std;

class ElectricCircuit;
class ElectricTerminal;
class Vehicle;

class RequestCarte
{
public:
	RequestCarte();
	~RequestCarte();

	//Obligatoires
	ElectricCircuit* mRefElectricCircuit = nullptr;
	Vehicle* mRefVehicleRequested = nullptr;
	//bool bMinimizeWaiting; //Search will take into account waiting time or not
	bool  bCreateActionWaiting; // When waiting, will create a Waiting action
	float fMinimizeWaitingHorizon; //After that time, we will not minimize waiting
	void AlwaysMinimizeWaitingHorizon();
	void NeverMinimizeWaitingHorizon();

	enum TChargingBehavior
	{
		eMAXIMUM = 0,
		eMINIMUM,
		ePREFER_CHARGING_TO_WAITING,
		eMAX
	};
	TChargingBehavior mChargingBehavior = eMAXIMUM;

	//TO DEPRICATE
	//bool bIncludeOtherVehiclePresence = true; //Will reserve and take other reservation into account
	//bool bIncludeOtherOptimizeWait = true; //Coop will not take the waiting time into account
	//Temps absolu maximal pour lequel on considere des attentes (donc des collisions pour les recharges). Pour les temps subsequent, on ignore celles-ci.
	//float fIncludeOtherVehicleMaxTimeHorizon = INFINITY;

	static const bool IsAcceptableDistance(double dDistance);

	//DEPRECATED
	std::unordered_set<ElectricTerminal*> mOptionnal_StationToIgnore;

	//Utilitaires
	void UtilConstructIndexLieuFromStationToIgnore(std::unordered_set<int> & out_Result) const;
	ElectricTerminal* IndexTerminalToTerminal(int inIdIndexTerminal) const;

	friend ostream& operator<<(ostream& os, const RequestCarte& dt);
};

ostream& operator<<(ostream& os, const RequestCarte& dt);