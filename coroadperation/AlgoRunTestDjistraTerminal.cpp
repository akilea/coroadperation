#include "AlgoRunTestDjistraTerminal.h"
#include <string>
#include <fstream>
#include "PathCache.h"
#include "RequestCarte.h"
#include "DjistraTerminalSpace.h"

using namespace std;

AlgoRunTestDjistraTerminal::AlgoRunTestDjistraTerminal()
{
}


AlgoRunTestDjistraTerminal::~AlgoRunTestDjistraTerminal()
{
}

void AlgoRunTestDjistraTerminal::TestMass(int src, DjistraTerminalSpace & dji, PathCache& cache, RequestCarte& rq)
{

	for (int i = 0; i <= 33; ++i)
	{
		//dji.PreComputePaths(src, i,cache,rq,cart);
		DisplayPathIfSolution(src, i, dji,rq);
	}
}

void AlgoRunTestDjistraTerminal::DisplayPathIfSolution(int src, int to, DjistraTerminalSpace & dji, RequestCarte& rq)
{
	if (dji.HasPath(to))
	{
		std::list<int>  shortPath;
		//dji.ActionSequenceTo(to, shortPath);
		std:cout << "From " << src << " to " << to << endl;
		if (dji.HasPath(to))
		{
			std::cout << "Path : ";
			std::copy(shortPath.begin(), shortPath.end(), std::ostream_iterator<int>(std::cout, " "));
			std::cout << std::endl << "Distance: " << dji.TrueCostTo() << std::endl;
		}
		else
		{
			std::cout << "No solution" << endl;
		}
		std::cout << "----------" << endl;
		std::cout << std::endl;
	}
}

void AlgoRunTestDjistraTerminal::DisplayPath(int src, int to, DjistraTerminalSpace & dji, RequestCarte& rq)
{
	std::list<int>  shortPath;
	//dji.ActionSequenceTo(to, shortPath);
	std:cout << "From " << src << " to " << to << endl;
	if (dji.HasPath(to))
	{
		std::cout << "Path : ";
		std::copy(shortPath.begin(), shortPath.end(), std::ostream_iterator<int>(std::cout, " "));
		std::cout << std::endl << "Distance: " << dji.TrueCostTo() << std::endl;
	}
	else
	{
		std::cout << "No solution" << endl;
	}
	std::cout << "----------" << endl;
	std::cout << std::endl;
}



int AlgoRunTestDjistraTerminal::Run(int argc, const char ** argv)
{
	//Load cache
	PathCache pathCache;
	string fileNameCache = "distanceBorne.cache";
	ifstream inputFile(fileNameCache);
	inputFile >> pathCache;
	inputFile.close();
	RequestCarte rq;

	//
	DjistraTerminalSpace djistra;
	for (int i = 0; i <= 33; ++i)
	{
		TestMass(i, djistra, pathCache, rq);
	}
	
	cin.get();

	return 0;
}
