#pragma once

#include <vector>

class CombinatorialExploration
{
private:
	const int mMinimum = 0;
	const int mMaximum = 5;
	std::vector<int> mVec;

public:
	CombinatorialExploration(int mini, int maxi);
	bool Next();
	int AddDepth();

	size_t Depth() const { return mVec.size(); }
	int at(int depth) const { return mVec[depth]; }

};

