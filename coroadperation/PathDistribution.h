#pragma once
#include <ostream>
using namespace std;
class SystemSolution;

class PathDistribution
{
public:
	PathDistribution(const PathDistribution& obj);
	PathDistribution(float wait = 0.f, float charge = 0.f, float road = 0.f);
	~PathDistribution();

	float mWaitTime		= 0.0f;
	float mChargeTime	= 0.0f;
	float mRoadTime		= 0.0f;

	void Set(float wait, float charge, float road);
	void Set0();
	void SetInf();
	float Total() const;

	PathDistribution normalize()const;

	PathDistribution operator+(const PathDistribution & rhs) const;
	PathDistribution operator-(const PathDistribution& rhs) const;
	PathDistribution operator*(const PathDistribution & rhs) const;
	PathDistribution operator/(const float & rhs) const;
	float operator/(const PathDistribution& rhs) const;
	PathDistribution& operator=(const PathDistribution & rhs);
	PathDistribution& operator+=(const PathDistribution& rhs);
	PathDistribution& operator-=(const PathDistribution& rhs);
	friend ostream& operator<<(ostream& os, const PathDistribution& dt);
	bool operator<(const PathDistribution & rhs);
};


