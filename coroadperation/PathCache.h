#pragma once
#include <set>
#include <map>
#include <iostream>
using namespace std;


typedef  const map<int, double> PathCacheNeighbourMap;


//TODO: cleanup: ids should be unit solely
class PathCache
{
private:
	//Map entre depart et la destination des lieux
	//map<index_bonre_start,index_borne_end>
	//Should be almost vector-like. It is just to avoid empty spaces
	//matrice creuse
	map<int,map<int,double>> mIndexLieuDistanceCache;

	size_t m_unHighestTerminalID = 0;

public:
	PathCache();
	virtual ~PathCache();

	void addPath(int depart, int destination, double distance);
	void removePath(int depart, int destination);
	void stripAtAndAbove(int indexToStripFrom); //Carefull because of car IDs...
	//Returns 0.0 when no path exists
	double getPath(int depart, int destination) const;
	PathCacheNeighbourMap getPathNeighbourMap(int depart) const;
	bool hasNeighbour(int depart) const;
	size_t getHighestTerminalID() const { return m_unHighestTerminalID; }

	float ComputeTerminalDensity(set<int>& vTerminalToIgnore) const;

	friend ostream& operator<< (ostream &out, const PathCache &p);
	friend istream& operator>> (istream &in, PathCache &p);
};

