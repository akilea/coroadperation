#include "Action.h"
#include <sstream>
#include <iomanip>

#include "ElectricCircuit.h"
#include "ElectricTerminal.h"

const string Action::enumAssociation[eMax] = { "Driving","Charging","Waiting" };

Action::Action(ElectricCircuit* refElectricCircuit, ElectricTerminal* refTerminal)
	:mRefCircuit(refElectricCircuit)

{
	if (refTerminal != nullptr)
	{
		mIndexTerminal = refTerminal->GetIndex();
	}
}

Action::~Action()
{
}

Action* Action::CopyActionDataTo(Action * inClonedAction) const
{
	inClonedAction->mDuration = this->mDuration;
	inClonedAction->mName = this->mName;
	inClonedAction->mPosition = this->mPosition;
	inClonedAction->mStartTime = this->mStartTime;
	inClonedAction->mIndexTerminal = this->mIndexTerminal;
	inClonedAction->mRefCircuit = this->mRefCircuit;
	return inClonedAction;
}

string Action::toString() const
{
	std::ostringstream ss;
	//Shortens the name
	Action::TAction myType = getType();
	ss << setw(12) << left << enumAssociation[myType] << setw(18) << left << mName.substr(0, 15) << mPosition << setw(10) << right << std::setprecision(1) << std::fixed << mStartTime << setw(3) <<std::internal <<"-" << setw(8) <<std::right<< (mStartTime + mDuration) << setw(10) << std::right << mDuration << "s" << endl;
	return ss.str();
}

void Action::ReplaceElectricCircuit(ElectricCircuit* inEC)
{
	mRefCircuit = inEC;
}

ElectricCircuit* Action::GetElectricCircuit() const
{
	return mRefCircuit;
}

ElectricTerminal* const Action::getTerminal() const
{
	return mRefCircuit->GetByIndexTerminal(mIndexTerminal);
}