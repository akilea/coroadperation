#include "ArrangementPermutation.h"
#include <algorithm>


ArrangementPermutation::ArrangementPermutation()
{
}


ArrangementPermutation::~ArrangementPermutation()
{
}

void ArrangementPermutation::startSequence(std::vector<Vehicle*>* in_refVector)
{
	Arrangement::startSequence(in_refVector);
	mIsFirstSequence = true;
}

std::vector<Vehicle*>* ArrangementPermutation::GetNextSequence()
{
	if (mIsFirstSequence)
	{
		mIsFirstSequence = false;
		//Permutation needs this
		std::sort(m_refVector->begin(), m_refVector->end());
		return m_refVector;
	}
	else if (next_permutation(m_refVector->begin(), m_refVector->end()))
	{
		return m_refVector;
	}
	else
	{
		return nullptr;
	}
}
