#include "ElectricTerminal.h"

#include "ElectricReservation.h"
#include "Vehicle.h"
#include <vector>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cassert>


setER::iterator ElectricTerminal::Find(TimeUnit timeStamp, setER& in_SearchIn, SearchMethodReservation in_SearchMethod)
{
	//Temporary reservation list to test
	ElectricReservation tempErl(timeStamp,nullptr);

	setER::iterator it = in_SearchIn.end();
	it = (in_SearchIn.*in_SearchMethod)(&tempErl);
	return it;
}


//Sometimes, the try reserve from in-computation does not match the try reserve when we try to commit the reservation because of a floating point error.
//We circumvent this by trying to reserve with a delta one...
bool ElectricTerminal::TryReserveDeltaOne(ElectricReservation* er)
{
	bool success = true;
	bool secondSuccess = true;
	try {
		std::vector<ElectricReservation*> overlap;
		ReservationOverlap(er, overlap);
		if (overlap.empty())
		{
			mOrderedSetReservation.insert(er);
		}
		else
		{
			success = false;
		}

	}
	catch (...)
	{
		success = false;
	}

	if (!success)
	{
		cout << "WARNING TryReserveDeltaOne in effect for " << *er << endl;
		cout << er->m_pVehicle->toString() << endl;
		cout << er->m_pVehicle->toStringScenario() << endl;
		cout << this->toString() << endl;

		er->mTimeRange.mStart += 1;
		er->mTimeRange.mEnd -= 1;
		try {
			std::vector<ElectricReservation*> overlap;
			ReservationOverlap(er, overlap);
			if (overlap.empty())
			{
				mOrderedSetReservation.insert(er);
			}
			else
			{
				secondSuccess = false;
			}

		}
		catch (...)
		{
			secondSuccess = false;
		}

		if (!secondSuccess)
		{
			cout << "ERROR TryReserveDeltaOne FAILED for " << *er << endl;
			assert(false);
		}
	}
	return success || secondSuccess;
}


ElectricTerminal::ElectricTerminal(Borne & in_RefBorne, int nIndexTerminal)
:mRefBorne(in_RefBorne)
, mIndexTerminal(nIndexTerminal)
{
}

ElectricTerminal::ElectricTerminal(const ElectricTerminal & otherTerminal)
{
	//Empty data
	for (ElectricReservationPtr var : mOrderedSetReservation)
	{
		delete var;
	}

	mOrderedSetReservation.clear();

	//Copy data
	this->mRefBorne = otherTerminal.mRefBorne;
	this->mIndexTerminal = otherTerminal.GetIndex();
	for (ElectricReservationPtr var : otherTerminal.mOrderedSetReservation)
	{
		mOrderedSetReservation.insert(new ElectricReservation(*var));
	}
}


ElectricTerminal::~ElectricTerminal()
{
	FreeAll();
}

bool ElectricTerminal::TryReserve(ElectricReservation * er)
{
	bool success = true;
	try {
		std::vector<ElectricReservation*> overlap;
		ReservationOverlap(er, overlap);
		if (overlap.empty())
		{
			mOrderedSetReservation.insert(er);
		}
		else
		{
			success = false;
		}

	}
	catch (...)
	{
		success = false;
	}

	if (!success)
	{
		cout << "WARNING TryReserve failed for " << *er << endl;
		cout << er->m_pVehicle->toString() << endl;
		cout << er->m_pVehicle->toStringScenario() << endl;
		cout << this->toString() << endl;
	}
	//cout << *this << endl;

	return success;
}

bool ElectricTerminal::TryFree(ElectricReservation * er)
{
	//cout << "ElectricTerminal::TryFree - START" << endl;
	//cout << "ElectricTerminal::TryFree - before state" << endl << *this<<endl;
	bool success = true;
	try {
		setER::iterator it = Find(er->GetTimeRange().mStart, mOrderedSetReservation, SET_OPERATOR_RESERVATION(find));

		if (it != mOrderedSetReservation.end())
		{
			//cout << "ElectricTerminal::TryFree - after find" << endl;
			mOrderedSetReservation.erase(it);
		}
		else
		{
			success = false;
		}

	}
	catch (...)
	{
		success = false;
	}
	//cout << "ElectricTerminal::TryFree - end state" << endl << *this<<endl;
	//cout << "ElectricTerminal::TryFree - success=" << success << endl;
	return success;
}

void ElectricTerminal::FreeAll()
{
	for (ElectricReservationPtr var : mOrderedSetReservation)
	{
		delete var;
	}

	mOrderedSetReservation.clear();
}

void ElectricTerminal::ReservationOverlap(ElectricReservation* er, std::vector<ElectricReservation*>& out_VecOverlapingReservation)
{
	ReservationOverlap(er->GetTimeRange(), out_VecOverlapingReservation);
}

void ElectricTerminal::ReservationOverlap(TimeRange tr, std::vector<ElectricReservation*>& out_VecOverlapingReservation)
{
	//cout << endl << endl;
	//cout << "TESTING OVERLAP  FOR " << tr << endl;
	//cout << "Terminal state is " << endl << *this << endl;
	//ElectricReservation tempStart(tr, nullptr);
	//TimeRange endRange(tr.mEnd,tr.mEnd + tr.Duration());
	//ElectricReservation tempEnd(endRange, nullptr);

	//cout << "TEMP RESERVATIONS ARE" << endl;
	//cout << tempStart << endl;
	//cout << tempEnd << endl;

	//setER::iterator itSoonest = mOrderedSetReservation.lower_bound(&tempStart);
	//setER::iterator itLatest = mOrderedSetReservation.lower_bound(&tempEnd);

	//if (itSoonest != mOrderedSetReservation.end())
	//{
	//	cout << "Soonest " << *itSoonest << endl;
	//}
	//if (itLatest != mOrderedSetReservation.end())
	//{
		//cout << "Latest" << *itLatest << endl;
		//itLatest++;
	//}

	//TODO: we have a iterator issue we do not handle properly... checking all the entries from the start
	for (setER::iterator it = mOrderedSetReservation.begin(); it != mOrderedSetReservation.end(); ++it)
	{
		//We have an overlap!
		TimeRange test = (*it)->GetTimeRange();
		//cout << "Overlap potential" << *it << endl;
		if ( test && tr)
		{
			//cout << "Overlap confirmed" << *it << endl;
			out_VecOverlapingReservation.push_back(*it);
		}
	}
}

TimeRange ElectricTerminal::FindNextAvailableReservation(TimeRange in_IdealRange)
{
	std::vector<ElectricReservation*> vOverlappingBuffer;
	TimeRange returnRange = in_IdealRange;
	TimeUnit duration = returnRange.Duration();

	bool bSearching = true;
	while (bSearching)
	{
		vOverlappingBuffer.clear();
		ReservationOverlap(returnRange, vOverlappingBuffer);
		if (vOverlappingBuffer.empty())
		{
			//We found it!
			bSearching = false;
			break;
		}

		TimeUnit highestEnd = 0;
		//We should continue the search at the end of the last one...
		for (auto var : vOverlappingBuffer)
		{
			highestEnd = std::max(highestEnd,var->GetTimeRange().mEnd);
		}
		returnRange = TimeRange(highestEnd, highestEnd + duration);
	}
	//cout << "RESERVATION RANGE RETURNED " << returnRange <<  endl;
	return returnRange;
}


string ElectricTerminal::toString() const
{
	std::ostringstream ss;
	ss << setw(20) << left << "Terminal name" << setw(20) << left << GetName() << endl;
	ss << setw(20) << left << "Terminal index" << setw(20) << left << GetIndex() << endl;
	ss << "Reservations" << endl;
	for (auto var : mOrderedSetReservation)
	{
		ss << *var << endl;
	}

	return ss.str();
}

ostream & operator<<(ostream & os, const ElectricTerminal & dt)
{
	os << dt.toString();
	return os;
}
