#include "VehicleSolverCooperation.h"

#include "IMapSolver.h"
#include "VehicleSolution.h"
#include "ElectricCircuit.h"
#include "RequestCarte.h"
#include "Path.h"
#include "IObjectiveFunction.h"
#include "SolutionCutter.h"


VehicleSolverCooperation::VehicleSolverCooperation(IMapSolver const * const inCarte, IObjectiveFunction * inObjectiveFunction, ElectricCircuit * inElectricCircuit, float in_CollisionHorizon)
	:IVehicleSolver(inObjectiveFunction, inElectricCircuit)
	, mRefCarte(inCarte)
	, mRefObjectiveFunction(inObjectiveFunction)
	, m_fCollisionHorizon(in_CollisionHorizon)
{
}

VehicleSolverCooperation::~VehicleSolverCooperation()
{
}

void VehicleSolverCooperation::SolveFor(Vehicle * inVehicleToSolve, VehicleSolution * out_RefSolution)
{
	RequestCarte request;
	request.mRefElectricCircuit = mRefElectricCircuit;
	request.mRefVehicleRequested = inVehicleToSolve;
	request.bCreateActionWaiting = true;
	request.fMinimizeWaitingHorizon = m_fCollisionHorizon;
	//cout << *mRefElectricCircuit << endl;
	//cout << "VehicleSolverCooperation::SolveFor -Request:" << endl;
	//cout << request << endl << endl;
	//cout << "VehicleSolverCooperation::SolveFor -Solution before" << endl;
	//cout << *out_RefSolution << endl << endl;
	//cout << "VehicleSolverCooperation::SolveFor -Solution before (detail)" << endl;
	//cout << out_RefSolution->toStringInteractive() << endl << endl;
	//cout << "VehicleSolverCooperation::SolveFor -Solution before (detail end)" << endl;
	mRefCarte->calculerTrajet((*out_RefSolution->mPath), request);
	//cout << "VehicleSolverCooperation::SolveFor -Solution after (detail)" << endl;
	//out_RefSolution->displayTrajectoryDetail(cout) << endl << endl;
	//cout <<*out_RefSolution << endl;
	//cout << "VehicleSolverCooperation::SolveFor -Solution after (detail end)" << endl;
	//cout << *out_RefSolution << endl << endl;
	SolutionCutter solCutter;
	solCutter.addRemainingReservation(out_RefSolution);
}
