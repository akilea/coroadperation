#pragma once
#include "Arrangement.h"

//Last one that was added will trickle down from first to last
class ArrangementCascade :
	public Arrangement
{
protected:
	size_t mIndexToMove = 0;
	size_t mQuantityToMove = 0;
public:
	ArrangementCascade();
	virtual ~ArrangementCascade();

	virtual void startSequence(std::vector<Vehicle*>* in_refVector);
	virtual std::vector<Vehicle*>* GetNextSequence();

};

