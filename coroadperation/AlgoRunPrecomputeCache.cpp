#include "AlgoRunPrecomputeCache.h"
#include "PathCache.h"
#include "carte.h"

#include <iostream>
#include <fstream>

using namespace std;


AlgoRunPrecomputeCache::AlgoRunPrecomputeCache()
{
}


AlgoRunPrecomputeCache::~AlgoRunPrecomputeCache()
{
}

int AlgoRunPrecomputeCache::Run(int argc, const char** argv)
{
	//Compute cache for borne and other
	string fileNameCarte = "quebec-carte.txt";
	string fileNameBorne = "quebec-bornes.txt";
	string fileNameCache = "distanceBorne.cache";


	//Compute distance between stations
	//PathCache pc;
	//ifstream inputFile(fileNameCache);
	//inputFile >> pc;
	//inputFile.close();
	

	Carte carte;
	{
		ifstream iscarte(fileNameCarte);
		if (iscarte.fail()) {
			cerr << "Erreur d'ouverture du fichier carte: '" << argv[1] << "' !" << endl;
			return 2;
		}
		iscarte >> carte;
		cerr << "Carte charg�e!" << endl;
	}
	{
		ifstream isbornes(fileNameBorne);
		if (isbornes.fail()) {
			cerr << "Erreur d'ouverture du fichier bornes: '" << argv[1] << "' !" << endl;
			return 2;
		}
		while (isbornes) {
			string nom;
			char L;
			int niveau;
			Point point;
			isbornes >> nom;
			if (!isbornes || nom.empty()) break;
			isbornes >> L >> niveau >> point >> ws;
			assert(L == 'L');
			cerr << "."; cerr.flush();
			carte.ajouterBorne(nom, point, niveau);
		}
		cerr << "Bornes charg�es!" << endl;
	}

	//Compute distance between stations
	ofstream outputFile(fileNameCache);
	carte.calculMatriceDistancesBornes(outputFile);
	outputFile.close();

	return 0;
}



