#include "AlgoRunTestPiercing.h"

#include "Scenario.h"
#include "ISystemSolver.h"
#include "SystemSolution.h"
#include "VehicleSolution.h"
#include "Path.h"
#include "PathDistribution.h"
#include "CollisionWithReference.h"
#include "CollisionAnalyser.h"
#include "ScenarioData.h"
#include "SolutionCutter.h"
#include "Vehicle.h"

#include <iostream>
#include <iomanip>

int AlgoRunTestPiercing::Run(int argc, const char** argv)
{

	AlgoRunCommon::Run(argc, argv);

	scenario->SetActivation(Scenario::eINIT, true);
	scenario->SetActivation(Scenario::eNO_COOP, true);
	scenario->SetActivation(Scenario::eNO_COOP_WAIT, true);
	scenario->SetActivation(Scenario::eCOOP_NO_OPTIMIZATION, true);
	scenario->SetActivation(Scenario::eHCA_FI, true);

	scenario->SolveAllVehicle();
	scenario->SolveAllSystem();
	std::set<Vehicle*> setVehicle;

	cout << "*********Solution before piercing**********" << endl;
	cout << *scenario->GetSystemSolver(Scenario::eNO_COOP_WAIT)->GetCurrentSystemSolution();
	//*********2 vehicle with same path*********

	//Pierce from the start, no time limit
	//Should be empty
	//OK
	//CollisionAnalyser::PierceFrom(setVehicle, scenario->GetSystemSolver(Scenario::eNO_COOP_WAIT)->GetCurrentSystemSolution(), scenario->GetRefData()->mVecVehicle[0], nullptr, 0.0f);

	//Should have vehicle 1 full and 2 empty
	//CollisionAnalyser::PierceFrom(setVehicle, scenario->GetSystemSolver(Scenario::eNO_COOP_WAIT)->GetCurrentSystemSolution(), scenario->GetRefData()->mVecVehicle[8], nullptr, 0.0f);




	//Take into account commitment so no change before first terminal
	//OK
	CollisionAnalyser::PierceFrom(setVehicle, scenario->GetSystemSolver(Scenario::eNO_COOP_WAIT)->GetCurrentSystemSolution(), scenario->GetRefData()->mVecVehicle[6], nullptr, 100.0f);

	//Change from second car from after first terminal
	//CollisionAnalyser::PierceFrom(setVehicle, scenario->GetSystemSolver(Scenario::eNO_COOP_WAIT)->GetCurrentSystemSolution(), scenario->GetRefData()->mVecVehicle[1], nullptr, 100.0f);

	//OK
	//CollisionAnalyser::PierceFrom(setVehicle, scenario->GetSystemSolver(Scenario::eNO_COOP_WAIT)->GetCurrentSystemSolution(), scenario->GetRefData()->mVecVehicle[0], nullptr, 9500.0f);

	//OK
	//CollisionAnalyser::PierceFrom(setVehicle, scenario->GetSystemSolver(Scenario::eNO_COOP_WAIT)->GetCurrentSystemSolution(), scenario->GetRefData()->mVecVehicle[0], nullptr, 600000.0f);





	cout << "*********Solution after piercing**********" << endl;
	cout << *scenario->GetSystemSolver(Scenario::eNO_COOP_WAIT)->GetCurrentSystemSolution();


	cout << "Affected vehicle" << endl;
	for (auto it : setVehicle)
	{
		cout << it->GetName() << endl;
	}
	cout << "*****End list****" << endl;

	cout << "ENDED WITHOUT CRASH" << endl;

	AlgoRunCommon::endRun();
	return 0;
}
