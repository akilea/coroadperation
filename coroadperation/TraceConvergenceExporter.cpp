#include "TraceConvergenceExporter.h"
#include <iostream>
#include <fstream>

bool TraceConvergenceExporter::m_isEnabled = false;

bool TraceConvergenceExporter::FileExists() const
{
	ifstream f(m_strExportName.c_str());
	return f.good();
}
TraceConvergenceExporter::TraceConvergenceExporter()
{
	m_vecTrace.reserve(1000);
}

void TraceConvergenceExporter::SetExportName(string name)
{
	m_strAlgoName = name;
}

void TraceConvergenceExporter::Trace(STrace in_trace)
{
	if (m_isEnabled)
	{
		float fBest = in_trace.valueA;
		if (in_trace.valueB < in_trace.valueA)
		{
			fBest = in_trace.valueB;
		}
		if (m_vecTrace.empty())
		{
			m_fAsIsOrder = fBest;
		}
		m_vecTrace.push_back(in_trace);
	}
}

void TraceConvergenceExporter::ExportFlat() const
{
	if (m_strAlgoName.empty() || !m_isEnabled)
	{
		return;
	}
	bool alreadyExist = this->FileExists();
	ofstream osExportDetail(m_strExportName, std::fstream::app);
	if (osExportDetail.fail()) {
		cerr << "Erreur d'ouverture du fichier detail: '" << m_strAlgoName << "' !" << endl;
	}
	else
	{
		if (!alreadyExist)
		{
			osExportDetail <<"Algorithm"<< "," <<"Iteration" << "," << "NbVoiture" << "," << "TypeValeur" << "," << "Valeur" << endl;
		}
		for (size_t i = 0; i < m_vecTrace.size(); ++i)
		{
			float fBest = m_vecTrace[i].valueA;
			int iBest = m_vecTrace[i].sizeA;
			if (m_vecTrace[i].valueB < m_vecTrace[i].valueA)
			{
				fBest = m_vecTrace[i].valueB;
				iBest = m_vecTrace[i].sizeB;
			}
			osExportDetail << m_strAlgoName << ","<< i + 1 << ","<< iBest << "," << "NouvelleValeur" << "," << m_vecTrace[i].valueA << endl;
			osExportDetail << m_strAlgoName << ","<< i + 1 << "," << iBest << "," << "ValeurGardee" << "," << m_vecTrace[i].valueB << endl;
			osExportDetail << m_strAlgoName << ","<< i + 1 << "," << iBest << "," << "PNCARR" << "," << m_fAsIsOrder << endl;
		}
		cerr << m_strExportName << "Trace export�e!" << endl;
	}
}

void TraceConvergenceExporter::ExportFlatWindowed() const
{
	if (m_strAlgoName.empty() || !m_isEnabled)
	{
		return;
	}
	bool alreadyExist = this->FileExists();
	ofstream osExportDetail(m_strExportName, std::fstream::app);
	if (osExportDetail.fail()) {
		cerr << "Erreur d'ouverture du fichier detail: '" << m_strAlgoName << "' !" << endl;
	}
	else
	{
		if (!alreadyExist)
		{
			osExportDetail << "Algorithm" << "," << "Iteration" << "," << "NbVoiture" << "," << "TempsFenetre" << "," << "Valeur" << endl;
		}
		for (size_t i = 0; i < m_vecTrace.size(); ++i)
		{
			float fBest = m_vecTrace[i].valueA;
			osExportDetail << m_strAlgoName << "," << i + 1 << "," << m_vecTrace[i].sizeA << "," << m_vecTrace[i].valueB+1 << "," << m_vecTrace[i].valueA << endl;
		}
		cerr << m_strExportName << "Trace export�e!" << endl;
	}
}