#pragma once

#include <string>
class Carte;

class CarteLoader
{
public:
	CarteLoader();
	virtual ~CarteLoader();

	void Load(Carte* ptrToLoad_ToLoad, std::string in_fileName);
};

