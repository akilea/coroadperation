#pragma once

#include <string>
#include <vector>
#include "IScenario.h"
class Vehicle;
class IMapSolver;
class ElectricCircuit;
class IScenarioLoader;

using namespace std;

class ScenarioData;
class IVehicleSolver;
class IObjectiveFunction;
class ISystemSolver;

class Scenario : public IScenario
{
protected:
	ScenarioData* mLoaderData;

public:
	enum SolverType
	{
		//Basic
		eINIT = 0,
		eNO_COOP,
		eNO_COOP_WAIT,
		eCOOP_NO_OPTIMIZATION,
		//Ordered
		eHCA_FI,
		eWHCA_LARGE_FI,
		eWHCA_MEDIUM_FI,
		eWHCA_SMALL_FI,
		eHCA_CASCADE,
		eWHCA_LARGE_CASCADE,
		eWHCA_MEDIUM_CASCADE,
		eWHCA_SMALL_CASCADE,
		eHCA_PERMUTATION,
		eWHCA_LARGE_PERMUTATION,
		eWHCA_MEDIUM_PERMUTATION,
		eWHCA_SMALL_PERMUTATION,
		PCARLG_PNCARR,
		PCARL_PNCARR,
		PCARLG_PNCARR_C,
		PCARL_PNCARR_C,
		PCARLG_PNCAR,
		PCARL_PNCAR,
		PCARLG_PNCAR_C,
		PCARL_PNCAR_C,
		ePCC,

		eMAX_SOLVERTYPE
	};

	enum TArrangement {
		eNONE = 0,
		eCASCADE,
		ePERMUTATION,

		eMAX_ARRANGEMENT
	};

	enum TObjective
	{
		eAVERAGEDURATION = 0,
		eWORSTDURATION,
		eSQUAREDDIFFERENCEDURATION,
		eAVERAGEPENALITY,
		eWORSTPENALITY,
		eSQUAREDDIFFERENCEPENALITY,
		eMAX
	};

	static const string sArrangementAssociation[eMAX_ARRANGEMENT];

	Scenario(IScenarioLoader* inScenarioLoader, IMapSolver const & inRefCarte,int in_nMetrique=0);
	~Scenario();

	virtual ScenarioData* GetRefData() const { return mLoaderData; }
	std::string toStringPerformance(IObjectiveFunction* ptrObjFx) const;
	void AfterSolveNextVehicleCallback(ISystemSolver* lastSolverUsed, Vehicle* lastVehicleAdded);

	void CreateLocalSolver(IMapSolver const& inRefCarte,bool isGreedy, bool isStartingFresh, bool isBaseCooperative);

};

