#include "ElectricReservationList.h"

#include "ElectricReservation.h"
#include <sstream>


ElectricReservationList::ElectricReservationList(TimeUnit in_TimeStamp)
:mTimeStamp(in_TimeStamp)
{
}

ElectricReservationList::ElectricReservationList()
{
}


ElectricReservationList::~ElectricReservationList()
{
	electricReservationList.erase();
}

bool ElectricReservationList::IsEmpty()
{
	return electricReservationList.length() == 0;
}

void ElectricReservationList::Insert(ElectricReservation * er)
{
	electricReservationList.insert(er);
}

void ElectricReservationList::Erase(ElectricReservation * er)
{
	electricReservationList.erase(er);
}

void ElectricReservationList::TryErase(ElectricReservation * er)
{
	if (electricReservationList.exists(er))
	{
		electricReservationList.erase(er);
	}
}

void ElectricReservationList::UnionOut(CRP::set<ElectricReservation*>& out_setToAdd)
{
	out_setToAdd = out_setToAdd | electricReservationList;
}

void ElectricReservationList::UnionWith(CRP::set<ElectricReservation*>& in_setToUnion)
{
	electricReservationList = electricReservationList | in_setToUnion;
}

void ElectricReservationList::UnionWith(ElectricReservationList & in_listToUnion)
{
	this->electricReservationList = this->electricReservationList | in_listToUnion.electricReservationList;
}

string ElectricReservationList::ReservationToString() const
{
	std::ostringstream ss;
	ss << "@" << GetTimeStamp() << ":";
	for (CRP::set<ElectricReservationPtr>::const_iterator i = electricReservationList.begin(); i != electricReservationList.end(); ++i) {
		ss << (*(*i)) << ", ";
	}
	return ss.str();
}

ostream & operator<<(ostream & os, const ElectricReservationList & dt)
{
	os << dt.GetTimeStamp();
	return os;
}

ostream & operator<<(ostream & os, const ElectricReservationListPtr & dt)
{
	return os << *dt;
}
