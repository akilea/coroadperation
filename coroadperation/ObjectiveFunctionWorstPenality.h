#pragma once
#include "IObjectiveFunction.h"
class ObjectiveFunctionWorstPenality :
    public IObjectiveFunction
{
private:
	SystemSolution* mRefReferenceSystemSolution = nullptr;

public:
	ObjectiveFunctionWorstPenality(SystemSolution* refReferenceSystemSolution);
	virtual ~ObjectiveFunctionWorstPenality() {}
	virtual std::string Description() const { return  "PirePenalite"; }
	virtual float Evaluate(SystemSolution const* const in_A);
};

