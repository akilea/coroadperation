#pragma once
#include <ostream>
#include <string>
#include <sstream>
#include <iomanip>
#include "SimulationSetting.h"
using namespace std;

class SModel
{
public:
	int m_nBestAutonomyMile; //Info � laquelle on a acc�s
	int m_nPercentRecurrence;

	SModel(int in_nBestAutonomyMile, int in_nPercentRecurrence);

	int AutonomyToMeterInWinter() const;

	string toString() const;
};


