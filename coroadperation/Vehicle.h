#pragma once

#include <string>
#include "srcMap/point.h"
#include "srcMap/StructureCarte.h"
#include "SimulationSetting.h"
class Path;
class Carte;
using namespace std;

class Vehicle
{
private:
	string mName = "None";
	float mDepartureTime = 0;
	Point mPointDeparture;
	Point mPointArrival;
	double mAutonomie = 0.0;
	float mL1MaxChargingTime = 1000.0f;

	//TODO: we consider average speed but it should change per segment.
	float mAverageSpeedMPerS = SimulationSetting::D_AVERAGE_VEHICLE_SPEED;

public:
	Vehicle();
	Vehicle(string name, Point a, Point b, double autonomie, float departureTime = 0, float L1FullChargingTime = 1800.0f);
	~Vehicle();

	string toString() const;
	string toStringScenario() const;

	string GetName() const { return mName; }
	float GetDepartureTime() const { return mDepartureTime; }
	Point GetPointDeparture() const { return mPointDeparture; }
	Point GetPointArrival() const { return mPointArrival; }
	double GetAutonomie() const { return mAutonomie; }
	float GetAverageSpeedMPerS() const { return mAverageSpeedMPerS; }
	float GetL1MaxChargingTime() const { return mL1MaxChargingTime; }

	friend ostream& operator<<(ostream& os, const Vehicle& dt);

};

ostream& operator<<(ostream& os, const Vehicle& dt);

bool VehicleAscendingSort(Vehicle* i, Vehicle* j);
