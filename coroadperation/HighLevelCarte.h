#pragma once
#include "IMapSolver.h"
#include "PathCache.h"
#include "srcMap/carte.h"


class HighLevelCarte :
	public IMapSolver
{
private :
	mutable PathCache mCache;
	Carte* mRefCarte;
	int m_idCacheVehicleStart = -1;
	int m_idCacheVehicleEnd = -1;

	struct TrajectoryTuple
	{
		TrajectoryTuple(int d, int e)
			:nIndexDepart(d),
			nIndexArrive(e)
		{}
		int nIndexArrive = 0;
		int nIndexDepart = 0;
	};

	mutable std::vector<TrajectoryTuple> m_vecPathToClear;

	//For start and end, respectively...
	//Point: latitude and longitude
	//mapID: Id in the cache
	//index: indexLieu in the lowlevel carte
	bool tryAddVehiclePathInCache(const Point &in_ptStart, const Point &in_ptEnd, int in_mapIDStart, int in_mapIDEnd, int in_nIndexStart, int in_nIndexEnd, double in_dMaxAutonomie) const;
	void clearAllVehiclePathInCache() const;
	void loadCache(string fileCache);
	void onPostLoadCache();
	int getStartVehicleID() const { return m_idCacheVehicleStart; }
	int getEndVehicleID() const { return m_idCacheVehicleEnd; }
public:
	HighLevelCarte(string fileCache,Carte* lowLevelCarte);
	virtual ~HighLevelCarte();
	float ComputeTerminalDensity() const;

	//IMapSolver interface implementation
	virtual double calculerTrajet(Path& in_out_path, RequestCarte & inRequestCarte) const;
	//Below are utilities for calculerTrajet
};

