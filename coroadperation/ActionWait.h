#pragma once
#include "Action.h"

class ElectricTerminal;
class ElectricCircuit;

class ActionWait :
	public Action
{
private:

public:
	ActionWait(ElectricCircuit* const inRefCircuit, ElectricTerminal* const inTerminal);
	virtual ~ActionWait();

	virtual TAction getType() const { return TAction::eWait; }

	virtual Action* clone() const;
};

