#include "PathCache.h"
#include <iostream>
#include <algorithm>
#include "RequestCarte.h"
#include <string>


PathCache::PathCache()
{
}


PathCache::~PathCache()
{
}

void PathCache::addPath(int depart, int destination, double distance)
{
	m_unHighestTerminalID = std::max((size_t)depart, m_unHighestTerminalID);
	m_unHighestTerminalID = std::max((size_t)destination, m_unHighestTerminalID);
	mIndexLieuDistanceCache[depart][destination] = distance;
	//cout << "VALUE" << endl;
	//cout << depart << endl;
	//cout << destination << endl;
	//cout << "mIndexLieuDistanceCache[depart][destination]" << mIndexLieuDistanceCache[depart][destination] << endl;
}

void PathCache::removePath(int depart, int destination)
{
	mIndexLieuDistanceCache[depart].erase(destination);
}

void PathCache::stripAtAndAbove(int indexToStripFrom)
{
	for (int i = 0; i < m_unHighestTerminalID;++i)
	{
		if (mIndexLieuDistanceCache.count(i) > 0)
		{
			if (i >= indexToStripFrom)
			{
				mIndexLieuDistanceCache.erase(i);
			}
			else
			{
				for (int j = indexToStripFrom; j < m_unHighestTerminalID + 1; ++j)
				{
					if (mIndexLieuDistanceCache.at(i).count(j) > 0)
					{
						mIndexLieuDistanceCache.at(i).erase(j);
					}
				}
			}
		}
	}
}

double PathCache::getPath(int depart, int destination) const
{
	return mIndexLieuDistanceCache.at(depart).at(destination);
}

PathCacheNeighbourMap PathCache::getPathNeighbourMap(int depart) const {
	return mIndexLieuDistanceCache.find(depart)->second;
}

bool PathCache::hasNeighbour(int depart) const
{
	auto it = mIndexLieuDistanceCache.find(depart);
	return it != mIndexLieuDistanceCache.end() && !it->second.empty();
}

float PathCache::ComputeTerminalDensity(set<int>& vTerminalToIgnore) const
{
	float density = 0.0f;
	size_t total = 0;
	if (mIndexLieuDistanceCache.size() > 0)
	{
		for (auto mapDest = mIndexLieuDistanceCache.begin(); mapDest != mIndexLieuDistanceCache.end(); mapDest++)
		{
			if (vTerminalToIgnore.count(mapDest->first) == 0)
			{
				++total;
				density += (float)mapDest->second.size();
			}
		}
	density /= (float)total;
	}
	return density;
}

ostream & operator<<(ostream & out, const PathCache & p)
{
	for (auto mapDest = p.mIndexLieuDistanceCache.begin(); mapDest != p.mIndexLieuDistanceCache.end(); mapDest++)
	{
		for (auto path = mapDest->second.begin(); path != mapDest->second.end(); path++)
		{
			out << mapDest->first << " " << path->first << " " << path->second << std::endl;
		}
	}
	return out;
}

istream & operator>>(istream & in, PathCache & p)
{
	int depart, destination;
	double dDistance = 0.0;
	while(in >> depart >> destination >> dDistance)
	{
		if(RequestCarte::IsAcceptableDistance(dDistance))
		{
			p.addPath(depart, destination, dDistance);
		}//Else skip the entry. We avoid useless tests in the final high level computation
	}
	return in;
}
