#include "SimulationSetting.h"

const double SimulationSetting::D_ACCEPTABLE_MAX_DISTANCE = 400000; //400000m = 400km
const double SimulationSetting::D_ACCEPTABLE_MIN_DISTANCE_SCENARIO = 109000; //Kia SOUL EV winter reach
const double SimulationSetting::D_ACCEPTABLE_MIN_DISTANCE = 5000; // 5km

const double SimulationSetting::D_AVERAGE_VEHICLE_SPEED = 20.0; // 20m/s (72km/h), correspond � 3.5h de route avec une autonomie de 250Km
const double SimulationSetting::D_AVERAGE_VEHICLE_CHARGE_RATE_M_PER_SECOND = 50.0;

const double SimulationSetting::D_COMPUTATION_TIME_CAP = 300.0; // How long simulation run on max in seconds

const std::vector<float> SimulationSetting::VF_VEC_WHCA_WINDOWS = { 10800,7200,3600 }; //3h,2h,1h
const std::vector<std::string> SimulationSetting::VSTR_VEC_WHCA_WINDOWS_NAME = { "large","medium","small" };
const float SimulationSetting::F_WHCA_RECOMPUTE_PERCENTAGE = 0.5f;

const std::string SimulationSetting::STR_LAST_ACTION_NAME = "to destination";