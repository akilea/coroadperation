#include "Collision.h"
#include "ActionTerminal.h"
#include "ActionWait.h"
#include "VehicleSolution.h"
#include "Vehicle.h"
#include "Path.h"


Collision::Collision(VehicleSolution* solW, ActionWait* actW, VehicleSolution* solB, ActionTerminal* actB)
{
	Set(solW, actW, solB, actB);
}

Collision::Collision()
{
	Set(nullptr, nullptr, nullptr, nullptr);
}

bool Collision::HasCollision() const
{
	return mWaiterSolution != nullptr && mWaiterWaitAction != nullptr && mBlockerSolution != nullptr && mBlockerTerminalAction != nullptr;
}

void Collision::Set(VehicleSolution* solW, ActionWait* actW, VehicleSolution* solB, ActionTerminal* actB)
{
	mWaiterSolution = solW;
	mWaiterWaitAction=actW;
	mBlockerSolution=solB;
	mBlockerTerminalAction=actB;
}

ostream& operator<<(ostream& os, const Collision& dt)
{
	os << "Displaying collision at " << dt.GetBlockerActionTerminal()->getTerminal()->GetName() << endl;
	os << "Start=" << dt.GetWaiterActionWait()->getStartTime() << endl;
	os << "Waiting=" << dt.GetWaiterActionWait()->getDuration() << endl;
	os << "WaiterName=" << dt.GetWaiterSolution()->mRefVehicle->GetName() << endl;
	os << "BlockerName=" << dt.GetBlockerSolution()->mRefVehicle->GetName() << endl;
	os << "Details waiter" << endl;
	os << *dt.GetWaiterSolution() << endl;
	os << "Details blocker" << endl;
	os << *dt.GetBlockerSolution() << endl;
	return os;
}
