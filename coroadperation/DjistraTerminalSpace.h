#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <list>

#include <limits> // for numeric_limits

#include <set>
#include <utility> // for pair
#include <algorithm>
#include <iterator>
#include "PathCache.h"
#include "PathDistribution.h"
#include "srcMap/point.h"
class Path;
class RequestCarte;
class Carte;
class ElectricTerminal;

const float max_weight = std::numeric_limits<float>::infinity();

class DjistraTerminalSpace
{
public:
	DjistraTerminalSpace();
	~DjistraTerminalSpace();

	//HOW TO USE: Construct the graph
	void PreComputePaths(int source, float fstartTime, int destination,const PathCache& in_refPathCache, RequestCarte & inRequestCarte, const Carte* in_Carte, PathDistribution pathD); //TODO: use strategy design pattern

	//Then use to get distance or full path (or both)
	double BeleivedCostTo();
	double TrueCostTo();
	PathDistribution DistributionTo();
	bool HasPath(int to); // False if self or inaccessible

	//Heavy function
	//Fully convert the data into a Path sequence
	void ActionSequenceTo(Path& out_Path, const RequestCarte & inRequestCarte, const Carte* in_Carte, float fDepartureTime, PathDistribution pathD);

private:
	typedef std::pair<float, int> ValueIndex;

	class SmallerFValueComparison
	{
	public:
		bool operator() (const ValueIndex& lhs, const ValueIndex&rhs) const
		{
			return (lhs.first < rhs.first);
		}
	} FValComp;

	//These are just to have cost calculation details
	// Cost = road_time + wait_time + charge_time
	//These are just to use when we create our actions

	std::vector<PathDistribution> path_distribution;
	PathDistribution total_path_distribution_to_destination;
	std::vector<float> mGScore;
	//Artefact so we can quantify the lost wait time
	std::vector<float> mTrueGScore;
	std::vector<float> mFScore;

	float h(int source, int dest, RequestCarte & inRequestCarte,const Carte* in_Carte);
	Point findPoint(int vertex,RequestCarte & inRequestCarte,const Carte* in_Carte);

	//Just a reminder from where we started
	int m_VertexSource = -1;
	int m_VertexDestination = -1;
	//Stack of visited nodes
	std::vector<int> mCameFrom;
	std::set<int> mSetOpen;
	std::set<int> mSetClosed;


	void DijkstraComputePaths(int source,
		float fstartTime,
		int destination,
		const PathCache& in_refPathCache,
		RequestCarte & inRequestCarte,
		const Carte* in_Carte,
		PathDistribution pathD);
};