/*  INF3105 - Structures de données et algorithmes
    UQAM | Faculté des sciences | Département d'informatique
    Été 2016 | TP3
    http://ericbeaudry.ca/INF3105/tp3/ -- beaudry.eric@uqam.ca    */
#include <cassert>
#include <cstdlib>
#include "carte.h"

/* Lire une carte. 
   Normalement, vous n'aurez pas à modifier ceci.
*/
istream& operator >> (istream& is, Carte& carte)
{
    // Lire les lieux
    while(is){
        string nomlieu;
        long long osmid;
        is >> nomlieu;
        if(nomlieu == "---") break;
        assert(nomlieu[0]=='n');
		string strIdLieu = nomlieu;
		strIdLieu.erase(0, 1);
		osmid = atoll(strIdLieu.c_str());
        Point point;
        char pv;
        is >> point >> pv;
        assert(pv==';');
        carte.ajouterNoeud(osmid, point);
    }

    // Lire les routes
    while(is){
        string nomroute;
        is >> nomroute;
        if(nomroute == "---") break;
        
        char deuxpoints;
        is >> deuxpoints;
        assert(deuxpoints == ':');
        
        std::list<long long> listenomslieux;
        while(is){
            string nomlieu;
            is>>nomlieu;
            if(nomlieu==";") break;
            assert(nomlieu!=":"); // pour robustesse
            assert(nomlieu.find(";")==string::npos); // pour robustesse
			long long osmid;
            assert(nomlieu[0]=='n');
            osmid = atoll(nomlieu.c_str()+1);
            listenomslieux.push_back(osmid);
        }
        carte.ajouterRoute(nomroute, listenomslieux);
    }

    return is;
}

