/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Été 2014 / TP1                                       *
 *  Eric Beaudry - beaudry.eric@uqam.ca - ericbeaudry.ca */

#include <math.h>
#include <assert.h>
#include "point.h"
#define RAYONTERRE 6371000  // en mètres
#define PI 3.14159265359

Point::Point(const Point& point)
  : latitude(point.latitude), longitude(point.longitude)
{
}

Point::Point(double latitude_, double longitude_)
  : latitude(latitude_*PI / 180.0), longitude(longitude_*PI / 180.0)
  //: latitude(latitude_), longitude(longitude_)
{
}

//Distance en metres
double Point::distance(const Point& point) const {
  double s1 = sin((point.latitude-latitude)/2);
  double s2 = sin((point.longitude-longitude)/2);
  return double(2)*double(RAYONTERRE) * asin(sqrt(s1*s1 + cos(latitude)*cos(point.latitude)*s2*s2));
}

Point& Point::operator+=(const Point& p){
  latitude  += p.latitude;
  longitude += p.longitude;
  return *this;
}
Point Point::operator+(const Point& p) const{
  Point r = *this;
  r += p;
  return r;
}
Point& Point::operator-=(const Point& p){
  latitude-=p.latitude;
  longitude-=p.longitude;
  return *this;
}
Point Point::operator-(const Point& c)const{
  Point r = *this;
  r-=c;
  return r;
}
bool Point::operator==(const Point &other) const
{
	return this->distance(other) <= FLT_EPSILON;
}

bool Point::operator!=(const Point &other) const
{
	return this->distance(other) > FLT_EPSILON;
}

Point& Point::operator*=(double s){
  latitude*=s;
  longitude*=s;
  return *this;
}
Point Point::operator*(double s)const{
  Point r = *this;
  r*=s;
  return r;
}
// produit scalaire
double Point::operator * (const Point& v) const{
  return latitude*v.latitude + longitude*v.longitude;
}

void Point::moveToMin(const Point& c){
    if(c.latitude<latitude)
        latitude=c.latitude;
    if(c.longitude<longitude)
        longitude=c.longitude;
}
void Point::moveToMax(const Point& c){
    if(c.latitude>latitude)
        latitude=c.latitude;
    if(c.longitude>longitude)
        longitude=c.longitude;
}

Point Point::newPointFromRadian(double lat, double longitude)
{
	Point pt;
	pt.latitude = lat;
	pt.longitude = longitude;
	return pt;
}

#include <random>
std::random_device rd;
std::mt19937 gen(rd());

Point genererPointAleatoire(const Point& min, const Point& max){
    std::uniform_real_distribution<> dist(0, 1);
	double lt = min.latitude + dist(gen) * (max.latitude - min.latitude) + 0.000000001;
	double lg = min.longitude + dist(gen) * (max.longitude - min.longitude);
    return Point::newPointFromRadian(lt,lg);
}

std::istream& operator >> (std::istream& is, Point& point) {
  char po, vir, pf;
  is >> po;
  if(is){
    is >> point.latitude >> vir >> point.longitude >> pf;
    assert(po=='(');
    assert(vir==',');
    assert(pf==')');
	point *= PI / 180.0;
  }
  return is;
}

std::ostream& operator << (std::ostream& os, const Point& point) {
  os << "(" 
     << (point.latitude * 180.0 / PI)
     << "," 
     << (point.longitude * 180.0 / PI)
     << ")";
  return os;
}


Point plusproche(const Point& A, const Point& C, const Point& D){
    Point CA = A-C;
    Point CD = D-C;
    double ratio = (CA*CD) / (CD*CD);
    if(ratio<0) ratio=0; else if(ratio>1) ratio=1;
    Point CACD = CD * ratio;
    return C + CACD;
}




Rectangle::Rectangle(const Point& c1_, const Point& c2_)
 : c1(c1_), c2(c2_)
{
}
using namespace std;
Point Rectangle::plusPres(const Point& c) const{
    Point r = c;
    //cout << "Rectangle::plusPres  :  r=" << r << endl;
    if(r.longitude<c1.longitude) r.longitude=c1.longitude;
    //cout << "Rectangle::plusPres  :  r=" << r << endl;
    if(r.latitude<c1.latitude) r.latitude=c1.latitude;
    //cout << "Rectangle::plusPres  :  r=" << r << endl;
    if(r.longitude>c2.longitude) r.longitude=c2.longitude;
    //cout << "Rectangle::plusPres  :  r=" << r << endl;
    if(r.latitude>c2.latitude) r.latitude=c2.latitude;
    //cout << "Rectangle::plusPres  :  r=" << r << endl;
    return r;
}

void Rectangle::englobe(const Point& c){
    if(c.longitude<c1.longitude) c1.longitude = c.longitude;
    if(c.latitude<c1.latitude) c1.latitude = c.latitude;
    if(c.longitude>c2.longitude) c2.longitude = c.longitude;
    if(c.latitude>c2.latitude) c2.latitude = c.latitude;
}

float Rectangle::surface_m2() const
{
	Point widthA = Point::newPointFromRadian(c1.latitude,0.f);
	Point widthB = Point::newPointFromRadian(c2.latitude,0.f);
	float width = widthA.distance(widthB);
	Point heightA = Point::newPointFromRadian(0.f,c1.longitude);
	Point heightB = Point::newPointFromRadian(0.f,c2.longitude);
	float height = heightA.distance(heightB);
	return width * height;
}
