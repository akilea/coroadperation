/*
  INF3105 -- Structures de données et algorithmes
  UQAM / Département d'informatique
  Éric Beaudry -- http://ericbeaudry.ca/INF3105 -- beaudry.eric@uqam.ca
  Été 2016
  Solution TP3
*/

#if !defined(_CARTE__H_)
#define _CARTE__H_

#include <assert.h>
#include <istream>
#include <map>
#include <list>
#include <vector>
#include <queue>
#include <set>
#include <string>
#include <vector>
#include "point.h"
#include "StructureCarte.h"
#include "../PathCache.h" // TO REMOVE
#include "../IMapSolver.h"


class Path;
class PathCache;
class RequestCarte;

using namespace std;

class Carte : public IMapSolver {
  public:
	Carte();
	Carte(string fileNameNode, string fileNameBorne, int maxBorne = INT_MAX);
    void ajouterNoeud(long long osmid, const Point& p);
    void ajouterRoute(const string& nom, list<long long >& noms);
    void ajouterBorne(const string& nom, const Point& p, int niveau=3);
    
    long long  getOsmIDPlusPres(const Point& p) const;
    int getIndexPlusPres(const Point& p) const;

    double calculerDistance(int origine, int destination, float maxdistance=160000.f) const;
    double calculerDistance(const Point& origine, const Point& destination, float maxdistance=160000.f) const;
    double calculerChemin(const Point& origine, const Point& destination, list<Point>& chemin, int maxdistance=160000) const;
    double calculerChemin(int origine, int destination, list<Point>& chemin, int maxdistance=160000) const;
    
    double calculerTrajet(Point a, Point b, list<Point>& chemin, list<string>& bornes, int autonomie=160000) const;
	double calculerTrajet(Path& in_out_path, RequestCarte & inRequestCarte) const; //Can ignore stations
	double calculerTrajetCache(Path& in_out_path, RequestCarte & inRequestCarte) const; //Can ignore stations


    void calculMatriceDistancesBornes(ostream& outSource, int minIndex = 0, int maxIndex = INT_MAX) const;
    
    void genererRequete(Point& a, Point& b, int& autonomie) const;

	const std::vector<Borne>& GetBorne() const { return bornes; }

	void LoadBorne(ifstream& inputStream, int maxBorne);
	void LoadBorne(string fileName, int maxBorne);

	void LoadNode(string fileName);
	void Load(string fileNameNode, string fileNameBorne, int maxBorne);

  private:
    // Informations attachées à un segment de route (les segments sont les arêtes du graphe).
	  const double mCstBigNumber = 10000000000000000000000000.0;

    // État de calculs de Dijkstra depuis un noeud d'origine.
    // Cela permet «résumer» l'algorithme de Dijkstra en évitant des calculs redondants.
    struct EtatCalculsDijkstra{
        map<int, InfoNoeud> visites;
        set<int> bornesatteignables;
        priority_queue<N> file; // file prioritaire de "références" de lieu (noeud)
    };
    std::vector<Lieu>     lieux;
    std::map<long long, int>   indexlieux; // index [id_osm] --> [index_du_lieu]
    std::vector<string>   routes;     // les noms des routes
    std::vector<Borne>    bornes; //Borne interne
    
    mutable map<int, EtatCalculsDijkstra> calculs; // [index de lieu] --> [Instance d'état de calculs depuis ce lieu]

	friend istream& operator >> (istream& is, Carte& carte);




    struct Niveau{
        Niveau();
        std::vector<int> numeros;
        Rectangle   rectenglobant;
        Niveau      *enfant1,
                    *enfant2;
        //double   distanceMin(const Coordonnee& c) const;
    };
    mutable Niveau* racinekdtree=NULL;
    
    void    creerKdTree() const;
    Niveau* creerNiveau(const std::vector<int>& numeros) const;
    void    detruireNiveau(Niveau*& niv) const;
    int     ipointPlusProche(const Point& point, double& distmin, int exception=-1) const;
    void    ipointPlusProche(const Niveau* niveau, const Point& point, double& distmin, int& bestpoint, int exception) const;
};

#endif

