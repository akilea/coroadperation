#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "carte.h"

int main(int argc, const char** argv){
    if(argc<2){
        cout << "./generer carte.txt bornes.txt" << endl;
        return 1;
    }
    Carte carte;
    {
        ifstream iscarte(argv[1]);
        if(iscarte.fail()){
            cerr << "Erreur d'ouverture du fichier carte: '" << argv[1] << "' !" << endl;
            return 2;
        }
        iscarte >> carte;
        cerr << "Carte chargée!" << endl;
    }
    if(argc>2)
    {
        ifstream isbornes(argv[2]);
        if(isbornes.fail()){
            cerr << "Erreur d'ouverture du fichier bornes: '" << argv[1] << "' !" << endl;
            return 2;
        }
        while(isbornes){
            string nom;
            char L;
            int niveau;
            Point point;
            isbornes >> nom;
            if(!isbornes || nom.empty()) break;
            isbornes >> L >> niveau >> point >> ws;
            assert(L=='L');
            cerr << "."; cerr.flush();
            carte.ajouterBorne(nom, point, niveau);
        }
        cerr << "Bornes chargées!" << endl;
    }

    int n = 100;
    if(argc>3)
        n = atoi(argv[3]);
    
    string prefix;
    prefix = argv[1];
    if(prefix.find(".txt")<prefix.size())
        prefix = prefix.substr(0, prefix.find(".txt"));
    if(prefix.find("-carte")<prefix.size())
        prefix = prefix.substr(0, prefix.find("-carte"));

    ofstream out0B(prefix + "-req0B.txt");
    ofstream out1B(prefix + "-req1B.txt");
    ofstream out2pB(prefix + "-req2+B.txt");
    out0B << std::setprecision(10);
    out1B << std::setprecision(10);
    out2pB << std::setprecision(10);
    int compteur0B=0, compteur1B=0, compteur2pB=0;
    while(compteur0B<n || compteur1B<n || compteur2pB<n){
        Point a, b;
        list<Point> chemin;
        list<string> bornes;
        int autonomie=160 * 1000;
        carte.genererRequete(a, b, autonomie);
        //double d = 
        carte.calculerTrajet(a, b, chemin, bornes, autonomie);
        cout << " >> Bornes : " << bornes.size();
        switch(bornes.size()){
            case 0:
                if(compteur0B>=n) break;
                out0B << a << "\t" << b << "\t" << autonomie << endl;
                compteur0B++;
                cout << " " << compteur0B;
                break;
            case 1:
                if(compteur1B>=n) break;
                out1B << a << "\t" << b << "\t" << autonomie << endl;
                compteur1B++;
                cout << " " << compteur1B;
                break;
            default:
                if(compteur2pB>=n) break;
                out2pB << a << "\t" << b << "\t" << autonomie << endl;
                compteur2pB++;
                cout << " " << compteur2pB;
                break;
        }
        cout << endl;
    }
    return 0;
}

