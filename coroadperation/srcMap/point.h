/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Été 2016 / TP3                                       *
 *  Eric Beaudry - beaudry.eric@uqam.ca - ericbeaudry.ca */

#if !defined(_POINT_SURFACE_SPHERE__H_)
#define _POINT_SURFACE_SPHERE__H_
#include <iostream>

class Point {

  public:
    Point():latitude(0.), longitude(0.){}
    Point(double latitude_, double longitude_);
    Point(const Point&);
    double distance(const Point&) const;

    Point& operator+=(const Point&);
    Point operator+(const Point&) const;
    Point& operator-=(const Point&);
    Point operator-(const Point&) const;
    bool operator==(const Point&) const;
	bool operator!=(const Point & other) const;
    Point& operator*=(double);
    Point operator*(double) const;
    double  operator * (const Point& v) const;
    
    void moveToMin(const Point&);
    void moveToMax(const Point&);

  private:
    double latitude;
    double longitude;

  friend class Rectangle;
  friend class Carte;
  friend std::ostream& operator << (std::ostream&, const Point&);
  friend std::istream& operator >> (std::istream&, Point&);
  friend Point plusproche(const Point& a, const Point& b, const Point& c);
  friend Point genererPointAleatoire(const Point& min, const Point& max);
  static Point newPointFromRadian(double lat, double longitude);
};


class Rectangle{
  public:
    Rectangle(){}
    Rectangle(const Point& c1, const Point& c2);
    
    Point plusPres(const Point& c) const;    
    void englobe(const Point& c);
    float surface_m2() const;
  private:
    Point c1, c2;
    friend class Carte;
};


#endif

