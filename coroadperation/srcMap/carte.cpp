/*
  INF3105 -- Structures de données et algorithmes
  UQAM / Département d'informatique
  Éric Beaudry -- http://ericbeaudry.ca/INF3105 -- beaudry.eric@uqam.ca
  Été 2016
  Solution TP3
*/

#include <algorithm>
#include <limits>
#include <fstream>
#include "carte.h"

#include "../Path.h"
#include "../RequestCarte.h"

//Far from ideal, but carte acts as a factory
#include "../ActionRoad.h"
#include "../ActionTerminal.h"
#include "../ActionWait.h"
#include "../Vehicle.h"
#include "../ElectricTerminal.h"

Carte::Carte()
{
}

Carte::Carte(string fileNameNode, string fileNameBorne, int maxBorne)
{
	Load(fileNameNode, fileNameBorne, maxBorne);
}

// Ajoute un nouveau lieu dans la carte.
void Carte::ajouterNoeud(long long osmid, const Point& p)
{
	static long long previousOsmid = 0;
	static long long previous2Osmid = 0;
	int& index = indexlieux[osmid];
	if (index != 0)
	{
		cout << "duplicate for lieu ID " << index << "(OSMID est " << osmid << ") " << "( previous est " << previousOsmid << ")" << p << lieux.size() << " " << lieux.max_size() <<endl;
	}
	//TODO: parler a Eric des duplicats
	assert(index == 0 || lieux.empty()); // S'assurer que le nom de lieu n'exsite pas déjà dans la carte.
	index = lieux.size(); // Attribuer un nouveau index (id)
	lieux.push_back(Lieu());
	Lieu& lieu = lieux[index];
	lieu.osmid = osmid;
	lieu.point = p;
	//cout << "osmid=" << osmid << " " << lieu.point << endl;
	previous2Osmid = previousOsmid;
	previousOsmid = osmid;
}

// Ajoute une nouvelle route dans la carte.
// Le représentation de Carte stocke les routes en segements de routes (arête).
void Carte::ajouterRoute(const string& nom, list<long long>& route)
{
	list<long long>::iterator iterroute = route.begin();
	if (iterroute == route.end()) return; // la route doit avoir au moins un lieu

	int idroute = routes.size();  // nouvel index (id) de route.
	routes.push_back(nom);        // ajouter le nom de route une fois.

	// rechercher le nom de lieu
	map<long long, int>::const_iterator iterindex = indexlieux.find(*iterroute);
	assert(iterindex != indexlieux.end()); // vérifier que le nom de lieu existe (robustesse face aux cartes erronnées).
	int lieu1 = iterindex->second; // garder le numéro de lieu
	++iterroute;
	while (iterroute != route.end())
	{
		// rechercher le nom de lieu
		iterindex = indexlieux.find(*iterroute);
		assert(iterindex != indexlieux.end()); // vérifier que le nom de lieu existe (robustesse face aux cartes erronnées).
		int lieu2 = iterindex->second;

		// Construire un segment de route (arête dans le graphe).
		SegmentRouteInfo& sri = lieux[lieu1].sortants[lieu2];
		sri.idroute = idroute;
		sri.longueur = lieux[lieu1].point.distance(lieux[lieu2].point); // précalculer la distance.

		// Dans ce TP3, les routes sont bidirectionnelles : donc, on ajoute le sens inverse.
		//SegmentRouteInfo& sri_sensinverse = lieux[lieu2].sortants[lieu1];
		//sri_sensinverse.idroute = idroute;
		//sri_sensinverse.longueur = sri.longueur;

		lieu1 = lieu2;
		++iterroute;
	}
}

void Carte::ajouterBorne(const string& nom, const Point& p, int niveau) {
	int index = getIndexPlusPres(p);
	if (lieux[index].borne < 0 || bornes[lieux[index].borne].niveau < niveau)
		lieux[index].borne = bornes.size(); //max(lieux[index].borne, niveau);
	Borne borne;
	borne.nom = nom;
	borne.niveau = niveau;
	borne.point = p;
	borne.indexlieu = index;
	bornes.push_back(borne);
}

// Trouver le nom de lieu le plus près de la coordonnée c.
long long Carte::getOsmIDPlusPres(const Point& c) const {
	int meilleurlieu = getIndexPlusPres(c);
	return lieux[meilleurlieu].osmid;
}

// Trouver l'index de lieu le plus près de la coordonnée c.
int Carte::getIndexPlusPres(const Point& p) const {
	int meilleurlieu = 0;
	double distancemeilleurlieu = numeric_limits<double>::infinity();
	if (!racinekdtree)
		creerKdTree();
	if (racinekdtree) {
		return ipointPlusProche(p, distancemeilleurlieu);
	}
	else
		cerr << "Pas KdTree!\n";
	for (unsigned int i = 0; i < lieux.size(); i++) {
		double d = lieux[i].point.distance(p);
		//cout << "d=" << d << endl;
		if (d < distancemeilleurlieu) {
			distancemeilleurlieu = d;
			meilleurlieu = i;
		}
	}
	//cout << "distancemeilleurlieu=" << distancemeilleurlieu << endl;
	return meilleurlieu;
}

double Carte::calculerDistance(const Point& origine, const Point& destination, float maxdistance) const
{
	int noeudorigine = getIndexPlusPres(origine);
	int noeuddestination = getIndexPlusPres(destination);
	calculerDistance(noeudorigine, noeuddestination, maxdistance);
	return calculs[noeudorigine].visites[noeuddestination].mind;
}

double Carte::calculerDistance(int origine, int destination, float maxdistance) const
{
	//cout << "origine=" << origine << "  destination=" << destination << endl;
	// Récupérer l'état de calcul de Dijkstra à partir du lieu origine.
	EtatCalculsDijkstra& calcul = calculs[origine];
	if (calcul.visites.empty()) {
		calcul.visites[origine].mind = 0.0;
		calcul.file.push(N(origine, 0.0));
	}

	// Boucle principale de Dijkstra. Arrêter dès que la file est vide ou que la distance optimale est trouvée pour la destination.
	while (!calcul.file.empty() && !calcul.visites[destination].optimal)
	{
		N courant = calcul.file.top();
		calcul.file.pop();
		if (courant.distance > maxdistance) {
			break;
		}
		const Lieu& n1 = lieux[courant.numero];
		if (n1.borne >= 0 && courant.numero != origine)
			calcul.bornesatteignables.insert(courant.numero);
		//cout << "n1: index=" << courant.numero << " osmid=" << lieux[courant.numero].osmid << endl;
		InfoNoeud& in1 = calcul.visites[courant.numero];
		//cout << "in1: mind=" << in1.mind << endl;
		if (!in1.optimal)
		{
			in1.optimal = true;

			// visiter les arêtes sortantes
			map<int, SegmentRouteInfo>::const_iterator iter = n1.sortants.begin();
			while (iter != n1.sortants.end())
			{
				//const Lieu& n2 = lieux[iter->first];
				InfoNoeud& in2 = calcul.visites[iter->first];
				double d = in1.mind + iter->second.longueur; // n1.coor.distance(n2.coor);
				//cout << "  d=" << d << endl;
				if (d < in2.mind) {
					in2.mind = d;
					in2.parent = courant.numero;
					in2.route = iter->second.idroute;
					calcul.file.push(N(iter->first, d));
					//cerr << " -->" << lieux[iter->first].osmid << " [" << d << "]" << endl;
				}
				++iter;
			}
		}
	}
	double result = calcul.visites[destination].mind;
	//cout << "result " << result << endl;
	return result <= maxdistance ? result : numeric_limits<double>::infinity();
}

double Carte::calculerChemin(const Point& origine, const Point& destination, list<Point>& chemin, int maxdistance) const {
	int noeudorigine = getIndexPlusPres(origine);
	int noeuddestination = getIndexPlusPres(destination);
	return calculerChemin(noeudorigine, noeuddestination, chemin, maxdistance);
}

double Carte::calculerChemin(int origine, int destination, std::list<Point>& chemin, int maxdistance) const {
	calculerDistance(origine, destination, maxdistance);
	int n = destination;
	while (n >= 0) {
		//cout << lieux[n].point << endl;
		chemin.push_front(lieux[n].point);
		n = calculs[origine].visites[n].parent;
	}
	return calculs[origine].visites[destination].mind;
}

//minIndex and maxIndex so we can chunk the computation when missing memory
void Carte::calculMatriceDistancesBornes(ostream& outSource, int minIndex, int maxIndex) const {
	cout << bornes.size() * bornes.size() << " total to do..." << endl;
	cout << "Index from " << minIndex << " to " << maxIndex << endl;

	for (unsigned int i = 0; i < bornes.size(); i++) {
		if (i >= minIndex && i < maxIndex)
		{
			for (unsigned int j = 0; j < bornes.size(); j++) {
				double d = calculerDistance(bornes[i].indexlieu, bornes[j].indexlieu, INFINITY);
				if (RequestCarte::IsAcceptableDistance(d))
				{
					outSource << i << " " << j << " " << d << endl;
					cout << i << " " << j << " " << d << endl;
				}
			}
			cout << i << " done!!!! " << endl;
		}

	}
	cout << "Done!" << endl;
}

double Carte::calculerTrajet(Point origine, Point destination, list<Point>& chemin, list<string>& bornesutilisees, int autonomie) const {
	//Init
	chemin.clear();
	bornesutilisees.clear();
	int iorigine = getIndexPlusPres(origine);
	int idest = getIndexPlusPres(destination);
	//Cas particulier
	if (iorigine == idest) return 0;

	map<int, InfoNoeud> noeuds;
	noeuds[iorigine].mind = 0;
	priority_queue<N> file;
	file.push(N(iorigine, 0.0));
	while (!file.empty()) {
		N courant = file.top();
		file.pop();
		if (courant.numero == idest) break;
		InfoNoeud& in1 = noeuds[courant.numero];
		//cout << "courant.numero=" << courant.numero << "  in1: mind=" << in1.mind 
		//     << (lieux[courant.numero].borne >= 0 ?  bornes[lieux[courant.numero].borne].nom : "")
		//     << endl;
		if (!in1.optimal) {
			in1.optimal = true;

			// Vérifier si on peut atteindre directement la destination
			double d = calculerDistance(courant.numero, idest, autonomie);
			//cout << "  d direct: " << d << endl;
			if (d <= autonomie && (courant.distance + d) < noeuds[idest].mind) {
				noeuds[idest].mind = courant.distance + d;
				//cout << "   courant.numero=" << courant.numero << " idest=" << idest << endl;
				noeuds[idest].parent = courant.numero;
				file.push(N(idest, courant.distance + d));
			}

			// visiter les bornes connectées
			const EtatCalculsDijkstra& calcul = calculs[courant.numero];
			const set<int>& bornesatteignables = calcul.bornesatteignables;
			for (set<int>::const_iterator iter = bornesatteignables.begin(); iter != bornesatteignables.end(); ++iter) {
				if (*iter == courant.numero) continue;
				if (calcul.visites.at(*iter).mind > autonomie) continue;
				//cout << "borne voisine: n2=" << *iter << " à d=" << calcul.visites.at(*iter).mind;
				//if(lieux[*iter].borne>=0) cout << "  " << bornes[lieux[*iter].borne].nom;
				//cout << endl;
				InfoNoeud& in2 = noeuds[*iter];
				double d = in1.mind + calcul.visites.at(*iter).mind;
				//if(in1.mind>0) // ???
				d += (30 * 60) * (20.0); // distance parcourue en 30 minutes à 20m/s (72km/h) : 36 km !
				//cout << "  d=" << d << endl;
				if (d < in2.mind) {
					//cout << "n2=" << *iter << "  in2.mind=" << in2.mind << " ==> " << d << endl;
					in2.mind = d;
					in2.parent = courant.numero;
					//in2.route = iter->second.idroute;
					file.push(N(*iter, d));
					//cerr << " -->" << lieux[iter->first].osmid << " [" << d << "]" << endl;
				}
			}
		}
	}

	int n = idest; //i = indice
	while (n >= 0) {
		//cout << "n=" << n << "  mind=" << noeuds[n].mind << endl;
		if (noeuds[n].parent >= 0)
			calculerChemin(noeuds[n].parent, n, chemin);
		if (lieux[n].borne >= 0 && n != idest && n != iorigine) {
			//cout << "À n=" << n << " : borne " << lieux[n].borne << " : " << bornes[lieux[n].borne].nom << endl;
			bornesutilisees.push_front(bornes[lieux[n].borne].nom);
		}
		n = noeuds[n].parent;
	}

	if (lieux.at(iorigine).borne < 0)
		calculs.erase(iorigine);

	return noeuds[idest].mind;
}

double Carte::calculerTrajet(Path &inout_path, RequestCarte & inRequestCarte) const
{
	//Init
	int iorigine;
	Action* actOrigin = nullptr;
	//Si la solution est vide, on commence de depart, sinon on commence d'ou on etait rendu
	if (inout_path.mSequenceAction.empty())
	{
		iorigine = getIndexPlusPres(inRequestCarte.mRefVehicleRequested->GetPointDeparture());
	}
	else
	{
		iorigine = getIndexPlusPres(inout_path.mSequenceAction.back()->getPosition());
		actOrigin = inout_path.mSequenceAction.back();
	}

	int idest = getIndexPlusPres(inRequestCarte.mRefVehicleRequested->GetPointArrival());
	//Cas particulier
	if (iorigine == idest) return 0;

	//Peut changer dynamiquement, mais je prefere ne pas modifier la structure de requete
	bool bIncludeOtherVehiculePresence = false;// inRequestCarte.bIncludeOtherVehiclePresence;

	unordered_set<int> ignoredStationIndexLieu;
	inRequestCarte.UtilConstructIndexLieuFromStationToIgnore(ignoredStationIndexLieu);

	map<int, InfoNoeud> noeuds;
	noeuds[iorigine].mind = 0;
	priority_queue<N> file;
	file.push(N(iorigine, 0.0));

	while (!file.empty()) {
		N courant = file.top();
		file.pop();
		if (courant.numero == idest) break;
		InfoNoeud& in1 = noeuds[courant.numero];
		//cout << "courant.numero=" << courant.numero << "  in1: mind=" << in1.mind 
		//     << (lieux[courant.numero].borne >= 0 ?  bornes[lieux[courant.numero].borne].nom : "")
		//     << endl;
		if (!in1.optimal) {
			in1.optimal = true;

			// Vérifier si on peut atteindre directement la destination
			//On vérifie aussi si une meilleure solution existe! Donc ça passe parfois
			//à plusieurs reprises...
			double d = calculerDistance(courant.numero, idest, (int)inRequestCarte.mRefVehicleRequested->GetAutonomie());
			//cout << "  d direct: " << d << endl;
			if (d <= inRequestCarte.mRefVehicleRequested->GetAutonomie() && (courant.distance + d) < noeuds[idest].mind) {
				noeuds[idest].mind = courant.distance + d;
				//cout << "   courant.numero=" << courant.numero << " idest=" << idest << endl;
				noeuds[idest].parent = courant.numero;
				file.push(N(idest, courant.distance + d));
			}

			// Visiter les bornes connectées
			//On ignore les bornes à éviter aussi
			const EtatCalculsDijkstra& calcul = calculs[courant.numero];
			const set<int>& bornesatteignables = calcul.bornesatteignables;
			for (set<int>::const_iterator iter = bornesatteignables.begin(); iter != bornesatteignables.end(); ++iter) {
				if (*iter == courant.numero) continue;
				if (calcul.visites.at(*iter).mind > inRequestCarte.mRefVehicleRequested->GetAutonomie()) continue;
				if (ignoredStationIndexLieu.find(*iter) != ignoredStationIndexLieu.end()) continue;
				//cout << "borne voisine: n2=" << *iter << " à d=" << calcul.visites.at(*iter).mind;
				//if(lieux[*iter].borne>=0) cout << "  " << bornes[lieux[*iter].borne].nom;
				//cout << endl;
				InfoNoeud& in2 = noeuds[*iter];
				double d = in1.mind + calcul.visites.at(*iter).mind;
				//if(in1.mind>0) // ???
				//TODO: comprendre ce calcul (demander a Eric B.)
				d += (30 * 60) * (inRequestCarte.mRefVehicleRequested->GetAverageSpeedMPerS()); // distance parcourue en 30 minutes à 20m/s (72km/h) : 36 km !
				//cout << "  d=" << d << endl;
				if (d < in2.mind) {
					//cout << "n2=" << *iter << "  in2.mind=" << in2.mind << " ==> " << d << endl;
					in2.mind = d;
					in2.parent = courant.numero;
					//in2.route = iter->second.idroute;
					file.push(N(*iter, d));
					//cerr << " -->" << lieux[iter->first].osmid << " [" << d << "]" << endl;
				}
			}
		}
	}

	int n = idest;
	int totalPoint = 0;
	Point lastPoint = inRequestCarte.mRefVehicleRequested->GetPointArrival();
	list<Point> chemin;
	while (n >= 0) {
		//cout << "n=" << n << "  mind=" << noeuds[n].mind << endl;
		if (noeuds[n].parent >= 0)
		{
			chemin.clear();
			double dist = calculerChemin(noeuds[n].parent, n, chemin);

			//If this is a borne, add the station
			if (lieux[n].borne >= 0 && n != idest && n != iorigine) {
				//cout << "À n=" << n << " : borne " << lieux[n].borne << " : " << bornes[lieux[n].borne].nom << endl;
				//bornesutilisees.push_front(bornes[lieux[n].borne].nom);

				//TODO: test... mais ne devrait plus etre utilise
				ElectricTerminal * const refTerminal = inRequestCarte.IndexTerminalToTerminal(lieux[n].borne);
				ActionTerminal* newAction = new ActionTerminal(inRequestCarte.mRefElectricCircuit,refTerminal);
				newAction->mName = bornes[lieux[n].borne].nom;
				newAction->mPosition = bornes[lieux[n].borne].point;
				//TODO: Assume we do not need the max charging time
				newAction->mDuration = inRequestCarte.mRefVehicleRequested->GetL1MaxChargingTime();
				//Insert right after last action ((as it was the road action that got us here)
				inout_path.mSequenceAction.insert(std::next(inout_path.mSequenceAction.begin()), newAction);
			}
			for (std::list<Point>::reverse_iterator itPoint = chemin.rbegin(); itPoint != chemin.rend(); ++itPoint)
			{
				Point p = *itPoint;
				//Ignore the point if we just added it
				if (p == lastPoint)
				{
					continue;
				}

				//Distance est en metres
				float segmentDistance = (float)calculerDistance(p, lastPoint, (int)inRequestCarte.mRefVehicleRequested->GetAutonomie());
				ElectricTerminal* const refTerminal = inRequestCarte.IndexTerminalToTerminal(lieux[n].borne);
				ActionRoad* newAction = new ActionRoad(inRequestCarte.mRefElectricCircuit, refTerminal);
				newAction->mName = "Point ";
				newAction->mPosition = p;
				//TODO: revoir pq on met 0 si infinie... c'est tres douteux!
				newAction->mDistanceFromLast = isinf(segmentDistance) ? 0.0f : segmentDistance;
				newAction->mDuration = (newAction->mDistanceFromLast / inRequestCarte.mRefVehicleRequested->GetAverageSpeedMPerS());
				inout_path.mSequenceAction.push_front(newAction);
				lastPoint = p;
				//Update the path
				inout_path.mDistance += newAction->mDistanceFromLast;
				totalPoint++;
			}
		}

		n = noeuds[n].parent;
	}

	if (lieux.at(iorigine).borne < 0)
		calculs.erase(iorigine);

	//inout_path.mDistance = (float)noeuds[idest].mind;
	//Compute the duration for each action
	float currentTime = (float)inRequestCarte.mRefVehicleRequested->GetDepartureTime();
	//If we have some Terminal reservation of waiting to do, we manage them as well
	//map d'actions a ajouter
	map<Action*, Action*> mapAddActionAfter;
	int nCompte = 0;
	auto itOrigin = inout_path.mSequenceAction.begin();
	if (actOrigin != nullptr)
	{
		itOrigin  = std::find(inout_path.mSequenceAction.begin(), inout_path.mSequenceAction.end(), actOrigin);
	}

	for (auto it = itOrigin; it != inout_path.mSequenceAction.end(); ++it)
	{
		Action* var = *it;
		bIncludeOtherVehiculePresence = bIncludeOtherVehiculePresence && currentTime <= inRequestCarte.fMinimizeWaitingHorizon;
		switch (var->getType())
		{
		case Action::eRoad:
			var->mName += to_string(nCompte);
			var->mStartTime = currentTime;
			++nCompte;
			currentTime += var->getDuration();
			//TODO: Selon la vitesse, calculer le temps de deplacement
			break;
		case Action::eTerminal:
		{
			if (bIncludeOtherVehiculePresence)
			{
				//Attente de chargement selon le niveau de la borne
							//Il est possible que l'on doive attendre aussi
							//Si aucune place n'est disponible

				ActionTerminal* actionTerminal = dynamic_cast<ActionTerminal*>(var);
				ElectricTerminal * const refTerminal = actionTerminal->getTerminal();
				//cout << *refTerminal << endl;
				TimeRange idealTimeRange(TimeUnit(currentTime), TimeUnit(currentTime + inRequestCarte.mRefVehicleRequested->GetL1MaxChargingTime()));
				TimeRange possibleTimeRange = refTerminal->FindNextAvailableReservation(idealTimeRange);

				//When we need to wait for a reservation to end
				//Add a wait action
				//cout << idealTimeRange << "   " <<  possibleTimeRange << endl;
				//cout << *refTerminal << endl << endl;
				if (possibleTimeRange != idealTimeRange)
				{
					ActionWait* newAction = new ActionWait(inRequestCarte.mRefElectricCircuit, refTerminal);
					newAction->mName = refTerminal->GetName();
					newAction->mPosition = actionTerminal->getPosition();
					newAction->mDuration = float(possibleTimeRange.mStart - idealTimeRange.mStart);
					newAction->mStartTime = currentTime;
					currentTime += newAction->getDuration();
					mapAddActionAfter.insert({ var ,newAction });
				}
			}
			var->mStartTime = currentTime;
			currentTime += var->getDuration();

		}break;
		case Action::eWait:
			assert(false); //Devrait planter...
			break;
		default:
			assert(false); //Devrait planter aussi...
			break;
		}
	}

	//TODO: OUCH!!!!
	//This might be slow...
	for (auto var : mapAddActionAfter)
	{
		auto it = std::find(inout_path.mSequenceAction.begin(), inout_path.mSequenceAction.end(), var.first);
		inout_path.mSequenceAction.insert(it, var.second);
	}

	return noeuds[idest].mind;
}

double Carte::calculerTrajetCache(Path &inout_path, RequestCarte & inRequestCarte) const
{
	//Init
	int iorigine;
	Action* actOrigin = nullptr;
	//Si la solution est vide, on commence de depart, sinon on commence d'ou on etait rendu
	if (inout_path.mSequenceAction.empty())
	{
		iorigine = getIndexPlusPres(inRequestCarte.mRefVehicleRequested->GetPointDeparture());
	}
	else
	{
		iorigine = getIndexPlusPres(inout_path.mSequenceAction.back()->getPosition());
		actOrigin = inout_path.mSequenceAction.back();
	}

	int idest = getIndexPlusPres(inRequestCarte.mRefVehicleRequested->GetPointArrival());
	//Cas particulier
	if (iorigine == idest) return 0;

	//Peut changer dynamiquement, mais je prefere ne pas modifier la structure de requete
	bool bIncludeOtherVehiculePresence = false;//inRequestCarte.bIncludeOtherVehiclePresence;

	//Liste des stations a ignorer car elles ne fournissent pas de bonne solutions localement.
	unordered_set<int> ignoredStationIndexLieu;
	inRequestCarte.UtilConstructIndexLieuFromStationToIgnore(ignoredStationIndexLieu);

	map<int, InfoNoeud> noeuds;
	noeuds[iorigine].mind = 0;
	priority_queue<N> file;
	file.push(N(iorigine, 0.0));

	while (!file.empty()) {
		N courant = file.top();
		file.pop();
		if (courant.numero == idest) break;
		InfoNoeud& in1 = noeuds[courant.numero];
		//cout << "courant.numero=" << courant.numero << "  in1: mind=" << in1.mind 
		//     << (lieux[courant.numero].borne >= 0 ?  bornes[lieux[courant.numero].borne].nom : "")
		//     << endl;
		//Si n'est pas traite, le sera a partir de maintenant
		if (!in1.optimal) {
			in1.optimal = true;

			//Calculer la distance entre le icourrant et idest
			double d = calculerDistance(courant.numero, idest, (int)inRequestCarte.mRefVehicleRequested->GetAutonomie());
			//Si on a assez d'autonomie et qu'on atteint la destination... 
			if (d <= inRequestCarte.mRefVehicleRequested->GetAutonomie() && (courant.distance + d) < noeuds[idest].mind) {
				noeuds[idest].mind = courant.distance + d;
				//cout << "   courant.numero=" << courant.numero << " idest=" << idest << endl;
				noeuds[idest].parent = courant.numero;
				file.push(N(idest, courant.distance + d));
			}

			// Visiter les bornes connectées
			//On ignore les bornes à éviter en plus
			const EtatCalculsDijkstra& calcul = calculs[courant.numero];
			const set<int>& bornesatteignables = calcul.bornesatteignables;
			for (set<int>::const_iterator iter = bornesatteignables.begin(); iter != bornesatteignables.end(); ++iter) {
				if (*iter == courant.numero) continue;
				if (calcul.visites.at(*iter).mind > inRequestCarte.mRefVehicleRequested->GetAutonomie()) continue;
				if (ignoredStationIndexLieu.find(*iter) != ignoredStationIndexLieu.end()) continue;
				//cout << "borne voisine: n2=" << *iter << " à d=" << calcul.visites.at(*iter).mind;
				//if(lieux[*iter].borne>=0) cout << "  " << bornes[lieux[*iter].borne].nom;
				//cout << endl;
				InfoNoeud& in2 = noeuds[*iter];
				double d = in1.mind + calcul.visites.at(*iter).mind;
				//if(in1.mind>0) // ???
				//TODO: comprendre ce calcul (demander a Eric B.)
				d += (30 * 60) * (inRequestCarte.mRefVehicleRequested->GetAverageSpeedMPerS()); // distance parcourue en 30 minutes à 20m/s (72km/h) : 36 km !
				//cout << "  d=" << d << endl;
				if (d < in2.mind) {
					//cout << "n2=" << *iter << "  in2.mind=" << in2.mind << " ==> " << d << endl;
					in2.mind = d;
					in2.parent = courant.numero;
					//in2.route = iter->second.idroute;
					file.push(N(*iter, d));
					//cerr << " -->" << lieux[iter->first].osmid << " [" << d << "]" << endl;
				}
			}
		}
	}

	int n = idest;
	int totalPoint = 0;
	Point lastPoint = inRequestCarte.mRefVehicleRequested->GetPointArrival();
	list<Point> chemin;
	while (n >= 0) {
		//cout << "n=" << n << "  mind=" << noeuds[n].mind << endl;
		if (noeuds[n].parent >= 0)
		{
			chemin.clear();
			double dist = calculerChemin(noeuds[n].parent, n, chemin);

			//If this is a borne, add the station
			if (lieux[n].borne >= 0 && n != idest && n != iorigine) {
				//cout << "À n=" << n << " : borne " << lieux[n].borne << " : " << bornes[lieux[n].borne].nom << endl;
				//bornesutilisees.push_front(bornes[lieux[n].borne].nom);
				ElectricTerminal * const refTerminal = inRequestCarte.IndexTerminalToTerminal(lieux[n].borne);
				ActionTerminal* newAction = new ActionTerminal(inRequestCarte.mRefElectricCircuit, refTerminal);
				newAction->mName = bornes[lieux[n].borne].nom;
				newAction->mPosition = bornes[lieux[n].borne].point;
				//TODO: Assume we do not need the max charging time
				newAction->mDuration = inRequestCarte.mRefVehicleRequested->GetL1MaxChargingTime();
				//Insert right after last action ((as it was the road action that got us here)
				inout_path.mSequenceAction.insert(std::next(inout_path.mSequenceAction.begin()), newAction);
			}
			for (std::list<Point>::reverse_iterator itPoint = chemin.rbegin(); itPoint != chemin.rend(); ++itPoint)
			{
				Point p = *itPoint;
				//Ignore the point if we just added it
				if (p == lastPoint)
				{
					continue;
				}

				//Distance est en metres
				float segmentDistance = (float)calculerDistance(p, lastPoint, (int)inRequestCarte.mRefVehicleRequested->GetAutonomie());
				ElectricTerminal* const refTerminal = inRequestCarte.IndexTerminalToTerminal(lieux[n].borne);
				ActionRoad* newAction = new ActionRoad(inRequestCarte.mRefElectricCircuit, refTerminal);
				newAction->mName = "Point ";
				newAction->mPosition = p;
				//TODO: revoir pq on met 0 si infinie... c'est tres douteux!
				newAction->mDistanceFromLast = isinf(segmentDistance) ? 0.0f : segmentDistance;
				newAction->mDuration = (newAction->mDistanceFromLast / inRequestCarte.mRefVehicleRequested->GetAverageSpeedMPerS());
				inout_path.mSequenceAction.push_front(newAction);
				lastPoint = p;
				//Update the path
				inout_path.mDistance += newAction->mDistanceFromLast;
				totalPoint++;
			}
		}

		n = noeuds[n].parent;
	}

	if (lieux.at(iorigine).borne < 0)
		calculs.erase(iorigine);

	//inout_path.mDistance = (float)noeuds[idest].mind;
	//Compute the duration for each action
	float currentTime = (float)inRequestCarte.mRefVehicleRequested->GetDepartureTime();
	//If we have some Terminal reservation of waiting to do, we manage them as well
	//map d'actions a ajouter
	map<Action*, Action*> mapAddActionAfter;
	int nCompte = 0;
	auto itOrigin = inout_path.mSequenceAction.begin();
	if (actOrigin != nullptr)
	{
		itOrigin = std::find(inout_path.mSequenceAction.begin(), inout_path.mSequenceAction.end(), actOrigin);
	}

	for (auto it = itOrigin; it != inout_path.mSequenceAction.end(); ++it)
	{
		Action* var = *it;
		bIncludeOtherVehiculePresence = bIncludeOtherVehiculePresence && currentTime <= inRequestCarte.fMinimizeWaitingHorizon;
		switch (var->getType())
		{
		case Action::eRoad:
			var->mName += to_string(nCompte);
			var->mStartTime = currentTime;
			++nCompte;
			currentTime += var->getDuration();
			//TODO: Selon la vitesse, calculer le temps de deplacement
			break;
		case Action::eTerminal:
		{
			if (bIncludeOtherVehiculePresence)
			{
				//Attente de chargement selon le niveau de la borne
							//Il est possible que l'on doive attendre aussi
							//Si aucune place n'est disponible

				ActionTerminal* actionTerminal = dynamic_cast<ActionTerminal*>(var);
				ElectricTerminal * const refTerminal = actionTerminal->getTerminal();
				//cout << *refTerminal << endl;
				TimeRange idealTimeRange(TimeUnit(currentTime), TimeUnit(currentTime + inRequestCarte.mRefVehicleRequested->GetL1MaxChargingTime()));
				TimeRange possibleTimeRange = refTerminal->FindNextAvailableReservation(idealTimeRange);

				//When we need to wait for a reservation to end
				//Add a wait action
				//cout << idealTimeRange << "   " <<  possibleTimeRange << endl;
				//cout << *refTerminal << endl << endl;
				if (possibleTimeRange != idealTimeRange)
				{
					ActionWait* newAction = new ActionWait(inRequestCarte.mRefElectricCircuit, refTerminal);
					newAction->mName = refTerminal->GetName();
					newAction->mPosition = actionTerminal->getPosition();
					newAction->mDuration = float(possibleTimeRange.mStart - idealTimeRange.mStart);
					newAction->mStartTime = currentTime;
					currentTime += newAction->getDuration();
					mapAddActionAfter.insert({ var ,newAction });
				}
			}
			var->mStartTime = currentTime;
			currentTime += var->getDuration();

		}break;
		case Action::eWait:
			assert(false); //Devrait planter...
			break;
		default:
			assert(false); //Devrait planter aussi...
			break;
		}
	}

	//TODO: OUCH!!!!
	//This might be slow...
	for (auto var : mapAddActionAfter)
	{
		auto it = std::find(inout_path.mSequenceAction.begin(), inout_path.mSequenceAction.end(), var.first);
		inout_path.mSequenceAction.insert(it, var.second);
	}

	return noeuds[idest].mind;
}

#include <random>
#define PI 3.14159265359

void Carte::genererRequete(Point& a, Point& b, int& autonomie) const {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> distL(0, lieux.size() - 1);
	std::uniform_real_distribution<> dist01(-1, 1);
	std::uniform_int_distribution<> distA(80, 172);

	a = lieux[distL(gen)].point;
	b = lieux[distL(gen)].point;
	a += Point(dist01(gen) * 0.0005 * PI / 180, dist01(gen) * 0.0005 * PI / 180);
	b += Point(dist01(gen) * 0.0005 * PI / 180, dist01(gen) * 0.0005 * PI / 180);
	//cout << lieux[getIndexPlusPres(a)].point.distance(a) << endl;
	//cout << lieux[getIndexPlusPres(b)].point.distance(b) << endl;
	autonomie = distA(gen) * 1000;
	cout << a << "\t" << b << "\t" << autonomie << endl;
}

void Carte::LoadBorne(ifstream & inputStream, int maxBorne)
{
	int currentBorneAmount = 0;
	while (inputStream && currentBorneAmount < maxBorne) {
		string nom;
		char L;
		int niveau;
		Point point;
		inputStream >> nom;
		if (!inputStream || nom.empty()) break;
		inputStream >> L >> niveau >> point >> ws;
		assert(L == 'L');
		cerr << "."; cerr.flush();
		ajouterBorne(nom, point, niveau);
		++currentBorneAmount;
	}
}

void Carte::LoadBorne(string fileNameBorne, int maxBorne)
{
	ifstream isbornes(fileNameBorne);
	if (isbornes.fail()) {
		cerr << "Erreur d'ouverture du fichier bornes: '" << fileNameBorne << "' !" << endl;
		assert(false);
	}
	else
	{
		LoadBorne(isbornes, maxBorne);
	}
	cerr << "Bornes chargées!" << endl;
}

void Carte::LoadNode(string fileNameCarte)
{
	ifstream iscarte(fileNameCarte);
	if (iscarte.fail()) {
		cerr << "Erreur d'ouverture du fichier carte: '" << fileNameCarte << "' !" << endl;
		assert(false);
	}
	else
	{
		iscarte >> *this;
	}
	cerr << "Carte chargée!" << endl;
}

void Carte::Load(string fileNameNode, string fileNameBorne, int maxBorne)
{
	LoadNode(fileNameNode);
	LoadBorne(fileNameBorne, maxBorne);
}




// ******************* KdTree ***********************

void Carte::creerKdTree() const {
	detruireNiveau(racinekdtree);
	racinekdtree = NULL;
	unsigned int n = lieux.size();
	if (n == 0) return;
	std::vector<int> numeros;
	for (unsigned int i = 0; i < n; i++)
		numeros.push_back(i);
	racinekdtree = creerNiveau(numeros);
}

Carte::Niveau* Carte::creerNiveau(const std::vector<int>& numeros) const {
	Niveau* niveau = new Niveau();
	niveau->rectenglobant = Rectangle(lieux[numeros[0]].point, lieux[numeros[0]].point);
	unsigned int n = numeros.size();
	for (unsigned int i = 0; i < n; i++) {
		niveau->rectenglobant.englobe(lieux[numeros[i]].point);
	}

	if (numeros.size() > 1000) {
		std::vector<double>tab(n);// = new double[n];
		if ((niveau->rectenglobant.c2.latitude - niveau->rectenglobant.c1.latitude) > (niveau->rectenglobant.c2.longitude - niveau->rectenglobant.c1.longitude))
			for (unsigned int i = 0; i < n; i++)
				tab[i] = lieux[numeros[i]].point.latitude;
		else
			for (unsigned int i = 0; i < n; i++)
				tab[i] = lieux[numeros[i]].point.longitude;
		std::sort(tab.begin(), tab.end());
		double mediane = tab[n / 2];
		//delete[] tab;
		std::vector<int> t1, t2;
		if ((niveau->rectenglobant.c2.latitude - niveau->rectenglobant.c1.latitude) > (niveau->rectenglobant.c2.longitude - niveau->rectenglobant.c1.longitude)) {
			for (unsigned int i = 0; i < n; i++)
				if (lieux[numeros[i]].point.latitude < mediane)
					t1.push_back(numeros[i]);
				else
					t2.push_back(numeros[i]);
		}
		else {
			for (unsigned int i = 0; i < n; i++)
				if (lieux[numeros[i]].point.longitude < mediane)
					t1.push_back(numeros[i]);
				else
					t2.push_back(numeros[i]);
		}
		if (!t1.empty() && !t2.empty()) {
			niveau->enfant1 = creerNiveau(t1);
			niveau->enfant2 = creerNiveau(t2);
		}
	}
	if (niveau->enfant1 == NULL) {
		assert(niveau->enfant2 == NULL);
		niveau->numeros = numeros;
	}
	return niveau;
}

Carte::Niveau::Niveau()
	: enfant1(NULL), enfant2(NULL)
{
}
/*
double Carte::Niveau::distanceMin(const Coordonnee& c) const{
	double dx=0, dy=0;
	if(point.x<minx) dx=minx-point.x;
	else if(point.x>maxx) dx=point.x-maxx;
	if(point.y<miny) dy=miny-point.y;
	else if(point.y>maxy) dy=point.y-maxy;
	return sqrt(dx*dx + dy*dy);
}
*/
void Carte::detruireNiveau(Niveau*& niveau) const {
	if (niveau == NULL) return;
	detruireNiveau(niveau->enfant1);
	detruireNiveau(niveau->enfant2);
	delete niveau;
	niveau = NULL;
}

bool solutionUnique;

int Carte::ipointPlusProche(const Point& point, double& distmin, int exception) const
{
	int bestpoint = -1;
	distmin = std::numeric_limits<double>::infinity();
	solutionUnique = false;
	ipointPlusProche(racinekdtree, point, distmin, bestpoint, exception);

	if (!solutionUnique)
		cerr << "Carte::ipointPlusProche ==> Plusieurs noeuds possibles pour " << point << " --> n" << lieux[bestpoint].osmid << endl;

	return bestpoint;
}

void Carte::ipointPlusProche(const Niveau* niveau, const Point& point, double& distmin, int& bestpoint, int exception) const
{

	for (unsigned int i = 0; i < niveau->numeros.size(); i++) {
		if (niveau->numeros[i] == exception) continue;
		double d = lieux[niveau->numeros[i]].point.distance(point);
		if (d < distmin) {
			bestpoint = niveau->numeros[i];
			distmin = d;
			solutionUnique = true;
		}
		else if (distmin == d) solutionUnique = false;
	}

	Point cpp = niveau->rectenglobant.plusPres(point);
	double dr = cpp.distance(point);
	if (dr > distmin) return;

	double dme1 = std::numeric_limits<double>::infinity(),
		dme2 = std::numeric_limits<double>::infinity();
	if (niveau->enfant1 != NULL) dme1 = niveau->enfant1->rectenglobant.plusPres(point).distance(point);
	if (niveau->enfant2 != NULL) dme2 = niveau->enfant2->rectenglobant.plusPres(point).distance(point);
	if (dme1 < dme2) {
		if (niveau->enfant1 != NULL && dme1 < distmin) ipointPlusProche(niveau->enfant1, point, distmin, bestpoint, exception);
		if (niveau->enfant2 != NULL && dme2 < distmin) ipointPlusProche(niveau->enfant2, point, distmin, bestpoint, exception);
	}
	else {
		if (niveau->enfant2 != NULL && dme2 < distmin) ipointPlusProche(niveau->enfant2, point, distmin, bestpoint, exception);
		if (niveau->enfant1 != NULL && dme1 < distmin) ipointPlusProche(niveau->enfant1, point, distmin, bestpoint, exception);
	}
}

