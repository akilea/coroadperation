#pragma once
#include "StructureCarte.h"
#include <algorithm>

InfoNoeud::InfoNoeud() : parent(-1), mind(numeric_limits<double>::infinity()), optimal(false) {
}

ostream& operator<<(ostream& os, const TimeRange& dt)
{
	os << "[" << dt.mStart << "," << dt.mEnd << "]";
	return os;
}

TimeRange operator&(const TimeRange & a, const TimeRange & b)
{
	TimeRange tr(std::max(a.mStart, b.mStart), std::min(a.mEnd, b.mEnd));
	if (tr.Duration() < 0)
	{
		tr.mStart = tr.mEnd = 0;
	}
	return tr;
}

bool TimeRange::operator&&(const TimeRange & right) const
{
	return ((*this) & right).Duration() != 0;
}

bool TimeRange::operator==(const TimeRange & right) const
{
	return this->mStart == right.mStart && this->mEnd == right.mEnd;
}

bool TimeRange::operator!=(const TimeRange & right) const
{
	return this->mStart != right.mStart || this->mEnd != right.mEnd;
}
