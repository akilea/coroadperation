#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "carte.h"

int main(int argc, const char** argv){
    if(argc<3){
        cout << "./tp3 carte.txt bornes.txt" << endl;
        return 1;
    }
    Carte carte;
    {
        ifstream iscarte(argv[1]);
        if(iscarte.fail()){
            cerr << "Erreur d'ouverture du fichier carte: '" << argv[1] << "' !" << endl;
            return 2;
        }
        iscarte >> carte;
        cerr << "Carte chargée!" << endl;
    }
    {
        ifstream isbornes(argv[2]);
        if(isbornes.fail()){
            cerr << "Erreur d'ouverture du fichier bornes: '" << argv[1] << "' !" << endl;
            return 2;
        }
        while(isbornes){
            string nom;
            char L;
            int niveau;
            Point point;
            isbornes >> nom;
            if(!isbornes || nom.empty()) break;
            isbornes >> L >> niveau >> point >> ws;
            assert(L=='L');
            cerr << "."; cerr.flush();
            carte.ajouterBorne(nom, point, niveau);
        }
        cerr << "Bornes chargées!" << endl;
    }

    while(cin){
        Point depart, destination;
        cin >> ws;
        if(cin.fail() || cin.eof())
            break;
        double autonomie;
        cin >> depart >> destination >> autonomie;
        //cerr << "calcul ..." << endl;
        double d=0.0;
        list<Point> chemin;
        list<string> bornes;
        d = carte.calculerTrajet(depart, destination, chemin, bornes, (int)autonomie);
        std::cout << std::setprecision(8);
        if(d < 1e100)
            cout << (int) round(d/20.0) << " s" << endl;
        else
            cout << "Impossible!" << endl;
        for(list<Point>::iterator iter = chemin.begin();iter!=chemin.end();++iter)
            cout << *iter << '\t';
        cout << endl;
        for(list<string>::iterator iter = bornes.begin();iter!=bornes.end();++iter)
            cout << *iter << '\t';
        cout << endl;
    }

    return 0;
}

