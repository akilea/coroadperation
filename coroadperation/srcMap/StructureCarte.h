#pragma once
#include <map>
#include "point.h"

using namespace std;

typedef int TimeUnit;


struct TimeRange
{
	TimeUnit mStart = 0;
	TimeUnit mEnd = 0;

	TimeRange(TimeUnit in_start = 0, TimeUnit in_end = 0)
		:mStart(in_start)
		,mEnd(in_end)
	{

	}

	TimeUnit Duration() const
	{
		return mEnd - mStart;
	}

	bool operator&&(const TimeRange &right) const;
	bool operator==(const TimeRange &right) const;
	bool operator!=(const TimeRange & right) const;
};
ostream& operator<<(ostream& os, const TimeRange& dt);
TimeRange operator&(const TimeRange& a, const TimeRange& b);


struct SegmentRouteInfo {
	double longueur; // longueur pr�calcul� du segment de route.
	int    idroute;  // numero de route (pour retrouver le nom complet).
	//double vitesselimite; // en m�tre / seconde
};

// Repr�sentation d'un lieu (les lieux sont les sommets du graphe).
struct Lieu {
	Lieu() : borne(-1) {}
	long long  osmid;
	Point point;
	// Les segments de route sortants sont stock�s dans un map : numero_noeud_destination --> informations sur le segment
	map<int, SegmentRouteInfo> sortants;
	int borne; // -1=aucune; 1=L1, 2=L2, 2=L3 <---- on dirait une erreur...
};

// Structure pour ins�rer les "r�f�rences" de lieux (sommets) dans un std::priority_queue.
struct N {
	N() {}
	N(int n, double d) :numero(n), distance(d) {}
	int numero;
	double distance;
	bool operator < (const N& n) const {
		// priority_queue utilise un ordre d�croissant
		return distance > n.distance;
	}
};

struct InfoNoeud {
	InfoNoeud();
	int parent;
	int route;
	double mind; //mininmum distance
	bool optimal; //true quand optiomal (quand va passer dessus)
};

struct Borne {
	string  nom;
	int niveau;
	Point point;
	int indexlieu;
};