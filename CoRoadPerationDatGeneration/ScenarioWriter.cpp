#include "ScenarioWriter.h"

#include "../coroadperation/ScenarioData.h"
#include "../coroadperation/Vehicle.h"
#include "Carte.h"

#include <string>
#include <algorithm>
#include <random>

Point ScenarioWriter::GenererPointAleatoirePossible(const Point & min, const Point & max, Carte* refCarte, float acceptableDistance)
{
	return GenererPointAleatoireAtteignable(min, max, refCarte, acceptableDistance);
}

ScenarioWriter::ScenarioWriter()
{
	mData = new ScenarioData();
}


ScenarioWriter::~ScenarioWriter()
{
	delete mData;
}

string ScenarioWriter::ProduceNextName()
{
	string nom = "Vehicle_" + std::to_string(mNamelessID);
	++mNamelessID;
	return nom;
}

void ScenarioWriter::AddVehicle(Vehicle * newVehicle)
{
	mData->mVecVehicle.push_back(newVehicle);
}

void ScenarioWriter::CreatePatch(const PatchData & data)
{
	std::default_random_engine generator;
	std::uniform_int_distribution<TimeUnit> departureTimeGen(data.minStartTime, data.maxStartTime);
	std::uniform_real_distribution<> autonomieGen(data.autonomieMin, data.autonomieMax);
	std::uniform_real_distribution<> chargingTimeGen(data.chargingTimeMin, data.chargingTimeMax);
	cout << "Creating path" << endl;
	for (int i = 0; i < (int)data.amount; i++)
	{
		cout << "Voiture" << i << endl;
		Point randomStart = GenererPointAleatoirePossible(data.startPointMin, data.startPointMax,data.vPointBorne,data.maxDistanceVolOiseauBorne);
		Point randomEnd = GenererPointAleatoirePossible(data.endPointMin, data.endPointMax, data.vPointBorne,data.maxDistanceVolOiseauBorne);
		float randomAutonomie = autonomieGen(generator);
		float fChargingTime = chargingTimeGen(generator);
		//Fyling distance test so we avoid impossible solution
		//Make sure the path exists
		float superMax = 10000000.f;
		double d = data.vPointBorne->calculerDistance(randomStart, randomEnd, superMax);
		if (d > superMax)
		{
			i--;
			cout << "On a attrape un chemin impossible pour i=" << i << endl;
			continue;
		}
		//Theorically can fail but unlikely
		Vehicle* simpleVec = new Vehicle(ProduceNextName(), randomStart, randomEnd, randomAutonomie, departureTimeGen(generator), fChargingTime);
		AddVehicle(simpleVec);
	}
}

void ScenarioWriter::Sort()
{
	cout << "Sorting" << endl;
	std::sort(mData->mVecVehicle.begin(), mData->mVecVehicle.end(), VehicleAscendingSort);
}

void ScenarioWriter::CommitTo(std::ostream & out_output)
{
	cout << "Commiting" << endl;
	for (auto var : mData->mVecVehicle)
	{
		out_output << var->toStringScenario() << endl;
	}
}

Point ScenarioWriter::GenererPointAleatoireAtteignable(const Point& min, const Point& max, Carte* refCarte, float acceptableDistance)
{
	cout << "GenererPointAleatoireAtteignable" << endl;
	Point pt;
	bool isPossible = false;
	while (!isPossible)
	{
		pt = genererPointAleatoire(min, max);
		for (auto it : refCarte->GetBorne())
		{
			//On s'assure qu'une borne est atteignable
			if (pt.distance(it.point) < acceptableDistance && refCarte->calculerDistance(pt, it.point, acceptableDistance) <= acceptableDistance)
			{
				isPossible = true;
				break;
			}
		}
	}
	cout << "Trouv�!" << endl;
	return pt;
}
