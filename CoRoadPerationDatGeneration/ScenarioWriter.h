#pragma once

#include <ostream>
#include <vector>
#include <set>
#include "../coroadperation/srcMap/StructureCarte.h"

class ScenarioData;
class Vehicle;
class Carte;

class ScenarioWriter
{
public:
	struct PatchData {
		Point			startPointMin = Point(0, 0);
		Point			startPointMax = Point(0, 0);
		Point			endPointMin = Point(0, 0);
		Point			endPointMax = Point(0, 0);
		unsigned int	amount = 5;
		TimeUnit		minStartTime = 0;
		TimeUnit		maxStartTime = 0;
		float			autonomieMin = 200000.0f;
		float			autonomieMax = 200000.0f;
		float			chargingTimeMin = 1500.0f;
		float			chargingTimeMax = 2200.0f;
		float			maxDistanceVolOiseauBorne = 100000.0f;
		Carte*			vPointBorne;

		void InverseStartEndPoint()
		{
			Point tmpMin = startPointMin;
			Point tmpMax = startPointMax;

			startPointMin	= endPointMin;
			startPointMax	= endPointMax;
			endPointMin		= tmpMin	 ;
			endPointMax		= tmpMax	 ;
		}
	};

private:
	int mNamelessID = 1;

	Point GenererPointAleatoirePossible(const Point& min, const Point& max, Carte* refCarte,float acceptableDistance);
public:
	ScenarioData* mData;
	ScenarioWriter();
	virtual ~ScenarioWriter();

	//Helper
	string ProduceNextName();

	void AddVehicle(Vehicle* newVehicle);
	void CreatePatch(const PatchData& data);
	void Sort();
	void CommitTo(std::ostream& out_output);

	static Point GenererPointAleatoireAtteignable(const Point& min, const Point& max, Carte* refCarte, float acceptableDistance);
};

