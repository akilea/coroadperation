
#include "ScenarioWriter.h"
#include <string>
#include <fstream>
#include <iostream>
#include "../coroadperation/srcMap/point.h"
#include "../coroadperation/IMapSolver.h"
#include "../coroadperation/Path.h"
#include "../coroadperation/srcMap/Carte.h"
#include "../coroadperation//ScenarioData.h"
#include <random>
#include <assert.h>
#include <windows.h>
#include "../coroadperation/SimulationSetting.h"

typedef  pair<Point, Point> CityLimit;
typedef  map<string, CityLimit> MapCityLimit;
//
//void CreateUQAMScenario(ScenarioWriter* writer, ofstream& outputFile)
//{
//	//for (unsigned int i = 0; i < 10; ++i)
//	//{
//	//	Vehicle* simpleVec = new Vehicle(writer->ProduceNextName(),Point(0,0),Point(0,0),122., distribution(generator));
//	//	writer->AddVehicle(simpleVec);
//	//}
//
//	ScenarioWriter::PatchData patchData;
//	patchData.amount = 100;
//	patchData.startPointMin = Point(45.508377, -73.568759);
//	patchData.startPointMax = Point(45.508377, -73.568759);
//	patchData.endPointMin = Point(45.509942, -73.57056);
//	patchData.endPointMax = Point(45.509942, -73.57056);
//	patchData.autonomieMin = 200.0f;
//	patchData.autonomieMax = 200.0f;
//	patchData.minStartTime = 0;
//	patchData.maxStartTime = 0;
//	patchData.chargingTimeMin = 500.0f;
//	patchData.chargingTimeMax = 500.0f;
//	writer->CreatePatch(patchData);
//}
//void CreateQuebecRandom24h(ScenarioWriter* writer, ofstream& outputFile)
//{
//	int carAmount = 10;
//
//	Point ptMontrealMin(45.567684, -73.560965);
//	Point ptMontrealMax(45.475795, -73.656416);
//
//	//Magog-Montreal patch
//	ScenarioWriter::PatchData patchData;
//	patchData.amount = carAmount;
//	patchData.startPointMin = ptMontrealMin;
//	patchData.startPointMax = ptMontrealMax;
//	patchData.endPointMin	= Point(45.282708, -72.157709);
//	patchData.endPointMax	= Point(45.267741, -72.129255);
//	patchData.autonomieMin = 200000.0f;
//	patchData.autonomieMax = 200000.0f;
//	patchData.minStartTime = 0;
//	patchData.maxStartTime = 86400; //24h
//	patchData.chargingTimeMin = 1500.0f;
//	patchData.chargingTimeMax = 2400.0f;
//	writer->CreatePatch(patchData);
//	patchData.InverseStartEndPoint();
//	writer->CreatePatch(patchData);
//
//	//Quebec-Montreal patch
//	patchData.amount = carAmount;
//	patchData.startPointMin = ptMontrealMin;
//	patchData.startPointMax = ptMontrealMax;
//	patchData.endPointMin	= Point(46.858326, -71.243104);
//	patchData.endPointMax	= Point(46.774969, -71.371749);
//	patchData.autonomieMin = 200000.0f;
//	patchData.autonomieMax = 200000.0f;
//	patchData.minStartTime = 0;
//	patchData.maxStartTime = 86400; //24h
//	patchData.chargingTimeMin = 1500.0f;
//	patchData.chargingTimeMax = 2400.0f;
//	writer->CreatePatch(patchData);
//	patchData.InverseStartEndPoint();
//	writer->CreatePatch(patchData);
//}
//void CreateMontrealQuebecAllerRetour(ScenarioWriter* writer, ofstream& outputFile, int carAmmoutEachSide,Carte* inCarte)
//{
//	int carAmount = carAmmoutEachSide;
//
//	Point ptMontrealMin(45.567684, -73.560965);
//	Point ptMontrealMax(45.475795, -73.656416);
//	Point ptQuebecMin(46.858326, -71.243104);
//	Point ptQuebecMax(46.774969, -71.371749);
//
//	ScenarioWriter::PatchData patchData;
//	patchData.amount = carAmount;
//	patchData.startPointMin = ptMontrealMin;
//	patchData.startPointMax = ptMontrealMax;
//	patchData.endPointMin = ptQuebecMin;
//	patchData.endPointMax = ptQuebecMax;
//	patchData.autonomieMin = 200000.0f;
//	patchData.autonomieMax = 200000.0f;
//	patchData.minStartTime = 0;
//	patchData.maxStartTime = 3600; //24h
//	patchData.chargingTimeMin = 1800.0f;
//	patchData.chargingTimeMax = 1800.0f;
//	patchData.maxDistanceVolOiseauBorne = patchData.autonomieMin / 2.f;
//	patchData.vPointBorne = inCarte;
//	writer->CreatePatch(patchData);
//	patchData.InverseStartEndPoint();
//	writer->CreatePatch(patchData);
//}
//
//void GenereExperienceQuebecMontreal(Carte* inCarte)
//{
//	vector<int> amountCar = {1,10,100,200};
//	string fileName = "scenario.txt";
//	for (auto it = amountCar.begin(); it != amountCar.end(); ++it)
//	{
//		int curCarAmount = *it;
//		string dirName = "MontrealQuebec"+ to_string(curCarAmount);
//		if (CreateDirectory(dirName.c_str(), NULL) ||ERROR_ALREADY_EXISTS == GetLastError())
//		{
//			ScenarioWriter* writer = new ScenarioWriter();
//			ofstream outputFile(dirName +"/"+fileName);
//			CreateMontrealQuebecAllerRetour(writer, outputFile, curCarAmount, inCarte);
//			writer->Sort();
//			writer->CommitTo(outputFile);
//			//writer->CommitTo(cout); //For testing...
//			outputFile.close();
//			delete writer;
//			cout << "Finished for " << to_string(curCarAmount) << " cars." << endl;
//		}
//		else
//		{
//			cout << "Error creating experiment..." << endl;
//			assert(false);
//		}
//	}
//	cout << "Done!" << endl;
//}

ScenarioWriter::PatchData CreatePatch(MapCityLimit& mapLimit, string cityA,string cityB, int nbCar, string strPathCarte, string strNbBorne)
{
	ScenarioWriter::PatchData patchData;
	patchData.amount = nbCar;
	patchData.startPointMin = mapLimit[cityA].first;
	patchData.startPointMax = mapLimit[cityA].second;
	patchData.endPointMin   = mapLimit[cityB].first;
	patchData.endPointMax   = mapLimit[cityB].second;
	patchData.autonomieMin = SimulationSetting::D_ACCEPTABLE_MIN_DISTANCE_SCENARIO;
	patchData.autonomieMax = SimulationSetting::D_ACCEPTABLE_MIN_DISTANCE_SCENARIO;
	patchData.minStartTime = 0;
	patchData.maxStartTime = 7200; //2h
	patchData.chargingTimeMin = 1800.0f;
	patchData.chargingTimeMax = 1800.0f;
	patchData.maxDistanceVolOiseauBorne = patchData.autonomieMin / 2.f;
	patchData.vPointBorne = new Carte(strPathCarte, strNbBorne);
	return patchData;
}

void FillCityPatch(MapCityLimit& mapCityLimit)
{
	//Les limites sont:
	//en haut a droite
	//En bas a gauche
	mapCityLimit["montreal"] = CityLimit(Point(45.567684, -73.560965), Point(45.475795, -73.656416)); //Autour de l'ile de Montreal
	mapCityLimit["quebec"]   = CityLimit(Point(46.858326, -71.243104), Point(46.774969, -71.371749)); //Autour de la ville de quebec
	mapCityLimit["matane"]    = CityLimit(Point(48.978627, -66.975120), Point(48.390472, -67.632531)); //Autour de Matane (environ 75km)
	mapCityLimit["rouyn"]    = CityLimit(Point(48.286308, -79.065620), Point(48.210972, -78.973414)); //Englobe Rouyn
	mapCityLimit["megantic"]    = CityLimit(Point(45.711067, -71.692751), Point(45.462250, -70.741759)); //Englobe Megantic
	mapCityLimit["saguenay"]    = CityLimit(Point(48.929216, -72.503957), Point(48.292208, -70.854881)); //Englobe Saguenay
}

void precomputeAccessibleBorne(ScenarioWriter* writer)
{
	cout << "Precomputing accessible borne distance" << endl;
	for (auto var : writer->mData->mVecVehicle)
	{
		
	}
}

int main(int argc, const char** argv) {

	enum TOption
	{
		eCARTE = 0,
		eBORNE,
		eOUTPUTBORNECACHE,
		eOUTPUTSCENARIO,
		eNBCAR,
		eDEPART,
		eDESTINATION,
		eDEPARTB,
		eDESTINATIONB,

		eMAX
	};
	string strOption[] = {
		 "quebec-carte.txt"
		,"bornes-osm.txt"
		,"bornes-osm.txtc"
		,"scenario.txt"
		,"100"
		,"montreal"
		,"matane"
		,""//,"megantic"
		,""//,"saguenay"
	};
	//Chargement des arguments
	for (int i = 1; i < argc; ++i)
	{
		strOption[i - 1] = argv[i];
	}

	for (int i = 0; i < 9; ++i)
	{
		cout << strOption[i] << endl;
	}

	bool bCrossingRoad = !strOption[eDEPARTB].empty() && !strOption[eDESTINATIONB].empty();
	int totalCar = stoi(strOption[eNBCAR]);
	int curCarAmount = bCrossingRoad ? totalCar / 4 : totalCar / 2;
	int carLeft = bCrossingRoad ? totalCar -3*curCarAmount : totalCar - curCarAmount;

	MapCityLimit mapCityLimit;
	FillCityPatch(mapCityLimit);
	ScenarioWriter* writer = new ScenarioWriter();
	ofstream outputFile(strOption[eOUTPUTSCENARIO]);

	ScenarioWriter::PatchData patchDataB;
	if (bCrossingRoad)
	{
		ScenarioWriter::PatchData patchDataB;
		patchDataB = CreatePatch(mapCityLimit, strOption[eDEPARTB], strOption[eDESTINATIONB], curCarAmount, strOption[eCARTE], strOption[eBORNE]);
		writer->CreatePatch(patchDataB);
		patchDataB.InverseStartEndPoint();
		delete patchDataB.vPointBorne;
		patchDataB.vPointBorne = new Carte(strOption[eCARTE], strOption[eBORNE]);
		writer->CreatePatch(patchDataB);
	}

	ScenarioWriter::PatchData patchData;
	patchData = CreatePatch(mapCityLimit, strOption[eDEPART], strOption[eDESTINATION], curCarAmount, strOption[eCARTE], strOption[eBORNE]);
	writer->CreatePatch(patchData);
	patchData.amount = carLeft;
	patchData.InverseStartEndPoint();
	delete patchData.vPointBorne;
	patchData.vPointBorne = new Carte(strOption[eCARTE], strOption[eBORNE]);
	writer->CreatePatch(patchData);

	writer->Sort();
	writer->CommitTo(outputFile);

	//writer->CommitTo(cout); //For testing...
	outputFile.close();

	//Now precompute all path to all accessible bornes
	precomputeAccessibleBorne(writer);

	delete writer;

	cout << "Finished for " << to_string(totalCar) << " cars." << endl;
	cout << "--------------Done!------------" << endl;

	//cin.get();
	return 0;
}

