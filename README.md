# How to use
You should start using the prepackaged tools.

Settings for all programs are in _Settings.ps1. You should modify it and dot import it after from the console.
. .\_Settings.ps1
so all programs have access to the variables.

## Map data generation
You need:
Map
Born data

## Borne cache


## Scenario generation
Scenarios should be in coroadperation\AutomatedExperimentation. There is a ExperimentTest folder preconfigured to test.

Copy CoRoadPerationDataGeneration.exe in the experiment directory. We suggest not modyfing this behavior since if you modify the program, you might affect an older experiment.
Configure and execute the data generation tool named _GenerateExperiment.ps1.
This yields:


2) Configure and execute the experiment execution tool named _RunExperiment.ps1

# Modules
Processus:
Concevoir, implementer, debugger, ameliorer

Current
- WHCA* dot supporter "no solution" VERIFIER
- Avoir une cache va grandement accélérer les calculs (sauvegarder la cache dans un fichier pour utilisation future)
- Cache doit etre utilisee
- Mettre a jour debut et fin de trajet pour les ID
Todo
- Pourquoi je met 0 si le chemin est impossible? (Carte ligne 431) Demeler les solutions impossibles...
- Implementer (terminer) le flow de donnees test (avoir no sol, small, large, etc...)
- Concevoir flow de visualisation
- Commencer a ecrire (penser au lock a 3 voitures pour contrer permutation)
- Concevoir du branch and bound (Ex: comparer pire solution ou integrer critere ou diff carre)
Done
- Debugger HCA*
- Concevoir classes IShuffler, ShufflerQueue, ShufflerCascade, ShufflerPermutation
- Concevoir une classe pour reprendre le calcul pour un SystemSolver (remplacer SetupPartialComputation)
- Debugger l'algorithme de wait
- Concevoir generation de donnees
- Implementer pour tester la permutation
- Importer ma mesure de performance du projet dans le cours d'Alex