Réservation de bornes (basé sur Éric Beaudry)
AVLTtree pour définir des intervals de réservations.
Ajout en O(xxxx)
Suppression en O(xxx)
Vérification en O(xxxx)

A* bas niveau
A* sur les routes pour aller d'un point A à B
Recherche O(i log i), i = intersection 

Précalcul entre bornes électriques avec A*
Offline mais peut se mettre à jour indépendament

A* haut niveau
Création de bornes virtuelles arrivée et départ
Calcul de A*
Recherche O(b log b), b = bornes + 2 

A* naif individuel
Aucune attente
Recherche O(b log b), b = bornes + 2

A* naif sans coopération (A*NC)
On attend si une borne
Recherche O(b log b), b = bornes + 2
pour chaque b, tenir compte de réservation de bornes

HCA*
Coopération entre voitures. On doit exécuter A*NC ce nombre de fois au total...
	Combinaisons (N = nb de voitures)
	Toutes possibilités (N!)
	Cascade (N^2)
	Dernier arrivé (N)
	
WHCA*
On limite les collisions avec une fenêtre de temps.

On a alors: Recherche O(r log r), r = borne accessible dans la fenêtre temporelle
Le calcul sera repris plus tard.
Réduit la complexité de l'exploration temporelle et non du nombre de voitures.