cls

#########################
#Scenario
#########################
$BASEPATH = "D:\Projet\CoRoadperation\AutomatedExperimentation\"
$BASEPATHASSET = $BASEPATH+'Carte\'
$SCENARIOPROGRAMPATH = $BASEPATH+'CoRoadPerationDataGeneration.exe'
$CARTEQUEBEC = $BASEPATHASSET+'quebec-carte.txt'
$BORNEQUEBEC = $BASEPATHASSET+'bornes-osm.txt'
$BORNECACHE =  $BORNEQUEBEC + "c"
$VARIATIONPREFIX = "No"
$EXPERIENCEPREFIX = "Route_"
$SUBEXPERIENCEPREFIX = "Voiture"
$SCENARIOFILE = 'scenario.txt'
$MAXEXPERIMENT = 15
$GENERATIONLOG = $BASEPATH+"Generation-MtlMtn.log"

#$CITYLIMIT = ,[System.Tuple]::Create("montreal","matane","megantic","saguenay")
$CITYLIMIT = ,[System.Tuple]::Create("montreal","matane")
$NBCAR = @(200)

#########################
#Experimentation
#########################
$RUNLOG = "RunningExperiment-MtlMtn.log"
$PLANNERPROGRAMPATH  = $BASEPATH+'CoRoadPeration.exe'
$RESULTS = "_Result.csv"
$DETAILLEDLOG = "PlannerFullDetail-MtlMtn.log"
$VIZ = "scenario_viz"
$SKIPUNTILLATITERATION = -1

#VARIATION COMPUTATION
#Contains $NBBORNE,$SCENARIO,$METRIQUE,$AUTONOMIE
$VARIATION = @()

#First entry is default entry
$BORNE = @('bornes-ce-L3.txt','bornes-osm.txt','bornes-ce-L2L3.txt')
$SCENARIO = @("ScenarioA","ScenarioB")
$METRIQUE = @(3,1,2,0,4,5)
$AUTONOMIE = @("Ioniq","Distribution")

#SECTION DENSIT� DE BORNES
Foreach($v in $BORNE)
{
    $VARIATION += ,@($v,$SCENARIO[0],$METRIQUE[0],$AUTONOMIE[0])
}

#SECTION ALGO
$VARIATION += ,@($BORNE[0],$SCENARIO[0],$METRIQUE[0],$AUTONOMIE[0])

#SECTION SCENARIOS
Foreach($v in $SCENARIO)
{
    $VARIATION += ,@($BORNE[0],$v,$METRIQUE[0],$AUTONOMIE[0])
}

#SECTION METRIQUES
Foreach($v in $METRIQUE)
{
    #2 scenarios
    $VARIATION += ,@($BORNE[0],$SCENARIO[0],$v,$AUTONOMIE[0])
    $VARIATION += ,@($BORNE[0],$SCENARIO[1],$v,$AUTONOMIE[0])
}

#SECTION AUTONOMIE
Foreach($v in $AUTONOMIE)
{
    $VARIATION += ,@($BORNE[0],$SCENARIO[0],$METRIQUE[0],$v)
}

#SECTION R�ALISTE
$VARIATION += ,@($BORNE[0],$SCENARIO[1],$METRIQUE[0],$AUTONOMIE[1])
$VARIATION += ,@($BORNE[2],$SCENARIO[1],$METRIQUE[0],$AUTONOMIE[1])

write-host With duplicate count: $VARIATION.Count
$VARIATION = $VARIATION | select -Unique
write-host Without duplicate count: $VARIATION.Count
$NBEXPERIMENTTOTAL = 4 * $VARIATION.Count
write-host Total simulations: $NBEXPERIMENTTOTAL
$ESTIMATEHOUR = 2
write-host Single computation estimate: $ESTIMATEHOUR h
write-host Single computation estimate: ($NBEXPERIMENTTOTAL*$ESTIMATEHOUR/24) days
#Bug fix to fix last entry
#$VARIATION[-1][-1] = $VARIATION[-2][-1] 
Foreach($v in $VARIATION)
{
    write-host $v -Separator " ,"
}


