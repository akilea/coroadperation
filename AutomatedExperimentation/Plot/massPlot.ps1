REM "C:\Program Files\gnuplot\bin\gnuplot.exe" -c ..\_plotting\InterpolatedPR.gnup 03 Long StemPorter BM25

$TITRE_SCENARIO = "Montréal et Québec"

"C:\Program Files\gnuplot\bin\gnuplot.exe" -c generalVEPlot.gnup "Temps de calcul selon le nombre de voitures (trajet $TITRE_SCENARIO)"      1 3 "Nombre de voitures" "Temps de calcul (s)"      300 601
"C:\Program Files\gnuplot\bin\gnuplot.exe" -c generalVEPlot.gnup "Temps de déplacement selon le nombre de voitures (trajet $TITRE_SCENARIO)" 1 3 "Nombre de voitures" "Temps de déplacement (s)" 300 15000
"C:\Program Files\gnuplot\bin\gnuplot.exe" -c generalVEPlot.gnup "Temps de déplacement selon le nombre de voitures (trajet $TITRE_SCENARIO)" 1 3 "Nombre de voitures" "Temps de déplacement (s)" 300 15000