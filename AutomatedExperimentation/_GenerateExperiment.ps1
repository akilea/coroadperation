Start-Transcript $GENERATIONLOG

#Informative text (informational only)
$NBEXP = $CITYLIMIT.length * $NBCAR.length * $MAXEXPERIMENT
Write-Host "Total experiment: " $NBEXP
Write-Host $CITYLIMIT
#Actual creation
foreach($CURRENTCITYPAIR in $CITYLIMIT)
{
    $START = $CURRENTCITYPAIR.Item1
    $END = $CURRENTCITYPAIR.Item2
    $STARTB = $CURRENTCITYPAIR.Item3
    $ENDB = $CURRENTCITYPAIR.Item4

    Write-Host "$START to $END"

	$FORMATNAME = $EXPERIENCEPREFIX+"$START"+"_to_"+"$END" +"_and_"+"$STARTB"+"_to_"+"$ENDB"
	if(!(Test-Path -Path $FORMATNAME )){
		New-Item -ItemType directory -Path $FORMATNAME | Out-Null
		Set-Location "$FORMATNAME" | Out-Null
		Get-Location | Write-Host
	
		foreach($CARAMOUNT in $NBCAR)
		{
			$SUBFORMATNAME = $SUBEXPERIENCEPREFIX+"$CARAMOUNT"
			if(!(Test-Path -Path $SUBFORMATNAME )){
				New-Item -ItemType directory -Path $SUBFORMATNAME | Out-Null
				Set-Location "$SUBFORMATNAME" | Out-Null
				Get-Location | Write-Host			
				for($i=1; $i -le $MAXEXPERIMENT; $i++){
					$EXPERIENCENAME = $VARIATIONPREFIX+"$i"
					if(!(Test-Path -Path $EXPERIENCENAME )){
						New-Item -ItemType directory -Path $EXPERIENCENAME | Out-Null
						Set-Location "$EXPERIENCENAME" | Out-Null
						Get-Location | Write-Host
						New-Item -ItemType file -Path $SCENARIOFILE | Out-Null
						#CoRoadDatGeneration.exe "quebec-carte.txt","quebec-bornes.txt","distanceBorne.cache","scenario.txt","10","montreal","quebec"
						#..\..\..\..\CoRoadPerationDataGeneration.exe ..\..\..\..\quebec-carte.txt ..\..\..\..\quebec-bornes.txt ..\..\..\..\distanceBorne.cache scenario-quebec.txt 2 montreal quebec
						#$CARTEQUEBEC
						#$BORNEQUEBEC
						#$BORNECACHE
						#$SCENARIOFILE
						#$CARAMOUNT
						#$START
						#$END

		                $cmd = "$SCENARIOPROGRAMPATH $CARTEQUEBEC $BORNEQUEBEC $BORNECACHE $SCENARIOFILE $CARAMOUNT $START $END $STARTB $ENDB"
                        Write-Host $cmd
                        Measure-Command { Invoke-Expression $cmd }

						Set-Location .. | Out-Null
						Get-Location | Write-Host
					}
				}			
			}
				Set-Location .. | Out-Null
				Get-Location | Write-Host	
		}
	}
		Set-Location .. | Out-Null
		Get-Location | Write-Host
}

Stop-Transcript

