#E:\Projet\CoRoadperation\coroadperation\AutomatedExperimentation\ExperimentTest\Final# echo nb_cell,t_wv,v_wv,t_gr,v_gr,t_bb,v_bb,t_bf,v_bf,t_at,v_at > Results.txt
# set back=%cd%
# quebec-carte.txt quebec-bornes.txt quebec-scenario.txt scenario_viz 40
#for /f "tokens=*" %%G in ('dir /b /s /a:d "*Experiment"') do CoRoadPeration.exe Carte\quebec-carte.txt Carte\quebec-bornes.txt Carte\distanceBorne.cache %%G\scenario.txt result.csv %%G\details.log scenario_viz >> RunDetailled.txt
#pause

Start-Transcript $RUNLOG
$SAVECOUNTER = 0

foreach($V in $VARIATION)
{
    #$NBBORNE,$SCENARIO,$METRIQUE,$AUTONOMIE
    Write-Host $V
    $NBBORNE = $BASEPATHASSET + $V[0]
	$NBBORNECACHE = $NBBORNE+"c"
    $SCENARIOFILTER = $V[1]+"/*.txt"
    $METRIC = $V[2]
    $AUTONOMY = $V[3]
    $MAXBORNE = 100000
    $CURRENTRESULT = $V[1] + $RESULTS
	$RES = 	Get-ChildItem -recurse -filter $SCENARIOFILTER -Name
    Write-Host $RES
	foreach($CURRENTEXPERIMENT in $RES)
	{
        $CURRENTSCENARIOPATH = $V[1]+"/" + $CURRENTEXPERIMENT
        if($SAVECOUNTER -gt $SKIPUNTILLATITERATION)
        {
            Write-Host "========================================================================="
            Write-host "Running $CURRENTEXPERIMENT for station:$CURRENTNBBORNE, metric:$METRIC, autonomy:$AUTONOMY"
            Write-Host "========================================================================="
		    $cmd = "$PLANNERPROGRAMPATH $CARTEQUEBEC $NBBORNE $NBBORNECACHE $CURRENTSCENARIOPATH $CURRENTRESULT $DETAILLEDLOG $VIZ $MAXBORNE $METRIC $AUTONOMY"
            Write-Host $cmd
            Invoke-Expression $cmd
        }else
        {
            Write-Host "========================================================================="
            Write-host "Skipping $CURRENTEXPERIMENT for station:$CURRENTNBBORNE, metric:$METRIC, autonomy:$AUTONOMY"
            Write-Host "========================================================================="
        }
        $SAVECOUNTER += 1
	}
}

Stop-Transcript