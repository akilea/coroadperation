package org.openstreetmap.gui.jmapviewer;

//License: GPL. Copyright 2009 by Stefan Zeller

/**
 * This class encapsulates a Point2D.Double and provide access
 * via <tt>lat</tt> and <tt>lon</tt>.
 *
 * @author Jan Peter Stotz
 *
 */
public class Coordinate /*implements Serializable*/ {
    //private transient Point2D.Double data;
    private double longitude, latitude;
    public final double RAYONTERRE = 6371000.0;

    public Coordinate(double lat, double lon) {
        this.latitude = lat;
        this.longitude = lon;
    }

    public double getLat() {
        return latitude;
    }

    public void setLat(double lat) {
        latitude = lat;
    }

    public double getLon() {
        return longitude;
    }

    public void setLon(double lon) {
        longitude = lon;
    }
    
    public double distanceTerre(Coordinate coor)
    {
    	double lon1=Math.PI*longitude/180.0,
                lon2=Math.PI*coor.longitude/180.0,
                lat1=Math.PI*latitude/180.0,
                lat2=Math.PI*coor.latitude/180.0;
    	
		    	double s1 = Math.sin((lat2-lat1)/2);
		        double s2 = Math.sin((lon2-lon1)/2);
		  
		        return 2*RAYONTERRE * Math.asin(Math.sqrt(s1*s1 + Math.cos(lat1)*Math.cos(lat2)*s2*s2));
    }
/*
    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeObject(data.x);
        out.writeObject(data.y);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        data = new Point2D.Double();
        data.x = (Double) in.readObject();
        data.y = (Double) in.readObject();
    }
*/
    @Override
    public String toString() {
        return "(" + latitude + "," + longitude + ")";
    }
}
