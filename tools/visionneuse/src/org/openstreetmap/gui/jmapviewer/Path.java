/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openstreetmap.gui.jmapviewer;

import java.awt.Color;
import java.util.LinkedList;

/**
 *
 * @author Eric Beaudry
 */
public class Path {
    
    public LinkedList<Coordinate> waypoints = new LinkedList<Coordinate>();
    public Color color = new Color(0, 0, 255, 128);
}
