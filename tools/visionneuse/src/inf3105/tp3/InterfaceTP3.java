package inf3105.tp3;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import org.openstreetmap.gui.jmapviewer.Coordinate;

/**
 *
 * @author Éric Beaudry
 */
public class InterfaceTP3 {
    protected Process        process;
    protected PrintWriter    toProcess;
    protected BufferedReader fromProcess;
    protected BufferedReader fromProcessErr;
    
    public static class Resultat{
        protected double duree;
        protected List<Coordinate> trajet = new LinkedList<>();
        protected Set<String> bornesUtilisees = new TreeSet<>();
    }

    public InterfaceTP3(String carte, String bornes) throws IOException {
        File file = new File("../../Debug/", "CoRoadPeration.exe");
        if(!file.exists())
            System.out.println("Fichier '" + file + "' introuvable ou non exécutable!");
        String absPath = file.getAbsolutePath();
        //ProcessBuilder pb = new ProcessBuilder(absPath, carte, bornes,"scenario.txt","interactive");
        ProcessBuilder pb = new ProcessBuilder(absPath);
        File wd =  new File("../../data/", "");
        String absWorkingDirectory = wd.getAbsolutePath();
        if(wd.exists())
            pb.directory(new File(absWorkingDirectory));
        System.out.println("Execution de : " + pb.toString());
        process = pb.start();

        toProcess = new PrintWriter(process.getOutputStream());
        fromProcess = new BufferedReader(new InputStreamReader(process.getInputStream()));
        BufferedReader err = new BufferedReader(new InputStreamReader(process.getErrorStream()));
    }
    
    public synchronized Resultat trajet(Coordinate depart, Coordinate arrivee, int autonomie) throws IOException{
        System.out.println("-------------------------------------------");
        String cmd = depart + " " + arrivee + " " + autonomie + " ."; //SUPER HACK LOL
        System.out.println(">>" + cmd);
        toProcess.println(cmd);
        toProcess.flush();
        String distance = fromProcess.readLine();
        System.out.println("<<" + distance);
        String strajet = fromProcess.readLine();
        System.out.println("<<" + strajet);
        String bornes = fromProcess.readLine();
        System.out.println("<<" + bornes);
        
        Resultat resultat = new Resultat();

        for(String borne : bornes.split("\\s"))
            resultat.bornesUtilisees.add(borne);
        
        StringTokenizer tokens = new StringTokenizer(strajet);
        LinkedList<Coordinate> trajet = new LinkedList<>();
        while(tokens.hasMoreTokens()){
            String token = tokens.nextToken();
            String champs[] = token.split("[(),]");
            double lat = Double.parseDouble(champs[1]);
            double lon = Double.parseDouble(champs[2]);
            Coordinate c = new Coordinate(lat, lon);
            resultat.trajet.add(c);
        }
        
        
        return resultat;
    }
    
}
