package inf3105.tp3;

/*
  INF3105 - Structures de données et algorithmes
  UQAM / Département d'informatique
  Éric Beaudry -- http://ericbeaudry.ca/INF3105 -- beaudry.eric@uqam.ca
  Visionneuse pour TP3 d'INF3105
*/

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * @author Eric Beaudry
 */
public class ParserOSM_SAX {

    static Set<String> OnewayValues = new TreeSet<String>(Arrays.asList(new String[]{"yes", "true", "1", "-1", "reverse"}));
    static Set<String> TypeHighWays = new TreeSet<String>();
    static boolean DEBUG = false;
    
    TreeSet<Long> bonsNoeuds = new TreeSet<Long>();
    Carte map = new Carte();
    long id;
    long lastNewNodeID=-1;
    boolean nodesMustBeSorted=true;
    double lat;
    double lon;
    String nom;
    Way wayTemp;
    
    TreeSet<String> nomsrues = new TreeSet<String>();
    ArrayList<Long> tempsIdsWay = new ArrayList<Long>();
    int nbSegmentRoute = 0;
    
    static{
        TypeHighWays.addAll(Arrays.asList(new String[]{"residential", "primary", "primary_link", "secondary", "secondary_link", "tertiary", "tertiary_link",  "motorway", "motorway_link", "trunk", "trunk_link", "unclassified"}));
        TypeHighWays.addAll(Arrays.asList(new String[]{"motorway", "motorway_link"}));
        //TypeHighWays.add("cycleway");
        //TypeHighWays.add("footway");
        TypeHighWays.add("service");
    }
    
    Runtime rt = Runtime.getRuntime();
    
    void printMemory(){
        System.out.println("Memory: " + ((rt.totalMemory()-rt.freeMemory())/1024/1024)
                +"MB / " + (rt.totalMemory()/1024/1024) + "MB"
                +"   max=" + (rt.maxMemory()/1024/1024) + "MB");
    }

    public Carte parse(File file) throws IOException, SAXException, ParserConfigurationException {

        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();

        DefaultHandler handler = new DefaultHandler() {
            boolean way = false; // présentement dans un noeud <way> ?
            boolean route = false;  // le présent noeud <way> a été détecté comme d'un type de highway intéressant (une route à ajouter) ?
            boolean inverse = false; // la liste de noeuds est dans un ordre inversé
            
            ArrayList<NodeOSM> nodes = new ArrayList<NodeOSM>(); // Tous les noeuds dans le fichier OSM. Dans un ArrayList, car plus efficace qu'un TreeMap.

            @Override
            public void startElement(String uri, String localName, String qName, Attributes attributes)
                    throws SAXException 
            {
                if (qName.equalsIgnoreCase("NODE")) {
                    id = Long.parseLong(attributes.getValue("id"));
                    nodesMustBeSorted |= id < lastNewNodeID;
                    lastNewNodeID = id;
                    lon = Double.parseDouble(attributes.getValue("lon"));
                    lat = Double.parseDouble(attributes.getValue("lat"));
                    NodeOSM nodeosm = new NodeOSM(id, lat, lon);
                    nodes.add(nodeosm);
                    if(DEBUG && nodes.size()%100000 == 0){
                        System.out.println(nodes.size() + " noeuds ...");
                        printMemory();
                    }                    
                }

                else if (qName.equalsIgnoreCase("WAY")) {
                    way = true;
                    id = Long.parseLong(attributes.getValue("id"));
                    wayTemp = new Way(id);
                    tempsIdsWay.clear();
                    if(nodesMustBeSorted){
                        Collections.sort(nodes);
                        nodesMustBeSorted = false;
                    }
                }

                else if (qName.equalsIgnoreCase("ND")) {
                    if (way) {
                        long ref = Long.parseLong(attributes.getValue("ref"));
                        int index = Collections.binarySearch(nodes, new NodeOSM(ref,0,0));
                        if(index<0){
                            System.out.println("Missing node " + ref + " in way " + wayTemp.id);
                        }else{
                            tempsIdsWay.add(ref); // Pour l'instant, on garde juste les IDs. On n'ajoute pas encore les NodeOSM, en cas que ce ne soit pas une route.
                        }
                    }
                }

                else if (qName.equalsIgnoreCase("tag")) {
                    if (way) {
                        String key = attributes.getValue("k");
                        String value = attributes.getValue("v");
                        if (key.equals("highway")) {
                            route = TypeHighWays.contains(value);
                            if (wayTemp.vitesseLimite_kmh == 0)
                                wayTemp.vitesseLimite_kmh = getHighwaysMaxSpeed(value);
                        }else if (key.equals("name")) {
                            nom = value;
                            if(nomsrues.contains(nom))
                                nom = nomsrues.floor(nom);
                            else
                                nomsrues.add(nom);
                            wayTemp.setName(nom);
                        }
                        else if (key.equals("oneway")) {
                            if (OnewayValues.contains(value))
                                wayTemp.oneway = true;
                            if (value.equals("-1") || value.equalsIgnoreCase("reverse"))
                                inverse = true;
                        }else if (key.equals("maxspeed")) {
                            String maxspeed = attributes.getValue("v");
                            if (maxspeed.contains("mph")) {
                                String[] fields = maxspeed.split(" ");
                                try {
                                    wayTemp.vitesseLimite_kmh = (short) (Short.parseShort(fields[0]) * 1.609344);
                                } catch (Exception e) {
                                    System.err.println("Error while parsing maxspeed. Value was : " + maxspeed);
                                }
                            }else if (wayTemp.vitesseLimite_kmh == 0) {
                                try {
                                    wayTemp.vitesseLimite_kmh = Short.parseShort(maxspeed);
                                } catch (Exception e) {
                                    System.err.println("Error while parsing maxspeed. Value was : " + maxspeed);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void endElement(String uri, String localName, String qName)
                    throws SAXException 
            {

                if (qName.equalsIgnoreCase("WAY")) {
                    if (route) {
                        for(long nid : tempsIdsWay){
                            int index = Collections.binarySearch(nodes, new NodeOSM(nid,0,0));
                            NodeOSM node = nodes.get(index);
                            wayTemp.nodes.add(node);
                            map.nodes.put(node.id, node); // node doit être conservé dans la carte
                            nbSegmentRoute++;
                        }
                        if (inverse) {
                            ArrayList<NodeOSM> reversenodes = new ArrayList<NodeOSM>();
                            for (int k = wayTemp.nodes.size() - 1; k >= 0; k--) {
                                reversenodes.add(wayTemp.nodes.get(k));
                            }
                            wayTemp.nodes = reversenodes;
                        }
                        map.ways.put(id, wayTemp);
                        if(DEBUG && map.ways.size()%100000 == 0){
                            System.out.println(map.ways.size() + " ways ...");
                            printMemory();
                        }
                    }
                    way = false;
                    route = false;
                    inverse = false;
                }
            }

            @Override
            public void characters(char ch[], int start, int length) throws SAXException {
            }

            @Override
            public void endDocument() throws SAXException {
            }
        };

        saxParser.parse(file, handler);
        System.out.println("Carte chargée: " + map.nodes.size() + " noeuds; " + map.ways.size() + " routes; ");
        return map;
    }
    
    public final static short getHighwaysMaxSpeed(String type) {
        switch (type) {
            case "residential":
                return 50;
            case "motorway":
                return 100;
            case "motorway_link":
                return 45;
            case "trunk" :
                return 90;
            case "trunk_link" :
                return 45;
            case "living_street" :
                return 40; 
            case "track" : 
                return 30;
            case "primary":
                return 70;
            case "primary_link":
                return 45;
            case "secondary":
                return 70;
            case "secondary_link":
                return 45;
            case "tertiary":
                return 50; 
            case "tertiary_link":
                return 45;
            case "unclassified":
                return 50;
            default : 
                return 50;
        }
    }

}
