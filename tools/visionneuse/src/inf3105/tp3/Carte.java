package inf3105.tp3;

/*
  INF3105 - Structures de données et algorithmes
  UQAM / Département d'informatique
  Éric Beaudry -- http://ericbeaudry.ca/INF3105 -- beaudry.eric@uqam.ca
  Été 2016
*/

import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 *
 * @author Éric Beaudry
 */
public class Carte {
    
    public Map<Long, NodeOSM> nodes = new TreeMap<Long, NodeOSM>();
    public Map<Long, Way> ways = new TreeMap<Long, Way>();
    
    public void renumeroter(){
        long id=0;
        for(NodeOSM n : nodes.values()){
            n.id = ++id;
        }
    }
    
    public NodeOSM trouverPlusProche(NodeOSM c){
        NodeOSM result = null;
        double dmin=Double.POSITIVE_INFINITY;
        for(NodeOSM n : nodes.values()){
            double d = c.distanceSurfaceTerre(n);
            if(d<dmin){
                dmin=d;
                result=n;
            }
        }
        return result;
    }
    
    private final boolean dumpOneWays = true;
    
    public void dump(PrintWriter pw) throws IOException{
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
        nf.setMaximumFractionDigits(6);
        for(NodeOSM n : nodes.values())
            pw.println("n" + n.id + " (" + nf.format(n.latitude) + "," + nf.format(n.longitude) + ");");
        pw.println("---");
        for(Way w : ways.values()){
            pw.print(w.getName() + "\t:\t"); // + w.vitesseLimite_kmh + "\t");
            for(NodeOSM n : w.nodes)
                pw.print("\tn" + n.id);
            pw.println("\t;");
            
            if(!w.oneway && dumpOneWays){ 
                pw.print(w.getName() + "\t:\t"); // + w.vitesseLimite_kmh + "\t");
                for(int i=w.nodes.size()-1;i>=0;i--)
                    pw.print("\tn" + w.nodes.get(i).id);
                pw.println("\t;");                
            }
        }
        //pw.println("---");        
    }
    
    
    public static Carte fusionner(Collection<Carte> maps){
        Carte result = new Carte();
        for(Carte map : maps)
            result.nodes.putAll(map.nodes);
        for(Carte map : maps)
            for(Way w : map.ways.values()){
                ArrayList<NodeOSM> nodes = new ArrayList<NodeOSM>();
                Way w2 = (Way) w.clone();
                w2.nodes = new ArrayList<NodeOSM>();
                for(NodeOSM n : w.nodes)
                    w2.nodes.add(result.nodes.get(n.id));
                result.ways.put(w2.id, w2);
            }
        return result;
    }
    
    public TreeSet<NodeOSM> getBonsNoeuds(NodeOSM n){
        TreeSet<NodeOSM> bons = new TreeSet<NodeOSM>();
        bons.add(n==null ? nodes.values().iterator().next() : n);
        int nb;
        do{
            nb = bons.size();
            for(Way route : ways.values()){
                boolean bon = false;
                for(NodeOSM node : route.nodes)
                    if(bons.contains(node))
                        bon = true;
                if(bon)
                    for(NodeOSM node : route.nodes)
                        bons.add(node);
            }
            //System.out.println("Nombre bon noeuds : " + bons.size());
        }while(nb<bons.size());
        System.out.println("Nombre bon noeuds : " + bons.size());
        return bons;
    }

}
