package inf3105.tp3;

import java.awt.*;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

/**
 *
 * @author Eric Beaudry
 */
public class MapMarkerText implements MapMarker {
    
    protected Coordinate coor;
    protected String  annotation = null;
    protected Color   color = Color.yellow;

    public MapMarkerText(Coordinate coor) {
        this.coor = coor;
    }
    public MapMarkerText(Coordinate coor, String annotation, Color color) {
        this.coor = coor;
        this.annotation = annotation;
        this.color = color;
    }
    @Override
    public double getLat() {
        return coor.getLat();
    }

    @Override
    public double getLon() {
        return coor.getLon();
    }

    @Override
    public void paint(Graphics g, Point position, int zoom) {
        Graphics2D g2d = (Graphics2D) g;
        int minZoomPourRect=1;
        if(zoom<minZoomPourRect) return;
        g2d.setColor(color);
        g.fillOval(position.x-4, position.y-4, 9, 9);
        g2d.setColor(Color.BLACK);
        g.drawOval(position.x-4, position.y-4, 9, 9);
        
        String texte = ""; //node.id>=0 ? "n" + node.id : "";
        if(annotation!=null) texte = annotation;
        int tw = g2d.getFontMetrics().stringWidth(texte);
        Shape rect = new Rectangle(position.x+5, position.y+5, tw+4, 18);

        g2d.setColor(color);
        
        int minZoomPourId=5;       
        if(zoom>=minZoomPourId)
            g2d.fill(rect);
        g2d.setColor(Color.BLACK);
        if(zoom>=minZoomPourId)
            g2d.draw(rect);

        if(zoom>=minZoomPourId)
            g2d.drawString(texte, position.x+6, position.y+18);
    }

    @Override
    public String toString() {
        return "Marker_" + annotation;
    }

}
