/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inf3105.tp3;

import org.openstreetmap.gui.jmapviewer.Coordinate;

/**
 *
 * @author Éric Beaudry
 */
public class Borne {
    protected String nom;
    protected int niveau;
    protected Coordinate position;
    protected boolean utilisee = false;
}
