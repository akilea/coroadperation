package inf3105.tp3;

/*
  INF3105 - Structures de données et algorithmes
  UQAM / Département d'informatique
  Éric Beaudry -- http://ericbeaudry.ca/INF3105 -- beaudry.eric@uqam.ca
  Hiver et Été 2013
  
  Visionneuse pour TP3 d'INF3105/2013E
*/


import java.text.Normalizer;
import java.util.ArrayList;

/**
 *
 * @author Éric Beaudry
 */
public class Way implements Comparable<NodeOSM>, Cloneable {
    public long id;
    private String name = "?";
    short vitesseLimite_kmh=0;
    public ArrayList<NodeOSM> nodes = new ArrayList<NodeOSM>();
    public boolean oneway = false;
    
    public Way(long id){
        this.id = id;
    }
    
    public void setName(String name){
        name = Normalizer.normalize(name, Normalizer.Form.NFD);
        name = name.replaceAll("[^\\p{ASCII}]", "");
        name = name//.replace(".", " ")
                //.replace("'", "_")
                //.replace("-", "_")
                //.replace("`", "_")
                .replace(" ", "_")
                //.replace(',', '_')
                .replace(';', '_')
                .replace(':', '_')
                .replace('"', '_')
                //.replace('+', '_')
                ;
        this.name = name;
    }
    
    public String getName(){
        return name;
    }
    
    @Override
    public int compareTo(NodeOSM o) {
        if(id<o.id) return -1;
        if(id>o.id) return +1;
        return 0;
    }
    
    @Override
    public Way clone(){
        Way copie = new Way(id);
        copie.name = name;
        copie.oneway = oneway;
        copie.nodes = nodes;
        return copie;
    }
}
