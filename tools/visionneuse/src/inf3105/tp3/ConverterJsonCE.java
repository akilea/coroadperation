package inf3105.tp3;


import java.io.FileReader;
import java.io.Reader;
import java.text.NumberFormat;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author Éric Beaudry
 */
public class ConverterJsonCE {
    public static void main(String[] args) throws Exception{
        String filename;
        if(args.length>0)
            filename = args[0];
        else
            filename = "CE-20160712_1905.json";
        Reader reader = new FileReader(filename);
        //JSONObject jsonobj = new JSONObject(new JSONTokener(reader));
        JSONArray a = new JSONArray(new JSONTokener(reader));
        for(Object o : a){
            if(o instanceof JSONObject){
                JSONObject station = (JSONObject) o;
                int level = station.getInt("Level");
                //int availablecount = station.getInt("AvailableCount");
                int count = station.getInt("Count");
                String parkname = station.getString("ParkName");
                parkname = parkname.replaceAll(" ", "_");
                JSONObject latlng = station.getJSONObject("LatLng");
                double latitude = latlng.getDouble("Lat");
                double longitude = latlng.getDouble("Lng");
                int NetworkId = station.getInt("NetworkId");
                
                if(level==3 /*&& NetworkId==2*/)
                    System.out.println("" + parkname + "\tL" + level + "\t(" + latitude + "," + longitude + ")"
                        //    + "\t" + NetworkId
                        );
            }
        }
    }
}
