package inf3105.tp3;


import java.io.File;
import java.io.PrintWriter;

/**
 *
 * @author Éric Beaudry
 */
public class ConvertirCarte {
    public static void main(String[]args) throws Exception{
        String in="quebec.osm", out="carte.txt";
        if(args.length>0)
            in = args[0];
        if(args.length>1)
            out = args[1];
        else{
            if(in.endsWith(".osm"))
                out = in.substring(0, in.length()-4) + "-carte.txt";
        }
        Carte carte = new ParserOSM_SAX().parse(new File(in));
        PrintWriter pw = new PrintWriter(out);
        carte.dump(pw);
        pw.flush();
        pw.close();
    }
}
