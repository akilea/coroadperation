/*
  INF3105 - Structures de données et algorithmes
  UQAM / Département d'informatique
  Éric Beaudry -- http://ericbeaudry.ca/INF3105 -- beaudry.eric@uqam.ca
  Hiver et Été 2013
  
  Visionneuse pour TP3 d'INF3105/2013E
*/
package inf3105.tp3;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Éric Beaudry
 */
public class ParserCarteTxt {
    
    public static Carte parse(File file) throws IOException, Exception{
        StreamTokenizer fst = new StreamTokenizer(new FileReader(file));
        return ParserCarteTxt.parse(fst);
    }
    
    public static Carte parse(StreamTokenizer fst) throws IOException, Exception
    {
        Carte map = new Carte();
        fst.wordChars('_', '_');
        fst.wordChars('?', '?');
        fst.wordChars('&', '&');
        while(fst.nextToken()!=StreamTokenizer.TT_EOF){
            if(fst.ttype=='-') break;
            if(fst.ttype!=StreamTokenizer.TT_WORD) 
                throw new Exception("Format de fichier incorrect à ligne #" + fst.lineno() + " caractère: " + fst.sval);
            String nom = fst.sval;           
            if(nom.equals("---")) break;
            if(fst.nextToken() != '(') 
                throw new Exception("Format de fichier incorrect.");
            if(fst.nextToken() != StreamTokenizer.TT_NUMBER)
                throw new Exception("Format de fichier incorrect.");
            double lat = fst.nval;
            if(fst.nextToken() != ',') 
                throw new Exception("Format de fichier incorrect.");
            if(fst.nextToken() != StreamTokenizer.TT_NUMBER) throw new Exception("Format de fichier incorrect.");
            double lon = fst.nval;
            if(fst.nextToken() != ')')
                throw new Exception("Format de fichier incorrect.");
            if(fst.nextToken() != ';')
                throw new Exception("Format de fichier incorrect.");
            long id = Long.parseLong(nom.substring(1));
            NodeOSM node = new NodeOSM(id, lat, lon);
            map.nodes.put(id, node);
        }
        fst.nextToken(); fst.nextToken(); // --
        
        long nextwayid = 1;
        long nbnodeslistes=0;
        fst.ordinaryChars('0', '9');
        fst.wordChars('0', '9');
        fst.wordChars('(', '(');
        fst.wordChars(')', ')');
        fst.wordChars('\'', '\'');
        fst.wordChars('#', '#');
        fst.wordChars('/', '/');
        fst.wordChars('!', '!');
        fst.wordChars('%', '%');
        fst.wordChars('<', '@');
        fst.wordChars('[', '`');
        fst.wordChars('{', '~');
        fst.wordChars('*', ',');
        while(fst.nextToken()!=StreamTokenizer.TT_EOF){
            if(fst.ttype=='-') break;
            if(fst.ttype!=StreamTokenizer.TT_WORD)
                throw new Exception("Format de fichier incorrect à ligne #" + fst.lineno() + " caractère: " + fst.sval);
            String nomrue = fst.sval;
            if(nomrue.equals("---")) break;
            if(fst.nextToken() != ':') {
                throw new Exception("Format de fichier incorrect à ligne #" + fst.lineno() + " caractère: " + fst.sval);
            }
            Way way = new Way(nextwayid);
            way.oneway=true;
            way.setName(nomrue);
            while(fst.nextToken()!=';'){
                if(fst.ttype != StreamTokenizer.TT_WORD)
                    throw new Exception("Format de fichier incorrect à ligne #" + fst.lineno() + " caractère: " + fst.sval);
                String nomnoeud = fst.sval;
                long id = -1;
                try{
                    id = Long.parseLong(nomnoeud.substring(1));
                }catch(Exception e){
                    e.printStackTrace();
                }
                NodeOSM node = map.nodes.get(id);
                if(node!=null)
                    way.nodes.add(node);
                else
                    System.out.println("-");
                nbnodeslistes++;
            }
            map.ways.put(nextwayid, way);
            nextwayid++;
        }
        fst.nextToken(); fst.nextToken(); // --
        
        System.out.println("Carte chargée: " + map.nodes.size() + " noeuds; " + map.ways.size() + " routes; " + nbnodeslistes + " références.");

        return map;
    }
    
    public static long parseOrigine(StreamTokenizer st)throws Exception {
        if(st.nextToken()==StreamTokenizer.TT_EOF) return 0;
        if(st.ttype!=StreamTokenizer.TT_WORD)  throw new Exception("Format de fichier incorrect.");
        String origine = st.sval;
        long id = Long.parseLong(origine.substring(1));
        for(int i=0;i<3;i++)
            if(st.nextToken()!='-')
                throw new Exception("Format de fichier incorrect.");
        return id;
    }

    public static List<Long> parseDestinations(StreamTokenizer st)throws Exception {
        ArrayList<Long> destinations = new ArrayList<Long>();
        while(st.nextToken()!=';'){
            if(st.ttype != StreamTokenizer.TT_WORD) throw new Exception("Format de fichier incorrect.");
            String nomnoeud = st.sval;
            long id = Long.parseLong(nomnoeud.substring(1));
            destinations.add(id);
            }
        return destinations;
    }    
}
