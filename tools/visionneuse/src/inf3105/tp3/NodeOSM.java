package inf3105.tp3;

/*
  INF3105 - Structures de données et algorithmes
  UQAM / Département d'informatique
  Éric Beaudry -- http://ericbeaudry.ca/INF3105 -- beaudry.eric@uqam.ca
  Été 2016
  
  Visionneuse pour TP3 d'INF3105/2013E
*/


/**
 *
 * @author Éric Beaudry
 */
public class NodeOSM implements Comparable<NodeOSM> {
    public long   id;
    public double longitude, latitude;
    
    public static final double RAYONTERRE = 6371000;
    
    public NodeOSM(long id_, double latitude_, double longitude_){
        this.id = id_;
        this.latitude=latitude_;
        this.longitude=longitude_;
    }
    
    public double distanceSurfaceTerre(NodeOSM n2){
    	double lon1=Math.PI*longitude/180.0,
                lon2=Math.PI*n2.longitude/180.0,
                lat1=Math.PI*latitude/180.0,
                lat2=Math.PI*n2.latitude/180.0;
    	
        double s1 = Math.sin((lat2-lat1)/2);
        double s2 = Math.sin((lon2-lon1)/2);

        return 2*RAYONTERRE * Math.asin(Math.sqrt(s1*s1 + Math.cos(lat1)*Math.cos(lat2)*s2*s2));
    }
    
    public double distanceDeg2(NodeOSM n2){
        double dlon = longitude - n2.longitude;
        double dlat = latitude - n2.latitude;
        return dlon*dlon + dlat*dlat;
    }

    @Override
    public int compareTo(NodeOSM o) {
        if(id<o.id) return -1;
        if(id>o.id) return +1;
        return 0;
    }
    
    @Override
    public String toString(){
        return "n" + id;
    }
}
