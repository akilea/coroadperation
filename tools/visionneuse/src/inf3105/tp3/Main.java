package inf3105.tp3;

import java.awt.Color;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;
import javax.swing.*;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MemoryTileCache;
import org.openstreetmap.gui.jmapviewer.OsmFileCacheTileLoader;
import org.openstreetmap.gui.jmapviewer.Path;

/**
 * @author Éric Beaudry
 */
public class Main extends JFrame {

    protected JMapViewer mapviewer = null;
    private MapControllerPerso mapcontroller;
    protected InterfaceTP3 interfaceTP3;
    protected String ficherCarte, fichierBornes;
    
    public Main() throws IOException, Exception {
        Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screenSize.getSize());
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("UQAM | INF3105 / 2016E / TP3");
        
        // Création du MapViewer
        mapviewer = new JMapViewer(new MemoryTileCache(), 2);
        File cacheDir=new File(".");
        mapviewer.setTileLoader(new OsmFileCacheTileLoader(mapviewer, cacheDir));
        mapviewer.setDisplayPositionByLatLon(45.509106, -73.568508, 16);
        mapcontroller = new MapControllerPerso(this);
        
        this.setContentPane(mapviewer);
        this.validate();
    }
    
    public static String trouverPremierFichierExistant(String[] candidats){
        for(String c : candidats)
            if(new File(c).exists())
                return c;
        return null;
    }
    
    public static void main(String args[]) throws Exception{
        String fichiercarte=trouverPremierFichierExistant(new String[]{"carte.txt", "uqam-carte.txt", "montreal-carte.txt", "quebec-carte.txt"});
        String fichierbornes=trouverPremierFichierExistant(new String[]{"bornes.txt", "uqam-bornes.txt", "montreal-bornes.txt", "quebec-bornes.txt"});
        if(args.length > 0) fichiercarte = args[0];
        if(args.length > 1) fichierbornes = args[1];
        Main mw = new Main();
        mw.setFichierBornes(fichierbornes);
        mw.setFichierCarte(fichiercarte);
        mw.startTP3();
    }
    
    Coordinate depart=null, destination=null;
    MapMarkerText mmDepart, mmDestination;
    int autonomie = 160000;
    
    void clickPointOnMap(Coordinate coor) {
        //System.out.println("Clic : " + coor);
        if(destination!=null){
            depart = destination = null;
            mapviewer.removeMapMarker(mmDepart);
            mapviewer.removeMapMarker(mmDestination);
        }
        
        if(depart==null){
            depart = coor;
            mapviewer.addMapMarker(mmDepart = new MapMarkerText(coor, "Depart", Color.red));
        }else {
            destination = coor;
            mapviewer.addMapMarker(mmDestination = new MapMarkerText(coor, "Arrivee", Color.green));
        }
        
        if(destination!=null && interfaceTP3!=null){
            try{
                mapviewer.clearMapPaths();
                InterfaceTP3.Resultat r = interfaceTP3.trajet(depart, destination, autonomie);
                List<Coordinate> trajet = r.trajet;
                Path path = new Path();
                path.waypoints = (LinkedList<Coordinate>) trajet;
                for(Borne b : bornes.values())
                    b.utilisee = r.bornesUtilisees.contains(b.nom);
                mapviewer.addMapPath(path);
            }catch(IOException ioe){
                ioe.printStackTrace();
            }
        }
    }
    
    private TreeMap<String, Borne> bornes = new TreeMap<String, Borne>();
    
    public void chargerBornes() {
        File file = new File(fichierBornes);
        System.out.println("Chargement des bornes depuis : " + file);
        bornes.clear();
        try{
            BufferedReader br = new BufferedReader(new FileReader(file));
            String ligne;
            while((ligne=br.readLine())!=null){
                if(ligne.isEmpty() || ligne.startsWith("#"))
                    continue;
                String[] champs = ligne.split("\\s");
                Borne b = new Borne();
                b.nom = champs[0];
                if(!champs[1].startsWith("L")){
                    System.out.println("Erreur format : " + ligne);
                    continue;
                }
                b.niveau = Integer.parseInt(champs[2]);
                champs = champs[3].split("[(),]");
                double lat = Double.parseDouble(champs[1]);
                double lng = Double.parseDouble(champs[2]);
                b.position = new Coordinate(lat, lng);
                bornes.put(b.nom, b);

                mapviewer.addMapMarker(new MapMarkerBorne(b, lat, lng));
            }
        }catch(IOException | NumberFormatException e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(rootPane, e);
        }
    }
    
    protected void setFichierCarte(String ficherCarte){
        this.ficherCarte=ficherCarte;
        boolean isuqam = ficherCarte.startsWith("uqam");
        if(isuqam)
            autonomie = 200;
        if(isuqam){
            try{
                System.out.println("Chargement carte : " + ficherCarte + " ...");
                Carte carte = ParserCarteTxt.parse(new File(ficherCarte));
                if(carte.nodes.size()<100000)
                    for(NodeOSM n : carte.nodes.values()){
                        MapMarkerText mmt = new MapMarkerText(new Coordinate(n.latitude, n.longitude), "n" + n.id, Color.yellow);
                        mapviewer.addMapMarker(mmt);
                    }
            }catch(Exception e){
                e.printStackTrace();
                JOptionPane.showMessageDialog(rootPane, e);
            }
        }
    }

    protected void setFichierBornes(String fichierBornes){
        this.fichierBornes=fichierBornes;
        chargerBornes();
    }
    
    protected void startTP3(){
        System.out.println("Lancement du programme coroadperation ...");
        try{
            interfaceTP3 = new InterfaceTP3(ficherCarte, fichierBornes);
        }catch(IOException ioe){
            ioe.printStackTrace();
            JOptionPane.showMessageDialog(rootPane, ioe);
        }     
    }


}
