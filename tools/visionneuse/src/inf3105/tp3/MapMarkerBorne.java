package inf3105.tp3;

import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

/**
 *
 * @author Eric Beaudry
 */

public class MapMarkerBorne implements MapMarker {
    
    double longitude, latitude;
    Borne borne;
    int id = nextid++;

    public MapMarkerBorne(Borne b, double lat, double lon) {
        borne = b;
        latitude=lat;
        longitude=lon;
    }

    @Override
    public double getLat() {
        return latitude;
    }

    @Override
    public double getLon() {
        return longitude;
    }
    
    private static Color bleu = new Color(0, 0x66, 0xFF, 255);
    private static Color orange = new Color(0xFF, 0x66, 0, 255);
    private static Color fond = new Color(0xFF, 0xDD, 0x55, 192);
    private static int nextid=1;
    private static String dernierNom = "";

    @Override
    public void paint(Graphics g, Point position, int zoom) {
        Graphics2D g2d = (Graphics2D) g;
        int size_h = 5;
        int size = size_h * 2;
        g.setColor(borne.utilisee ? bleu : orange);
        g.fillOval(position.x - size_h, position.y - size_h, size, size);
        g.setColor(Color.BLACK);
        g.drawOval(position.x - size_h, position.y - size_h, size, size);
        int minZoomPourId=8;
        if(zoom<minZoomPourId) return;

        //String scoor = "(" + latitude + "," + longitude + ")";
        int tw = g2d.getFontMetrics().stringWidth(borne.nom);
        Shape rect = new Rectangle(position.x+5, position.y+5, tw+4, 18);
        //String sprecision = "" + precision + "m";
        //int tw2 = g2d.getFontMetrics().stringWidth(sprecision);
        int tw2 = g2d.getFontMetrics().stringWidth("L" + borne.niveau);
        Shape rect2 = new Rectangle(position.x+5, position.y+5+18, tw2+4, 18);
        g2d.setColor(borne.utilisee ? bleu : fond);
        g2d.fill(rect);
        g2d.fill(rect2);
        g2d.setColor(Color.BLACK);
        g2d.draw(rect);
        g2d.draw(rect2);
        g2d.setColor(Color.BLACK);
        g2d.drawString(borne.nom, position.x+6, position.y+18);
        //g2d.drawString(sprecision, position.x+6, position.y+18+18);
        g2d.drawString("L" + borne.niveau, position.x+6, position.y+18+18);
    }


}
